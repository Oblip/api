# frozen_string_literal: true

require 'rake/testtask'

task :default do
  puts `rake -T`
end

# Configuration only -- not for direct calls
task :config do
  require_relative 'config/environments.rb' # load config info
  @app = Oblip::Api
  @config = @app.config
end

desc 'Run all the tests'
Rake::TestTask.new(:spec) do |t|
  t.pattern = 'specs/*_spec.rb'
  t.warning = false
end

desc 'Run all local tests'
Rake::TestTask.new(:local_spec) do |t| 
  t.pattern = 'specs/local_*_spec.rb'
  t.warning = false
end

desc 'Run all specs depending on third party'
Rake::TestTask.new(:tp_spec) do |t| 
  t.pattern = 'specs/tp_*_spec.rb'
  t.warning = false
end

desc 'Test only one file'
task :single_spec, [:filename] do |task, args|
  puts "Testing #{args[:filename]}"

  # require_relative 'specs/test_load_all.rb'
  # require_relative 'specs/spec_helper'

  sh "ruby specs/#{args[:filename]}"
end

task :print_env do
  puts "Environment: #{ENV['RACK_ENV'] || 'development'}"
end

desc 'Run application console (pry)'
task :console => :print_env do
  # sh 'pry -r ./specs/test_load_all'
  sh 'pry -r ./init.rb'
end

namespace :quality do
  CODE = '**/*.rb'

  desc 'Run all quality checks'
  task all: %i[rubocop reek flog]

  desc 'Run Rubocop quality checks'
  task :rubocop do
    # sh "rubocop #{CODE}"
    sh "rubocop"
  end

  desc 'Run Reek quality checks'
  task :reek do
    # sh "reek #{CODE}"
    sh 'reek'
  end

  desc 'Run Flog quality checks'
  task :flog do
    # sh "flog #{CODE}"
    sh 'flog'
  end
end

namespace :worker do
  namespace :run do
    desc 'Run the background transaction worker in development mode'
    task :development => :config do
      sh 'RACK_ENV=development bundle exec shoryuken -r ./workers/transaction_worker.rb -C ./workers/shoryuken_dev.yml'
    end

    desc 'Run the background transaction worker in testing mode'
    task :test => :config do
      sh 'RACK_ENV=test bundle exec shoryuken -r ./workers/transaction_worker.rb -C ./workers/shoryuken_test.yml'
    end

    desc 'Run the background cloning worker in testing mode'
    task :app_test => :config do
      sh 'RACK_ENV=app_test bundle exec shoryuken -r ./workers/transaction_worker.rb -C ./workers/shoryuken_dev.yml'
    end

    desc 'Run the background cloning worker in testing mode'
    task :staging => :config do
      sh 'RACK_ENV=staging bundle exec shoryuken -r ./workers/transaction_worker.rb -C ./workers/shoryuken_staging.yml'
    end

    desc 'Run the background cloning worker in production mode'
    task :production => :config do
      sh 'RACK_ENV=production bundle exec shoryuken -r ./workers/transaction_worker.rb -C ./workers/shoryuken.yml'
    end
  end
end

namespace :queues do
  require 'aws-sdk-sqs'

  desc 'Purge messages in SQS queue for Shoryuken'
  task :purge => :config do
    sqs = Aws::SQS::Client.new( access_key_id: @config.AWS_ACCESS_KEY_ID,
      secret_access_key: @config.AWS_SECRET_ACCESS_KEY,
      region: @config.AWS_REGION)
    begin
      # q_url = sqs.get_queue_url(queue_name: queue_name).queue_url
      q_url = @config.TRANSACTION_QUEUE_URL
      sqs.purge_queue(queue_url: q_url)
      puts "Queue purged"
    rescue StandardError => error
      puts "Error purging queue: #{error}"
    end
  end

end

namespace :db do
  require_relative 'config/environments.rb' # load config info

  Sequel.extension :migration
  app = Oblip::Api

  desc 'Creates required database extenions'
  task :create_extenions do
    sh "psql #{ENV['DATABASE_URL']} -c 'CREATE EXTENSION IF NOT EXISTS cube; CREATE EXTENSION IF NOT EXISTS earthdistance '", verbose: false
  end

  desc 'Run migrations'
  task :migrate => :print_env do
    require_relative 'config/environments.rb' # load config info
    puts "Migrating #{app.environment} database to latest"

    # Add Postgres extentions for geolocation
    Rake::Task['db:create_extenions'].execute

    Sequel::Migrator.run(app.DB, 'infrastructure/database/migrations')
  end

  desc 'Database seed'
  task :seed => :print_env do
    sh 'ruby infrastructure/database/seeds/create_seed.rb'
  end

  desc 'Drop all tables'
  task :drop => :print_env do
    require_relative 'config/environments.rb'

    if app.environment != :production
      app.DB.drop_table?(:agent_commissions, cascade: true)
      app.DB.drop_table?(:payouts, cascade: true)
      app.DB.drop_table?(:transactions, cascade: true)
      app.DB.drop_table?(:bank_account_requests, cascade: true)
      app.DB.drop_table?(:bank_accounts, cascade: true)
      app.DB.drop_table?(:registered_banks, cascade: true)
      app.DB.drop_table?(:admin_accounts, cascade: true)
      app.DB.drop_table?(:businesses, cascade: true)
      app.DB.drop_table?(:checking_account_creds, cascade: true)
      app.DB.drop_table?(:clients, cascade: true)
      app.DB.drop_table?(:accounts, cascade: true)
      app.DB.drop_table?(:countries, cascade: true)
      app.DB.drop_table?(:schema_info, cascade: true)
      app.DB.drop_table?(:schema_migrations, cascade: true)
    end
  end

  # desc 'Delete dev or test database file'
  # task :wipe do
  #   if app.environment == :production
  #     puts 'Cannot wipe production database!'
  #     return
  #   elsif app.environment == :staging
  #     puts 'use rake db:drop'
  #     return
  #   end

  #   FileUtils.rm(app.config.DB_FILENAME)
  #   puts "Deleted #{app.config.DB_FILENAME}"
  # end
end

namespace :generate do
  desc 'Generates keys for public key encryption'
  task :public_auth_keys do
    require_relative './lib/public_encryption.rb'
    keypair = PublicEncryption.generate_keypair

    puts "PUBLIC_KEY: #{keypair[:public_key]}"
    puts "PRIVATE_KEY: #{keypair[:private_key]}"
  end

  desc 'Generates keys for signature verifications'
  task :sig_keys do
    require_relative './lib/verify_request.rb'
    keypair = VerifyRequest.generate_keypair

    puts "PUBLIC_KEY: #{keypair[:verify_key]}"
    puts "PRIVATE_KEY: #{keypair[:signing_key]}"
  end
end

