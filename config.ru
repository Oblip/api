# frozen_string_literal: true

require 'faye'
require './init.rb'

if ENV['RACK_ENV'] != 'development'
  use Faye::RackAdapter, :mount => '/faye', :timeout => 25
end

run Oblip::Api.freeze.app
