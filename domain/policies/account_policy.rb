# frozen_string_literal: true

module Oblip
  # Policy for Accounts (Personal & Business)
  class AccountPolicy
    def self.valid_username?(username)
      (username =~ /^\w+$/) == 0
    end

    def self.transform_username(username)
      username.downcase
    end
  end
end
