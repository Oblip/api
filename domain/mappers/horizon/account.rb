# frozen_string_literal: fals

module Oblip
  # Horizon Wrapper
  module Horizon
    # Account
    class Account
      attr_writer :account_id

      def initialize(account_id = nil)
        @gateway = Oblip::Horizon::Api
        @account_id = account_id
      end

      def load
        data = @gateway.load_account(@account_id)
        DataMapper.new(data).build_entity
      end

      def create
        data = @gateway.create_account
        DataMapper.new(data).build_entity
      end

      # Maps Horizon account data
      class DataMapper
        def initialize(data)
          @data = data
        end

        def build_entity
          Entity::CheckingAccount.new(
            id: id,
            funds: funds
          )
        end

        private

        def id
          @data['id']
        end

        def funds
          @data['balances'].map do |asset|
            type = asset['asset_type']
            code = asset['asset_code']

            asset_entity = Entity::Asset.new(
              asset_code: type == 'native' ? 'XLM' : code,
              asset_issuer: asset['asset_issuer'],
              asset_type: asset['asset_type'],
              is_authorized: asset['is_authorized']
            )

            Entity::Fund.new(
              balance: asset['balance'],
              asset: asset_entity
            )
          end
        end
      end
    end
  end
end
