# frozen_string_literal: false

folders = %w[horizon]
folders.each do |folder|
  require_relative "#{folder}/init.rb"
end
