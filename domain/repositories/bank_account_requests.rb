# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Bank Account Requests
    class BankAccountRequests
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0

      def self.find_id(id)
        db_record = Database::BankAccountRequestOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.filter(type = nil, status = nil)
        if !type.nil? && !status.nil?
          Database::BankAccountRequestOrm.where(type: type, status: status)
        elsif !type.nil?
          Database::BankAccountRequestOrm.where(type: type)
        elsif !status.nil?
          Database::BankAccountRequestOrm.where(status: status)
        else
          Database::BankAccountRequestOrm
        end
      end

      def self.find_all_requests(bank_account_id = nil, type = nil,
                                 status = nil, offset = nil, limit = nil)

        db_records = filter(type, status)
        db_records = db_records.where(bank_account_id: bank_account_id) unless
          bank_account_id.nil?

        db_records = db_records.reverse(:created_at)
                               .limit(limit || DEFAULT_LIMIT)
                               .offset(offset || DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.update_status(id, status, comment = nil)
        db_record = Database::BankAccountRequestOrm.find(id: id)
        db_record.status = status
        db_record.status_comment = comment || db_record.status_comment
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_transaction_id(id, tx_id)
        db_record = Database::BankAccountRequestOrm.find(id: id)
        db_record.transaction_id = tx_id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::BankAccountRequestOrm.create(
          bank_account_id: entity.bank_account_id,
          registered_bank_id: entity.registered_bank_id,
          ref_no: entity.ref_no,
          amount: entity.amount,
          asset: entity.asset,
          type: entity.type
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        entity = Entity::BankAccountRequest.new(
          id: db_record.id,
          bank_account_id: db_record.bank_account_id,
          registered_bank_id: db_record.registered_bank_id,
          ref_no: db_record.ref_no,
          amount: db_record.amount,
          asset: db_record.asset,
          type: db_record.type,
          status_code: db_record.status,
          transaction_id: db_record.transaction_id,
          status_comment: db_record.status_comment,
          created_at: db_record.created_at.rfc2822
        )

        entity.bank_account = BankAccounts.find_id(db_record.bank_account_id)
        entity.registered_bank = RegisteredBanks.find_id(db_record.registered_bank_id)
        entity
      end
    end
  end
end
