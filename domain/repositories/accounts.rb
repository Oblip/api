# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Accounts
    class Accounts
      def self.all
        Database::AccountOrm.all
                            .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::AccountOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.delete(id)
        account = Database::AccountOrm.find(id: id)
        account.delete
      end

      def self.find_phone_number(phone_number)
        hashed = SecureDB.hash_content(phone_number)
        db_record = Database::AccountOrm.find(phone_number_hash: hashed)
        rebuild_entity(db_record)
      end

      def self.find_username(username)
        db_record = Database::AccountOrm.find(username: username)
        rebuild_entity(db_record)
      end

      def self.find_checking_account(checking_id)
        db_record = Database::AccountOrm.find(checking_account_id: checking_id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        raise Exceptions::Duplication if find_phone_number(entity.phone_number)

        profile = entity.profile
        db_record = Database::AccountOrm.create(
          username: entity.username,
          first_name: profile.name.first_name,
          middle_name: profile.name.middle_name,
          last_name: profile.name.last_name,
          phone_number: entity.phone_number,
          secret_code: entity.secret_code
        )
        rebuild_entity(db_record)
      end

      def self.update(entity)
        db_record = Database::AccountOrm.find(id: entity&.id)
        db_record&.email = entity&.email || db_record&.email

        db_record = update_profile_fields(db_record, entity)
        db_record.save

        rebuild_entity(db_record)
      end

      def self.set_checking_account(account_id, checking_account_id)
        db_record = Database::AccountOrm.find(id: account_id)
        db_record.checking_account_id = checking_account_id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        profile = rebuild_profile(db_record)
        Entity::Account.new(
          id: db_record.id,
          username: db_record.username,
          phone_number: db_record.phone_number,
          email: db_record.email,
          profile: profile,
          checking_account_id: db_record.checking_account_id,
          secret_code: db_record.secret_code
        )
      end

      private_class_method :new

      def self.update_profile_fields(db_record, entity)
        return nil unless db_record

        db_record = db_record.clone
        profile = entity&.profile
        db_record = update_name_fields(db_record, profile&.name)
        db_record = update_address_fields(db_record, profile&.address)
        db_record&.dob = profile&.dob || db_record&.dob

        db_record
      end

      # name is an entity (Entity::Account::Name)
      def self.update_name_fields(db_record, name)
        return nil unless db_record && name

        db_record = db_record.clone

        db_record&.first_name = name&.first_name || db_record&.first_name
        db_record&.middle_name = name&.middle_name || db_record&.middle_name
        db_record&.last_name = name&.last_name || db_record&.last_name
        db_record
      end

      # rubocop:disable Metrics/AbcSize
      # rubocop:disable Metrics/CyclomaticComplexity
      # rubocop:disable Metrics/PerceivedComplexity
      def self.update_address_fields(db_record, address_entity)
        return nil unless db_record && address_entity

        db_record = db_record.clone
        address = address_entity.clone
        db_record.street_name = address&.street_name || db_record&.street_name
        db_record.state = address&.state || db_record&.state
        db_record.city = address&.city || db_record&.city
        db_record.zip_code = address&.zip_code || db_record&.zip_code
        db_record.country_id = address&.country&.id || db_record&.country_id
        db_record
      end
      # rubocop:enable Metrics/AbcSize
      # rubocop:enable Metrics/CyclomaticComplexity
      # rubocop:enable Metrics/PerceivedComplexity

      def self.rebuild_profile(db_record)
        return nil unless db_record

        name = rebuild_name(db_record)
        address = rebuild_address(db_record)
        Entity::Account::Profile.new(
          name: name,
          address: address,
          dob: db_record.dob,
          photo_url: db_record.photo_url
        )
      end

      def self.rebuild_name(db_record)
        return nil unless db_record

        Entity::Account::Name.new(
          first_name: db_record.first_name,
          middle_name: db_record.middle_name,
          last_name: db_record.last_name
        )
      end

      def self.rebuild_address(db_record)
        return nil unless db_record

        country = Repository::Countries.find_id(db_record.country_id)
        Entity::Account::Address.new(
          street_name: db_record.street_name,
          city: db_record.city,
          state: db_record.state,
          zip_code: db_record.zip_code,
          country: country
        )
      end
    end
  end
end
