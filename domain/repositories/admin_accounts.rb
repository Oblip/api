# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Accounts
    class AdminAccounts
      def self.all
        Database::AdminAccountOrm.all
                                 .map do |db_account|
                                   rebuild_entity(db_account)
                                 end
      end

      def self.find_id(id)
        db_record = Database::AdminAccountOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_email(email_address)
        db_record = Database::AdminAccountOrm.find(email_address: email_address)
        rebuild_entity(db_record)
      end

      def self.credentials_valid?(email_address, password)
        db_record = Database::AdminAccountOrm.find(email_address: email_address)
        raise StandardError unless db_record
        raise StandardError unless db_record.password?(password)

        rebuild_entity(db_record)
      rescue StandardError
        nil
      end

      def self.create(entity, password)
        db_record = Database::AdminAccountOrm.create(
          full_name: entity.full_name,
          email_address: entity.email_address,
          type: entity.type,
          password: password
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::AdminAccount.new(
          id: db_record.id,
          full_name: db_record.full_name,
          email_address: db_record.email_address,
          type: db_record.type
        )
      end
    end
  end
end
