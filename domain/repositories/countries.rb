# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Countries
    class Countries
      def self.all
        Database::CountryOrm.all
                            .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::CountryOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_code(code)
        db_record = Database::CountryOrm.find(code: code)
        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::Country.new(
          id: db_record.id,
          name: db_record.name,
          code: db_record.code,
          phone_code: db_record.phone_code,
          currency: db_record.currency
        )
      end
    end
  end
end
