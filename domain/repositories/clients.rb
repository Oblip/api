# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Clients
    class Clients
      def self.all
        Database::ClientOrm.all
                           .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::ClientOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_many_by_account(account_id)
        db_records = Database::ClientOrm.where(account_id: account_id)

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.credentials_valid?(client_id, secret_code)
        db_record = Database::ClientOrm.find(id: client_id)
        if db_record&.secret_code_valid?(secret_code) &&
           db_record&.is_verified

          rebuild_entity(db_record)
        else
          false
        end
      end

      def self.update_verification_flag(client_id, flag)
        db_record = Database::ClientOrm.find(id: client_id)
        db_record.is_verified = flag
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_fcm_token(client_id, fcm)
        db_record = Database::ClientOrm.find(id: client_id)
        db_record.fcm_token = fcm
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::ClientOrm.create(
          account_id: entity.account_id,
          secret_code: '',
          verifier_key: entity.verifier_key,
          wrapper_key: entity.wrapper_key,
          fcm_token: entity.fcm_token,
          is_verified: entity.is_verified,
          verification_token: entity.verification_token,
          status: entity.status.id
        )

        rebuild_entity(db_record)
      end

      def self.save_verification_token(client_id, token)
        db_record = Database::ClientOrm.find(id: client_id)
        db_record.verification_token = token
        db_record.save

        rebuild_entity(db_record)
      end

      def self.save_secret(client_id, secret)
        db_record = Database::ClientOrm.find(id: client_id)
        db_record.secret_code = secret
        db_record.save

        rebuild_entity(db_record)
      end

      def self.save_account(client_id, account_id)
        db_record = Database::ClientOrm.find(id: client_id)
        db_record.account_id = account_id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        status = Entity::Client::Status.find_id(db_record.status)
        Entity::Client.new(
          id: db_record.id,
          account_id: db_record.account_id,
          secret_code: db_record.secret_code,
          verifier_key: db_record.verifier_key,
          wrapper_key: db_record.wrapper_key,
          fcm_token: db_record.fcm_token,
          is_verified: db_record.is_verified,
          verification_token: db_record.verification_token,
          status: status
        )
      end
    end
  end
end