# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Transactions
    class Transactions
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0
      
      def self.find_id(id)
        db_record = Database::TransactionOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_from_account(username, offset, limit)
        db_records = Database::TransactionOrm.where(from_internal: username)
                                             .or(to_internal: username)
        db_records = db_records.reverse(:created_at)
                               .limit(limit || DEFAULT_LIMIT)
                               .offset(offset || DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.create(entity)
        db_record = Database::TransactionOrm.create(entity.to_h)
        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::Transaction.new(
          id: db_record.id,
          tx_hash: db_record.tx_hash,
          from_internal: db_record.from_internal,
          to_internal: db_record.to_internal,
          message: db_record.message,
          paging_token: db_record.paging_token,
          network_fee: db_record.network_fee,
          amount: db_record.amount,
          asset: db_record.asset,
          type: db_record.type
        )
      end
    end
  end
end
