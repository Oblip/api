# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Usernames
    class Usernames
      def self.find_username(username)
        db_record = Database::UsernameOrm.find(username: username)
        rebuild_entity(db_record)
      end

      def self.find_checking_account(id)
        db_record = Database::UsernameOrm.find(checking_account_id: id)
        rebuild_entity(db_record)
      end

      def self.find_all_checking_account(ck_list)
        db_records = Database::UsernameOrm.where(checking_account_id: ck_list)
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::Username.new(
          reference_id: db_record.reference_id,
          username: db_record.username,
          name: db_record.name,
          type: db_record.type,
          checking_account_id: db_record.checking_account_id,
          master_account_id: db_record.master_account_id,
          is_agent: db_record.is_agent
        )
      end
    end
  end
end
