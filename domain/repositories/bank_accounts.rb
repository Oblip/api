# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Bank Accounts
    class BankAccounts
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0

      def self.find_id(id)
        db_record = Database::BankAccountOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::BankAccountOrm.create(
          username: entity.username,
          registered_bank_id: entity&.registered_bank&.id,
          account_number: entity.account_number,
          account_name: entity.account_name,
          is_partner: entity.is_partner || false
        )

        rebuild_entity(db_record)
      end

      def self.find_all_bank_accounts(username = nil, offset, limit)
        db_records = if !username.nil?
                       Database::BankAccountOrm.where(username: username)
                     else
                       Database::BankAccountOrm
                     end

        db_records = db_records.reverse(:created_at)
                               .limit(limit || DEFAULT_LIMIT)
                               .offset(offset || DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.bank_account_exists?(username, registered_bank_id, acc_number)
        acc_number_hashed = SecureDB.hash_content(acc_number)
        db_record = Database::BankAccountOrm.find(username: username,
                                                  registered_bank_id: registered_bank_id,
                                                  account_number_hash: acc_number_hashed)
        rebuild_entity(db_record)
      end

      def self.update_status(bank_account_id, status_code, comment)
        db_record = Database::BankAccountOrm.find(id: bank_account_id)
        db_record.status = status_code
        db_record.status_comment = comment || db_record.status_comment
        db_record.save

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        bank = RegisteredBanks.find_id(db_record.registered_bank_id)

        Entity::BankAccount.new(
          id: db_record.id,
          username: db_record.username,
          registered_bank: bank,
          account_number: db_record.account_number,
          account_name: db_record.account_name,
          is_partner: db_record.is_partner,
          status_code: db_record.status,
          status_comment: db_record.status_comment
        )
      end
    end
  end
end
