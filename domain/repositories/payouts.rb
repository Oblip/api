# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Payouts
    class Payouts
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0
      
      def self.find_id(id)
        db_record = Database::PayoutOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_available_by_username(username)
        db_record = Database::PayoutOrm.find(
          username: username, is_paid: false)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::PayoutOrm.create(
          amount: entity.amount,
          payout_ref: entity.payout_ref,
          username: entity.username
        )

        rebuild_entity(db_record)
      end

      def self.update_amount(id, increment)
        db_record = Database::PayoutOrm.find(id: id)
        db_record.amount = (db_record.amount.to_f + increment).to_s
        db_record.save

        rebuild_entity(db_record)
      end

      def self.find_all(username, offset = nil, limit = nil)

        db_records = if !username.nil?
                      Database::PayoutOrm.where(username: username)
                    else
                      Database::PayoutOrm
                    end

        db_records = db_records.reverse(:created_at)
                               .limit(limit || DEFAULT_LIMIT)
                               .offset(offset || DEFAULT_OFFSET).all
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record
        
        Entity::Payout.new(
          id: db_record.id,
          amount: db_record.amount,
          payout_ref: db_record.payout_ref,
          username: db_record.username,
          is_paid: db_record&.is_paid,
          paid_date: db_record&.paid_date&.rfc2822
        )
      end
    end
  end
end
