# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for AgentCommissions
    class AgentCommissions
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0

      def self.find_all(username, offset = nil, limit = nil)
        # db_records = Database::AgentCommissionOrm.where(username: username)

        db_records = if !username.nil?
                        Database::AgentCommissionOrm.where(username: username)
                      else
                        Database::AgentCommissionOrm
                      end

        db_records = db_records.reverse(:created_at)
                               .limit(limit || DEFAULT_LIMIT)
                               .offset(offset || DEFAULT_OFFSET).all
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.create(entity)
        db_record = Database::AgentCommissionOrm.create(
          transaction_id: entity.transaction_id,
          username: entity.username,
          amount: entity.amount,
          asset: entity.asset,
          payout_id: entity.payout_id
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        transaction = Transactions.find_id(db_record.transaction_id)
        payout = Payouts.find_id(db_record.payout_id)

        entity = Entity::AgentCommission.new(
          id: db_record.id,
          transaction_id: db_record.transaction_id,
          username: db_record.username,
          amount: db_record.amount,
          asset: db_record.asset,
          payout_id: db_record.payout_id
        )

        entity.transaction = transaction
        entity.payout = payout
        entity
      end
    end
  end
end
