# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Checking Account Creds
    class CheckingAccountCreds
      def self.find_id(id)
        db_record = Database::CheckingAccountCredOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_checking_account(id)
        db_record = Database::CheckingAccountCredOrm
                    .find(checking_account_id: id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::CheckingAccountCredOrm.create(
          checking_account_id: entity.checking_account_id,
          secret: entity.secret,
          wrapper_key: entity.wrapper_key
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::CheckingAccountCred.new(
          id: db_record.id,
          checking_account_id: db_record.checking_account_id,
          secret: db_record.secret,
          wrapper_key: db_record.wrapper_key
        )
      end
    end
  end
end
