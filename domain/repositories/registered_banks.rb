# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Registered Banks
    class RegisteredBanks
      def self.all
        Database::RegisteredBankOrm.all.map do |db_record|
          rebuild_entity(db_record)
        end
      end

      def self.find_id(id)
        db_record = Database::RegisteredBankOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::RegisteredBankOrm.create(
          name: entity.name,
          account_number: entity.account_number,
          account_name: entity.account_name,
          country_id: entity.country&.id
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        country = Countries.find_id(db_record.country_id)
        Entity::RegisteredBank.new(
          id: db_record.id,
          name: db_record.name,
          account_name: db_record.account_name,
          account_number: db_record.account_number,
          country: country,
          balance: db_record.balance
        )
      end
    end
  end
end
