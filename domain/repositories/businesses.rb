# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Businesses
    class Businesses
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0
      DEFAULT_DISTANCE = 10

      def self.find_id(id)
        db_record = Database::BusinessOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_all_businesses(is_agent = nil, offset, limit)
        db_records = if !is_agent.nil?
                       Database::BusinessOrm.where(is_agent: is_agent)
                     else
                       Database::BusinessOrm
                     end

        db_records = db_records.reverse(:created_at)
                               .limit(limit || DEFAULT_LIMIT)
                               .offset(offset || DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      # Params:
      #   offset (int), limit (int), lat (float), lng (float),
      #   distance (int), is_agent (bool)
      # TODO: refactor
      # rubocop:disable AbcSize
      def self.find_all_nearby(params:)
        agent_condition = " AND is_agent = #{params[:is_agent]}"
        extra_condition = params[:is_agent].nil? ? '' : agent_condition
        query = 'SELECT *, point(?, ?) <@> point(lng, lat)::point as distance'
        query += ' FROM businesses WHERE (point(?, ?) <@> point(lng, lat)) '
        query += '< ? ' + extra_condition
        query += ' ORDER BY distance'

        builder = Api.DB.fetch(query, params[:lng], params[:lat], params[:lng],
                               params[:lat],
                               (params[:distance] || DEFAULT_DISTANCE))
        db_records = builder.reverse(:distance)
                            .limit(params[:limit] || DEFAULT_LIMIT)
                            .offset(params[:offset] || DEFAULT_OFFSET).all

        db_records.map do |record|
          r = OpenStruct.new(record)
          rebuild_entity(r)
        end
      end
      # rubocop:enable AbcSize

      def self.find_by_account(id)
        db_records = Database::BusinessOrm.where(account_id: id).all

        db_records.map do |record|
          rebuild_entity(record)
        end
      end

      # TODO: refactor to rubocop advises
      # rubocop:disable AbcSize
      # rubocop:disable PerceivedComplexity, CyclomaticComplexity
      def self.update(entity)
        db_record = Database::BusinessOrm.find(id: entity.id)
        db_record.name = entity.name || db_record.name
        db_record.legal_name = entity.legal_name || db_record.legal_name
        db_record.username = entity.username || db_record.username
        db_record.street = entity.street || db_record.street
        db_record.city = entity.city || db_record.city
        db_record.country_id = entity.country.id || db_record.country_id
        db_record.lat = entity.lat || db_record.lat
        db_record.lng = entity.lng || db_record.lng
        db_record.is_agent = entity.is_agent.nil? ? db_record.is_agent : entity.is_agent
        db_record.save

        rebuild_entity(db_record)
      end
      # rubocop:enable AbcSize
      # rubocop:enable PerceivedComplexity, CyclomaticComplexity

      def self.set_checking_account(business_id, checking_account_id)
        db_record = Database::BusinessOrm.find(id: business_id)
        db_record.checking_account_id = checking_account_id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::BusinessOrm.create(
          name: entity.name,
          legal_name: entity.legal_name,
          username: entity.username,
          street: entity.street,
          city: entity.city,
          country_id: entity.country.id,
          lat: entity.lat,
          lng: entity.lng,
          is_agent: entity.is_agent,
          account_id: entity.account_id
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        country = Countries.find_id(db_record.country_id)
        Entity::Business.new(
          id: db_record.id,
          name: db_record.name,
          legal_name: db_record.legal_name,
          username: db_record.username,
          street: db_record.street,
          city: db_record.city,
          country: country,
          lat: db_record.lat,
          lng: db_record.lng,
          is_agent: db_record.is_agent,
          account_id: db_record.account_id,
          checking_account_id: db_record.checking_account_id,
          distance: db_record.respond_to?(:distance) ? db_record.distance : nil
        )
      end
    end
  end
end
