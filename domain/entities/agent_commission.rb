# frozen_string_literal: false

require_relative 'transaction'

module Oblip
  module Entity
    # Domain entity object for Agent Commission
    class AgentCommission < Dry::Struct
      attribute :id, DryTypes::Coercible::String.optional
      attribute :transaction_id, DryTypes::String.optional
      attribute :username, DryTypes::Strict::String.optional
      attribute :amount, DryTypes::Strict::String.optional
      attribute :asset, DryTypes::Strict::String.optional  
      attribute :payout_id, DryTypes::Strict::String.optional

      attr_accessor :transaction, :payout

      module Exception
        class NotAvailable < StandardError; end
      end

      # Asset: BZD
      def self.calculate_commission(amount)
        case amount
        when 0.99..1.99
          0.079
        when 2..10.13
          0.16
        when 10.14..20.05
          0.18
        when 20.06..29.98
          0.2
        when 29.99..49.82
          0.22
        when 49.83..69.67
          0.24
        when 69.68..99.43
          0.28
        when 99.44..149.05
          0.4
        when 149.06..198.66
          0.56
        when 198.67..297.88
          0.79
        when 297.89..397.31
          1.09
        when 397.32..496.53
          1.41
        when 496.54..595.76
          1.73
        when 597.77..694.98
          2.04
        when 694.99..794.21
          2.36
        when 794.22..893.43
          2.68
        when 893.44..992.66
          2.98
        when 992.67..1191.11
          3.77
        when 1191.12..1389.16
          3.77
        else
          raise Exception::NotAvailable
        end
      end
    end
  end
end
