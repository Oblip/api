# frozen_string_literal: false

require 'ruby-enum'
require_relative 'bank_account'
require_relative 'registered_bank'
require_relative '../../lib/helpers.rb'

module Oblip
  module Entity
    # Domain entity object for a Bank Account Request
    class BankAccountRequest < Dry::Struct
      attribute :id, DryTypes::Strict::String.optional
      attribute :bank_account_id, DryTypes::Strict::String.optional
      attribute :registered_bank_id, DryTypes::Strict::String.optional
      attribute :ref_no, DryTypes::Strict::String.optional
      attribute :amount, DryTypes::Strict::Float.optional
      attribute :asset, DryTypes::Strict::String.optional
      attribute :type, DryTypes::Strict::String.optional
      attribute :transaction_id, DryTypes::Strict::String.optional
      attribute :status_code, DryTypes::Strict::String.optional
      attribute :status_comment, DryTypes::Strict::String.optional
      attribute :created_at, DryTypes::Strict::String.optional

      def notify_confirmation(webhook_url)
        client = Discordrb::Webhooks::Client.new(url: webhook_url)
        client.execute do |builder|
          builder.add_embed do |embed|
            embed.title = "Request - User Confirmed"
            embed.description = 'User has confirmed request'
            embed.timestamp = Time.now
            embed.add_field(name: 'Request ID', value: id)
            embed.add_field(name: 'Type', value: type)
            embed.add_field(name: 'Amount', value: amount.to_s)
            embed.add_field(name: 'Asset', value: asset)
          end
        end
      end

      def status
        Status.build(status_code, status_comment)
      end

      # TODO: refactor this
      def bank_account=(entity)
        @bank_account = entity
      end

      def registered_bank=(entity)
        @registered_bank = entity
      end

      def bank_account
        @bank_account
      end

      def registered_bank
        @registered_bank
      end
      # End refactor here

      def self.build_from_hash(data)
        new(
          id: nil,
          bank_account_id: data['bank_account_id'],
          registered_bank_id: data['registered_bank_id'],
          ref_no: generate_ref,
          amount: data['amount'].to_f,
          asset: data['asset'],
          type: data['type'],
          transaction_id: nil,
          status_code: nil,
          status_comment: nil,
          created_at: nil
        )
      end
      
      # Defines the type of request
      class Type
        include Ruby::Enum

        define :deposit, 'Deposit'
        define :withdraw, 'Withdraw'
      end

      # Defines the statuses of a Bank Account
      class Status
        include Ruby::Enum

        define :unconfirmed, 'Unconfirmed'
        define :user_confirmed, 'User Confirmed'
        define :verified, 'Verified'
        define :unable_to_verify, 'Unable to verify'
        define :canceled, 'Canceled'

        def self.build(key, comment)
          value = self.value(key.to_sym)
          return nil unless value

          CodeName.new key.to_s, value, comment
        end
      end
    end
  end
end
