# frozen_string_literal: false

require_relative 'country'

module Oblip
  module Entity
    # Domain entity object for Account
    class Account < Dry::Struct
      # Sub Domain Entity Name
      class Name < Dry::Struct
        attribute :first_name, DryTypes::Strict::String.optional
        attribute :middle_name, DryTypes::Strict::String.optional
        attribute :last_name, DryTypes::Strict::String.optional
      end

      # Sub Domain Entity Address
      class Address < Dry::Struct
        attribute :street_name, DryTypes::Strict::String.optional
        attribute :city, DryTypes::Strict::String.optional
        attribute :state, DryTypes::Strict::String.optional
        attribute :zip_code, DryTypes::Strict::String.optional
        attribute :country, DryTypes.Instance(Country).optional
      end

      # Sub Domain Entity Profile
      class Profile < Dry::Struct
        attribute :name, DryTypes.Instance(Name).optional
        attribute :address, DryTypes.Instance(Address).optional
        attribute :dob, DryTypes::DateTime.optional
        attribute :photo_url, DryTypes::Strict::String.optional

        def self.build_name(name)
          new(name: name, address: nil, dob: nil, photo_url: nil)
        end
      end

      attribute :id, DryTypes::Coercible::String.optional
      attribute :username, DryTypes::Strict::String.optional
      attribute :phone_number, DryTypes::Strict::String.optional
      attribute :email, DryTypes::Strict::String.optional
      attribute :profile, DryTypes.Instance(Profile).optional
      attribute :secret_code, DryTypes::Strict::String.optional
      attribute :checking_account_id, DryTypes::Strict::String.optional
      attribute :type, DryTypes::Strict::String.default('personal')

      def self.build_hash(hash_sym)
        name = Name.new(
          first_name: hash_sym[:first_name],
          middle_name: hash_sym[:middle_name],
          last_name: hash_sym[:last_name]
        )
        new(
          id: hash_sym[:id], username: hash_sym[:username],
          phone_number: hash_sym[:phone_number], email: nil,
          profile: Profile.build_name(name),
          checking_account_id: nil,
          secret_code: SecureDB.generate_key
        )
      end
    end
  end
end
