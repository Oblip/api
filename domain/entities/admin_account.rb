# frozen_string_literal: false

module Oblip
  module Entity
    # Domain entity object for Asset
    class AdminAccount < Dry::Struct
      attribute :id, DryTypes::Strict::String.optional
      attribute :full_name, DryTypes::Strict::String.optional
      attribute :email_address, DryTypes::Coercible::String.optional
      attribute :type, DryTypes::Strict::String.optional

      def self.build(hash)
        new(
          id: nil,
          full_name: hash['full_name'],
          email_address: hash['email_address'],
          type: hash['type']
        )
      end
    end
  end
end
