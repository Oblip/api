# frozen_string_literal: false

require 'ruby-enum'

module Oblip
  module Entity
    # Domain entity object for Transaction
    class Transaction < Dry::Struct
      attribute :id, DryTypes::Coercible::String.optional
      attribute :tx_hash, DryTypes::Strict::String.optional
      attribute :from_internal, DryTypes::Strict::String.optional
      attribute :to_internal, DryTypes::Strict::String.optional
      attribute :message, DryTypes::Strict::String.optional
      attribute :paging_token, DryTypes::Strict::String.optional
      attribute :network_fee, DryTypes::Strict::Integer.optional
      attribute :amount, DryTypes::Strict::String.optional
      attribute :asset, DryTypes::Strict::String.optional
      attribute :type, DryTypes::Strict::String.optional

      # Defines the type of request
      class Type
        include Ruby::Enum

        define :payment, 'payment'
        define :bank_deposit, 'bank_deposit'
        define :bank_withdrawal, 'bank_withdrawal'
        define :partner_topup, 'partner_topup'
        define :partner_cashout, 'partner_cashout'
      end
    end
  end
end
