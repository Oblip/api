# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    class DeviceNotification < Dry::Struct
      attribute :title, DryTypes::Strict::String
      attribute :message, DryTypes::Strict::String
      # attribute :platform_type, Types::Strict::String
      # attribute :click_action, Types::Strict::String.optional
      attribute :type, DryTypes::Strict::String
      attribute :data, DryTypes::Strict::Hash.optional

      module Types
        TRANSACTION = 'type_transaction'.freeze
        BANK_ACCOUNT = 'type_bank_account'.freeze
      end

      def to_hash
        notification_data =  {
          title: title,
          body: message,
          sound: 'default',
          badge: 1, # TODO: check this again
          color: '#172B49'
        }
        {
          android: {
            priority: 'high',
            notification: {
              default_sound: true,
              title: title,
              icon: 'notification_icon_small',
              color: '#000000'
            }
          },
          notification: {
            title: title,
            body: message
          },
          content_available: true,
          priority: 'high',
          data: {
            # payload: notification_data,
            extras: data ? { type: type, payload: data } : { type: type }
          }
        }
      end
    end
  end
end