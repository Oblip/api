# frozen_string_literal: false

# require_relative '../../infrastructure/horizon/api.rb'

module Oblip
  module Entity
    # Domain entity object for Username
    class Username < Dry::Struct
      attribute :reference_id, DryTypes::Strict::String.optional
      attribute :username, DryTypes::Strict::String.optional
      attribute :name, DryTypes::Strict::String.optional
      attribute :type, DryTypes::Strict::String.optional
      attribute :is_agent, DryTypes::Strict::Bool.optional
      attribute :checking_account_id, DryTypes::Strict::String.optional
      attribute :master_account_id, DryTypes::Strict::String.optional

      def self.build_fund_identity
        fund = Horizon::Api.fund
        new(
          reference_id: nil,
          username: fund[:username],
          name: fund[:name],
          type: fund[:type],
          checking_account_id: fund[:account].address,
          master_account_id: nil,
          is_agent: nil
        )
      end
    end
  end
end
