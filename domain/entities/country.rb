# frozen_string_literal: false

require_relative 'country'

module Oblip
  module Entity
    # Domain entity object for Account
    class Country < Dry::Struct
      attribute :id, DryTypes::Strict::Integer.optional
      attribute :name, DryTypes::Strict::String.optional
      attribute :code, DryTypes::Strict::String.optional
      attribute :phone_code, DryTypes::Strict::String.optional
      attribute :currency, DryTypes::Strict::String.optional

      def self.build_id(id)
        new(
          id: id,
          name: nil,
          code: nil,
          phone_code: nil,
          currency: nil
        )
      end
    end
  end
end
