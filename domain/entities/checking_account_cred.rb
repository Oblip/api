# frozen_string_literal: false

module Oblip
  module Entity
    # Domain entity object for Checking Account Credentials
    class CheckingAccountCred < Dry::Struct
      attribute :id, DryTypes::Strict::String.optional
      attribute :checking_account_id, DryTypes::Strict::String.optional
      attribute :secret, DryTypes::Strict::String.optional
      attribute :wrapper_key, DryTypes::Strict::String.optional

      # TODO: decrypt secret here :p
    end
  end
end
