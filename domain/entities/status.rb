# frozen_string_literal: false

module Oblip
  module Entity
    # Domain entity object for Status
    class Status < Dry::Struct
      attribute :id, DryTypes::Strict::Integer.optional
      attribute :name, DryTypes::Strict::String.optional
    end
  end
end
