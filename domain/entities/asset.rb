# frozen_string_literal: false

require 'ruby-enum'

module Oblip
  module Entity
    # Domain entity object for Asset
    class Asset < Dry::Struct
      attribute :asset_code, DryTypes::Strict::String.optional
      attribute :asset_type, DryTypes::Strict::String.optional
      # attribute :balance, DryTypes::Coercible::String.optional
      attribute :asset_issuer, DryTypes::Strict::String.optional
      attribute :is_authorized, DryTypes::Strict::Bool.optional

      # attr_reader :asset_name

      def asset_name
        Asset.find_asset_info(asset_code)[:name]
      end

      def self.find_asset_info(asset_code)
        case asset_code
        when 'BZD'
          { name: 'Belize Dollar' }
        else
          { name: 'Lumen' }
        end
      end

      # Defines the available assets
      class AvailableAssets
        include Ruby::Enum

        define :bzd, 'BZD'
      end
    end
  end
end
