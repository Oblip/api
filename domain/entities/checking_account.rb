# frozen_string_literal: false

require_relative 'fund'

module Oblip
  module Entity
    # Domain entity object for Checking Account
    class CheckingAccount < Dry::Struct
      attribute :id, DryTypes::Coercible::String.optional
      attribute :funds, DryTypes::Strict::Array.of(Fund).optional

      # Starting balance in XLM (Lumens)
      def self.starting_balance
        2.3 # XLM
      end
    end
  end
end
