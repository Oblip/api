# frozen_string_literal: false

require_relative 'country'

module Oblip
  module Entity
    # Domain entity object for Status
    class RegisteredBank < Dry::Struct
      attribute :id, DryTypes::Strict::String.optional
      attribute :name, DryTypes::Strict::String.optional
      attribute :account_number, DryTypes::Strict::String.optional
      attribute :account_name, DryTypes::Strict::String.optional
      # TODO: replace with Money class
      attribute :balance, DryTypes::Strict::Float.optional
      attribute :country, DryTypes.Instance(Country).optional

      def self.build_id(id)
        new(
          id: id,
          name: nil,
          account_number: nil,
          account_name: nil,
          balance: nil,
          country: nil
        )
      end
    end
  end
end
