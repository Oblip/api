# frozen_string_literal: false

require_relative 'asset.rb'

module Oblip
  module Entity
    # Domain entity object for Fund
    class Fund < Dry::Struct
      attribute :balance, DryTypes::Coercible::String.optional
      attribute :asset, DryTypes.Instance(Asset).optional
    end
  end
end
