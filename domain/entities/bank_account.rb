# frozen_string_literal: false

require_relative 'registered_bank'
require 'ruby-enum'
require 'discordrb/webhooks'

module Oblip
  module Entity
    # Domain entity object for a Bank Account
    class BankAccount < Dry::Struct
      attribute :id, DryTypes::Strict::String.optional
      attribute :username, DryTypes::Strict::String.optional
      attribute :account_name, DryTypes::Strict::String.optional
      attribute :is_partner, DryTypes::Strict::Bool.optional
      attribute :registered_bank, DryTypes.Instance(RegisteredBank).optional
      attribute :account_number, DryTypes::Strict::String.optional
      attribute :status_code, DryTypes::Strict::String.optional
      attribute :status_comment, DryTypes::Strict::String.optional

      def status
        Status.build(status_code, status_comment)
      end

      def notify_verification(webhook_url)
        client = Discordrb::Webhooks::Client.new(url: webhook_url)
        client.execute do |builder|
          builder.add_embed do |embed|
            embed.title = 'Bank Account Verification'
            embed.description = '**Immdediate attention:** Please verify this  bank account'
            embed.timestamp = Time.now
            embed.fields.push(
              Discordrb::Webhooks::EmbedField.new(name: 'Username', value: username, inline: true)
            )
            embed.fields.push(
              Discordrb::Webhooks::EmbedField.new(name: 'Is Partner', value: is_partner, inline: true)
            )
            embed.fields.push(
              Discordrb::Webhooks::EmbedField.new(name: 'Bank Account ID', value: id, inline: true)
            )
          end
        end
        client = nil
      end

      # Defines the statuses of a Bank Account
      class Status
        include Ruby::Enum

        define :processing, 'Processing'
        define :verified, 'Verified'
        define :unable_to_verify, 'Unable to verify'

        def self.build(key, comment)
          value = self.value(key.to_sym)
          return nil unless value

          CodeName.new key.to_s, value, comment
        end
      end

      # private_constant :Status
    end
  end
end
