# frozen_string_literal: false

require_relative 'country.rb'

module Oblip
  module Entity
    # Domain entity object for Business
    class Business < Dry::Struct
      attribute :id, DryTypes::Coercible::String.optional
      attribute :name, DryTypes::Strict::String.optional
      attribute :legal_name, DryTypes::Strict::String.optional
      attribute :username, DryTypes::Strict::String.optional
      attribute :street, DryTypes::Strict::String.optional
      attribute :city, DryTypes::Strict::String.optional
      attribute :country, DryTypes.Instance(Country).optional
      attribute :lat, DryTypes::Strict::Float.optional
      attribute :lng, DryTypes::Strict::Float.optional
      attribute :is_agent, DryTypes::Strict::Bool.optional
      attribute :distance, DryTypes::Strict::Float.optional
      attribute :type, DryTypes::Strict::String.default('business')
      attribute :account_id, DryTypes::Coercible::String.optional
      attribute :checking_account_id, DryTypes::Strict::String.optional

      def self.build_from_hash(hash)
        new(
          id: hash['id'],
          name: hash['name'],
          legal_name: hash['legal_name'],
          username: hash['username'],
          street: hash['street'],
          city: hash['city'],
          country: Country.build_id(hash['country_id']),
          lat: hash['lat'],
          lng: hash['lng'],
          is_agent: hash['is_agent'],
          distance: nil,
          account_id: hash['account_id'],
          checking_account_id: nil
        )
      end
    end
  end
end
