# frozen_string_literal: false

require_relative 'status.rb'

module Oblip
  module Entity
    # Domain entity object for Client
    class Client < Dry::Struct
      attribute :id, DryTypes::Coercible::String.optional
      attribute :account_id, DryTypes::Coercible::String.optional
      attribute :secret_code, DryTypes::Strict::String.optional
      attribute :verifier_key, DryTypes::Strict::String.optional
      attribute :wrapper_key, DryTypes::Strict::String.optional
      attribute :fcm_token, DryTypes::Strict::String.optional
      attribute :is_verified, DryTypes::Strict::Bool.optional
      attribute :verification_token, DryTypes::Strict::String.optional
      attribute :status, DryTypes.Instance(Status).optional

      # Contains the statuses of this entity
      module Status
        ACTIVE = Entity::Status.new(id: 1, name: 'Active')
        DEACTIVATED = Entity::Status.new(id: 2, name: 'Deactivated')

        def self.find_id(id)
          case id
          when 1
            ACTIVE
          when 2
            DEACTIVATED
          else
            raise 'Invalid ID'
          end
        end
      end

      def self.prebuild(verifier_key:, wrapper_key:, fcm_token:)
        new(
          id: nil,
          account_id: nil,
          secret_code: nil,
          verifier_key: verifier_key,
          wrapper_key: wrapper_key,
          fcm_token: fcm_token,
          is_verified: false,
          verification_token: nil,
          status: Status::ACTIVE
        )
      end
    end
  end
end
