# frozen_string_literal: false

module Oblip
  module Entity
    # Domain entity object for Payout
    class Payout < Dry::Struct
      attribute :id, DryTypes::Coercible::String.optional
      attribute :amount, DryTypes::Strict::String.optional
      attribute :payout_ref, DryTypes::Strict::String.optional
      attribute :username, DryTypes::Strict::String.optional
      attribute :is_paid, DryTypes::Strict::Bool.optional
      attribute :paid_date, DryTypes::Strict::String.optional
    end
  end
end
