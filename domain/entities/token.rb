# frozen_string_literal: false

module Oblip
  module Entity
    # Domain entity object for auth tokens
    # Usage:
    #   creates refresh token
    #   creates access token
    class Token < Dry::Struct
      attribute :token, DryTypes::Strict::String.optional
      attribute :expires_on, DryTypes::Strict::Integer.optional

      # Creates access token that expires every 30 minutes
      def self.create_access_token(account_entity, client_entity)
        # account_raw = AccountRepresenter.new(account_entity).to_json

        data = {
          account_id: account_entity.id,
          username: account_entity.username,
          checking_account_id: account_entity.checking_account_id,
          client_id: client_entity.id,
          type: account_entity.type,
          is_agent: account_entity.type == 'business' ?  account_entity.is_agent : false
        }

        token = AccessToken.create(data.to_json, AccessToken::ONE_HOUR / 2)
        new(token: token[:token], expires_on: token[:expires_on])
      end

      # Creates refresh token that does not expire
      def self.create_refresh_token(client_id, secret)
        token = "#{client_id}.#{secret}"
        new(token: token, expires_on: nil)
      end
    end
  end
end
