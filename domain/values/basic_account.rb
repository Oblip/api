# frozen_string_literal: true

module Oblip
  BasicAccount = Struct.new(:username, :name, :type)
end
