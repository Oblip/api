# frozen_string_literal: true

module Oblip
  Assets = Struct.new(:assets)
end
