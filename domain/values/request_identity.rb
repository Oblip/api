# frozen_string_literal: true

module Oblip
  RequestIdentity = Struct.new :code, :message, :data
end
