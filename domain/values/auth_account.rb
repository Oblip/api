# frozen_string_literal: true

module Oblip
  AuthAccount = Struct.new :account, :tokens
end
