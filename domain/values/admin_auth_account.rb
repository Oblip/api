# frozen_string_literal: true

module Oblip
  AdminAuthAccount = Struct.new :account, :access
end
