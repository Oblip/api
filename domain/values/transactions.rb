# frozen_string_literal: true

module Oblip
  Transactions = Struct.new :transactions
end
