# frozen_string_literal: true

module Oblip
  Accounts = Struct.new :accounts
end
