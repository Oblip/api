# frozen_string_literal: true

module Oblip
  CodeName = Struct.new :code, :name, :message
end
