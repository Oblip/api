# frozen_string_literal: true

module Oblip
  AccountSigner = Struct.new :account, :signer_seed, :wrapper_public_key
end
