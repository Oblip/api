# frozen_string_literal: true

module Oblip
  Token = Struct.new :token, :expires_on
end
