# frozen_string_literal: true

require_relative 'vault_asset_representer'

module Oblip
  # Represents OLP vault information for API output
  class VaultRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :address
    property :blockchain_source_fund_id
    property :initial_native_funds
    property :initial_operation_cost
    property :is_paid
    property :salt
    property :sequence_number
    collection :assets, extend: VaultAssetRepresenter, class: OpenStruct
  end
end
