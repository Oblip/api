# frozen_string_literal: true

require_relative 'oblip_account_representer'

module Oblip
  # Represents authenticated account information for API output
  class AuthAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :oblip_account, extend: OblipAccountRepresenter, class: OpenStruct
    property :auth_token
  end
end
