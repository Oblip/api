# frozen_string_literal: true

require_relative 'contact_info_representer.rb'

module Oblip
  # Represents contact information for API output
  class ContactRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :owner_id
    property :contact,
             extend: ContactInfoRepresenter, class: OpenStruct
  end
end
