# frozen_string_literal: true

require 'roar/decorator'
require 'roar/json'

# folders = %w[staff]
# folders.each do |folder|
#   require_relative "#{folder}/init.rb"
# end

require_relative "staff/init.rb"

Dir.glob("#{File.dirname(__FILE__)}/*.rb").each do |file|
  require file
end
