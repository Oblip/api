# frozen_string_literal: true

require_relative 'country_representer'

module Oblip
  # Represents personal detail information for API output
  class PersonalDetailRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :first_name
    property :middle_name
    property :last_name
    property :gender
    property :birthday
    property :country, extend: CountryRepresenter, class: OpenStruct
    property :photo_url
  end
end
