# frozen_string_literal: true

require_relative 'contact_detail_representer'
require_relative 'oblip_type_representer'
require_relative 'business_representer'

module Oblip
  # Represents contact detail for API output
  class ContactInfoRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :type, extend: OblipTypeRepresenter, class: OpenStruct
    property :personal_details,
             extend: ContactDetailRepresenter, class: OpenStruct
    property :business,
              extend: BusinessRepresenter, class: OpenStruct
  end
end
