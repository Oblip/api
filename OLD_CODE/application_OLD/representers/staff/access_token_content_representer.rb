# frozen_string_literal: true

require_relative '../oblip_type_representer.rb'

module Oblip
  module Representer
    module Staff
      # Represents authenticated account information for API output
      class AccessTokenContentRepresenter < Roar::Decorator
        include Roar::JSON

        property :id
        property :username # staff
        property :type, extend: OblipTypeRepresenter, class: OpenStruct # both
        property :permissions # staff
      end
    end
  end
end
