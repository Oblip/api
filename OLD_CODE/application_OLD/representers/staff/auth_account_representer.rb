# frozen_string_literal: true

require_relative '../access_data_representer.rb'
require_relative 'basic_account_representer'

module Oblip
  module Representer
    module Staff
      # Represents authenticated account information for API output
      class AuthAccountRepresenter < Roar::Decorator
        include Roar::JSON

        property :account, extend: BasicAccountRepresenter, class: OpenStruct
        property :access_data, extend: AccessDataRepresenter, class: OpenStruct
      end
    end
  end
end
