# frozen_string_literal: true
# frozen_string_literal: true

require_relative '../oblip_type_representer'
require_relative '../status_representer'

module Oblip
  module Representer
    module Staff
      # Represents authenticated account information for API output
      class BasicAccountRepresenter < Roar::Decorator
        include Roar::JSON

        property :id
        property :username
        property :email_address
        property :first_name
        property :middle_name
        property :last_name
        property :email_verified
        property :photo_url
        property :type, extend: OblipTypeRepresenter, class: OpenStruct
        property :status, extend: StatusRepresenter, class: OpenStruct
        property :permissions
      end
    end
  end
end
