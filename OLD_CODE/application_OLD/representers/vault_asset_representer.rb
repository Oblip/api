# frozen_string_literal: true

require_relative 'oblip_type_representer'

module Oblip
  # Represents contact information for API output
  class VaultAssetRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :code
    property :balance
    property :type,
              extend: OblipTypeRepresenter, class: OpenStruct
    property :authorized
    property :issuer
  end
end
