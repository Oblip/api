# frozen_string_literal: true

require_relative 'transaction_representer'

# Represents a collection of fund account for API output
module Oblip
  class TransactionsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :transactions, extend: TransactionRepresenter
  end
end