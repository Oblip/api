# frozen_string_literal: true

require_relative 'status_representer.rb'
require_relative 'bank_representer.rb'

module Oblip
  # Represents contact information for API output
  class OblipAccountBankRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :bank,
             extend: BankRepresenter, class: OpenStruct
    property :oblip_account_id
    property :account_number
    property :status,
      extend: StatusRepresenter, class: OpenStruct
  end
end
