# frozen_string_literal: true

module Oblip
  # Represents contact information for API output
  class BusinessRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :brand_name
    property :registration_name
    property :address
    property :oblip_account_id
    property :logo_url
    property :is_verified
    property :created_at
    property :updated_at
  end
end
