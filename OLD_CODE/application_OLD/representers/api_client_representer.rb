# frozen_string_literal: true

require_relative 'oblip_type_representer'
require_relative 'status_representer'

# Represents non-confidential information of api client  for API output
module Oblip
  class ApiClientRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :oblip_account_id
    property :platform_type
    property :type, extend: OblipTypeRepresenter, class: OpenStruct
    property :is_verified
    property :status, extend: StatusRepresenter, class: OpenStruct
    property :last_used
    property :last_ip_address

    # TODO: add created_at and updated_at
  end
end