# frozen_string_literal: true

require_relative 'oblip_type_representer.rb'
require_relative 'country_representer.rb'

module Oblip
  # Represents authenticated account information for API output
  class AccessTokenContentRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :phone_number # user
    property :type, extend: OblipTypeRepresenter, class: OpenStruct # both
    property :country, extend: CountryRepresenter, class: OpenStruct # user
    property :base_anchor_code # user
    property :vault_id # user
  end
end
