# frozen_string_literal: true

require_relative 'company_bank_account_representer'

# Represents a collection of banks for API output
module Oblip
  class CompanyBankAccountsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :accounts, extend: CompanyBankAccountRepresenter
  end
end