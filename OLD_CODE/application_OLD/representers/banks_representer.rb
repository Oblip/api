# frozen_string_literal: true

require_relative 'bank_representer'

# Represents a collection of banks for API output
module Oblip
  class BanksRepresenter < Roar::Decorator
    include Roar::JSON

    collection :banks, extend: BankRepresenter
  end
end