# frozen_string_literal: true

require_relative 'contact_info_representer'
require_relative 'transaction_representer'
# TODO: decide if we should still keep this or not.
module Oblip
  # Represents contact detail for API output
  class ProfileRepresenter < Roar::Decorator
    include Roar::JSON
    property :contact_id
    property :account, extend: ContactInfoRepresenter, class: OpenStruct
    # collection :transactions, extend: TransactionRepresenter, class: OpenStruct
  end
end
