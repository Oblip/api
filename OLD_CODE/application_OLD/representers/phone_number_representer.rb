# frozen_string_literal: true

require_relative 'country_representer.rb'

module Oblip
  # Represents phone_number information for API output
  class PhoneNumberRepresenter < Roar::Decorator
    include Roar::JSON

    property :phone_number
    property :country, extend: CountryRepresenter, class: OpenStruct
  end
end
