# frozen_string_literal: true

module Oblip
  # Represents a phone number verification box
  class PhoneVerificationBoxRepresenter < Roar::Decorator
    include Roar::JSON

    property :phone_number
    property :code
  end
end
