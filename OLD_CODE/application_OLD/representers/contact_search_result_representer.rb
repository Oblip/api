# frozen_string_literal: true

require_relative 'oblip_type_representer'
require_relative 'contact_info_representer'

module Oblip
  # Represents account information for API output
  class ContactSearchResultRepresenter < Roar::Decorator
    include Roar::JSON

    property  :query
    property  :is_in_contact_list
    property  :result, extend: ContactInfoRepresenter, class: OpenStruct
  end
end
