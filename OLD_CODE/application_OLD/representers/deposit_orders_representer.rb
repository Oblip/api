# frozen_string_literal: true

require_relative 'deposit_order_representer'

# Represents a collection of bank order API output
module Oblip
  class DepositOrdersRepresenter < Roar::Decorator
    include Roar::JSON

    collection :deposit_orders, extend: DepositOrderRepresenter
  end
end