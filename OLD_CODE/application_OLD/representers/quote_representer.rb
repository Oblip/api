# frozen_string_literal: true

require_relative 'anchor_asset_representer'
require_relative 'oblip_type_representer'
require_relative 'fee_breakdown_representer'

module Oblip
  # Represents transaction information for API output
  class QuoteRepresenter < Roar::Decorator
    include Roar::JSON

    property :amount
    property :anchor_asset, extend: AnchorAssetRepresenter, class: OpenStruct
    property :anchor_fee_breakdown, extend: FeeBreakdownRepresenter, class: Struct
    property :anchor_fee
    property :agent_fee
    property :requires_sender_signature
    property :exchange_rate
    property :type, extend: OblipTypeRepresenter, class: OpenStruct
    property :unsigned_tx_envelope
  end
end
