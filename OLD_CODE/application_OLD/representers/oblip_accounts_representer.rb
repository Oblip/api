# frozen_string_literal: true

require_relative 'oblip_account_representer'

module Oblip
  # Represents essential Account information for API output
  class OblipAccountsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :oblip_accounts, extend: OblipAccountRepresenter
  end
end
