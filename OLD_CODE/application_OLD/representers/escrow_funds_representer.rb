# frozen_string_literal: true

require_relative 'escrow_fund_representer'

# Represents a collection of bank order API output
module Oblip
  class EscrowFundsRepresenter < Roar::Decorator
    include Roar::JSON
    
    collection :escrow_funds, extend: EscrowFundRepresenter
  end
end