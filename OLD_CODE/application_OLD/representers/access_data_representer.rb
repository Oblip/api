# frozen_string_literal: true

module Oblip
  # Represents access token content information for API output
  class AccessDataRepresenter < Roar::Decorator
    include Roar::JSON

    property :token
    property :expires_on
  end
end
