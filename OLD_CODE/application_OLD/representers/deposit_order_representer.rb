# frozen_string_literal: true

require_relative 'transaction_representer'
require_relative 'company_bank_account_representer'
require_relative 'key_name_representer'
require_relative 'status_representer'

module Oblip
  # Represents contact information for API output
  class DepositOrderRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :oblip_account_id
    property :transaction,
             extend: TransactionRepresenter, class: OpenStruct
    property :bank_account,
             extend: CompanyBankAccountRepresenter, class: OpenStruct
    property :deposit_method,
             extend: KeyNameRepresenter, class: OpenStruct
    property :status,
             extend: StatusRepresenter, class: OpenStruct
    property :created_at
    property :updated_at
  end
end
