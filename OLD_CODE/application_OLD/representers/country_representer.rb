# frozen_string_literal: true

require_relative 'anchor_asset_representer'

module Oblip
  # Represents country information for API output
  class CountryRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :code
    property :phone_code
    property :currency
    property :anchor_asset, extend: AnchorAssetRepresenter, class: OpenStruct
    property :allow_cross_border_trade
  end
end
