# frozen_string_literal: true

module Oblip
  # Represents contact information for API output
  class ContactDetailRepresenter < Roar::Decorator
    include Roar::JSON

    property :first_name
    property :middle_name
    property :last_name
    property :photo_url
  end
end
