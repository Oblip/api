# frozen_string_literal: true

# Represents a anchor fee calculation API output
module Oblip
  class FeeBreakdownRepresenter < Roar::Decorator
    include Roar::JSON
    
    property :percentage
    property :fixed
  end
end