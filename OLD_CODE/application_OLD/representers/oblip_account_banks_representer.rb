# frozen_string_literal: true

require_relative 'oblip_account_bank_representer'

# Represents a collection of banksfor API output
module Oblip
  class OblipAccountBanksRepresenter < Roar::Decorator
    include Roar::JSON

    collection :bank_accounts, extend: OblipAccountBankRepresenter
  end
end