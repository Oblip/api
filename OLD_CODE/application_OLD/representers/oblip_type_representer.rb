# frozen_string_literal: true

module Oblip
  # Represents status information for API output
  class OblipTypeRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
  end
end
