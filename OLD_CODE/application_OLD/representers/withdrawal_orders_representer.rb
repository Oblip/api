# frozen_string_literal: true

require_relative 'withdrawal_order_representer'

# Represents a collection of bank order API output
module Oblip
  class WithdrawalOrdersRepresenter < Roar::Decorator
    include Roar::JSON

    collection :withdrawal_orders, extend: WithdrawalOrderRepresenter
  end
end