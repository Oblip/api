# frozen_string_literal: true

require_relative 'anchor_asset_representer'
require_relative 'oblip_type_representer'
require_relative 'status_representer'
require_relative 'contact_info_representer' # TODO; rename this representer later

module Oblip
  # Represents transaction information for API output
  class TransactionRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :anchor_asset, extend: AnchorAssetRepresenter, class: OpenStruct
    property :amount
    property :anchor_fee
    property :agent_fee
    property :network_fee
    # TODO: add anchor and agent escrow
    property :exchange_rate
    property :sender_account, extend: ContactInfoRepresenter, class: OpenStruct
    property :recipient_account, extend: ContactInfoRepresenter, class: OpenStruct
    property :sender_asset_balance
    property :recipient_asset_balance
    property :remarks
    property :type, extend: OblipTypeRepresenter, class: OpenStruct
    property :signed_tx_envelope
    property :unsigned_tx_envelope
    property :vault_tx_hash
    property :status, extend: StatusRepresenter, class: OpenStruct
    property :created_at
  end
end
