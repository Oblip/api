# frozen_string_literal: true

require_relative 'anchor_asset_representer'
require_relative 'status_representer'
require_relative 'oblip_account_representer'

module Oblip
  # Represents contact information for API output
  class EscrowFundRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :anchor_asset, extend: AnchorAssetRepresenter, class: OpenStruct
    property :oblip_account, extend: OblipAccountRepresenter, class: OpenStruct
    property :funds_payable
    property :funds_collected
    property :balance
    property :belongs_to_anchor
    property :is_locked
    property :status, extend: StatusRepresenter, class: OpenStruct
  end
end