# frozen_string_literal: true

require_relative 'contact_representer'

# Represents a collection of fund account for API output
module Oblip
  class ContactsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :contacts, extend: ContactRepresenter
  end
end