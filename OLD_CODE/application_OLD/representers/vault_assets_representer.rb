# frozen_string_literal: true

require_relative 'vault_asset_representer'

# Represents a collection of fund account for API output
module Oblip
  class VaultAssetsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :assets, extend: VaultAssetRepresenter
  end
end