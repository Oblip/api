# frozen_string_literal: true

require_relative 'anchor_asset_representer'

# Represents a collection of fund account for API output
module Oblip
  class AnchorAssetsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :assets, extend: AnchorAssetRepresenter
  end
end