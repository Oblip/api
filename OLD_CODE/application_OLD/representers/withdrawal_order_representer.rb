# frozen_string_literal: true

require_relative 'transaction_representer'
require_relative 'oblip_account_bank_representer'
require_relative 'status_representer'

module Oblip
  # Represents contact information for API output
  class WithdrawalOrderRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :transaction,
             extend: TransactionRepresenter, class: OpenStruct
    property :bank_account,
             extend: OblipAccountBankRepresenter, class: OpenStruct
    property :status,
             extend: StatusRepresenter, class: OpenStruct
  end
end
