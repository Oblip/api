# frozen_string_literal: true

module Oblip
  # Represents authenticated account information for API output
  class AnchorAssetRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :code
    property :symbol
    property :issuer
    property :is_tethered
    property :current_exchange_rate
    property :volume
  end
end
