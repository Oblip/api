# frozen_string_literal: true

require_relative 'access_data_representer'

module Oblip
  # Represents authorization tokens information for API output
  class AuthTokensRepresenter < Roar::Decorator
    include Roar::JSON

    property :access_data, extend: AccessDataRepresenter, class: OpenStruct
    property :refresh_token
  end
end
