# frozen_string_literal: true

require_relative 'oblip_type_representer'

# Represents basic information of api client  for API output
module Oblip
  class BasicApiClientRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :type, extend: OblipTypeRepresenter, class: OpenStruct
    property :is_verified
  end
end