# frozen_string_literal: true

require_relative 'status_representer'
require_relative 'oblip_type_representer'
require_relative 'anchor_asset_representer'
require_relative 'personal_detail_representer'
require_relative 'business_representer'

module Oblip
  # Represents account information for API output
  class OblipAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property  :id
    property  :phone_number
    property  :is_phone_verified # TODO: remove
    property  :type, extend: OblipTypeRepresenter, class: OpenStruct
    property  :kyc_level
    property  :status, extend: StatusRepresenter, class: OpenStruct
    property  :personal_details,
              extend: PersonalDetailRepresenter, class: OpenStruct
    property :business,
              extend: BusinessRepresenter, class: OpenStruct
    property  :base_anchor_asset,
              extend: AnchorAssetRepresenter, class: OpenStruct
  end
end
