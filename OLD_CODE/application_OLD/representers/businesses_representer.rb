# frozen_string_literal: true

require_relative 'business_representer'

# Represents a collection of banks for API output
module Oblip
  class BusinessesRepresenter < Roar::Decorator
    include Roar::JSON

    collection :businesses, extend: BusinessRepresenter
  end
end