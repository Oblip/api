# frozen_string_literal: true

require_relative 'country_representer.rb'

module Oblip
  # Represents contact information for API output
  class BankRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :company_name
    property :address
    property :logo_url
    property :swift_code
    property :routing_number
    property :country,
             extend: CountryRepresenter, class: OpenStruct
  end
end
