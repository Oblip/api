# frozen_string_literal: true

require_relative 'status_representer.rb'
require_relative 'bank_representer.rb'

module Oblip
  # Represents contact information for API output
  class CompanyBankAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :bank,
             extend: BankRepresenter, class: OpenStruct
    property :account_number
    property :status,
      extend: StatusRepresenter, class: OpenStruct
  end
end
