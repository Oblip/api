 # frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to add a bank account of a user in  our database
  # Usage:
  #   result = AddContact.call(auth_account:, bank_id:, account_number:)
  #   result.success?
  module AddOblipAccountBank
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, bank_id:, account_number:)
      raise OException::Unauthorized unless auth_account

      bank_accounts = Repository::For[Entity::OblipAccount]
        .get_all_bank_accounts(auth_account['id'])

      policy = OblipAccountBankPolicy.new(auth_account, bank_accounts)
      raise OException::PolicyRestriction unless policy.can_add_more?

      it_exists = Repository::For[Entity::OblipAccountBank]
        .account_exists?(auth_account['id'], bank_id, account_number)

      raise OException::PolicyRestriction if it_exists

      entity = Entity::OblipAccountBank.build(
        bank_id: bank_id,
        oblip_account_id: auth_account['id'],
        account_number: account_number
      )

      bank = Repository::For[Entity::OblipAccountBank]
        .create(entity)

      Success(Result.new(:created, bank))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::PolicyRestriction
      Failure(Result.new(:bad_request, 'Unable to add this bank account.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
