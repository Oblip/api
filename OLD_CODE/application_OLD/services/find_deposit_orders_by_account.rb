# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a bank orders by oblip account from our database
  # Usage:
  #   result = FindDepositOrdersByAccount.call(auth_account:, type:)
  # type = 0 returns all
  #   result.success?
  module FindDepositOrdersByAccount
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      orders = Repository::For[Entity::OblipAccount]
        .get_all_deposit_orders(auth_account['id'])

      Success(Result.new(:ok, DepositOrders.new(orders)))
    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end

