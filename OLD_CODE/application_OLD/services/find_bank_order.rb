# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a banks from our database
  # Usage:
  #   result = FindBankOrdercall(auth_account:, order_id:)
  #   result.success?
  module FindBankOrder
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, order_id:)
      raise OException::Unauthorized unless auth_account

      # TODO: add policy here check if owner or staff/admin

      order = Repository::For[Entity::BankOrder]
        .find_id(order_id)
      
      Success(Result.new(:ok, order))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
