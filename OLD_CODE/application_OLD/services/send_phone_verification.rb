# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to send a OTP verification code to a phone number
  # Usage:
  #   result = SendPhoneVerification.call(config:, phone_number:)
  #   result.success?
  module SendPhoneVerification
    extend Dry::Monads::Result::Mixin

    def self.call(config:, client_id:, signature:, data:)
      # device = Repository::For[Entity::Device]
      #   .find_id(device_id)

      client = Repository::ApiClients.find_id(client_id)

      raise OException::NotFound unless client

      request_verifier = VerifyRequest.new(client.public_key)
      data = request_verifier.parse(signature, data.to_json)

      # generate short token to verify
      code_result = ShortCodeToken.create
      code = code_result[:code]
      token_data = code_result[:token_data]

      phone_number = Phonelib.parse(data[:phone_number])
      
      raise OException::InvalidValueError unless phone_number.valid?

      client = Repository::ApiClients
        .save_verification_token(client.id, token_data[:token])

      message = "Oblip verification code: #{code}. "
      message += 'You are binding this phone number to your Oblip account. '
      message += 'Valid for 20 minutes.'

      if(ENV['RACK_ENV'] == 'production' || ENV['RACK_ENV'] == 'staging') # don't run if running tests
        puts 'We just sent and SMS Message :o'
        msg_result = SendSMSMessage.call(
          config: config,
          phone_number: phone_number.to_s,
          type: SMS::TYPE_TRANSACTIONAL,
          message: message
        )
      end

      Success(Result.new(:ok, PhoneVerificationBox.new(phone_number.to_s, code) ))
    
    rescue VerifyRequest::SignatureVerificationFailed
      puts 'Sig failed'
      Failure(Result.new(:not_found, 'We could not send the verification code.'))
    rescue OException::NotFound
      puts 'Device not found'
      Failure(Result.new(:not_found, 'We could not send the verification code.'))
    rescue OException::InvalidValueError
      Failure(Result.new(:bad_request, 'The phone number you entered is invalid. Try again.'))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'We could not send the verification code. Try agian.'))
    end
  end
end
