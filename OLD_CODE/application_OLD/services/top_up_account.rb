# frozen_string_literal: true

require 'dry/transaction'


module Oblip
  # Service to top up account (THIS is a test Service Class! No longer available after admin endpoints are created)
  # Usage:
  #   result = TopUpAccount.new.call(config:, auth_account:, recipient_id:, amount:)
  #   result.success? Returns OblipAccount
  class TopUpAccount
    include Dry::Transaction

    step :create_blockchain_tx
    step :record_fees
    step :update_local_copy

    def create_blockchain_tx(config:, auth_account:, recipient_id:, amount:)
      raise OException::Unauthorized unless auth_account && auth_account['type']['id'] == Entity::OblipAccount::Types::ANCHOR.id # TODO: later (policy management)
     
      # TODO: add exception handling for no recipient_id or no amount but it might not matter
      # as this service is only for testing

      input = {}
      horizon = Horizon::Api.new(config)
      
      recipient_vault = Repository::For[Entity::OblipAccount]
        .find_vault(recipient_id)

      recipient_keypair = horizon.keypair_from_address(recipient_vault.address)

      recipient_info = recipient_id ? Repository::OblipAccounts.find_id(recipient_id) : nil

      sender_type = Entity::OblipAccount::Types::ANCHOR
      recipient_type = Entity::OblipAccount::Types.find(recipient_info.type.id)

      anchor_asset = Repository::AnchorAssets.find_code('BZD')

      funding_pool = Repository::BlockchainFunds.find_optimal_fund(
        Entity::BlockchainFund::SupportedAssets::XLM,
        0.1
      )

      recipient_vault_asset = Repository::Vaults.find_vault_asset(recipient_vault.id, 'BZD')

      transaction = Entity::Transaction.build(
        vault_source: nil,
        sender_type: sender_type,
        recipient_type: recipient_type,
        tx_data: {
          anchor_asset: anchor_asset,
          amount: amount,
          remarks: 'Anchor Deposit',
          tx_type: Entity::Transaction::Types::DEPOSIT,
          sender_account: nil,
          recipient_account:  Entity::OblipAccount.prebuild(recipient_id),
          sender_asset_balance: nil,
          recipient_asset_balance: recipient_vault_asset.balance,
          vault_tx_hash: nil,
          signed_tx_envelope: nil,
          status: Entity::Status::Transaction::COMPLETED
        }
      )

      input[:amount_transacted] = transaction.calculate_transaction(sender_type, recipient_type)

      signed_envelope = horizon.create_payment_envelope(
        asset_code: anchor_asset.code,
        amount: input[:amount_transacted][:received_amount],
        fee: transaction.anchor_fee,
        should_sign: true,
        keys: {
          from_account: horizon.anchor,
          to_account: recipient_keypair,
          signer: horizon.credit_issuer
        }
      )
      
      # creates blockchain transaction
      tx_hash = horizon.submit_payment_by(signed_envelope)

      # assigns the tx_hash and the signed envelope to the transaction entity
      transaction = transaction.new(
        vault_tx_hash: tx_hash,
        signed_tx_envelope: signed_envelope
      )

      # add the network fee and pool to the transaction entity
      transaction = Entity::Transaction.apply_funding(transaction, funding_pool)
      
      # updates our blockchain fund pool; removing the tx network fee from balance
      Repository::BlockchainFunds.update_balance(funding_pool.id,
        -(transaction.network_fee))

      # used to pass through the other functions
      input[:recipient_vault] = recipient_vault

      # creates a record of the transaction in our server
      input[:tx_result] = Repository::For[Entity::Transaction]
        .create(transaction)

      # updates the asset's volume
      Repository::AnchorAssets.update_volume(anchor_asset.code, input[:amount_transacted][:received_amount])

      input[:config] = config
      Success(input)

    rescue OException::Unauthorized => error
      puts 'Not Authorized'
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong sending Topping Up Account.'))
    end

    def record_fees(input)
      if input[:tx_result].anchor_fee > 0
        anchor_escrow = Repository::EscrowFunds
          .find_open_anchor_escrow_fund(input[:tx_result].anchor_asset.code)

          # we'll create a new escrow if an open one does not exist
        if !anchor_escrow
          escrow_entity = Entity::EscrowFund.new(
            id: nil,
            anchor_asset: input[:tx_result].anchor_asset,
            oblip_account: nil,
            funds_payable: input[:tx_result].anchor_fee,
            funds_collected: input[:tx_result].anchor_fee,
            balance: input[:tx_result].anchor_fee,
            belongs_to_anchor: true,
            is_locked: false,
            status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION
          )

          anchor_escrow = Repository::EscrowFunds.create(escrow_entity)
        else
          Repository::EscrowFunds.update_funds_payable(anchor_escrow.id,
            input[:tx_result].anchor_fee)
          Repository::EscrowFunds.update_funds_collected(anchor_escrow.id,
            input[:tx_result].anchor_fee)
          Repository::EscrowFunds.update_balance(anchor_escrow.id,
            input[:tx_result].anchor_fee)
        end

        # save anchor escrow in our transaction
        Repository::Transactions.save_anchor_escrow(input[:tx_result].id,
        anchor_escrow)
      end

      # NOTE: this is an anchor transaction so there is no agent fee ever :p
      
      Success(input)
    end

    def update_local_copy(input)
      tx_entity = input[:tx_result]
      amount_transacted = input[:amount_transacted]
      
      account_mapper = Horizon::AccountMapper.new(input[:config])

      # getting data from blockchain
      recipient_ledger_data = account_mapper
        .load(input[:recipient_vault].address)

      # update the vault
      Repository::For[Entity::Vault]
        .add_or_update_vault_and_assets_using_raw(
          input[:recipient_vault].id, recipient_ledger_data)
      
      if(ENV['RACK_ENV'] != 'test')
        fcm = FCM.new(input[:config]['FCM_SERVER_KEY'])

        recipient_devices = Repository::For[Entity::OblipAccount]
          .get_all_devices(tx_entity.recipient_fund.oblip_account_id)


        default_asset = tx_entity.recipient_fund.default_anchor_asset
        local_amount = (tx_entity.amount * default_asset.current_exchange_rate)

        recipient_device_token_ids = recipient_devices.map {|x| x.device_token }
        notif_title = 'Oblip Deposit'
        notif_message = "#{local_amount.truncate(2)} #{default_asset.code} "
        notif_message += "deposit successful"

        options = Entity::DeviceNotification.new(
          title: notif_title,
          message: notif_message,
          type: Entity::DeviceNotification::Types::TRANSACTION,
          data: { transaction_id: tx_entity.id, }
        )

        fcm.send(recipient_device_token_ids, options.to_hash)

      end

      Success(Result.new(:created, tx_entity))
      
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong sending Topping Up Account.'))
    end
  end
end
