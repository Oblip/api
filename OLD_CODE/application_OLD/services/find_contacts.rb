# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all contacts of an account from our database
  # Usage:
  #   result = FindContacts.call(auth_account:)
  #   result.success?
  module FindContacts
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      contacts = Repository::For[Entity::OblipAccount]
        .get_all_contacts(auth_account['id'])

      Success(Result.new(:ok, Contacts.new(contacts)))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong finding contacts.'))
    end
  end
end
