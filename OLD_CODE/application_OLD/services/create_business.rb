# frozen_string_literal: true

require 'dry/transaction'
require 'tempfile'
require "mini_magick"

module Oblip
  # Service to create a business in our database
  # Usage:
  #   result = CreateBusiness.call(auth_account:, data:)
  #   result.success? returns Entity::Business
  class CreateBusiness
    include Dry::Transaction

    step :upload_logo
    step :create_business

    def upload_logo(config:, auth_account:, data:)
      raise OException::Unauthorized unless auth_account
      user_types = Entity::OblipAccount::Types
      current_type = auth_account['type']['id']
      invalid_exception = OException::InvalidValueError
      raise invalid_exception unless current_type == user_types::BUSINESS.id || current_type == user_types::AGENT.id
      
      id = auth_account['id']

      logo_base64 = data[:logo_base64]
      data[:oblip_account_id] = id

      raise OException::MissingParameterError unless logo_base64

      time = Time.now.to_f.round
      main_key = "#{time}#{id}"

      file = Base64.decode64(logo_base64)
      temp_file = Tempfile.new(main_key)
      temp_file << file
      temp_file.close

      main_object = CloudStorage.upload(
        bucket: :app,
        name: "business_logos/#{main_key}",
        path: temp_file.path
      )

      thumbnail = MiniMagick::Image.open(temp_file.path)
      thumbnail.resize "30%"
      thumbnail.format "jpeg"

      small_object = CloudStorage.upload(
        bucket: :app,
        name: "business_logos/#{main_key}_thumbnail",
        path: thumbnail.path
      )

      temp_file.unlink # deletes temp file

      data[:logo_url] = main_object.public_url

      Success(data)

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::InvalidValueError
      Failure(Result.new(:bad_request, 'Not allowed to create a business.'))
    rescue OException::MissingParameterError
      # placeholder
      data[:logo_url] = "https://s3.amazonaws.com/#{config['AWS_APP_BUCKET_NAME']}/static_assets/business_placeholder.png"
      Success(data)
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def create_business(data)

      entity = Entity::Business.new(
        id: nil,
        brand_name: data[:brand_name],
        is_verified: false,
        registration_name: data[:registration_name],
        address: data[:address],
        oblip_account_id: data[:oblip_account_id],
        logo_url: data[:logo_url],
        created_at: nil,
        updated_at: nil
      )

      business = Repository::Businesses.create(entity)

      # update oblip_account
      # oblip_account = Repository::OblipAccounts
      #   .add_business(data[:oblip_account_id], business.id)

      Success(Result.new(:created, business))

    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
