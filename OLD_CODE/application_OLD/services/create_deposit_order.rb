# frozen_string_literal: true

require 'dry/transaction'


module Oblip
  # Service to create a bank order in our database
  # Usage:
  #   result = CreateDepositOrder.new.call(auth_account: (hash), config:, oblip_bank_id:, amount: )
  #   result.success? Returns OblipAccount
  module CreateDepositOrder
    extend Dry::Monads::Result::Mixin

    def self.call(config:, auth_account:, company_bank_id:,
      amount:)

      raise OException::Unauthorized unless auth_account

      current_total = Repository::DepositOrders.get_total_account_orders(auth_account['id'])

      # do policy checks
      policy = DepositPolicy.new(auth_account)
      policy.can_create_deposit_order?(current_total)

      company_bank_account = Repository::For[Entity::CompanyBankAccount]
        .find_id(company_bank_id)

      recipient_type = Entity::OblipAccount::Types.find(auth_account['type']['id'])
      anchor_asset = Repository::AnchorAssets.find_code(company_bank_account.bank.country.currency)
      
      recipient_vault = Repository::For[Entity::OblipAccount]
        .find_vault(auth_account['id'])
      recipient_vault_asset = Repository::Vaults.find_vault_asset(recipient_vault.id,
        anchor_asset.code)

      # TODO: handle anchor asset doesn't exist here

      tx_entity = Entity::Transaction.build(
        vault_source: nil,
        sender_type: Entity::OblipAccount::Types::ANCHOR,
        recipient_type: recipient_type,
        tx_data: {
          anchor_asset: anchor_asset,
          amount: amount,
          remarks: 'Deposit to Bank & Confirm',
          tx_type: Entity::Transaction::Types::DEPOSIT,
          sender_account: nil,
          recipient_account: Entity::OblipAccount.prebuild(auth_account['id']),
          sender_asset_balance: nil,
          recipient_asset_balance: recipient_vault_asset.balance, 
          vault_tx_hash: nil,
          signed_tx_envelope: nil,
          status: Entity::Status::Transaction::AWAITING_DEPOSIT
        }
      )

      transaction = Repository::For[Entity::Transaction]
        .create(tx_entity)

      default_method = Entity::FinancialMethod::BANK  # default for version 1
      order_entity = Entity::DepositOrder.build(
        oblip_account_id: auth_account['id'],
        transaction: transaction,
        bank_account: company_bank_account,
        deposit_method: default_method
      )

      result = Repository::For[Entity::DepositOrder]
        .create(order_entity)

      Success(Result.new(:created, result))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, "Resource not found"))
    rescue OException::PolicyRestriction
      Failure(Result.new(:bad_request,
        'Policy Restriction: Not allowed to have more than two pending deposit orders'))
    rescue OException::InsufficentFundingPool 
      Failure(Result.new(:cannot_process, 'No available funding pool at the moment.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
