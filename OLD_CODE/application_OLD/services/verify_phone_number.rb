# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to verify phone number from sms token
  # Usage:
  #   result = VerifyPhoneNumber.call(client_id: xx, data:, signature:)
  #   result.success?
  module VerifyPhoneNumber
    extend Dry::Monads::Result::Mixin

    def self.call(client_id:, data:, signature:)
      client = Repository::ApiClients.find_id(client_id)
      raise OException::NotFound unless client

      request_verifier = VerifyRequest.new(client.public_key)

      payload = ShortCodeToken.payload(client.verification_token)
      data = request_verifier.parse(signature, data.to_json)

      if(ENV['RACK_ENV'] == 'production' || ENV['RACK_ENV'] == 'staging')
        # TODO: refactor later
        if payload['code'] == data[:code]
          client = Repository::ApiClients
            .update_verification_flag(client_id, true)
          Success(Result.new(:ok, client))
        else
          Failure(Result.new(:unauthorized, 'Verification code is invalid'))
        end
      else
        client = Repository::ApiClients
            .update_verification_flag(client_id, true)

        Success(Result.new(:ok, client))
      end

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::NotFound
      puts 'Client not found'
      Failure(Result.new(:not_found, "Resource not found"))
    rescue AccessToken::ExpiredTokenError
      Failure(Result.new(:unauthorized, 'Verification code has expired.'))
    rescue StandardError => error
      puts "Verify Phone # error: #{error.message}"
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again later.'))
    end
  end
end
