# frozen_string_literal: true

require 'dry-monads'
# TODO: Refactor this to handle auth_account
module Oblip
  # Service to find a account from our database
  # Usage:
  #   result = FindOblipAccount.call(account_id)
  #   result.success?
  module FindOblipAccount
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account)
      raise OException::Unauthorized unless auth_account

      account = Repository::For[Entity::OblipAccount].find_id(auth_account['id'])
      if account
        Success(Result.new(:ok, account))
      else
        Failure(Result.new(:not_found, 'Could not find the account'))
      end

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, "Resource not found"))
    end
  end
end
