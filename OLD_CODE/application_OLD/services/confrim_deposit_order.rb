# frozen_string_literal: true

require 'dry-monads'
require 'concurrent'

module Oblip
  # Service to confirm a deposit order in our database
  # Usage:
  #   result = ConfirmDepositOrder.call(auth_account:, contact_account_id:)
  #   result.success?
  module ConfirmDepositOrder
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, deposit_order_id:)
      raise OException::Unauthorized unless auth_account

      deposit_order = Repository::DepositOrders.find_id(deposit_order_id)

      raise OException::NotFound unless deposit_order

      raise OException::Unauthorized if deposit_order.oblip_account_id != auth_account['id']

      transaction = Repository::Transactions
      .change_status(deposit_order.transaction.id, Entity::Transaction::Status::CONFIRMATION_REVIEW)

      deposit_order = Repository::DepositOrders
        .change_status(deposit_order_id, Entity::DepositOrder::Status::USER_CONFIRMED)

      # sends an email concurrently
      Concurrent::Promise.execute{ EmailNotification.notify_deposit_confirmed(deposit_order) } if ENV['RACK_ENV'] != 'test'

      Success(Result.new(:ok, deposit_order))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::NotFound
      puts "Policy Restriction"
      Failure(Result.new(:bad_request, 'Deposit Order does not exist'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
