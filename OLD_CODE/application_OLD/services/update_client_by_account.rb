# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to update device from our database
  # Usage:
  #   result = UpdateClientByAccount.call(auth_account:, client_id:, ip_address:)
  #   result.success? returns a Device Entity
  module UpdateClientByAccount
    extend Dry::Monads::Result::Mixin

    # rubocop:disable MethodLength
    def self.call(auth_account:, client_id:, ip_address:)
      raise OException::Unauthorized unless auth_account

      client = Repository::ApiClients.find_id(client_id)
      policy = ApiClientPolicy.new(auth_account['id'], client)

      raise OException::PolicyRestriction unless policy.can_update?

      updated_client = Repository::ApiClients
               .update_last_seen(client_id,ip_address)

      Success(Result.new(:ok, updated_client))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::PolicyRestriction
      puts 'Policy Restriction'
      Failure(Result.new(:not_found, 'Not found'))
    rescue StandardError => error
      puts error
      Failure(Result.new(:internal_error, 
        'Some thing went wrong. Try later.'))
    end
  end
end
