# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find an api client by account
  # Usage:
  #   result = FindAccountApiClient.call(auth_account:, client_id:)
  #   result.success?
  module FindAccountApiClient
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, client_id:)
      raise OException::Unauthorized unless auth_account

      client = Repository::ApiClients.find_id(client_id)
      raise OException::NotFound unless client

      policy = ApiClientPolicy.new(auth_account['id'], client)
      raise OException::PolicyRestriction unless policy.can_view?

      Success(Result.new(:ok, client))
     
    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::PolicyRestriction
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::NotFound
      Failure(Result.new(:not_found, 'Could not find client'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
