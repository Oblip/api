# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find an escrow fund from our database
  # Usage:
  #   result = FindEscrowFund(auth_account:, escrow_fund_id:)
  #   result.success?
  module FindEscrowFund
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, escrow_fund_id:)
      raise OException::Unauthorized unless auth_account
      escrow = Repository::EscrowFunds.find_id(escrow_fund_id)
      raise OException::NotFound unless escrow
      raise OException::Unauthorized unless escrow.oblip_account.id == auth_account['id']
      

      Success(Result.new(:ok, escrow))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::NotFound
      Failure(Result.new(:bad_request, 'Escrow fund does not exist.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
