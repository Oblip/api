# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to find a fund accounts of an account from our database
  # Usage:
  #   result = FindContactByPhoneNumber.call(auth_account:, phone_number:)
  #   result.success?
  module FindContactByPhoneNumber
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, phone_number:)
      raise OException::Unauthorized unless auth_account
      phone_number = Phonelib.parse(phone_number)
      raise OException::InvalidValueError unless phone_number.valid?

      result = Repository::For[Entity::OblipAccount]
        .find_phone_number(phone_number.to_s)

      raise OException::NotFound unless result
      raise OException::NotFound if result.id == auth_account['id']

      is_in_contact_list = Repository::For[Entity::Contact]
        .find_by_owner(auth_account['id'], result.id)

      search_result = Entity::ContactSearchResult.new(
        query: phone_number.to_s,
        is_in_contact_list: !is_in_contact_list.nil?,
        result: result
      )

      Success(Result.new(:ok, search_result))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::InvalidValueError
      Failure(Result.new(:bad_request, 'Invalid phone number supplied.'))
    rescue OException::NotFound
      Failure(Result.new(:not_found, 'No results'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong finding fund accounts.'))
    end
  end
end
