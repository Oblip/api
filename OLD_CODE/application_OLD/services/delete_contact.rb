# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to delete a contact of an account from our database
  # Usage:
  #   result = DeleteContact.call(auth_account:, contact_id:)
  #   result.success?
  module DeleteContact
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, contact_id:)
      raise OException::Unauthorized unless auth_account

      contact = Repository::For[Entity::Contact]
        .find_id(contact_id)

      policy = ContactPolicy.new(auth_account, contact)
      raise OException::PolicyRestriction unless policy.can_delete?

      deleted_contact = Repository::For[Entity::Contact]
        .delete(contact_id)

      Success(Result.new(:ok, deleted_contact))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Service unavilable.'))
    rescue OException::PolicyRestriction
      Failure(Result.new(:not_found, 'Something is not right.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
