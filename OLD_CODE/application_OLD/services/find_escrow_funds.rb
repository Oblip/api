# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find paginated oblip account escrows from database
  # Usage:
  #   result = FindOblipAccountEscrows.call(auth_account:, offset:, limit:)
  #   result.success?
  module FindEscrowFunds
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, offset:, limit:)
      raise OException::Unauthorized unless auth_account
      # TODO: check again if it's a staff here
      
      results = Repository::EscrowFunds
        .find_all_escrow_funds(offset, limit)

      Success(Result.new(:ok, EscrowFunds.new(results)))
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end