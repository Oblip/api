# frozen_string_literal: true

require 'dry/transaction'
require 'sequel'
require 'phonelib'

module Oblip
  # Service to create a account in our database
  # Usage:
  #   result = CreateOblipAccount.new(Api.config).call(client_id:, data:, ip_address:, signature:)
  #   result.success? returns refresh_token and access_token
  class CreateOblipAccount
    include Dry::Transaction

    ERROR_MESSAGE = 'Something went wrong. Try again.'

    step :verify_signature
    step :check_phone_number_account
    step :prepare_entities
    step :create_account
    step :generate_auth_token

    def verify_signature(client_id:, data:, ip_address:, signature:)

      client = Repository::ApiClients.find_id(client_id)

      raise OException::NotFound unless client
      raise OException::Unauthorized unless client.is_verified

      request_verifier = VerifyRequest.new(client.verifier_key)
      data = request_verifier.parse(signature, data.to_json)

      input = {}
      input[:client] = client
      input[:data] = data
      input[:ip_address] = ip_address

      Success(input)

    rescue VerifyRequest::SignatureVerificationFailed
      puts 'Account Sig failed'
      Failure(Result.new(:not_found, "Resource not found"))
    rescue OException::Unauthorized
      Failure(Result.new(:unauthorized, "Please verify your phone number"))
    rescue OException::NotFound
      puts 'Client not found'
      Failure(Result.new(:not_found, "Resource not found"))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, ERROR_MESSAGE))
    end

    def check_phone_number_account(input)
      phone_number = Phonelib.parse(input[:data][:phone_number])
      raise OException::InvalidValueError unless phone_number.valid?

      account_exists = Repository::For[Entity::OblipAccount]
                          .find_phone_number(phone_number.to_s)

      raise OException::DuplicationError if !account_exists.nil?

      input[:data][:phone_number] = phone_number.to_s
      country = Repository::For[Entity::Country]
                  .find_code(phone_number.country)
      input[:data][:country_id] = country.id
      
      input[:anchor_asset] = Repository::AnchorAssets.find_code(country.currency)

      Success(input)

    rescue OException::InvalidValueError
      Failure(Result.new(:bad_request, 'Invalid phone number supplied.'))
    rescue OException::DuplicationError
      Failure(Result.new(:cannot_process, 'An account already exists. Try to restore the account.'))
    end

    def prepare_entities(input)
      new_input = {}

      new_input[:account_entity] = Entity::OblipAccount.create_new(
        phone_number: input[:data][:phone_number],
        type: Entity::OblipAccount::Types.find(input[:data][:type]),
        base_anchor_asset: input[:anchor_asset],
        status: Entity::OblipAccount::Status::INCOMPLETE
      )

      new_input[:detail_entity] = Entity::PersonalDetail.create_new(
        first_name: input[:data][:first_name],
        middle_name: input[:data][:middle_name],
        last_name: input[:data][:last_name],
        birthday: input[:data][:birthday],
        country_id: input[:data][:country_id]
      )

      new_input[:secret_code] = SecureDB.generate_key

      # TODO: add secret_code to api_clients
      client = input[:client].new(
        secret_code: new_input[:secret_code],
        status: Entity::ApiClient::Status::ACTIVE
      )

      new_input[:client] = client
      new_input[:ip_address] = input[:ip_address]

      Success(new_input)
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, ERROR_MESSAGE))
    end

    def create_account(input)
      
      account = Repository::For[Entity::OblipAccount].create(
        detail_entity: input[:detail_entity],
        account_entity: input[:account_entity],
        client_entity: input[:client]
      )
      
      new_input = {}
      new_input[:account] = account
      new_input[:client] = input[:client]
      new_input[:secret_code] = input[:secret_code]
      new_input[:ip_address] = input[:ip_address]

      Success(new_input)

    rescue OException::DuplicationError
      Failure(Result.new(:bad_request, 'The phone number already exits. Try to restore account.'))
    rescue StandardError => error
      puts "Error #2: #{error.message}"
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, ERROR_MESSAGE))
    end

    def generate_auth_token(input)
      oblip_account = input[:account]
      client = input[:client]

      access_meta_data = Entity::AccessMetaData.create(oblip_account)
      auth_tokens = Entity::RefreshMetaData
        .create_auth(client.id, input[:secret_code], access_meta_data)

      # update client's last seen
      Repository::ApiClients.update_last_seen(client.id, input[:ip_address])
      
      Success(Result.new(:created, auth_tokens))

    rescue StandardError => error
      puts "Error #5: #{error.message}"
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, ERROR_MESSAGE))
    end
  end
end
