# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a personal details by account id from our database
  # Usage:
  #   result = FindPersonalDetailsByAccount.call(account_id)
  #   result.success?
  module FindPersonalDetailsByAccount
    extend Dry::Monads::Result::Mixin

    def self.call(account_id)
      detail = Repository::For[Entity::OblipAccount].get_personal_details(account_id)
      if detail
        Success(Result.new(:ok, detail))
      else
        Failure(Result.new(:not_found, 'Could not find the personal details'))
      end
    end
  end
end
