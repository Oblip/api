# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to add a mobile client to our database
  # Usage:
  #   result = RegisterMobileClient.call(config, data)
  #   result.success?
  module RegisterMobileClient
    extend Dry::Monads::Result::Mixin

    def self.call(config:, data:)
      data = Hash[data.map { |k, v| [k.to_sym, v] }]
      # TODO: check if missing parameters values
      data[:type] = Entity::ApiClient::Types::USER
      
      entity = Entity::ApiClient.prebuild(data)
      client = Repository::ApiClients.create(entity)

      Success(Result.new(:created, client))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
