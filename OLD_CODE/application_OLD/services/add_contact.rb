# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to add a new contacts to an account from our database
  # Usage:
  #   result = AddContact.call(auth_account:, contact_account_id:)
  #   result.success?
  module AddContact
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, contact_account_id:)
      raise OException::Unauthorized unless auth_account

      exists = Repository::For[Entity::Contact]
        .contact_exists?(auth_account['id'], contact_account_id)

      raise OException::DuplicationError if exists

      contact = Repository::For[Entity::Contact]
        .create(auth_account['id'], contact_account_id)

      Success(Result.new(:created, contact))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::DuplicationError
      Failure(Result.new(:bad_request, 'Contact already exists your list.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
