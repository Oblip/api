# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to create secure pin
  # Usage:
  #   result = SaveSecurePIN.call(auth_account: (hash), pin: (string))
  #   result.success?
  module SaveSecurePIN
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, pin:)
      raise OException::Unauthorized unless auth_account
      if pin.size >= 6
        Repository::For[Entity::OblipAccount]
            .save_secure_pin(auth_account['id'], pin)
        Success(Result.new(:ok, 'Secure PIN was saved successfully.'))
      else
        Failure(Result.new(:bad_request, 'Invalid PIN length.'))
      end
    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue AccessToken::ExpiredTokenError
      # TODO; handle an expired token - make a new one and send it back?
    rescue AccessToken::InvalidTokenError
      Failure(Result.new(:not_found, "Resource not found"))
    # TODO; Add InvalidLength error to OException module later
    rescue Repository::OblipAccounts::Errors::InvalidError
      Failure(Result.new(:bad_request, 'Invalid PIN length.'))
    rescue StandardError => error
      puts error
      Failure(Result.new(:internal_error, 
        'Some thing went wrong. Try later.'))
    end
  end
end
