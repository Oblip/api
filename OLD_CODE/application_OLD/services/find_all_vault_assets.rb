# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all vault assets of a logged in user
  # Usage:
  #   result = FindAnchorAssets.call(auth_account:)
  #   result.success?
  module FindAllVaultAssets
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      account = Repository::OblipAccounts.find_id(auth_account['id'])
      vault = Repository::Vaults.find_id(account.vault_id)

      Success(Result.new(:ok, vault))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
