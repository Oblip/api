# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to paginated deposit orders of all users
  # Usage:
  #   result = FindDepositOrdersByAccount.call(auth_account:, type:)
  # type = 0 returns all
  #   result.success?
  module FindDepositOrders
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, offset:, limit:)
      raise OException::Unauthorized unless auth_account
      raise OException::Unauthorized unless auth_account['type']['id'] >= Entity::StaffAccount::Type::SUPER_ADMIN.id

      results = Repository::DepositOrders.find_all_deposit_orders(offset, limit)

      Success(Result.new(:ok, DepositOrders.new(results)))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
