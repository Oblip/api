# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a single deposit order from our database
  # Usage:
  #   result = FindDepositOrder.call(auth_account:, deposit_order_id:)
  #   result.success?
  module FindDepositOrder
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, deposit_order_id:)
      raise OException::Unauthorized unless auth_account

      deposit_order = Repository::DepositOrders.find_id(deposit_order_id)

      raise OException::NotFound unless deposit_order
      raise OException::Unauthorized unless DepositPolicy::AccountScope.viewable?(auth_account, deposit_order)

      Success(Result.new(:ok, deposit_order))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::NotFound
      Failure(Result.new(:bad_request, 'Could not find deposit order.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong finding contacts.'))
    end
  end
end
