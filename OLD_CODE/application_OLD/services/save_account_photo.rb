# frozen_string_literal: true

require 'dry-monads'
require 'tempfile'
# require 'fastimage_resize'
require "mini_magick"
require 'concurrent'

module Oblip
  # Service to save the account photo of a user
  # Usage:
  #   result = SaveAccountPhoto.call(auth_account:, base64_file:)
  #   result.success?
  module SaveAccountPhoto
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, base64_file:)
      raise OException::Unauthorized unless auth_account
      id = auth_account['id']

      # TODO: check to see if a picture already exists for this user
      # and delete it - or keep it as a copy (history trail?) ??
      time = Time.now.to_f.round
      main_key = "#{time}-business-logo-#{id}"
      file = Base64.decode64(base64_file)
      temp_file = Tempfile.new(main_key)
      temp_file << file
      temp_file.close

      main_object = CloudStorage.upload(
        bucket: :app,
        name: "account_photos/#{main_key}",
        path: temp_file.path
      )

      # outfile = FastImage.resize(temp_file.path, 0, 100)

      thumbnail = MiniMagick::Image.open(temp_file.path)
      thumbnail.resize "30%"
      thumbnail.format "jpeg"


      small_object = CloudStorage.upload(
        bucket: :app,
        name: "account_photos/#{main_key}_thumbnail",
        path: thumbnail.path
      )

      temp_file.unlink # deletes temp file
      # thumbnail.unlink
      # outfile.unlink

      # entity =  Entity::PersonalDetail.new(photo_url: object.public_url)
      entity = Entity::PersonalDetail.create_from_hash(photo_url: main_object.public_url)
      result = Repository::For[Entity::OblipAccount]
        .update_personal_details(auth_account['id'], entity)

      # sends an email concurrently
      Concurrent::Promise.execute{ EmailNotification.notify_new_account(result) } if ENV['RACK_ENV'] != 'test'
      
      Success(Result.new(:ok, result))

      # TODO: Figure out what exception upload throws when upload fails

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found')) # TODO: should it return 404?
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Unable save account photo. Try again later.'))
    end
  end
end