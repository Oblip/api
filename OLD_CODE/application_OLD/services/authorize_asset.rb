# frozen_string_literal: true

require 'dry/transaction'


module Oblip
  # Service to create a bank order in our database
  # Usage:
  #   result = AuthorizeAsset(auth_account:, asset_code:, config:)
  #   result.success? Returns OblipAccount
  module AuthorizeAsset
    extend Dry::Monads::Result::Mixin

    def self.call(config:, auth_account:, asset_code:)
      raise OException::Unauthorized unless auth_account

      vault = Repository::For[Entity::OblipAccount]
        .find_vault(auth_account['id'])

      horizon = Horizon::Api.new(config)
      
      # sync data from blockchain to our database
      mapper = Horizon::AccountMapper.new(config)
      loaded_data = mapper.load(vault.address)
      Repository::For[Entity::Vault]
        .add_or_update_vault_and_assets_using_raw(vault.id, loaded_data)
      
        # Repository::For[Entity::Vault]
      #   .update_sequence_number(vault.id, loaded_data.sequence_number)

      asset_repo =  Repository::For[Entity::VaultAsset]
      # # create assets if it doesn't exist in our db
      # loaded_data.assets.map do |asset|
      #   exists = asset_repo.find_code(asset.code)
      #   asset_repo.create(asset.new(vault_id: vault.id)) unless exists
      # end

      asset = asset_repo.find_code(vault.id, asset_code)
      raise OException::InvalidValueError unless asset
      raise OException::InvalidValueError if asset.issuer != config['ANCHOR_ACCOUNT_ID']


      # TODO: make this concurrent
      tx_hash = horizon.allow_trust(vault.address, asset_code)
      updated_asset = asset_repo.update_authorization(vault.id, asset_code, true)


      Success(Result.new(:ok, updated_asset))


    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, "Not found."))
    rescue OException::InvalidValueError
      Failure(Result.new(:bad_request, 'The asset was not issued by us. Not supported.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
