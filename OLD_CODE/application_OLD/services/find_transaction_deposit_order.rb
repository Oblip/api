# frozen_string_literal: true

require 'dry/transaction'

module Oblip
  # Service to find transaction deposit order
  # Usage:
  #   result = FindTransactionDepositOrder.call(auth_account:, transaction_id:)
  #   result.success?
  module FindTransactionDepositOrder
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, transaction_id:)
      raise OException::Unauthorized unless auth_account

      deposit_order = Repository::DepositOrders.find_by_transaction(transaction_id)

      raise OException::NotFound unless deposit_order

      raise OException::PolicyRestriction if deposit_order.oblip_account_id != auth_account['id']

      Success(Result.new(:ok, deposit_order))
  
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Not found'))
    rescue OException::PolicyRestriction
      Failure(Result.new(:bad_request, 'Unable to complete request'))
    rescue OException::NotFound
      Failure(Result.new(:bad_request, 'Could not find the deposit order'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
