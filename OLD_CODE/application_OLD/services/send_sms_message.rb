# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to send sms message
  # Usage:
  #   result = SendSMSMessage.call(config:, phone_number:, type:, message:)
  #   result.success?
  module SendSMSMessage
    extend Dry::Monads::Result::Mixin

    def self.call(config:, phone_number:, type:, message:)
      client = SMS::Api.new(config)
      resp = client.send_message(
        phone_number: phone_number, 
        type: type,
        message: message
      )

      Success(Result.new(:ok, resp))

      # TODO: Handle exception errors here
    end
  end
end

