# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to update personal detail from our database
  # Usage:
  #   result = UpdatePersonalDetailByAccount.call(auth_account:, hash_data:)
  #   result.success? returns a Personal Detail Entity
  module UpdatePersonalDetailByAccount
    extend Dry::Monads::Result::Mixin

    # rubocop:disable MethodLength
    def self.call(auth_account:, hash_data:)
      raise OException::Unauthorized unless auth_account
      entity = Entity::PersonalDetail.create_from_hash(hash_data)

      detail = Repository::For[Entity::OblipAccount]
               .update_personal_details(auth_account['id'], entity)

      if detail
        Success(Result.new(:ok, detail))
      else
        Failure(Result.new(:internal_error, 'Could not update. Try again later!'))
      end

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue AccessToken::ExpiredTokenError
      # TODO; handle an expired token - make a new one and send it back?
    rescue AccessToken::InvalidTokenError
      Failure(Result.new(:not_found, "Resource not found"))
    rescue StandardError => error
      puts error
      Failure(Result.new(:internal_error, 
        'Some thing went wrong. Try later.'))
    end
  end
end
