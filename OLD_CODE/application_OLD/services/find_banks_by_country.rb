# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all the banks from our database
  # Usage:
  #   result = FindBanksByCountry.call(auth_account:)
  #   result.success?
  module FindBanksByCountry
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      country_id = auth_account['country']['id']
     
      banks = Repository::Banks.find_many_by_country(country_id)
      
      Success(Result.new(:ok, Banks.new(banks)))

    rescue OException::Unauthorized
      puts 'UnAuthorized'
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
