# frozen_string_literal: true

require 'dry/transaction'
require 'rotp'
require 'ostruct'

module Oblip
  module Service
    module Staff
      # Service to authenticate a staff account
      # Usage:
      #   result = Authenticate.call(data, signature)
      #   result.success? returns id, access_data
      class Authenticate
        include Dry::Transaction

        step :verify_signature
        step :verify_credentials
        step :generate_auth_token

        def verify_signature(config:, data:, signature:)
          verify_key = config['ALLOWED_APPLICATION_VERIFY_KEY']

          request_verifer = VerifyRequest.new(verify_key)
          data = request_verifer.parse(signature, data.to_json)

          Success(data)

        rescue VerifyRequest::SignatureVerificationFailed
          Failure(Result.new(:unauthorized, 'Unauthorized'))
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong.'))
        end

        def verify_credentials(data)
          account = Repository::StaffAccounts
            .credentials_valid?(data[:username], data[:password], data[:otp])
          
          raise OException::Unauthorized unless account

          Success(account)

          rescue OException::Unauthorized
            puts "Unauthorized"
            Failure(Result.new(:unauthorized, 'Unauthorized'))
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            Failure(Result.new(:internal_error, 'Something went wrong.'))
        end

        def generate_auth_token(account)

          access_data = Entity::StaffAccessData.create(account)

          data = {
            account: account,
            access_data: access_data
          }

          Success(Result.new(:ok, OpenStruct.new(data)))
          
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong.'))
        end
      end
    end
  end
end
