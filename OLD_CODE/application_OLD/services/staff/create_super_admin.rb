# frozen_string_literal: true

require 'dry/transaction'
require 'rotp'

module Oblip
  module Service
     # Service to create a super admin account in our database
     # as initial staff account (Do't use again once created!)
    # Usage:
    #   result = CreateSuperAdmin.new.call(data[Hash])
    #   result.success? returns otp_secret, access_data
    module Staff
      class CreateSuperAdmin
        include Dry::Transaction
  
        step :create_account
        step :generate_auth_info
  
        def create_account(data)
          # there should only be one super admin account
          super_admin_count = Repository::StaffAccounts.super_admin_count
          raise OException::PolicyRestriction if super_admin_count > 0

          entity = Entity::StaffAccount.new(
            id: nil,
            username: data['username'],
            email_address: data['email_address'],
            permissions: data['permissions'],
            email_verified: true,
            first_name: data['first_name'],
            middle_name: data['middle_name'],
            last_name: data['last_name'],
            photo_url: nil,
            type: Entity::StaffAccount::Type::SUPER_ADMIN,
            status: Entity::StaffAccount::Status::ACTIVE
          )

          otp_secret = ROTP::Base32.random_base32
          account = Repository::StaffAccounts.create(entity, 
            data['password'], otp_secret)

          Success(account: account, otp_secret: otp_secret)
          

        rescue OException::PolicyRestriction
          Failure(Result.new(:bad_request, 'A super admin account already exists'))
        end

        def generate_auth_info(input)
          account = input[:account]
          otp_secret = input[:otp_secret]

          access_data = Entity::StaffAccessData.create(account)

          issuer_type = "Oblip Staff (#{ENV['RACK_ENV'] ? ENV['RACK_ENV'] : 'development'})"
          totp = ROTP::TOTP.new(otp_secret, issuer: issuer_type)

          otp_url = totp.provisioning_uri(issuer_type)

          Success(otp_secret: otp_secret, otp_url: otp_url, access_data: access_data)
        end
      end
    end
  end
end