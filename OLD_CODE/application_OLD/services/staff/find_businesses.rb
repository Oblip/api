# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find an oblip account's business info in our database
  # Usage:
  #   result = FindOblipAccountBusiness.call(auth_account:)
  #   result.success?
  module FindBusinesses
    extend Dry::Monads::Result::Mixin
    def self.call(auth_account:, offset:, limit:)
      raise OException::Unauthorized unless auth_account
      raise OException::Unauthorized unless auth_account['type']['id'] >= Entity::StaffAccount::Type::SUPER_ADMIN.id

      results = Repository::Businesses.find_all_businesses(offset, limit)

      Success(Result.new(:ok, Businesses.new(results)))
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end

