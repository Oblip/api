# frozen_string_literal: true

require 'dry-monads'
require 'dry/transaction'
require 'fcm'

module Oblip
  module Service
    module Staff
      # Service to approve a deposit order
      # Usage:
      #   result = ApproveDepositOrder.new.call(config:, staff_auth_account:, deposit_order_id:)
      #   result.success? returns id, access_data
      class ApproveDepositOrder
        include Dry::Monads::Result::Mixin
        include Dry::Transaction

        step :verify
        step :create_blockchain_transaction
        step :update_transaction_table
        step :record_collected_fees
        step :update_vault
        step :update_deposit_order

        def verify(config:, staff_auth_account:, deposit_order_id:)
          raise OException::Unauthorized unless staff_auth_account
          raise OException::Unauthorized unless staff_auth_account['type']['id'] == Entity::StaffAccount::Type::SUPER_ADMIN.id

          deposit_order = Repository::DepositOrders.find_id(deposit_order_id)
          raise OException::InvalidValueError unless deposit_order
          raise OException::OException::PolicyRestriction unless deposit_order.status != Entity::DepositOrder::Status::COMPLETED

          # get funding_pool
          funding_pool = Repository::BlockchainFunds.find_optimal_fund(
            Entity::BlockchainFund::SupportedAssets::XLM,
            0.1
          )

          raise OException::InsufficentFundingPool unless funding_pool


          Success(config: config, deposit_order: deposit_order,
            funding_pool: funding_pool)

        rescue OException::Unauthorized
          Failure(Result.new(:unauthorized, 'Unauthorized'))
        rescue OException::InvalidValueError
          Failure(Result.new(:bad_request, 'Deposit order does not exist.'))
        rescue OException::PolicyRestriction
          Failure(Result.new(:bad_request, 'Deposit Order was already approved!'))
        rescue OException::InsufficentFundingPool
          Failure(Result.new(:cannot_process, 'Insufficent Funding Pool'))
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong.'))
        end

        def create_blockchain_transaction(config:, deposit_order:, funding_pool:)
          horizon = Horizon::Api.new(config)
          recipient_vault = Oblip::Repository::For[Entity::OblipAccount]
            .find_vault(deposit_order.oblip_account_id)

          recipient_keypair = horizon.keypair_from_address(recipient_vault.address)
          transaction = deposit_order.transaction


          signed_envelope = horizon.create_payment_envelope(
            asset_code: transaction.anchor_asset.code,
            amount: transaction.amount,
            fee: transaction.anchor_fee,
            should_sign: true,
            keys: {
              from_account: horizon.anchor,
              to_account: recipient_keypair,
              signer: horizon.credit_issuer
            }
          )

          # creates blockchain transaction
          tx_hash = horizon.submit_payment_by(signed_envelope)

          # assigns the tx_hash and the signed envelope to the transaction entity
          transaction = transaction.new(
            vault_tx_hash: tx_hash,
            signed_tx_envelope: signed_envelope
          )

          transaction = Entity::Transaction.apply_funding(transaction, funding_pool)

           # updates our blockchain fund pool; removing the tx network fee from balance
           Repository::BlockchainFunds.update_balance(funding_pool.id,
            -(transaction.network_fee))
          

          Success(config: config, recipient_vault: recipient_vault, deposit_order: deposit_order, transaction: transaction )

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong.'))
        end

        def update_transaction_table(config:, recipient_vault:, deposit_order:, transaction:)
          
          asset = Oblip::Repository::VaultAssets.find_code(
            recipient_vault.id,
            transaction.anchor_asset.code
          )

          transaction = transaction.new(
              sender_asset_balance: asset.balance,
              remarks: 'Deposited via Bank',
              status: Entity::Status::Transaction::COMPLETED
            )

          # updates table
          transaction = Oblip::Repository::Transactions.update(transaction)

          Success(config: config, recipient_vault: recipient_vault, 
            deposit_order: deposit_order, transaction: transaction)
        end

        def record_collected_fees(config:, recipient_vault:, deposit_order:, transaction:)
          transaction = transaction

          if transaction.anchor_fee > 0
            anchor_escrow = Repository::EscrowFunds
              .find_open_anchor_escrow_fund(transaction.anchor_asset.code)

            if !anchor_escrow
              escrow_entity = Entity::EscrowFund.new(
                id: nil,
                anchor_asset: transaction.anchor_asset,
                oblip_account: nil,
                funds_payable: transaction.anchor_fee.to_f,
                funds_collected: transaction.anchor_fee.to_f,
                balance: transaction.anchor_fee.to_f,
                belongs_to_anchor: true,
                is_locked: false,
                status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION
              )

              anchor_escrow = Repository::EscrowFunds.create(escrow_entity)
            else
              Repository::EscrowFunds.update_funds_payable(anchor_escrow.id,
                transaction.anchor_fee)
              Repository::EscrowFunds.update_funds_collected(anchor_escrow.id,
                transaction.anchor_fee)
              Repository::EscrowFunds.update_balance(anchor_escrow.id,
                transaction.anchor_fee)
            end


            # save anchor escrow in our transaction
            transaction = Repository::Transactions.save_anchor_escrow(transaction.id,
                anchor_escrow)
          end

          # no agent fee through a bank (Jan 21, 2019)

          deposit_order = deposit_order.new(transaction: transaction)

          Success(config: config, recipient_vault: recipient_vault, 
            deposit_order: deposit_order)
  
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
        end

        def update_vault(config:, recipient_vault:, deposit_order:)
          account_mapper = Horizon::AccountMapper.new(config)

          recipient_ledger_data = account_mapper
            .load(recipient_vault.address)

          # we should not add the initial cost in deposits

          # update vault and assets
          Repository::For[Entity::Vault]
            .add_or_update_vault_and_assets_using_raw(recipient_vault.id, recipient_ledger_data)

          # updates volume for anchor asset
          Repository::AnchorAssets.update_volume(deposit_order.transaction.anchor_asset.code,
            deposit_order.transaction.amount)

          Success(config: config, deposit_order: deposit_order)
          
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong.'))
        end

        def update_deposit_order(config:, deposit_order:)
          deposit_order = Oblip::Repository::DepositOrders
            .change_status(deposit_order.id, Oblip::Entity::DepositOrder::Status::COMPLETED)

          transaction = deposit_order.transaction
          if(ENV['RACK_ENV'] != 'test') # Don't send notifications when running tests
            
            fcm = FCM.new(config['FCM_SERVER_KEY'])

            recipient_clients = Repository::OblipAccounts
              .get_all_clients(transaction.recipient_account.id)

            recipient_fcm_tokens = recipient_clients.map {|x| x.fcm_token }
            puts recipient_fcm_tokens.to_s
            notif_title = 'Deposit Order'
            notif_message = "Your #{transaction.amount} #{transaction.anchor_asset.code} deposit order is complete."
            
            options = Entity::DeviceNotification.new(
              title: notif_title,
              message: notif_message,
              type: Entity::DeviceNotification::Types::TRANSACTION,
              data: { transaction_id: transaction.id, }
            )

            fcm.send(recipient_fcm_tokens, options.to_hash)
          end


          Success(Result.new(:ok, deposit_order))

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          Failure(Result.new(:internal_error, 'Something went wrong.'))
        end
      end
    end
  end
end