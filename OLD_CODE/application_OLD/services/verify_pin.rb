# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to verify pin from our database
  # Usage:
  #   result = VerifyPIN.call(account_id: xx, pin: xx)
  #   result.success?
  module VerifyPIN
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, pin:)
      raise OException::Unauthorized unless auth_account;
      result = Repository::For[Entity::OblipAccount]
                             .correct_pin?(auth_account['id'], pin)
      
      if result
        Success(Result.new(:ok, "PIN is valid"))
      else
        Failure(Result.new(:bad_request, "PIN is not valid"))
      end
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue AccessToken::ExpiredTokenError
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error
      Failure(Result.new(:internal_error, "PIN could not be validated. Try later."))
    end
  end
end
