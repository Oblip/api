# frozen_string_literal: true

require 'dry/transaction'


module Oblip
  # Service to create a bank order in our database
  # Usage:
  #   result = CreateDepositOrder.new.call(auth_account: (hash), config:, oblip_bank_id:, amount: )
  #   result.success? Returns OblipAccount
  class CreateWithdrawalOrder
    include Dry::Transaction

    step :build_entity
    step :create_transaction
    step :record_fees
    step :update_vault
    step :create_withdrawal_order

    def build_entity(config:, auth_account:, bank_account_id:,
      amount:, signed_tx_envelope:)

      raise OException::Unauthorized unless auth_account
      input = {}

      bank_account  = Repository::For[Entity::OblipAccountBank]
        .find_id(bank_account_id)

      raise OException::PolicyRestriction if bank_account.status.id != Entity::Status::OblipAccountBank::VERIFIED.id

      input[:sender_vault] = Repository::For[Entity::OblipAccount]
        .find_vault(auth_account['id'])

      input[:sender_type] = Entity::OblipAccount::Types.find(auth_account['type']['id'])
      input[:recipient_type] = Entity::OblipAccount::Types::ANCHOR
      
      anchor_asset = Repository::AnchorAssets.find_code(bank_account.bank.country.currency)
      input[:funding_pool] = Repository::BlockchainFunds.find_optimal_fund(
        Entity::BlockchainFund::SupportedAssets::XLM,
        0.1
      )

      input[:sender_vault_asset] = Repository::Vaults.find_vault_asset(input[:sender_vault].id,
        anchor_asset.code)

      raise OException::InsufficentFundingPool unless input[:funding_pool]

      tx_entity = Entity::Transaction.build(
        vault_source: input[:sender_vault],
        sender_type: input[:sender_type],
        recipient_type: input[:recipient_type],
        tx_data: {
          anchor_asset: anchor_asset,
          amount: amount,
          remarks: 'Withdrawal Via Bank',
          sender_account: Entity::OblipAccount.prebuild(auth_account['id']),
          recipient_account: nil,
          sender_asset_balance: input[:sender_vault_asset].balance,
          recipient_asset_balance: nil,
          vault_tx_hash: nil,
          signed_tx_envelope: signed_tx_envelope,
          tx_type: Entity::Transaction::Types::WITHDRAWAL,
          status: Entity::Status::Transaction::AWAITING_WITHDRAWAL
        }
      )
      
      # add the network fee and pool to the transaction entity
      tx_entity = Entity::Transaction.apply_funding(tx_entity, input[:funding_pool])
        
      # Checks to see if sender has enough balance to make this transaction
      check_sender_fund(tx_entity, input[:sender_type])

      input[:tx_entity] = tx_entity
      input[:bank_account] = bank_account
      input[:config] = config
      input[:auth_account] = auth_account
      
      
      Success(input)

    rescue OException::Unauthorized => error
      puts 'Not Authorized'
      Failure(Result.new(:not_found, "Resource not found"))
    rescue OException::InsufficentFundingPool 
      Failure(Result.new(:cannot_process, 'No available funding pool at the moment.'))
    rescue OException::PolicyRestriction
      Failure(Result.new(:bad_request, 'Not Allowed: Your bank account is not verified.'))
    rescue OException::UnderFunded
      Failure(Result.new(:cannot_process, 'Not enough funds to process this transaction.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    # checks if the sender's fund account has enough funds
    # TODO: review this again?
    def check_sender_fund (tx_entity, user_type)
      total = tx_entity.amount + (tx_entity.anchor_fee + tx_entity.agent_fee)
      sender_balance = tx_entity.sender_asset_balance
      raise OException::UnderFunded if sender_balance < total
    end

    def create_transaction(input)
      horizon = Horizon::Api.new(input[:config])

      tx_entity = input[:tx_entity]
      tx_hash = horizon.submit_payment_by(tx_entity.signed_tx_envelope)
      tx_entity = tx_entity.new(vault_tx_hash: tx_hash)
      
      # updates our blockchain fund pool; removing the tx network fee from balance
      Repository::BlockchainFunds.update_balance(input[:funding_pool].id,
        -(tx_entity.network_fee))

      input[:tx_result] = Repository::For[Entity::Transaction]
        .create(tx_entity)

      # TODO: send push to queue or something here (real-time data)

      input[:horizon] = horizon

      Success(input)
    rescue OException::InsufficientFee
      Failure(Result.new(:cannot_process, 'Insufficent vault balance for fee'))
    rescue Horizon::Api::Errors::UnderFunded
      Failure(Result.new(:cannot_process, 'Your fund acccount is underfunded.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end

    def record_fees(input)
      if input[:tx_result].anchor_fee > 0
        anchor_escrow = Repository::EscrowFunds
          .find_open_anchor_escrow_fund(input[:tx_result].anchor_asset.code)

        # we'll create a new escrow if an open one does not exist
        if !anchor_escrow
          escrow_entity = Entity::EscrowFund.new(
            id: nil,
            anchor_asset: input[:tx_result].anchor_asset,
            oblip_account: nil,
            funds_payable: input[:tx_result].anchor_fee.to_f,
            funds_collected: input[:tx_result].anchor_fee.to_f,
            balance: input[:tx_result].anchor_fee.to_f,
            belongs_to_anchor: true,
            is_locked: false,
            status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION
          )

          anchor_escrow = Repository::EscrowFunds.create(escrow_entity)
        else
          Repository::EscrowFunds.update_funds_payable(anchor_escrow.id,
            input[:tx_result].anchor_fee)
          Repository::EscrowFunds.update_funds_collected(anchor_escrow.id,
            input[:tx_result].anchor_fee)
          Repository::EscrowFunds.update_balance(anchor_escrow.id,
            input[:tx_result].anchor_fee)
        end

        # save anchor escrow in our transaction
        Repository::Transactions.save_anchor_escrow(input[:tx_result].id,
          anchor_escrow)
      end

      # NOTE: there are no agent fee via bank withdrawal. Maybe in the future
      # it would be an option.

      Success(input)
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end

    def update_vault(input)
      account_mapper = Horizon::AccountMapper.new(input[:config])
      
      sender_ledger_data = account_mapper
        .load(input[:sender_vault].address)

      amount_transacted = input[:tx_result].calculate_transaction(input[:sender_type],
        input[:recipient_type])
  

      if !input[:sender_vault].is_paid
        sender_ledger_data = sender_ledger_data.new(is_paid: true)
        input[:horizon].grant_master_access(input[:sender_vault].address)
      end
      
      Repository::For[Entity::Vault]
        .add_or_update_vault_and_assets_using_raw(input[:sender_vault].id, sender_ledger_data)
      
      Repository::AnchorAssets.update_volume(input[:tx_entity].anchor_asset.code,
        amount_transacted[:removed_amount])
        
      Success(input)
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end

    def create_withdrawal_order(input)
      order_entity = Entity::WithdrawalOrder.build(
        oblip_account_id: input[:auth_account]['id'],
        transaction: input[:tx_result],
        bank_account: input[:bank_account]
      )

      order_result = Repository::For[Entity::WithdrawalOrder]
        .create(order_entity)

      # TODO: send email to staff or notification or some shit here

      Success(Result.new(:created, order_result))
      
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.')) 
    end

  end
end
