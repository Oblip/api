# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to generate a transaction quote.
  # Usage:
  #   result = GenerateQuote.call(onfig:, auth_account:, asset_code:,
  #   amount:, recipient_account_id:, type_id:)
  #   result.success?
  module GenerateQuote
    extend Dry::Monads::Result::Mixin

    def self.call(config:, auth_account:, asset_code:,
      amount:, recipient_account_id:, type_id:)

      raise OException::Unauthorized unless auth_account

      oblip_types = Entity::OblipAccount::Types

      sender_vault = nil
      sender_type = nil
      sendber_fund = nil
      sender_account_id = nil

      recipient_type = nil
      recipient_vault = nil
      recipient_fund = nil
      
      recipient_info = recipient_account_id ? Repository::OblipAccounts.find_id(recipient_account_id) : nil
      recipient_type = recipient_info ? oblip_types.find(recipient_info.type.id) : nil

      from_account_address = nil
      to_account_address = nil

      horizon = Horizon::Api.new(config)
      
      # TODO: incapsulate this in a reusable function
      if type_id == Entity::Transaction::Types::DEPOSIT.id
        if auth_account['id'] == recipient_account_id
          sender_type = oblip_types::ANCHOR

          # recipient_fund = Repository::For[Entity::OblipAccount]
          #   .get_default_fund_account(recipient_account_id)
          recipient_account_id = recipient_account_id
          recipient_vault = Repository::For[Entity::OblipAccount]
            .find_vault(recipient_account_id)

            from_account_address = horizon.anchor.address
            to_account_address = recipient_vault.address

        elsif auth_account['type']['id'] == oblip_types::AGENT.id && recipient_account_id != auth_account['id']
          sender_type = oblip_types::AGENT
          # sender_fund = Repository::For[Entity::OblipAccount]
          #   .get_default_fund_account(auth_account['id'])
          sender_account_id = auth_account['id']
          sender_vault = Repository::For[Entity::OblipAccount]
            .find_vault(auth_account['id'])

          # recipient_fund = Repository::For[Entity::OblipAccount]
          #   .get_default_fund_account(recipient_account_id)
          
          recipient_vault = Repository::For[Entity::OblipAccount]
            .find_vault(recipient_account_id)

          from_account_address = sender_vault.address
          to_account_address = recipient_vault.address
        else
          raise OException::PolicyRestriction
        end

      elsif type_id == Entity::Transaction::Types::WITHDRAWAL.id

        # the recipient is an anchor here
        if recipient_account_id == nil 
          sender_type = oblip_types.find(auth_account['type']['id'])
          recipient_type = oblip_types::ANCHOR

          # sender_fund = Repository::For[Entity::OblipAccount]
          #   .get_default_fund_account(auth_account['id'])
          sender_vault = Repository::For[Entity::OblipAccount]
            .find_vault(auth_account['id'])

          sender_account_id = auth_account['id']

          from_account_address = sender_vault.address
          to_account_address = horizon.anchor.address

        elsif recipient_type == oblip_types::AGENT
          sender_type = oblip_types.find(auth_account['type']['id'])
        
          sender_account_id = auth_account['id']
          sender_vault = Repository::For[Entity::OblipAccount]
            .find_vault(auth_account['id'])

          recipient_vault = Repository::For[Entity::OblipAccount]
            .find_vault(recipient_account_id)

          from_account_address = sender_vault.address
          to_account_address = recipient_vault.address
        else
          raise OException::PolicyRestriction
        end
      elsif type_id == Entity::Transaction::Types::PAYMENT.id
        if recipient_account_id == nil
          raise OException::PolicyRestriction
        else
          recipient_vault = Repository::For[Entity::OblipAccount]
            .find_vault(recipient_account_id)
        end

        sender_type = oblip_types.find(auth_account['type']['id'])
        sender_account_id = auth_account['id']
        sender_vault = Repository::For[Entity::OblipAccount]
          .find_vault(auth_account['id'])

        from_account_address = sender_vault.address
        to_account_address = recipient_vault.address
      else
        raise OException::PolicyRestriction
      end
      
      anchor_asset = Repository::AnchorAssets.find_code(asset_code)
      transaction_type = Entity::Transaction::Types.find(type_id)

      requires_signature = true

      # check if we should require the user's signature
      if !sender_vault # because it's an achor for sure.
        requires_signature = false
      elsif !sender_vault.is_paid
        requires_signature = false
      end

   


      tx_entity = Entity::Transaction.build(
        vault_source: sender_vault,
        sender_type: sender_type,
        recipient_type: recipient_type,
        tx_data: {
          anchor_asset: anchor_asset,
          amount: amount,
          remarks: nil,
          requires_sender_signature: requires_signature,
          tx_type: transaction_type,
          sender_account: Entity::OblipAccount.prebuild(sender_account_id),
          recpient_account: Entity::OblipAccount.prebuild(recipient_account_id),
          recipient_asset_balance: nil,
          sender_asset_balance: nil,
          signed_tx_envelope: nil,
          vault_tx_hash: nil,
          status: nil
        }
      )

      anchor_fee =  tx_entity.anchor_fee
      agent_fee =  tx_entity.agent_fee

      amount_transacted = tx_entity.calculate_transaction(sender_type, recipient_type)
      
      unsigned_envelope = horizon.create_payment_envelope(
        asset_code: asset_code,
        amount: amount,
        fee: tx_entity.anchor_fee,
        should_sign: false,
        keys: {
          from_account: horizon.keypair_from_address(from_account_address),
          to_account: horizon.keypair_from_address(to_account_address)
        }
      )
 
      tx_entity = tx_entity.add_unsigned_tx_envelope(unsigned_envelope)
      
      Success(Result.new(:ok, tx_entity))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found')) # TODO: should it return 404?
    rescue OException::PolicyRestriction
      Failure(Result.new(:bad_request, 'Policy Restriction: not allowed.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong  generating a quote'))
    end
  end
end
