# frozen_string_literal: true

require 'dry/transaction'
require 'phonelib'

module Oblip
  # Service to create a save phone number in our database and
  # send verification code
  # Usage:
  #   result = SavePhoneNumber.new.call(config:,auth_account:, phone_number:)
  #   result.success?
  class SavePhoneNumber
    include Dry::Transaction

    step :save_phone_number
    step :send_sms_message

    def save_phone_number(config:, auth_account:, phone_number:)
      raise OException::Unauthorized unless auth_account;
      
      code_result = ShortCodeToken.create

      phone_number = Phonelib.parse(phone_number)

      account = Repository::For[Entity::OblipAccount]
                .save_phone_number(auth_account['id'], phone_number, code_result[:token])

      Success(config: config, account: account, phone_number: phone_number.to_s, code: code_result[:code])

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue AccessToken::InvalidTokenError
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue AccessToken::ExpiredTokenError
      # TODO; handle an expired token - make a new one and send it back?
    rescue Repository::OblipAccounts::Errors::InvalidValueError
      Failure(Result.new(:bad_request, 'The phone number you entered is invalid. Try again.'))
    rescue StandardError => error
      puts "Save Phone Number Error:cc #{error.message}"
      Failure(Result.new(:internal_error, "Could not save phone number. Try again later."))
    end

    def send_sms_message(config:, account:,phone_number:, code:)
      message = "Oblip verification code: #{code}. "
      message += 'You are binding this phone number to your Oblip account. '
      message += 'Valid for 20 minutes.'

      if(ENV['RACK_ENV'] != 'test')
        msg_result = SendSMSMessage.call(
          config: config,
          phone_number: phone_number,
          type: SMS::TYPE_TRANSACTIONAL,
          message: message
        )
      end

      Success(Result.new(:ok, account))

      rescue StandardError => error
        puts error
        Failure(Result.new(
          :internal_error, 
          'We could not send the verification code. Try agian.'
          ))
    end

  end
end
