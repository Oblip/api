# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all the company banks from our database
  # Usage:
  #   result = FindCompanyBankAccountscall(auth_account:)
  #   result.success?
  module FindCompanyBankAccounts
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      country_id = auth_account['country']['id']
      accounts = Repository::For[Entity::CompanyBankAccount]
        .find_many_by_country(country_id)
      
      Success(Result.new(:ok, CompanyBankAccounts.new(accounts)))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
