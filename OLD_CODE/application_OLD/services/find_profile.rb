 # frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all contacts of an account from our database
  # Usage:
  #   result = FindProfile.call(auth_account:, account_id:)
  #   result.success?
  module FindProfile
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, account_id:)
      raise OException::Unauthorized unless auth_account

      account= Repository::For[Entity::OblipAccount]
        .find_id(account_id)

      raise OException::NotFound unless account
      contact = Repository::For[Entity::Contact]
        .find_by_owner(auth_account['id'], account_id)

      profile = Profile.new(
        contact ? contact.id : nil, account)
      # TODO: figure out if we should add transactions here.
      
      Success(Result.new(:ok, profile))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::NotFound
      Failure(Result.new(:not_found, 'Could not find the profile.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong finding contacts.'))
    end
  end
end
