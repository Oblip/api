# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a personl details from our database
  # Usage:
  #   result = FindPersonalDetail.call(personal_detail_id)
  #   result.success?
  module FindPersonalDetail
    extend Dry::Monads::Result::Mixin

    def self.call(id)
      detail = Repository::For[Entity::PersonalDetail].find_id(id)

      if detail
        Success(Result.new(:ok, detail))
      else
        Failure(Result.new(:not_found, 'Could not find the personal details'))
      end
    end
  end
end
