# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find paginated oblip account escrow funds from database
  # Usage:
  #   result = FindOblipAccountEscrowFunds.call(auth_account:, offset:, limit:)
  #   result.success?
  module FindOblipAccountEscrowFunds
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, offset:, limit:)
      raise OException::Unauthorized unless auth_account
      
      results = Repository::EscrowFunds
        .find_all_account_escrow_funds(auth_account['id'], offset, limit)

      Success(Result.new(:ok, EscrowFunds.new(results)))
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end