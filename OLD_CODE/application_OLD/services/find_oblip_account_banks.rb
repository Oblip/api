# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a transaction in our database
  # Usage:
  #   result = FindOblipAccountBanks.call(auth_account:)
  #   result.success?
  module FindOblipAccountBanks
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      results = Repository::OblipAccounts
        .get_all_bank_accounts(auth_account['id'])

      # if(results.size > 0)
      #   policy = OblipAccountBankPolicy.new(auth_account, results[0])
      #   raise OException::Unauthorized unless policy.can_view?
      # end
      
      Success(Result.new(:ok, OblipAccountBanks.new(results)))
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
