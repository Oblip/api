# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to transaction between two users from our database
  # Usage:
  #   result = FindTransactionsBetweenAccounts.call(auth_account:, target_id:, offset:, limit:)
  #   result.success?
  module FindTransactionsBetweenAccounts
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, target_id:, offset:, limit:)
      raise OException::Unauthorized unless auth_account

      results = Repository::Transactions
        .find_between_oblip_accounts(
          first_id: auth_account['id'],
          second_id: target_id,
          offset: offset,
          limit: limit
        )
      raise OException::NotFound unless results

      Success(Result.new(:ok, Transactions.new(results)))

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::NotFound
      Failure(Result.new(:not_found, 'Unable to find fund account'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
