# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a banks from our database
  # Usage:
  #   result = FindBank.call(auth_account:, bank_id:)
  #   result.success?
  module FindBank
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, bank_id:)
      raise OException::Unauthorized unless auth_account

      bank = Repository::For[Entity::Bank]
        .find_id(bank_id)
      
      Success(Result.new(:ok, bank))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
