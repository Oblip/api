# frozen_string_literal: true

require 'dry-monads'
require 'http'

module Oblip
  # Service to send email verification
  # Usage:
  #   result = SendEmailVerification.call(account_data)
  #   result.success?
  # TODO: Can add the SENDGRID API to Infrastructure (Refactor?)
  module SendEmailVerification
    extend Dry::Monads::Result::Mixin

    def self.call(input)
      send_email(input)
      Success(Result.new(:ok, 'Email verification was successfully'))
    rescue StandardError => error
      puts error
      Failure(Result.new(:internal_error, 'Could not send verification email; please check email address'))
    end

    private_class_method
    def self.send_email(data)
      url = 'https://api.sendgrid.com/v3/mail/send'
      HTTP.auth(
        "Bearer #{data[:config].SENDGRID_KEY}"
      ).post(
        url,
        json: {
          personalizations: [{
            to: [{ 'email' => data[:email] }],
            substitutions: {
              "%name%": data[:name],
              "%confirmation_code%": data[:code]
            }
          }],
          from: { 'name' => 'Oblip Admin Notification', 'email' => 'noreply@oblip.com' },
          template_id: data[:config].EMAIL_VERIFICATION_TEMPLATE
        }
      )
      rescue StandardError => error
        puts error.message
        raise StandardError
    end
  end
end
