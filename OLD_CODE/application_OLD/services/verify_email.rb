# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to verify email from email token
  # Usage:
  #   result = VerifyEmail.call(auth_account: xx, email_token: xx)
  #   result.success?
  module VerifyEmail
    extend Dry::Monads::Result::Mixin
    # TODO: reuse the error message; duplication.
    def self.call(auth_account:, email_code:)
      raise OException::Unauthorized unless auth_account;

      token = Repository::For[Entity::OblipAccount]
              .get_email_verification_token(auth_account['id'])

      payload = ShortCodeToken.payload(token)

      if payload['code'] == email_code
        account = Repository::For[Entity::OblipAccount]
                  .change_verify_status(
                    auth_account['id'],
                    Repository::For[Entity::OblipAccount]::VERIFY_EMAIL_ADDRESS
                  )
        Success(Result.new(:ok, account))
      else
        Failure(Result.new(:bad_request, 'Code does not match. Try again.'))
      end

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue ShortCodeToken::InvalidTokenError
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue ShortCodeToken::ExpiredTokenError
      Failure(Result.new(:not_acceptable, 'Verification code has expired.'))
    rescue AccessToken::InvalidTokenError
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue AccessToken::ExpiredTokenError
      # TODO; handle an expired token - make a new one and send it back?
    rescue StandardError => error
      puts "At verify email error: #{error.message}"
      Failure(Result.new(:internal_error, 'Unable to verify email. Try again later.'))
    end
  end
end
