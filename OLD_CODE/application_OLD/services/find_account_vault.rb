# frozen_string_literal: true

require 'dry/transaction'

module Oblip
  # Service to find token vault by account
  # Usage:
  #   result = FindAccountVault.call(auth_account)
  #   result.success?
  module FindAccountVault
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account)
      raise OException::Unauthorized unless auth_account;

      vault = Repository::For[Entity::OblipAccount]
      .find_vault(auth_account['id'])

      if vault
        Success(Result.new(:ok, vault))
      else
        Failure(Result.new(:not_found, 'Could not find the account vault'))
      end

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, "Resource not found"))
    end
  end
end
