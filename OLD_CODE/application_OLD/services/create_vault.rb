# frozen_string_literal: true

require 'dry/transaction'


module Oblip
  # Service to create a OLP vault in our database
  # Usage:
  #   result = CreateVault.new.call(auth_account: (hash), config:, address:, salt:)
  #   result.success? Returns OblipAccount
  class CreateVault
    include Dry::Transaction

    step :create_stellar_account
    step :save_vault

    def create_stellar_account(auth_account:,address:, salt:,
      config:)
      raise OException::Unauthorized unless auth_account

      account_vault = Repository::For[Entity::OblipAccount]
        .find_vault(auth_account['id'])
        
      raise OException::DuplicationError if account_vault

      vault_entity = Entity::Vault.build_new(address, salt)
     
      fund_entity = Repository::BlockchainFunds.find_optimal_fund(
        Entity::BlockchainFund::SupportedAssets::XLM,
        vault_entity.initial_native_funds
      )
      
      raise OException::UnderFunded unless fund_entity
      # raise OException::UnderFunded unless fund_entity.fund_balance >= vault_entity.initial_native_funds

      vault_entity = Entity::Vault.init_funding(vault_entity, fund_entity)

      accountMapper = Horizon::AccountMapper.new(config)
      new_vault = accountMapper.create_account(vault_entity)

      # update blockchain funds
      Repository::BlockchainFunds.update_balance(fund_entity.id, 
        -(vault_entity.initial_operation_cost))
      
      Success(account_id: auth_account['id'], vault_data: new_vault)

    rescue OException::Unauthorized => error
     
      Failure(Result.new(:not_found, "Resource not found"))
    rescue OException::DuplicationError
      Failure(Result.new(:conflict, 'A vault for the account already exits.'))
    rescue OException::UnderFunded
      # TODO: email staff to notify them about no more funds
      Failure(Result.new(:cannot_process, 'Unable to create a vault account at the moment. Please try again later.'))
    rescue StandardError => error
      puts 'Create Vault StandardError:'
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Unable to create vault.'))
    end

    def save_vault(input)
      vault_data = input[:vault_data]
      vault = Repository::For[Entity::OblipAccount]
        .create_vault(input[:account_id], vault_data)

      # create asset records
      assets = vault_data.assets.map do |asset|
        asset = asset.new(vault_id: vault.id)
        Repository::For[Entity::VaultAsset]
          .create(asset)
      end

      vault = vault.new(assets: assets)
      Success(Result.new(:created, vault))

    rescue StandardError => error
      puts "Create vault error:"
      puts error.message
      puts error.inspect
      puts error.backtrace

      Failure(Result.new(:internal_error, error.message))
    end
  end
end
