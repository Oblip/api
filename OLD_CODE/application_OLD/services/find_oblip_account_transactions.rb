# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a transaction in our database
  # Usage:
  #   result = FindOblipAccountTransactions.call(auth_account:, offset:, limit:)
  #   result.success?
  # TODO: Add Policy Management to this.
  module FindOblipAccountTransactions
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, offset:, limit:)
      raise OException::Unauthorized unless auth_account

      results = Repository::For[Entity::Transaction]
        .find_from_oblip_account(
          oblip_account_id: auth_account['id'],
          offset: offset,
          limit: limit
        )

      if(results)
        Success(Result.new(:ok, Transactions.new(results)))
      else
        Failure(Result.new(:not_found, 'Unable to find transactions'))
      end

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
