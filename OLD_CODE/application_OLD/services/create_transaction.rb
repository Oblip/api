# frozen_string_literal: true

require 'dry/transaction'
require 'fcm'

module Oblip
  # Service to create a transaction in our database
  # Usage:
  #   result = CreateTransaction.new.call(config:, auth_account:, asset_code:,
  #     amount:, remarks:, recipient_account_id:, type_id:, signed_tx_envelope:)
  #   result.success?
  class CreateTransaction
    include Dry::Transaction

    step :build_entity
    # step :check_policies
    step :create_transaction
    step :record_fees
    step :update_vaults
   
    def build_entity(config:, auth_account:, asset_code:,
      amount:, remarks:, recipient_account_id:, type_id:, signed_tx_envelope:)
      raise OException::Unauthorized unless auth_account

      oblip_types = Entity::OblipAccount::Types
      input = {}

      input[:sender_account_id] = nil

      input[:sender_vault] = nil
      input[:sender_type] = nil
      input[:sender_fund] = nil

      input[:recipient_type] = nil
      input[:recipient_vault] = nil
      input[:recipient_fund] = nil

      recipient_info = recipient_account_id ? Repository::OblipAccounts.find_id(recipient_account_id) : nil
      input[:recipient_type] = recipient_info ? oblip_types.find(recipient_info.type.id) : nil
      

      input[:from_account_address] = nil
      input[:to_account_address] = nil

      # TODO: incapsulate this in a reusable function
      if type_id == Entity::Transaction::Types::DEPOSIT.id
       
        # only agents are allowed to make deposit through here
        if auth_account['type']['id'] == oblip_types::AGENT.id && recipient_account_id != auth_account['id']
          input[:sender_type] = oblip_types::AGENT
          input[:sender_account_id] = auth_account['id']
          input[:sender_vault] = Repository::For[Entity::OblipAccount]
            .find_vault(auth_account['id'])

          
          input[:recipient_vault] = Repository::For[Entity::OblipAccount].find_vault(recipient_account_id)

          input[:from_account_address] = input[:sender_vault].address
          input[:to_account_address] = input[:recipient_vault].address
        else
          raise OException::PolicyRestriction
        end
      elsif type_id == Entity::Transaction::Types::WITHDRAWAL.id
        if !recipient_account_id.nil? && input[:recipient_type] == oblip_types::AGENT
          input[:sender_type] = oblip_types.find(auth_account['type']['id'])
          input[:sender_account_id] = auth_account['id']
          input[:sender_vault] = Repository::For[Entity::OblipAccount]
            .find_vault(auth_account['id'])
          
          input[:recipient_vault] = Repository::For[Entity::OblipAccount]
            .find_vault(recipient_account_id)
          
          input[:from_account_address] = input[:sender_vault].address
          input[:to_account_address] = input[:recipient_vault].address
        else
          raise OException::PolicyRestriction
        end
      elsif type_id == Entity::Transaction::Types::PAYMENT.id
        if recipient_account_id == nil
          raise OException::PolicyRestriction
        else
          input[:recipient_vault] = Repository::For[Entity::OblipAccount]
            .find_vault(recipient_account_id)
        end

        input[:sender_type] = oblip_types.find(auth_account['type']['id'])
        input[:sender_account_id] = auth_account['id']
        input[:sender_vault] = Repository::For[Entity::OblipAccount]
          .find_vault(auth_account['id'])

        input[:from_account_address] = input[:sender_vault].address
        input[:to_account_address] = input[:recipient_vault].address

      else
        raise OException::PolicyRestriction
      end

      input[:anchor_asset] = Repository::AnchorAssets.find_code(asset_code)

      input[:funding_pool] = Repository::BlockchainFunds.find_optimal_fund(
        Entity::BlockchainFund::SupportedAssets::XLM,
        0.1
      )

      raise OException::InsufficentFundingPool unless input[:funding_pool]

      input[:sender_vault_asset] = Repository::Vaults.find_vault_asset(input[:sender_vault].id,
        asset_code)
      
      # puts "Sender Vault address : #{input[:sender_vault].address}"
      # puts "Recipient Vault address : #{input[:recipient_vault].address}"


      input[:recipient_vault_asset] = Repository::Vaults.find_vault_asset(input[:recipient_vault].id,
        asset_code)

      # TODO: handle the vault asset not existing
      # puts "Sender Account ID: #{input[:sender_account_id]}"
      # puts "Recipeint Account ID: #{recipient_account_id}"
      
      # puts "Recipeint Asset Code: #{input[:recipient_vault_asset].code}"

      # puts "Sender Asset Balance: #{input[:sender_vault_asset].balance}"
      # puts "Recipient Asset Balance: #{input[:sender_vault_asset].balance}"

      input[:tx_entity] = Entity::Transaction.build(
        vault_source: input[:sender_vault],
        sender_type: input[:sender_type],
        recipient_type: input[:recipient_type],
        tx_data:{
          anchor_asset:  input[:anchor_asset],
          amount: amount,
          remarks: remarks,
          requires_sender_signature: nil,
          tx_type: Entity::Transaction::Types.find(type_id),
          sender_account: Entity::OblipAccount.prebuild(auth_account['id']),
          sender_asset_balance: input[:sender_vault_asset].balance,
          recipient_asset_balance: input[:recipient_vault_asset].balance,
          recipient_account: Entity::OblipAccount.prebuild(recipient_account_id),
          signed_tx_envelope: signed_tx_envelope,
          vault_tx_hash: nil,
          status:  Entity::Status::Transaction::COMPLETED, # TODO: change this to processing later?
        }
      )

       # add the network fee and pool to the transaction entity
      input[:tx_entity] = Entity::Transaction.apply_funding(input[:tx_entity], input[:funding_pool])
      
      # Checks to see if sender has enough balance to make this transaction
      check_sender_fund(input[:tx_entity], input[:sender_type])

      input[:config] = config
      input[:auth_account] = auth_account

      Success(input)

    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::InsufficentFundingPool 
      puts 'No Funding Pool'
      Failure(Result.new(:cannot_process, 'No available funding pool at the moment.'))
    rescue OException::UnderFunded
      puts ' Not enough funds'
      Failure(Result.new(:cannot_process, 'Not enough funds to process transaction.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end

    # checks if the sender's fund account has enough funds
    # TODO: review this again?
    def check_sender_fund (tx_entity, sender_type)
      total = tx_entity.amount + (tx_entity.anchor_fee + tx_entity.agent_fee)
      sender_balance = tx_entity.sender_asset_balance
      raise OException::UnderFunded if sender_balance < total && sender_type != Entity::OblipAccount::Types::AGENT # agent
    end

    def check_policies(input)
    account = Repository::OblipAccounts.find_id(input[:auth_account]['id'])
    policies = TransactionPolicy.new(input[:auth_account],
      account.kyc_level, input[:tx_entity])

    total_amount_transacted = Repository::OblipAccounts
      .get_total_amount_transacted(input[:auth_account]['id'])
      
    # check all policies 
    # policies.can_create_transaction?(total_amount_transacted)

    Success(input)

    rescue OException::PolicyRestriction
      Failure(Result.new(:bad_request, 'Policy Restriction: The attempted transaction violates our policies. Make sure you have completed your account verification.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  
    def create_transaction(input)
      horizon = Horizon::Api.new(input[:config])
      
      tx_entity = input[:tx_entity]

      # creates blockchain transaction
      tx_hash = horizon.submit_payment_by(tx_entity.signed_tx_envelope)
      tx_entity = tx_entity.new(vault_tx_hash: tx_hash)

       # updates our blockchain fund pool; removing the tx network fee from balance
       Repository::BlockchainFunds.update_balance(input[:funding_pool].id,
        -(tx_entity.network_fee))

      input[:tx_result] = Repository::For[Entity::Transaction]
        .create(tx_entity)

      # TODO: send push to queue or something here (real-time data)
      
      input[:horizon] = horizon

      Success(input)

      # TODO: handle other exceptions from the blockchain here
    rescue OException::InsufficientFee
      Failure(Result.new(:cannot_process, 'Insufficent vault balance for fee'))
    rescue Horizon::Api::Errors::UnderFunded
      Failure(Result.new(:cannot_process, 'Your fund acccount is underfunded.'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end

    def record_fees(input)
      if input[:tx_result].anchor_fee > 0

        anchor_escrow = Repository::EscrowFunds
          .find_open_anchor_escrow_fund(input[:tx_result].anchor_asset.code)

        # we'll create a new escrow if an open one does not exist
        if !anchor_escrow
          escrow_entity = Entity::EscrowFund.new(
            id: nil,
            anchor_asset: input[:tx_result].anchor_asset,
            oblip_account: nil,
            funds_payable: input[:tx_result].anchor_fee.to_f,
            funds_collected: input[:tx_result].anchor_fee.to_f,
            balance: input[:tx_result].anchor_fee.to_f,
            belongs_to_anchor: true,
            is_locked: false,
            status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION
          )

          anchor_escrow = Repository::EscrowFunds.create(escrow_entity)
        else
          Repository::EscrowFunds.update_funds_payable(anchor_escrow.id,
            input[:tx_result].anchor_fee)
          Repository::EscrowFunds.update_funds_collected(anchor_escrow.id,
            input[:tx_result].anchor_fee)
          Repository::EscrowFunds.update_balance(anchor_escrow.id,
            input[:tx_result].anchor_fee)
        end

        # save anchor escrow in our transaction
        Repository::Transactions.save_anchor_escrow(input[:tx_result].id,
          anchor_escrow)

      end

      # there is agent fee only if the sender_account is type 'Agent'
      if input[:tx_result].agent_fee > 0

        agent_escrow = Repository::EscrowFunds
          .find_account_open_escrow_fund(input[:tx_result].sender_account.id,
            input[:tx_result].anchor_asset.code)
        
        if !agent_escrow
          escrow_entity = Entity::EscrowFund.new(
            id: nil,
            anchor_asset: input[:tx_result].anchor_asset,
            oblip_account: input[:tx_result].sender_account,
            funds_payable: input[:tx_result].agent_fee,
            funds_collected: 0.0, # no change
            balance: 0.0, # no change
            belongs_to_anchor: false,
            is_locked: false,
            status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION
          )

          agent_escrow = Repository::EscrowFunds.create(escrow_entity)
        else
          Repository::EscrowFunds.update_funds_payable(agent_escrow.id,
            input[:tx_result].anchor_fee)
        end
        
        # save anchor escrow in our transaction
        Repository::Transactions.save_agent_escrow(input[:tx_result].id,
          agent_escrow)
  
      end

      Success(input)
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end


    def update_vaults (input)

      # TODO: refactor? Reccurent ? 
      sender_vault = input[:sender_vault]
      recipient_vault = input[:recipient_vault]
      account_mapper = Horizon::AccountMapper.new(input[:config])
      
      sender_ledger_data = account_mapper
        .load(sender_vault.address)

      recipient_ledger_data = account_mapper
        .load(recipient_vault.address)


      # we assume that the anchor fee includes the paid amount or else it wouldn't have
      # reached this far; so we update it.
      if !sender_vault.is_paid
        # sender_vault = sender_vault.new(is_paid: true)
        sender_ledger_data = sender_ledger_data.new(is_paid: true)
        
        # TODO: send this to a queue for a worker to process :p 
        
        input[:horizon].grant_master_access(sender_vault.address)
        
      end

      Repository::For[Entity::Vault]
        .add_or_update_vault_and_assets_using_raw(sender_vault.id, sender_ledger_data)
      Repository::For[Entity::Vault]
        .add_or_update_vault_and_assets_using_raw(recipient_vault.id, recipient_ledger_data)

      tx_entity = input[:tx_result]

      if(ENV['RACK_ENV'] != 'test') # Don't send notifications when running tests
        # Send Notification here
        # TODO: send to a worker to do this notification handling
        sender_account = input[:tx_result].sender_account
        sender_details = sender_account.personal_details

        fcm = FCM.new(input[:config]['FCM_SERVER_KEY'])

        recipient_clients= Repository::For[Entity::OblipAccount]
          .get_all_clients(tx_entity.recipient_account.id)

        recipient_fcm_tokens = recipient_clients.map {|x| x.fcm_token }

        notif_title = 'Payment'
        notif_message = "#{sender_details.first_name} sent you "
        notif_message += "#{tx_entity.amount} "
        notif_message += "#{tx_entity.anchor_asset.code}"


        options = Entity::DeviceNotification.new(
          title: notif_title,
          message: notif_message,
          type: Entity::DeviceNotification::Types::TRANSACTION,
          data: JSON.parse(TransactionRepresenter.new(tx_entity).to_json)
        )

        fcm.send(recipient_fcm_tokens , options.to_hash)
      end

      Success(Result.new(:created, tx_entity))

    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end

  end
end
