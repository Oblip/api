# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a withdrawal orders by oblip account from our database
  # Usage:
  #   result = FindWithdrawalOrdersByAccount.call(auth_account:)
  # type = 0 returns all
  #   result.success?
  module FindWithdrawalOrdersByAccount
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:)
      raise OException::Unauthorized unless auth_account

      # TODO: add deposit policy here (only owners & admin can find this order)
      orders = Repository::For[Entity::OblipAccount]
        .get_all_withdrawal_orders(auth_account['id'])

      Success(Result.new(:ok, WithdrawalOrders.new(orders)))

    rescue OException::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
