# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to verify and refresh access token
  # Usage:
  #   result = RefreshAccessToken.call(config, signature:, ip_address:, data:)
  #   result.success?
  module RefreshAccessToken2
    extend Dry::Monads::Result::Mixin

    def self.call(signature:, ip_address:, data:)
      raise OException::Unauthorized unless data || signature
      raise OException::Unauthorized unless data['refresh_token']

      refresh_token = data['refresh_token']
      
      # extract data from refresh_token
      refresh_data = refresh_token.split('.')
      raise OException::Unauthorized unless refresh_data.size == 2

      client_id = refresh_data[0]
      secret_code = refresh_data[1]

      # check if client_id and secret code match and if client is verified
      client = Repository::ApiClients
        .is_credentials_valid?(client_id, secret_code)

      raise OException::Unauthorized unless client

      # check if the signature is authentic
      request_verifier = VerifyRequest.new(client.public_key)
      signed_refresh_token = request_verifier.parse(signature, data.to_json)

      # check when was the last time access token was issued; refuse if <= 30 minutes
      # expires = client.last_used + (Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW - (2 *60))
      # puts "Last Used: #{client.last_used}"
      # puts "Expires On: #{expires}"
      # current_time = Time.now

      # puts "Current Time: #{current_time}"
     
      # raise OException::PolicyRestriction if current_time.to_i < expires.to_i
      
      # get the information of the oblip account
      oblip_account = Repository::OblipAccounts.find_id(client.oblip_account_id)
       
      # update client's last seen
      Repository::ApiClients.update_last_seen(client.id, ip_address)

      # create access token
      access_data = Entity::AccessMetaData.create(oblip_account)

      Success(Result.new(:created, access_data))

    rescue VerifyRequest::SignatureVerificationFailed
      # TODO: handle this, email someone?
      # TODO: unverify client. Ask for phone verifiaction in the app or email verification for staff

      Failure(Result.new(:unauthorized, "Resource not found"))
    rescue OException::Unauthorized => error
      # TODO: handle if client is mobile, email someone?
       # TODO: unverify client. Ask for phone verifiaction in the app or email verification for staff

      Failure(Result.new(:unauthorized, 'Resource not found'))
    rescue OException::PolicyRestriction
      # TODO: log this shit somewhere :p
       # TODO: unverify client. Ask for phone verifiaction in the app or email verification for staff

      Failure(Result.new(:bad_request, 'This is not allowed and has been logged!'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
