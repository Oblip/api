# frozen_string_literal: true

require 'dry/transaction'

module Oblip
  # Service to find transaction by id
  # Usage:
  #   result = FindTransaction.call(auth_account:, tx_id:)
  #   result.success?
  module FindTransaction
    extend Dry::Monads::Result::Mixin

    def self.call(auth_account:, tx_id:)
      raise OException::Unauthorized unless auth_account

      tx = Repository::For[Entity::Transaction]
        .find_id(tx_id)

      raise OException::NotFound unless tx

      # TODO: add transaction viwable policy here

      Success(Result.new(:ok, tx))
  
    rescue OException::Unauthorized => error
      Failure(Result.new(:not_found, 'Resource not found'))
    rescue OException::NotFound
      Failure(Result.new(:not_found, 'Could not find transaction'))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
