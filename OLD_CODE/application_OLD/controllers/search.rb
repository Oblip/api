# frozen_string_literal: true

module Oblip
  # Web API for Account Secret Phrases
  class Api < Roda
    plugin :all_verbs

    route('search') do |routing|
      routing.on('contacts') do
        routing.on String do |phone_number|
          routing.is do
            routing.get do
              result = FindContactByPhoneNumber.call(
                auth_account: @auth_account,
                phone_number: phone_number
              )
              represent_response(result, ContactSearchResultRepresenter)
            end
          end
        end
      end
    end
  end
end
