# frozen_string_literal: true

module Oblip
  # Web API
  class Api < Roda
    plugin :all_verbs

    route('oblip_account') do |routing|
      routing.is do
        routing.get do
          find_result = FindOblipAccount.call(@auth_account)
          represent_response(find_result, OblipAccountRepresenter)

        rescue StandardError => error
          puts error.message
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, error.message)
          )
          response.status = http_response.http_code
          http_response.to_json
          
        end
        # POST - Create a new Account
        routing.post do
          req_body = JSON.parse(routing.body.read)
          signature = req_body['signature']
          data = req_body['data']
          ip_address = routing.env['REMOTE_ADDR']
          
          client_id = req_body['client_id']

          new_account = CreateOblipAccount.new
            .call(
              client_id: client_id,
              data: data,
              ip_address: ip_address,
              signature: signature
            )
          
          represent_response(new_account, AuthTokensRepresenter) 

        rescue Sequel::MassAssignmentRestriction
          http_response = HttpResponseRepresenter.new(
            Result.new(:forbidden, 'This has been logged and we have been informed of your actions')
          )
          response.status = http_response.http_code
          http_response.to_json

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong. Try again later.' )
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end # END OF /oblip_account
    end
  end
end
