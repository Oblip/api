# frozen_string_literal: true

module Oblip
  # Sends an SMS OTP code to a supplied phone number
  class Api < Roda
    plugin :all_verbs
    route('sms_manager') do |routing|
      routing.on('send_verification_code') do
        routing.is do
          routing.post do
            req_body = JSON.parse(routing.body.read)
            signature = req_body['signature']
            data = req_body['data']
            client_id = req_body['client_id']

            result = SendPhoneVerification.call(
              config: Api.config,
              client_id: client_id,
              signature: signature,
              data: data
            )

            result.value!.message = ''
  
            represent_response(result, nil)
            
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
            
          end
        end
      end
    end
  end
end
