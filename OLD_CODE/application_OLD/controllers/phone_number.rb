# frozen_string_literal: true

module Oblip
  # Web API for Personal Details
  class Api < Roda
    plugin :all_verbs

    route('oblip_account/phone_number') do |routing|
      routing.is do
        # GET - retrieve
        routing.get do
          raise OException::Unauthorized unless @auth_account;
          account_result = FindOblipAccount.call(@auth_account['id'])
          phone_number = account_result.value!.message.phone_number
          country = account_result.value!.message.personal_details.country
          PhoneNumberRepresenter.new(
            PhoneNumber.new(phone_number, country)
          ).to_json
        rescue OException::Unauthorized => error
          http_response = HttpResponseRepresenter.new(
            Result.new(:not_found, 'Resource not found.')
          )
          response.status = http_response.http_code
          http_response.to_json
        rescue StandardError => error
          puts error.message
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, error.message)
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        # PUT - Update
        routing.put do
          data = JSON.parse(routing.body.read)

          result = SavePhoneNumber.new.call(
            config: Api.config,
            auth_account: @auth_account,
            phone_number: data['phone_number']
          )

          represent_response(result, OblipAccountRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
