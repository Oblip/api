# frozen_string_literal: true

module Oblip
  # Web API
  class Api < Roda
    plugin :all_verbs

    route('anchor_assets') do |routing|
      routing.is do
        routing.get do
          result = FindAnchorAssets.call(auth_account: @auth_account)
          
          represent_response(result, AnchorAssetsRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
