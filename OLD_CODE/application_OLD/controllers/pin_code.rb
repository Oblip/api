# frozen_string_literal: true

module Oblip
  # Web API for Account PIN code
  class Api < Roda
    plugin :all_verbs

    route('oblip_account/pin_code') do |routing|
      routing.is do
        # PUT - Update
        # TODO; should I use POST instead of PUT?
        routing.put do
          data = JSON.parse(routing.body.read)
          result = SaveSecurePIN.call(
            auth_account: @auth_account,
            pin: data['pin_code']
          )
          
          http_response = HttpResponseRepresenter.new(result.value)
          response.status = http_response.http_code
          http_response.to_json
          
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
