# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs
    
    route('escrow_funds') do |routing|
      routing.is do
        routing.get do
          params = routing.params # offset & limit
          escrow_scope = EscrowFundPolicy::AccountScope.new(@auth_account, params)

          results = escrow_scope.all_viewable

          represent_response(results, EscrowFundsRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      #{API_ROOT}/escrow_funds/:id
      routing.on String do |id|
        routing.is do
          routing.get do
            result = FindEscrowFund.call(
              auth_account: @auth_account,
              escrow_fund_id: id
            )

            represent_response(result, EscrowFundRepresenter)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
  end
end
