# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs
    
    route('withdrawals') do |routing|
      routing.is do
        routing.get do

          results = FindWithdrawalOrdersByAccount.call(
            auth_account: @auth_account,
          )

          represent_response(results, WithdrawalOrdersRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          data = JSON.parse(routing.body.read)

          result = CreateWithdrawalOrder.new.call(
            config: Api.config,
            auth_account: @auth_account,
            bank_account_id: data['bank_account_id'],
            amount: data['amount'],
            signed_tx_envelope: data['signed_tx_envelope']
          )

          represent_response(result, WithdrawalOrderRepresenter)

        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      #{API_ROOT}/withdrawals/:id
      routing.on String do |id|
        routing.get do
          parameters = routing.params

          result = FindWithdrawal.call( # TODO
            auth_account: @auth_account,
            id: id
          )

          represent_response(result, BankOrderRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
