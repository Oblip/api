# frozen_string_literal: true

require 'roda'
require_relative 'route_helpers'
require_relative '../exception_errors.rb'

module Oblip
  # Web API
  class Api < Roda
    plugin :halt
    plugin :all_verbs
    plugin :multi_route
    plugin :request_headers

    # TODO: Include other controllers here
    # require_relative 'sms_manager.rb'
    # require_relative 'oblip_account.rb'
    # require_relative 'account_verification.rb'
    # require_relative 'personal_details.rb'
    # require_relative 'phone_number.rb'
    # require_relative 'pin_code.rb'
    # require_relative 'secret_phrases.rb'
    # require_relative 'vault.rb'
    # require_relative 'contacts.rb'
    # require_relative 'transactions.rb'
    # require_relative 'quote.rb'
    # require_relative 'search.rb'
    # require_relative 'profile.rb'
    # require_relative 'anchor_assets.rb'
    # require_relative 'topup.rb'
    # require_relative 'api_clients.rb'
    # require_relative 'banks.rb'
    # require_relative 'oblip_account_banks.rb'
    # require_relative 'deposits'
    # require_relative 'company_bank_accounts'
    # require_relative 'withdrawals'
    # require_relative 'token'

    # # staff
    # require_relative './staff/staff_accounts'
    # require_relative './staff/auth.rb'
   

    def secure_request?(routing)
      routing.scheme.casecmp(Api.config.SECURE_SCHEME).zero?
    end

    # TODO: revise this later
    def authenticated_account(routing)
      return nil unless routing.headers['AUTHORIZATION']
      scheme, auth_token = routing.headers['AUTHORIZATION'].split(' ')
      account_payload = AccessToken.payload(auth_token)
      scheme.match?(/^Bearer$/i) ? JSON.parse(account_payload) : nil

    rescue AccessToken::InvalidTokenError
      nil
    rescue AccessToken::ExpiredTokenError
      nil
    end

    route do |routing|
      response['Content-Type'] = 'application/json'
      secure_request?(routing) ||
        routing.halt(403, { message: 'TLS/SSL Requested' }.to_json)

      @auth_account = authenticated_account(routing)


      # GET / request
      routing.root do
        message = "Oblip API v1 up in #{Api.environment} mode"
        HttpResponseRepresenter.new(Result.new(:ok, message)).to_json
      end

      routing.on 'api' do
        # /api/v1 branch
        routing.on 'v1' do
          @api_root = '/api/v1'
          routing.multi_route
        end
      end
    end
  end
end
