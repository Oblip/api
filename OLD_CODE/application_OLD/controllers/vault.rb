# frozen_string_literal: true

module Oblip
  # Web API for Account Secret Phrases
  class Api < Roda
    plugin :all_verbs

    route('vault') do |routing|
      routing.is do
        routing.get do
          result = FindAccountVault.call(@auth_account)
          represent_response(result, VaultRepresenter)

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        routing.post do
          data = JSON.parse(routing.body.read)

          result = CreateVault.new.call(
            auth_account: @auth_account,
            address: data['address'],
            salt: data['salt'],
            config: Api.config
          )
          
          represent_response(result, VaultRepresenter)

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      # /v1/api/vault/assets
      routing.on('assets') do
        routing.is do
          routing.get do

            result = FindAllVaultAssets.call(
              auth_account: @auth_account
            )

            represent_response(result, VaultAssetsRepresenter)

          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end

          routing.post do
            # TODO
          end
        end
      end
      
      # /v1/api/vault/authorize_asset
      routing.on('authorize_asset') do
        routing.post do
          data = JSON.parse(routing.body.read)

          result = AuthorizeAsset.call(
            auth_account: @auth_account,
            asset_code: data['asset_code'],
            config: Api.config
          )
          
          represent_response(result, VaultAssetRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

    end
  end
end
