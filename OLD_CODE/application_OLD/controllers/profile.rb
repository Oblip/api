# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    route('profile') do |routing|
      #{API_ROOT}/profile/:id
      routing.on String do |id|
        routing.is do
          routing.get do
            result = FindProfile.call(
              auth_account: @auth_account,
              account_id: id
            )
            
            represent_response(result, ProfileRepresenter)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
   
        #{API_ROOT}/profile/:id/transactions
        routing.on('transactions') do
          routing.get do
            parameters = parameters = routing.params
            result = FindTransactionsBetweenAccounts.call(
              auth_account: @auth_account,
              target_id: id,
              offset: parameters['offset'],
              limit: parameters['limit']
            )

            represent_response(result, TransactionsRepresenter)

          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
  end
end
