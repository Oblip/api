# frozen_string_literal: true

module Oblip
  # Web API
  class Api < Roda
    plugin :all_verbs

    route('quote') do |routing|
      routing.is do
        routing.post do
          data = JSON.parse(routing.body.read)
          # data = routing.params

          result = GenerateQuote.call(
            config: Api.config,
            auth_account: @auth_account,
            asset_code: data['asset_code'],
            amount: data['amount'],
            recipient_account_id: data['recipient_account_id'],
            type_id: data['type_id']
          )

          represent_response(result, QuoteRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
