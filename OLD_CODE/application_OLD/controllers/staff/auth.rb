# frozen_string_literal: true

module Oblip
  # Web API for Personal Details
  class Api < Roda
    plugin :all_verbs

    route('staff_accounts/auth') do |routing|
      routing.is do
        routing.post do
          body = JSON.parse(routing.body.read)

          result = Service::Staff::Authenticate.new.call(
            config: Api.config,
            data: body['data'],
            signature: body['signature']
          )

          represent_response(result, Representer::Staff::AuthAccountRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

    end
  end
end
