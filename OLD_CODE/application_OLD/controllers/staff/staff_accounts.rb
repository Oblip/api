# frozen_string_literal: true

module Oblip
  # Web API
  class Api < Roda
    plugin :all_verbs

    route('staff_accounts') do |routing|
      routing.is do
        routing.get do
         
          # TODO
          

        rescue StandardError => error
          puts error.message
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, error.message)
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        # POST - Create a new Account
        routing.post do
          
          # TODO

        rescue Sequel::MassAssignmentRestriction
          http_response = HttpResponseRepresenter.new(
            Result.new(:forbidden, 'This has been logged and we have been informed of your actions')
          )
          response.status = http_response.http_code
          http_response.to_json

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong. Try again later.' )
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end # END OF /staff_accounts
    end
  end
end
