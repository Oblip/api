# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    route('clients') do |routing|
      #{API_ROOT}/devices
      routing.is do 
        routing.get do
          # TODO: GET a list of clients (need auth)
        end

        routing.post do
          # TODO
        end

      end

      routing.on('mobile') do
        routing.is do
          routing.post do
            data = JSON.parse(routing.body.read)

            ip_address = data['ip_address']
            ip_address = routing.env['REMOTE_ADDR'] if ip_address.nil?

            result = RegisterMobileClient.call(
              config: Api.config,
              data: {
                public_key: data['public_key'],
                type: Oblip::Entity::ApiClient::Types::USER.id,
                platform_type: data['platform_type'],
                fcm_token: data['fcm_token'],
                ip_address: ip_address
              }
            )
            
            represent_response(result, BasicApiClientRepresenter)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
        
        #{API_ROOT}/clients/mobile/:client_id
        routing.on String do |client_id|
          routing.is do
            routing.get do
              result = FindAccountApiClient.call(
                auth_account: @auth_account,
                client_id: client_id
              )
              
              represent_response(result, ApiClientRepresenter)
            rescue StandardError => error
              puts error.inspect
              puts error.backtrace
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something does not seem right.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end

            routing.patch do
              ip_address = routing.env['REMOTE_ADDR']
              
              result = UpdateClientByAccount.call(
                auth_account: @auth_account,
                client_id: client_id,
                ip_address: ip_address
              )

              represent_response(result, ApiClientRepresenter)
            rescue StandardError => error
              puts error.inspect
              puts error.backtrace
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something does not seem right.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end

          routing.on('verify') do
            routing.is do
              routing.post do
                req_body = JSON.parse(routing.body.read)
                
                data = req_body['data']
                signature = req_body['signature']

                result = VerifyPhoneNumber.call(
                  client_id: client_id,
                  data: data,
                  signature: signature
                )

                represent_response(result, BasicApiClientRepresenter)
              rescue StandardError => error
                puts error.inspect
                puts error.backtrace
                http_response = HttpResponseRepresenter.new(
                  Result.new(:internal_error, 'Something does not seem right.')
                )
                response.status = http_response.http_code
                http_response.to_json
              end
            end
          end
        end
      end
    end
  end
end
