# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    route('banks') do |routing|
      routing.is do
        routing.get do
          results = FindBanksByCountry.call(
            auth_account: @auth_account
          )
          
          represent_response(results, BanksRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      #{API_ROOT}/banks/:id
      routing.on String do |id|
        routing.get do

          result = FindBank.call(
            auth_account: @auth_account,
            bank_id: id
          )

          represent_response(result, BankRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
