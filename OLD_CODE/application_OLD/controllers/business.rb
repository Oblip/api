# frozen_string_literal: true

module Oblip
  # Web API for Business Details
  class Api < Roda
    plugin :all_verbs

    route('oblip_account/business') do |routing|
      routing.is do
        routing.get do
          
          result = FindOblipAccountBusiness.call(
            auth_account: @auth_account
          )
  
          represent_response(result, BusinessRepresenter)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
        end

        routing.post do
          data = JSON.parse(routing.body.read)

          result = CreateBusiness.new.call(
            config: Api.config,
            auth_account: @auth_account,
            data: {
              brand_name: data['brand_name'],
              registration_name: data['registration_name'],
              address: data['address'],
              logo_base64: data['logo_base64']
            }
          )

          represent_response(result, BusinessRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

      end
    end
  end
end
