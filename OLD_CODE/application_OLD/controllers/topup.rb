# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    route('topup') do |routing|
      #{API_ROOT}/topup
      routing.is do 
        routing.post do
          data = JSON.parse(routing.body.read)

          result = TopUpAccount.new.call(
            config: Api.config,
            auth_account: @auth_account,
            recipient_id: data['recipient_id'],
            amount: data['amount']
          )
          
          represent_response(result, TransactionRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
