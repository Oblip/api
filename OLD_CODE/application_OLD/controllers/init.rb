# frozen_string_literal: false

require_relative 'app.rb'
require_relative "staff/init.rb"

Dir.glob("#{File.dirname(__FILE__)}/**/*.rb").each do |file|
  require file
end