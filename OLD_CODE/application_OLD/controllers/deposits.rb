# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs
    
    route('deposits') do |routing|
      routing.is do
        routing.get do
          params = routing.params # offset & limit

          deposit_scope = DepositPolicy::AccountScope.new(@auth_account, params)

          results = deposit_scope.all_viewable
          
          represent_response(results, DepositOrdersRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          data = JSON.parse(routing.body.read)

          result = CreateDepositOrder.call(
            config: Api.config,
            auth_account: @auth_account,
            company_bank_id: data['company_bank_id'],
            amount: data['amount']
          )

          represent_response(result, DepositOrderRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      #{API_ROOT}/deposits/:id
      routing.on String do |id|
        routing.is do
          routing.get do
            parameters = routing.params
  
            result = FindDepositOrder.call(
              auth_account: @auth_account,
              deposit_order_id: id
            )
  
            represent_response(result, DepositOrderRepresenter)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end

        routing.on('status') do

          routing.is do
            # GET {API_ROOT}/deposits/:id/status
            routing.get do
              # TODO: should return the status of an order
            end
          end

          routing.on('confirm') do
            routing.post do

              result = ConfirmDepositOrder.call(
                auth_account: @auth_account,
                deposit_order_id: id
              )

              represent_response(result, DepositOrderRepresenter)

            rescue StandardError => error
              puts error.inspect
              puts error.backtrace
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something does not seem right.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end

          routing.on('approve') do
            routing.post do
  
              result = Service::Staff::ApproveDepositOrder.new.call(
                config: Api.config,
                staff_auth_account: @auth_account,
                deposit_order_id: id
              )
  
              represent_response(result, DepositOrderRepresenter)
            rescue StandardError => error
              puts error.inspect
              puts error.backtrace
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something does not seem right.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end
        end
      end
    end
  end
end
