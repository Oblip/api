# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    route('businesses') do |routing|
      routing.is do
        routing.get do
          params = routing.params # offset & limit

          results = FindBusinesses.call(
            auth_account: @auth_account,
            offset: params['offset'],
            limit: params['limit']
          )

          represent_response(results, BusinessesRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
