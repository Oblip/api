# frozen_string_literal: true

module Oblip
  # Web API
  class Api < Roda
    plugin :all_verbs

    route('account_verification') do |routing|
      # api/v1/account_verification/email
      routing.on('email') do 
        routing.is do
          routing.post do
            
            data = JSON.parse(routing.body.read)

            result = VerifyEmail.call(
              auth_account: @auth_account,
              email_code: data['email_code']
            )

            represent_response(result, OblipAccountRepresenter)

          rescue StandardError => error
            puts "Error in by_email route: "
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end # END of by_email route

      # api/v1/account_verification/phone_number
      routing.on('phone_number') do 
        routing.is do
          routing.post do
            raise OException::Unauthorized unless @auth_account;
            data = JSON.parse(routing.body.read)
            result = VerifyPhoneNumber.call(
              account_id: @auth_account['id'],
              sms_code: data['sms_code']
            )
            represent_response(result, OblipAccountRepresenter)

            # Exception Handling 

          rescue OException::Unauthorized => error
            
            http_response = HttpResponseRepresenter.new(
              Result.new(:not_found, 'Resource not found.')
            )
            response.status = http_response.http_code
            http_response.to_json
          rescue AccessToken::InvalidTokenError
            http_response = HttpResponseRepresenter.new(
              Result.new(:not_found, 'Resource not found.')
            )
            response.status = http_response.http_code
            http_response.to_json
          rescue StandardError => error
            puts "Error in by_email route: "
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end # END of phone verification

    end
  end
end