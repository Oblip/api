# frozen_string_literal: true

module Oblip
  # Web API for Personal Details
  class Api < Roda
    plugin :all_verbs

    route('oblip_account/personal_details') do |routing|
      routing.is do
        # GET - retrieve
        routing.get do
          raise OException::Unauthorized unless @auth_account;
          result = FindPersonalDetailsByAccount.call(@auth_account['id'])
          
          represent_response(result, PersonalDetailRepresenter)
        rescue OException::Unauthorized => error
          
          http_response = HttpResponseRepresenter.new(
            Result.new(:not_found, 'Resource not found.')
          )
          response.status = http_response.http_code
          http_response.to_json
        rescue AccessToken::InvalidTokenError
          http_response = HttpResponseRepresenter.new(
            Result.new(:not_found, 'Resource not found.')
          )
          response.status = http_response.http_code
          http_response.to_json
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        # PUT - Update
        routing.put do
          raise OException::Unauthorized unless @auth_account;
          data = JSON.parse(routing.body.read)

          result = UpdatePersonalDetailByAccount.call(
            auth_account: @auth_account,
            hash_data: data
          )

          represent_response(result, PersonalDetailRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
      # oblip_account/personal_details/account_photo
      routing.on('account_photo') do
        routing.is do
          routing.get do
            # TODO: get the photo_url from the database

          end
          routing.post do
            raise OException::Unauthorized unless @auth_account;
            data = JSON.parse(routing.body.read)

            result = SaveAccountPhoto.call(
              auth_account: @auth_account,
              base64_file: data['base64_file']
            )

            represent_response(result, PersonalDetailRepresenter)
            
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end

    end
  end
end
