# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    route('contacts') do |routing|
      routing.is do
        routing.get do 
          
          result = FindContacts.call(
            auth_account: @auth_account
          )
          
          represent_response(result, ContactsRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          data = JSON.parse(routing.body.read)

          result = AddContact.call(
            auth_account: @auth_account,
            contact_account_id: data['contact_account_id']
          )
          
          represent_response(result, ContactRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      #{API_ROOT}/contacts/:id
      routing.on String do |id|
        routing.delete do

          result = DeleteContact.call(
            auth_account: @auth_account,
            contact_id: id
          )

          represent_response(result, ContactRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
