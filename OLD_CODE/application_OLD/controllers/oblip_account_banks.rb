# frozen_string_literal: true

module Oblip
  # Web API for Personal Details
  class Api < Roda
    plugin :all_verbs

    route('oblip_account/banks') do |routing|
      routing.is do
        # GET - retrieve
        routing.get do
          results = FindOblipAccountBanks.call(
            auth_account: @auth_account
          )
          
          represent_response(results, OblipAccountBanksRepresenter)
        
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        # POST
        routing.post do
          data = JSON.parse(routing.body.read)

          result = AddOblipAccountBank.call(
            auth_account: @auth_account,
            bank_id: data['bank_id'],
            account_number: data['account_number'],
          )

          represent_response(result, OblipAccountBankRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
      
      #{API_ROOT}/oblip_account/banks/:id
      routing.on String do |id|
        routing.is do
          routing.get do
            # TODO
          end
          routing.put do
             # TODO
          end
        end
      end

    end
  end
end
