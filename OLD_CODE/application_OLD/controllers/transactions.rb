# frozen_string_literal: true

module Oblip
  # Web API
  class Api < Roda
    plugin :all_verbs

    route('transactions') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params

          result = FindOblipAccountTransactions.call(
            auth_account: @auth_account,
            offset: parameters['offset'],
            limit: parameters['limit']
          )
          
          represent_response(result, TransactionsRepresenter)

        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          data = JSON.parse(routing.body.read)

          result = CreateTransaction.new.call(
            config: Api.config,
            auth_account: @auth_account,
            asset_code: data['asset_code'], 
            amount: data['amount'],
            remarks: data['remarks'],
            recipient_account_id: data['recipient_account_id'],
            type_id: data['type_id'],
            signed_tx_envelope: data['signed_tx_envelope']
          )

          represent_response(result, TransactionRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      routing.on String do |tx_id|
        routing.is do
          routing.get do
          
            find_result = FindTransaction.call(
              auth_account: @auth_account,
              tx_id: tx_id
            )
  
            represent_response(find_result, TransactionRepresenter)
  
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end

        routing.on('deposit_order') do

          result = FindTransactionDepositOrder.call(
            auth_account: @auth_account,
            transaction_id: tx_id
          )

          represent_response(result, DepositOrderRepresenter)
        rescue StandardError => error
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something does not seem right.')
          )
          response.status = http_response.http_code
          http_response.to_json

        end
      end

    end
  end
end
