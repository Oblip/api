# Module with exception errors
module Oblip
  module OException
    class Unauthorized < StandardError; end
    class NotFound < StandardError; end
    class DuplicationError < StandardError; end
    class MissingParameterError < StandardError; end
    class InvalidValueError < StandardError; end
    # class UnavailableError < StandardError; end
    class UnderFunded < StandardError; end
    class InsufficientFee < StandardError; end
    class InsufficentFundingPool < StandardError; end
    class PolicyRestriction < StandardError; end
  end
end
