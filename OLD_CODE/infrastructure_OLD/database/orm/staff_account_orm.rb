# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Personal Details
    class StaffAccountOrm < Sequel::Model(:staff_accounts)

      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id      
      # plugin :whitelist_security
      # set_allowed_columns :first_name, :middle_name, :last_name, :gender,
      #                     :birthday, :country_id, :photo_url     
      
      def password=(new_password)
        self.salt = SecureDB.new_salt
        self.password_hash = SecureDB.hash_password(salt, new_password)
      end

      def password?(try_password)
        try_hashed = SecureDB.hash_password(salt, try_password)
        try_hashed == password_hash
      end

      def permissions=(text)
        self.permissions_secure = SecureDB.encrypt(text)
      end

      def permissions
        SecureDB.decrypt(permissions_secure)
      end

      def otp_secret=(text)
        self.otp_secret_secure = SecureDB.encrypt(text)
      end

      def otp_secret
        SecureDB.decrypt(otp_secret_secure)
      end
     
      def email_address
        SecureDB.decrypt(email_address_secure)
      end

      def email_address=(text)
        self.email_address_secure = SecureDB.encrypt(text)
      end

      def first_name
        SecureDB.decrypt(first_name_secure)
      end

      def first_name=(text)
        self.first_name_secure = SecureDB.encrypt(text)
      end

      def middle_name
        SecureDB.decrypt(self.middle_name_secure)
      end

      def middle_name=(text)
        self.middle_name_secure = SecureDB.encrypt(text)
      end

      def last_name
        SecureDB.decrypt(last_name_secure)
      end

      def last_name=(text)
        self.last_name_secure = SecureDB.encrypt(text)
      end

      def photo_url
        SecureDB.decrypt(photo_url_secure)
      end

      def photo_url=(text)
        self.photo_url_secure = SecureDB.encrypt(text)
      end
    end
  end
end
