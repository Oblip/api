# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Businesses
    class BusinessOrm < Sequel::Model(:businesses)
      plugin :uuid, field: :id
      plugin :timestamps, update_on_create: true
    end
  end
end
