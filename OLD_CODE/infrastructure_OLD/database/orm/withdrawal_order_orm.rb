# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class WithdrawalOrderOrm < Sequel::Model(:withdrawal_orders)  
      plugin :uuid, field: :id    
      plugin :timestamps, update_on_create: true
    end
  end
end
