# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Assets
    class VaultAssetOrm < Sequel::Model(:vault_assets)  
      plugin :uuid, field: :id    
      plugin :timestamps, update_on_create: true
    end
  end
end
