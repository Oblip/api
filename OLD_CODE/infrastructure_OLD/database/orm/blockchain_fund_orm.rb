# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class BlockchainFundOrm < Sequel::Model(:blockchain_funds)  
      plugin :timestamps, update_on_create: false
    end
  end
end
