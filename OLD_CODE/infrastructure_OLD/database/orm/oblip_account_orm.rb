# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Accounts
    class OblipAccountOrm < Sequel::Model(:oblip_accounts)
      one_to_one    :country,
                    class: :'Oblip::Database::CountryOrm'
        
      one_to_many   :contacts,
                    class: :'Oblip::Database::ContactOrm',
                    key: :owner_id

      one_to_many   :clients,
                    class: :'Oblip::Database::ApiClientOrm',
                    key: :oblip_account_id

      one_to_many   :banks,
                    class: :'Oblip::Database::OblipAccountBankOrm',
                    key: :oblip_account_id
      
      one_to_many   :locations,
                    class: :'Oblip::Database::OblipLocationOrm'

      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :is_phone_verified,
                          :phone_number, :status, :business_id,
                          :type, :personal_detail_id,
                          :base_anchor_asset_code,
                          :kyc_level, :vault_id

      def phone_number
        SecureDB.decrypt(phone_number_secure)
      end

      def phone_number=(text)
        self.phone_number_secure = SecureDB.encrypt(text)
        self.phone_number_hash = SecureDB.hash_content(text)
      end
    end
  end
end
