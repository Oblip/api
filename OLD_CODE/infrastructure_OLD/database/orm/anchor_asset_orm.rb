# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Anchor Assets
    class AnchorAssetOrm < Sequel::Model(:anchor_assets)
    end
  end
end
