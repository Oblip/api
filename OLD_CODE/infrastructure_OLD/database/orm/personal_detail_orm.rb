# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Personal Details
    class PersonalDetailOrm < Sequel::Model(:personal_details)
      one_to_one  :country,
                  class: :'Oblip::Database::AccountOrm'

      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id      
      plugin :whitelist_security
      set_allowed_columns :first_name, :middle_name, :last_name, :gender,
                          :birthday, :country_id, :photo_url      
      def first_name
        SecureDB.decrypt(first_name_secure)
      end

      def first_name=(text)
        self.first_name_secure = SecureDB.encrypt(text)
      end

      def middle_name
        SecureDB.decrypt(self.middle_name_secure)
      end

      def middle_name=(text)
        self.middle_name_secure = SecureDB.encrypt(text)
      end

      def last_name
        SecureDB.decrypt(last_name_secure)
      end

      def last_name=(text)
        self.last_name_secure = SecureDB.encrypt(text)
      end

      def gender
        SecureDB.decrypt(self.gender_secure)
      end

      def gender=(text)
        self.gender_secure = SecureDB.encrypt(text)
      end

      def photo_url
        SecureDB.decrypt(photo_url_secure)
      end

      def photo_url=(text)
        self.photo_url_secure = SecureDB.encrypt(text)
      end

      def birthday
        SecureDB.decrypt(birthday_secure)
      end

      def birthday=(date)
        self.birthday_secure = SecureDB.encrypt(date)
      end
    end
  end
end
