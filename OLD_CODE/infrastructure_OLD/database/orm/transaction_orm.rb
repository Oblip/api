# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transactions
    class TransactionOrm < Sequel::Model(:transactions)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
    end
  end
end
