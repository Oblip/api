# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class CompanyBankAccountOrm < Sequel::Model(:company_bank_accounts)
      plugin :timestamps, update_on_create: true
    end
  end
end
