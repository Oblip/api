# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Payouts
    class PayoutOrm < Sequel::Model(:payouts)  
      plugin :uuid, field: :id    
      plugin :timestamps, update_on_create: true
    end
  end
end
