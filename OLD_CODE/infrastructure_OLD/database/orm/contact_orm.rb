# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class ContactOrm < Sequel::Model(:contacts)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
    end
  end
end
