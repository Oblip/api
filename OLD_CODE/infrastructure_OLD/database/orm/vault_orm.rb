# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Document Types
    class VaultOrm < Sequel::Model(:vaults)

      one_to_many :assets,
                  class: :'Oblip::Database::VaultAssetOrm',
                  key: :vault_id
      
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :address, :salt,:blockchain_source_fund_id,
                          :sequence_number, :initial_native_funds,
                          :initial_operation_cost, :is_paid


      def address
          SecureDB.decrypt(address_secure)
      end

      def address=(text)
        self.address_secure = SecureDB.encrypt(text)
      end

      # def seed
      #   SecureDB.decrypt(seed_secure)
      # end

      # def seed=(text)
      #   self.seed_secure = SecureDB.encrypt(text)
      # end      
    end
  end
end
