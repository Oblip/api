# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Statuses
    class StatusOrm < Sequel::Model(:statuses)
    end
  end
end
