# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Api Clients
    class ApiClientOrm < Sequel::Model(:api_clients)

      plugin :timestamps, update_on_create: true

      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :oblip_account_id,:secret_code, :expiry_date,
                          :platform_type, :public_key, :is_verified,
                          :fcm_token, :status, :type, :verification_token,
                          :last_used, :last_ip_address

      
      # def is_verification_code_valid?(code)
      #   code ? SecureDB.hash_content(code) == self.verification_code_hash : nil
      # end

      # def verification_token
      #   self.verification_code_hash
      # end
                        
      # def verification_code=(code)
      #   self.verification_code_hash == code ? SecureDB.hash_content(code) :nil
      # end
              
      def is_secret_code_valid?(code)
        code ? SecureDB.hash_content(code) == self.secret_code_hash : nil
      end

      def secret_code
        self.secret_code_hash
      end

      def secret_code=(code)
        self.secret_code_hash = code ? SecureDB.hash_content(code) : nil
      end 

      # def last_used
      #   # self.last_used.strftime("%F %I:%M %p")
      #   self.last_used
      # end

      # def last_used=(date)
      #   self.last_used = date
      # end

      def platform_type
        SecureDB.decrypt(platform_type_secure)
      end

      def platform_type=(text)
        self.platform_type_secure = SecureDB.encrypt(text)
      end

      def public_key
        SecureDB.decrypt(public_key_secure)
      end

      def public_key=(text)
        self.public_key_secure = SecureDB.encrypt(text)
      end

      def fcm_token
        SecureDB.decrypt(fcm_token_secure)
      end

      def fcm_token=(token)
        self.fcm_token_secure = SecureDB.encrypt(token)
      end

      def device_token=(text)
        self.device_fm_secure = SecureDB.encrypt(text)
      end

      def last_ip_address
        SecureDB.decrypt(last_ip_address_secure)
      end

      def last_ip_address=(text)
        self.last_ip_address_secure = SecureDB.encrypt(text)
      end    
    end
  end
end
