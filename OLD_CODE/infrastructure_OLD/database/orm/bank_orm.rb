# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class BankOrm < Sequel::Model(:banks)
      plugin :timestamps, update_on_create: true
    end
  end
end
