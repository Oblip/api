# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class OblipAccountBankOrm < Sequel::Model(:oblip_account_banks)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
    end
  end
end
