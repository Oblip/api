# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transaction Types
    class OblipLocationOrm < Sequel::Model(:oblip_locations)
      
    end
  end
end
