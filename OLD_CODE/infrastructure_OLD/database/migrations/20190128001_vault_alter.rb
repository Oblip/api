
# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:vaults) do
      add_foreign_key :blockchain_source_fund_id, :blockchain_funds
    end
  end
end
