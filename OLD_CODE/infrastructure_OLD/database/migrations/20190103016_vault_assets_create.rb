# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:vault_assets) do
      uuid :id, primary_key: true, unique: true
      uuid :vault_id, foreign_key: true, table: :vaults
      String :name
      String :code
      Float :balance
      Integer :type
      Bool :authorized
      String :issuer

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
