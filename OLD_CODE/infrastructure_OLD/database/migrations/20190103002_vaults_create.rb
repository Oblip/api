# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:vaults) do
      uuid :id, primary_key: true

      String :address_secure, unique: true, null: false
      String :salt
      String :sequence_number, null: false
      Float :initial_native_funds
      Float :initial_operation_cost
      Bool :is_paid, default: false

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
