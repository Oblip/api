# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:api_clients) do
      uuid :id, primary_key: true, unique: true
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts
      String :secret_code_hash
      # DateTime :expiry_date
      String :platform_type_secure
      String :public_key_secure
      String :fcm_token_secure
      String :verification_token
      Bool :is_verified, default: false
      Integer :type
      Integer :status
      String :last_ip_address_secure
      Time :last_used
      Time :created_at
      Time :updated_at
    end
  end
end
