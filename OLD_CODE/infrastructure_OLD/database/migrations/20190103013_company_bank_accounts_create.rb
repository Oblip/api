# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:company_bank_accounts) do
      primary_key :id
      foreign_key :bank_id, table: :banks

      String :account_name
      String :account_number
      Integer :status, default: 1
    
      DateTime :created_at
      DateTime :updated_at
    end

    seed_file = File.read('infrastructure/database/initializations/company_bank_account_inits.yml')
    data = YAML.safe_load(seed_file)

    data.each do |account|
      from(:company_bank_accounts).insert(account)
    end
    
  end
end
