# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:statuses) do
      primary_key :id
      String :code, null: false, unique: true
      String :name, null: false
      String :description
    end
    
    seed_file = File.read('infrastructure/database/initializations/status_inits.yml')
    data = YAML.safe_load(seed_file)

    data.each do |status|
      from(:statuses).insert(status)
    end
  end
end
