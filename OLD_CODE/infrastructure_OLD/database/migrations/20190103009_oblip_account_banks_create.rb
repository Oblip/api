# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:oblip_account_banks) do
      uuid :id, primary_key: true, unique: true
      foreign_key :bank_id, table: :banks
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts

      # String :account_name
      String :account_number
      Integer :status
    
      DateTime :created_at
      DateTime :updated_at
    end
    
  end
end
