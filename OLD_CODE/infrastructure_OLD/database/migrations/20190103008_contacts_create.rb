# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:contacts) do
      uuid :id, primary_key: true, unique: true
      uuid :owner_id, foreign_key: true, table: :oblip_accounts
      uuid :contact_id, foreign_key: true, table: :oblip_accounts

      DateTime :created_at
      DateTime :updated_at
    end
    
  end
end
