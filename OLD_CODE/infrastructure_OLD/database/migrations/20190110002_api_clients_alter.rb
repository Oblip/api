# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:api_clients) do
      set_column_default :verification_attempts, 0
    end
  end
end
