# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:businesses) do
      uuid :id, primary_key: true, unique: true
      String :brand_name
      String :registration_name
      String :address
      String :logo_url

      Bool :is_verified, default: false
      
      uuid :oblip_account_id

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
