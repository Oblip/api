# frozen_string_literal: true

require 'sequel'
require 'yaml'

Sequel.migration do
  change do
    create_table(:anchor_assets) do
      primary_key :id
      String :name, unique: true, null: false
      String :code
      String :symbol, null: true
      String :issuer, null: false
      Bool :is_tethered, default: true
      Float :current_exchange_rate, null: true
      Float :volume

    end
    
    seed_file = File.read('infrastructure/database/initializations/anchor_asset_inits.yml')
    data = YAML.safe_load(seed_file)

    data.each do |currency|
      
      currency['issuer'] = Oblip::Api.config['ANCHOR_ACCOUNT_ID']

      from(:anchor_assets).insert(currency)
    end

  end
end
