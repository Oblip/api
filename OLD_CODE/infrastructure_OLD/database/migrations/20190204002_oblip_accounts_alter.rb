
# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:oblip_accounts) do
      add_column :business_id, :uuid
      add_foreign_key [:business_id], :businesses
      # set_column_allow_null :business_id
      # set_column_type :business_id, String
    end
  end
end
