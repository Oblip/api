# frozen_string_literal: true

require 'sequel'
require 'yaml'

Sequel.migration do
  change do
    create_table(:countries) do
      primary_key :id

      String :name, unique: true, null: false
      String :code, unique: true, null: false
      String :phone_code, unique: true, null: false
      String :currency, unique: true
      Bool :allow_cross_border_trade, default: false
    end
    
    seed_file = File.read('infrastructure/database/initializations/country_inits.yml')
    data = YAML.safe_load(seed_file)

    data.each do |country|
      from(:countries).insert(country)
    end

  end
end
