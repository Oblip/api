# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:deposit_orders) do
      uuid :id, primary_key: true, unique: true
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts
      uuid :transaction_id, foreign_key: true, table: :transactions
      Integer :company_bank_account_id, 
           foreign_key: true, table: :company_bank_accounts

      Integer :deposit_method # bank = 1; etc..

      Integer :status

      DateTime :created_at
      DateTime :updated_at
    end
    
  end
end
