# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:staff_accounts) do
      uuid :id, primary_key: true, unique: true
      String :username, unique: true
      String :email_address_secure

      String :password_hash
      String :salt
      String :otp_secret_secure
      Bool :email_verified, default: false


      String :first_name_secure
      String :middle_name_secure
      String :last_name_secure
      String :photo_url_secure

      Integer :type
      Integer :status
      String :permissions_secure


      DateTime :created_at
      DateTime :updated_at
    end
  end
end
