# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:blockchain_funds) do
      primary_key :id

      Float :amount_purchased
      Float :balance
      Float :price_purchased
      Float :incurred_fees
      String :asset_name

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
