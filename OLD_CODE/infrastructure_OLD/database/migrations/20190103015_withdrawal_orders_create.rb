# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:withdrawal_orders) do
      uuid :id, primary_key: true, unique: true
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts
      uuid :transaction_id, foreign_key: true, table: :transactions
      uuid :oblip_account_bank_id, 
           foreign_key: true, table: :oblip_account_banks

      Integer :status

      DateTime :created_at
      DateTime :updated_at
    end
    
  end
end
