# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:personal_details) do
      uuid :id, primary_key: true, unique: true
      foreign_key :country_id, table: :countries

      String :first_name_secure, null: false
      String :middle_name_secure
      String :last_name_secure, null: false
      String :gender_secure, null: true
      String :birthday_secure
      String :photo_url_secure, unique: true

      DateTime :updated_at
    end
  end
end
