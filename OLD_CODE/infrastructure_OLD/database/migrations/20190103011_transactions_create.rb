# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:transactions) do
      uuid :id, primary_key: true, unique: true
     
      String :anchor_asset_code, null: false
      Float :amount, null: true
      Float :anchor_fee, null: true
      Float :agent_fee, null: true
      Float :network_fee, null: true

      # Removed FK because of failing constraints
      Integer :network_fee_funding_id, null: true # references blockchain_funds
      uuid :anchor_escrow_fund_id, null: true
      uuid :agent_escrow_fund_id,  null: true
      
      Float :exchange_rate

      # Removed FK because of failing constraints
      uuid :sender_account_id,  null: true
      uuid :recipient_account_id, null: true
      
      Float :sender_asset_balance, null: true
      Float :recipient_asset_balance, null: true
      
      Integer :type
      String :remarks, null: true
      String :signed_tx_envelope, null: true

      Integer :status

      String :vault_tx_hash, null: true

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
