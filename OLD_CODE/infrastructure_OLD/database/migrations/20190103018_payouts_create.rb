# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:payouts) do
      uuid :id, primary_key: true, unique: true
      uuid :tx_deposit_id, foreign_key: true, table: :transactions, null: true
      uuid :escrow_fund_id, foreign_key: true, table: :escrow_funds
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts, null: true
      Float :amount
      String :anchor_asset_code
      Integer :type
      Integer :status
      
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
