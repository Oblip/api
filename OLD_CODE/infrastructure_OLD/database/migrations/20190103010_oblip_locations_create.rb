# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:oblip_locations) do
      primary_key :id
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts

      String :name
      String :description
      String :address

      Float :lat
      Float :lng
    
      DateTime :created_at
      DateTime :updated_at
    end
    
  end
end
