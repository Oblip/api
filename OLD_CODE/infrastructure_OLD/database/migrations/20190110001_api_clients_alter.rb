# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:api_clients) do
      add_column :last_attempted_verification, Time
      add_column :verification_attempts, Integer
    end
  end
end
