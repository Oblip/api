# frozen_string_literal: true

require 'sequel'
require 'yaml'

Sequel.migration do
  change do
    create_table(:banks) do
      primary_key :id
      String :name, null: false, unique: true
      String :company_name
      String :address, null: true
      String :logo_url
      String :swift_code, null: true
      String :routing_number, null: true
      foreign_key :country_id, table: :countries
      
      DateTime :created_at
      DateTime :updated_at
    end

    
    seed_file = File.read('infrastructure/database/initializations/bank_inits.yml')
    data = YAML.safe_load(seed_file)

    data.each do |bank|
      from(:banks).insert(bank)
    end
    
  end
end
