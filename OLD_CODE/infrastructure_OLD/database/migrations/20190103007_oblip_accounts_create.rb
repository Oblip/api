# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:oblip_accounts) do
      uuid :id, primary_key: true, unique: true

      uuid :vault_id, foreign_key: true, table: :vaults, null: true
      Integer :status, null: false
      uuid :personal_detail_id, foreign_key: true, table: :personal_details, null: true
      String :base_anchor_asset_code, null: false

      Bool :is_phone_verified, default: false
      String :phone_number_secure, unique: true, null: true
      String :phone_number_hash, unique: true

      Integer :kyc_level, default: 1

      Integer :type, default: 1
      
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
