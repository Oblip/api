# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:escrow_funds) do
      uuid :id, primary_key: true, unique: true
      String :anchor_asset_code
      uuid :oblip_account_id, foreign_key: true, table: :oblip_accounts, null: true
      Float :funds_payable, default: 0
      Float :funds_collected, default: 0
      Float :balance, default: 0

      Bool :belongs_to_anchor, default: false
      Bool :is_locked, default: false
      Integer :status

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
