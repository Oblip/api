# frozen_string_literal: false

require 'rest-client'
require 'stellar-base'

module Oblip
  module Horizon
    class Api
      module Errors
        BadSequence = Class.new(StandardError)
        UnderFunded = Class.new(StandardError)
        InsufficientFee = Class.new(StandardError)
      end

      # OBLIP_CURRENCY_CODE = 'OLP'

      attr_reader :anchor, :credit_issuer

      def initialize(config)
        @network_url = config['STELLAR_API_URL']

        # signers
        @credit_issuer = keypair_from_seed(config['CREDIT_ISSUER_SECRET'])
        @mama_secret_keypair = keypair_from_seed(config['MAMA_SECRET'])
        @baba_secret_keypair = keypair_from_seed(config['BABA_SECRET'])

        # account references
        @zhenfu_account_keypair = keypair_from_address(config['ZHENFU_ACCOUNT_ID'])
        @anchor = keypair_from_address(config['ANCHOR_ACCOUNT_ID'])
        @fuel_account_keypair = keypair_from_address(config['FUEL_ACCOUNT_ID'])

        # our assets
        @main_asset_code = config['MAIN_ASSET_CODE']
      end

      def keypair_from_seed(seed)
        Stellar::KeyPair.from_seed(seed)
      end

      def keypair_from_address(address)
        Stellar::KeyPair.from_address(address)
      end

      def account_data(address)
        get_account_data(keypair_from_address(address))
      end

      # Creates a Stellar Transaction XDR envelope for a payment operation
      #
      # @param attributes [String] asset_code: 
      # @param attributes [Float] :amount (Required)
      # @param attributes [Float] :fee (Required)
      # @param attributes [Bool] should_sign:, should the envelope be signed?
      # @param keys: attributes [Stellar::KeyPair] from_account: (Required)
      # @param keys: attributes [Stellar::KeyPair] to_account: (Required)
      # @param keys: attributes [Stellar::KeyPair] signer: (Optional)
      def create_payment_envelope(asset_code:, amount:, fee:, should_sign:, keys:)
        sender_next_sequence = sequence_from_account(keys[:from_account]) + 1
        repayment_next_sequence = sequence_from_account(@fuel_account_keypair) + 1
        
        base_stoop = 0.0000001
        num_ops = 2
        num_ops += 1 if fee > 0

        ledger_fee = 100 * num_ops

        main_tx = Stellar::Transaction.payment({
          account: keys[:from_account],
          destination: keys[:to_account],
          fee: ledger_fee,
          sequence: sender_next_sequence,
          amount: [:alphanum4, asset_code, @anchor, amount]
        })

        repayment_tx = Stellar::Transaction.payment({
          account: @fuel_account_keypair,
          destination: keys[:from_account],
          fee: ledger_fee,
          sequence: repayment_next_sequence,
          amount: [:native, ledger_fee * base_stoop]
        })

        fee_tx = Stellar::Transaction.payment({
          account: keys[:from_account],
          destination: @zhenfu_account_keypair,
          fee: ledger_fee,
          sequence: sender_next_sequence,
          amount: [:alphanum4, asset_code, @anchor, fee]
        })

        total_tx = repayment_tx.merge(main_tx)
        total_tx = total_tx.merge(fee_tx) if fee > 0
        
        
        if should_sign
          signers = [@mama_secret_keypair]
          if keys[:from_account] == @anchor
            signers.push(@credit_issuer)
          else
            signers.push(@baba_secret_keypair, keys[:signer])
          end
          total_tx.to_envelope(*signers).to_xdr(:base64)
        else
          total_tx.to_envelope(@mama_secret_keypair, @baba_secret_keypair).to_xdr(:base64)
        end
      end

      def submit_payment_by(signed_tx_envelope)
        tx_response = submit_transaction_operation(signed_tx_envelope)
        tx_hash = tx_response['hash']
        tx_hash

      rescue Errors::InsufficientFee
        raise OException::InsufficientFee
      end

      # Simulates the config on mobile (for testing)
      def config_new_account(asset_code, master_account, signer_account)
        current_sequence = sequence_from_account(master_account)
        allow_trust = Stellar::Transaction.change_trust({
          account:    master_account,
          sequence:  current_sequence + 1,
          fee: 300,
          line: Stellar::Asset.alphanum4(asset_code,  @anchor)
        })

        # initially we want baba to have full control of the account
        baba_signer_tx = Stellar::Transaction.set_options({
          account: master_account,
          sequence:  current_sequence + 1,
          fee: 300,
          signer: Stellar::Signer.new(key: Stellar::SignerKey.new(:signer_key_type_ed25519, @baba_secret_keypair.raw_public_key), weight: 5)
        })

        # setup all configs and revoke access from the master account
        init_config_tx = Stellar::Transaction.set_options({
          account: master_account,
          sequence:  current_sequence + 1,
          fee: 300,
          master_weight: 1,
          high_threshold: 5,
          med_threshold: 3,
          low_threshold: 2,
          signer: Stellar::Signer.new(key: Stellar::SignerKey.new(:signer_key_type_ed25519, signer_account.raw_public_key), weight: 2)
        })

        xdr_envelope = allow_trust.merge(baba_signer_tx).merge(init_config_tx).to_envelope(master_account).to_xdr(:base64)
        tx_response = submit_transaction_operation(xdr_envelope)
        tx_hash = tx_response['hash']
        tx_hash
      end

      ##
      ## Set's master weight to 5
      ##
      def grant_master_access(address)
        account = keypair_from_address(address)
        current_sequence = sequence_from_account(account)
        num_ops = 3
        base_stoop = 0.0000001

        grant_tx = Stellar::Transaction.set_options({
          account: account,
          fee: (num_ops - 1) * 100,
          sequence:  current_sequence + 1, 
          master_weight: 5,
          # signer: Stellar::Signer.new(key: Stellar::SignerKey.new(:signer_key_type_ed25519, @baba_secret_keypair.raw_public_key), weight: 2)
        })

        repayment_tx = Stellar::Transaction.payment({
          account:     @fuel_account_keypair,
          destination: account,
          fee: (num_ops - 1) * 100,
          sequence:    current_sequence + 1,
          amount:    [:native, (num_ops * 100) * base_stoop]
        })

        xdr_envelope = grant_tx.merge(repayment_tx).to_envelope(@baba_secret_keypair, @mama_secret_keypair)
          .to_xdr(:base64)

        tx_response = submit_transaction_operation(xdr_envelope)
        tx_hash = tx_response['hash']

        # remove the master access to baba seperately
        another_sequence = sequence_from_account(account)
        remove_access = Stellar::Transaction.set_options({
          account: account,
          fee: 100,
          sequence: another_sequence  + 1, 
          signer: Stellar::Signer.new(key: Stellar::SignerKey.new(:signer_key_type_ed25519, @baba_secret_keypair.raw_public_key), weight: 2)
        })

        remove_access_xdr = remove_access.to_envelope(@baba_secret_keypair).to_xdr(:base64)
        another_tx = submit_transaction_operation(remove_access_xdr)

        tx_hash
      end

      def allow_trust(address, asset_code)
        account = keypair_from_address(address)
        current_sequence = sequence_from_account(@anchor)

        # num_ops = asset_code == @main_asset_code ? 3 : 2
        num_ops = 2
        base_stoop = 0.0000001

        tx = Stellar::Transaction.allow_trust({
          trustor: account,
          account: @anchor,
          asset: [:alphanum4, asset_code, @anchor],
          authorize: true,
          fee: num_ops * 100,
          sequence:  current_sequence + 1, 
        })

        repayment_tx = Stellar::Transaction.payment({
          account:     @fuel_account_keypair,
          destination: @anchor,
          fee: num_ops * 100,
          sequence:    current_sequence + 1,
          amount:    [:native, (num_ops * 100) * base_stoop]
        })

        xdr_envelope = nil

        xdr_envelope = tx.merge(repayment_tx).to_envelope(@baba_secret_keypair, @mama_secret_keypair)
          .to_xdr(:base64)

        
        tx_response = submit_transaction_operation(xdr_envelope)
        tx_hash = tx_response['hash']
        tx_hash

        # TODO: add exception handling here
      end

      def trust_issuer(account)
        account_current_sequence = sequence_from_account(account)
        tx = Stellar::Transaction.change_trust({
          account:    account,
          sequence:  account_current_sequence + 1,
          line: Stellar::Asset.alphanum4(@main_asset_code,  @anchor)
        })

        xdr_envelope = tx.to_envelope(account).to_xdr(:base64)
        tx_response = submit_transaction_operation(xdr_envelope)
        tx_response
      end

      def create_account(vault_entity)
        begin
          retries ||= 0
          new_account = Stellar::KeyPair.from_address(vault_entity.address)
          current_sequence = sequence_from_account(@fuel_account_keypair)
          
          tx = Stellar::Transaction.create_account({
            account:     @fuel_account_keypair,
            destination: new_account,
            sequence:    current_sequence + 1,
            starting_balance: Entity::Vault::INITIAL_XLM_BALANCE
          })

          xdr_envelope = tx.to_envelope(@mama_secret_keypair).to_xdr(:base64)
          tx_response = submit_transaction_operation(xdr_envelope)
          { keypair: new_account, data: get_account_data(new_account) }

        rescue Errors::BadSequence
          retry if (retries += 1) < 3
          raise StandardError, 'Failed to create stellar account'
        end
      end

      private

      # Fetches the current sequence of the account
      # account: key_pair
      # returns an integer
      def sequence_from_account(account)
        data = get_account_data(account)
        data['sequence'].to_i
      end

      def get_account_data(keypair)
        begin
          response = RestClient.get "#{@network_url}/accounts/#{keypair.address}"
          JSON.parse(response.body)
        rescue RestClient::Exceptions::OpenTimeout
          raise StandardError, 'Timed out connecting to server'
        end
      end

      def build_unsigned_olp_transaction(from_account_address:, to_account_address:, 
        amount:, ledger_fee:, current_sequence:)
        from_account = keypair_from_address(from_account_address)
        to_account = keypair_from_address(to_account_address)
        # account_current_sequence = sequence_from_account(from_account)
        tx = Stellar::Transaction.payment({
          account:     from_account,
          destination: to_account,
          fee: ledger_fee,
          sequence:    current_sequence + 1,
          amount:    [:alphanum4, @main_asset_code, @anchor, amount ]
        })
        tx
      end
      

      def submit_transaction_operation(xdr_envelope)
        begin
          response = RestClient.post "#{@network_url}/transactions", { tx: xdr_envelope }
          JSON.parse(response.body)

        rescue RestClient::ExceptionWithResponse => e
          puts "Error in create transaction: #{e.message}"
          puts e.response
          code = e.response.code
          data = JSON.parse(e.response.body)

          # TODO; handle this better
          case code
          when 400
            transaction_msg = data['extras']['result_codes']['transaction']
            if(transaction_msg == 'tx_insufficient_fee')
              raise Errors::InsufficientFee
            else
              error = data['extras']['result_codes']['operations'][0]
              case error
              when 'tx_bad_seq'
                raise Errors::BadSequence
              when 'op_underfunded'
                raise Errors::UnderFunded
              when 'tx_insufficient_balance'
                raise Errors::UnderFunded
              end
            end

          end
        end
      end
      
    end
  end
end
