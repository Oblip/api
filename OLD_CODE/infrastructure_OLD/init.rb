# frozen_string_literal: false

folders = %w[database/orm stellar currency_exchange aws]
folders.each do |folder|
  require_relative "#{folder}/init.rb"
end
