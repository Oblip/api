# frozen_string_literal: false

require 'uri'
require 'net/http'

module Oblip
  class EmailNotification
    
    # module Types
    #   NEW_ACCOUNT_FINISHED = :new_account_finished
    #   DEPOSIT_CONFIRMED = :deposit_confirmed
    #   WITHDRAWAL_REQUEST = :withdrawal_request
    # end

    def self.setup(config)
      @sendgrid_key = config['SENDGRID_KEY']
  
      @new_account_template_id = config['NEW_ACCOUNT_EMAIL_TEMPLATE_ID']
      @deposit_confirmed_template_id = config['DEPOSIT_ORDER_CONFIRMED_TEMPLATE_ID']
      @withdrawal_template_id = config['WITHDRAWAL_ORDER_TEMPLATE_ID']

      @sendgrid_api_url = 'https://api.sendgrid.com/v3/mail/send'
      @recipient_email = config['ACTIVITY_NOTIFICATION_EMAIL']
    end

    def self.notify_deposit_confirmed(deposit_order)

      identity = get_identity(deposit_order.transaction.recipient_account)
      subject = "Deposit Order Confirmed"
      
      data ={
        order_id: deposit_order.id,
        name: identity[:name],
        user_photo: identity[:photo_url],
        subject: subject
      }

      payload = generate_payload(data,  @deposit_confirmed_template_id)
      send_request(payload)

    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
    end

    def self.notify_new_account(oblip_account)
      identity = get_identity(oblip_account)
      subject = "New #{oblip_account.type.name} Account Created"
      
      data ={
        id: oblip_account.id,
        name: identity[:name],
        user_photo: identity[:photo_url],
        subject: subject
      }

      payload = generate_payload(data, @new_account_template_id)
      send_request(payload)

    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
    end

    private

    def self.generate_payload(data, template_id)
      from_email = "#{ENV['RACK_ENV'] ? ENV['RACK_ENV'] : 'development'}@oblip.com"
      data = {
        personalizations: [
          {
           to: [
              {
                email: @recipient_email
              }
            ],
            dynamic_template_data: data
          }
        ],
        from: {
          email: from_email,
          name: "[#{ENV['RACK_ENV'] ? ENV['RACK_ENV'].capitalize : 'Development'}] Oblip Admin Notification"
        },
        reply_to: {
          email: from_email,
          name: "[#{ENV['RACK_ENV'] ? ENV['RACK_ENV'].capitalize : 'Development'}] Oblip Admin Notification"
        },
        template_id: template_id
      }

      data
    end

    def self.get_identity(oblip_account)
      details = oblip_account.personal_details
      personal_name = "#{details.first_name} #{details.last_name}"
      business_name = oblip_account.business ? oblip_account.business.brand_name : ''
      name = oblip_account.business ? business_name : personal_name
      photo_url = oblip_account.business ? oblip_account.business.logo_url : details.photo_url

      {name: name, photo_url: photo_url}
    end

    def self.send_request(data)
      url = URI(@sendgrid_api_url)

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Post.new(url)
      request["authorization"] = "Bearer #{@sendgrid_key}"
      request["content-type"] = 'application/json'
      request.body = data.to_json

      response = http.request(request)
      response.read_body
    end
    
  end

end