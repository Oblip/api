# frozen_string_literal: false
require 'aws-sdk'

module Oblip
  module SMS
    TYPE_PROMOTIONAL = 'Promotional'
    TYPE_TRANSACTIONAL = 'Transactional'
    class Api
      module Errors
        class InvalidMessageLength < StandardError; end
        class InvalidValue < StandardError; end
      end

      def initialize(config)
        @client = Aws::SNS::Client.new(
          region: config['AWS_REGION'],
          access_key_id: config['AWS_ACCESS_KEY_ID'],
          secret_access_key: config['AWS_SECRET_ACCESS_KEY']
        )
      end

      def send_message(phone_number:, type:, message:)
        raise InvalidMessageLength unless message.size < 160

        resp = @client.publish({
          phone_number: phone_number,
          message: message,
          message_attributes: {
            'AWS.SNS.SMS.SenderID' => {
              data_type: 'String',
              string_value: 'Oblip'
            },
            'AWS.SNS.SMS.SMSType' => {
              data_type: 'String',
              string_value: type
            },
            'AWS.SNS.SMS.MaxPrice' => {
              data_type: 'String',
              string_value: '0.50'
            }
          }
        })
      rescue Aws::SNS::Errors::InvalidValue => error
        puts "SMS error: #{error.message}"
        raise Errors::InvalidValue, error.message
      end

    end
  end
end
