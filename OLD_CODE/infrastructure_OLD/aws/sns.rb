# frozen_string_literal: false
require 'aws-sdk'

module Oblip
  module AWS
    module SNS
      TYPE_PROMOTIONAL = 'Promotional'
      TYPE_TRANSACTIONAL = 'Transactional'

      module Errors
        class InvalidMessageLength < StandardError; end
        class InvalidValue < StandardError; end
        class MissingParameter < StandardError; end
      end

      class Api
        def initialize(config)
          @client = Aws::SNS::Client.new(
            region: config['AWS_REGION'],
            access_key_id: config['AWS_ACCESS_KEY_ID'],
            secret_access_key: config['AWS_SECRET_ACCESS_KEY']
          )

          @config = config
        end

        def send_sms(phone_number:, type:, message:)
          raise InvalidMessageLength unless message.size < 160

          resp = @client.publish({
            phone_number: phone_number,
            message: message,
            message_attributes: {
              'AWS.SNS.SMS.SenderID' => {
                data_type: 'String',
                string_value: 'Oblip'
              },
              'AWS.SNS.SMS.SMSType' => {
                data_type: 'String',
                string_value: type
              },
              'AWS.SNS.SMS.MaxPrice' => {
                data_type: 'String',
                string_value: '0.50'
              }
            }
          })
        rescue Aws::SNS::Errors::InvalidValue => error
          puts "SMS error: #{error.message}"
          raise Errors::InvalidValue, error.message
        end

        # # TODO: remove? We don't use this anymore
        # def create_device_endpoint(device_unique_id, token, platform_type)
        #   android_arn = @config['AWS_SNS_PLATFORM_APPLICATION_ARN_ANDROID']
        #   ios_arn = @config['AWS_SNS_PLATFORM_APPLICATION_ARN_IOS']
          
        #   arn = platform_type == Oblip::Entity::Device::ANDROID ? android_arn : ios_arn

        #   resp = @client.create_platform_endpoint({
        #     platform_application_arn: arn, # required
        #     token: token, # required
        #     custom_user_data: "{ device_unique_id: #{device_unique_id} }"
        #   })

        #   resp.endpoint_arn

        # rescue ArgumentError => error
        #   raise Errors::MissingParameter, error.message
        # rescue Aws::SNS::Errors::InvalidValue => error
        #   raise Errors::InvalidValue, error.message
        # end

        #  # TODO: remove? We don't use this anymore
        # def delete_device_endpoint(endpoint_arn)
        #   resp = @client.delete_endpoint({
        #     endpoint_arn: endpoint_arn, # required
        #   })
        #   resp
        # end
        #  # TODO: remove? We don't use this anymore
        # def push_notification_to_device(target_arn, data)
        #   resp = @client.publish({
        #     target_arn: target_arn,
        #     message: data, # required
        #     message_structure: "json"
        #   })

        #   resp
        # end

      end # end of class

    end
  end
end
