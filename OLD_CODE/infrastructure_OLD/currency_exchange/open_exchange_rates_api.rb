# frozen_string_literal: false
require 'rest-client'

module Oblip
  module OpenExchangeRates
    class Api
      def initialize(app_id)
        @api_url = 'https://openexchangerates.org/api/'
        @app_id = app_id
      end

      def load_exchange_rates
        response = RestClient.get "#{@api_url}/latest.json/?app_id=#{@app_id}&base=USD"
        JSON.parse(response.body)
      end
    end
  end
end
