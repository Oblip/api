# frozen_string_literal: true

module Oblip
  Fees = Struct.new  :percentage, :fixed
end
