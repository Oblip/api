# frozen_string_literal: true

module Oblip
  TxInvoice = Struct.new :amount, :anchor_fee_breakdown, :anchor_fee, :agent_fee
end
