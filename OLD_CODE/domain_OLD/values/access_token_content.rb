# frozen_string_literal: true

module Oblip
  AccessTokenContent = Struct.new :id, :phone_number, :type, :country, :base_asset_code, :vault_id
end
