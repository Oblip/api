# frozen_string_literal: true

module Oblip
  Result = Struct.new :code, :message
end
