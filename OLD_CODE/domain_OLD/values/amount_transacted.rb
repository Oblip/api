# frozen_string_literal: true

module Oblip
  AmountTransacted = Struct.new  :received_amount, :removed_amount
end
