# frozen_string_literal: true

module Oblip
  CompanyBankAccounts = Struct.new :accounts
end
