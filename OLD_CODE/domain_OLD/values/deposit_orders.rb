# frozen_string_literal: true

module Oblip
  DepositOrders = Struct.new(:deposit_orders)
end
