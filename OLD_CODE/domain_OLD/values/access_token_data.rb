# frozen_string_literal: true

module Oblip
  AccessTokenData = Struct.new :token, :expiration_date
end
