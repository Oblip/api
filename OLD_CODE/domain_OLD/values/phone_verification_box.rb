# frozen_string_literal: true

module Oblip
  PhoneVerificationBox = Struct.new :phone_number, :code
end
