# frozen_string_literal: true

module Oblip
  Businesses = Struct.new(:businesses)
end
