# frozen_string_literal: true

module Oblip
  EscrowFunds = Struct.new(:escrow_funds)
end
