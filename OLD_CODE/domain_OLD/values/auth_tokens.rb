# frozen_string_literal: true

module Oblip
  AuthTokens = Struct.new :access_token, :refresh_token
end
