# frozen_string_literal: true

module Oblip
  OblipAccountBanks = Struct.new :bank_accounts
end
