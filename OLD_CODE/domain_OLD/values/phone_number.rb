# frozen_string_literal: true

module Oblip
  PhoneNumber = Struct.new :phone_number, :country
end
