# frozen_string_literal: true

module Oblip
  AnchorAssets = Struct.new :assets
end
