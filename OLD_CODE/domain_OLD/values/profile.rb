# frozen_string_literal: true

module Oblip
  Profile = Struct.new :contact_id, :account
end
