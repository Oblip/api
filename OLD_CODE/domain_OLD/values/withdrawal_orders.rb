# frozen_string_literal: true

module Oblip
  WithdrawalOrders = Struct.new(:withdrawal_orders)
end
