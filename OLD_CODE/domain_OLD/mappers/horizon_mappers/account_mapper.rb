# frozen_string_literal: fals

module Oblip
  # Provids access to Stellar's Account Data
  module Horizon
    # Data Mapper for Account Mapper
    class AccountMapper
      def initialize(config, gateway = Oblip::Horizon::Api)
        @config = config
        @gateway_class = gateway
        @gateway = @gateway_class.new(@config)
      end

      def load(account_id)
        data = @gateway.account_data(account_id)
        AccountMapper.build_entity(nil, data, @config['MAIN_ASSET_NAME'])
      end

      def create_account(vault_entity)
        new_account = @gateway.create_account(vault_entity)
        data = @gateway.account_data(new_account[:keypair].address)
        # data['salt'] = vault_entity.salt
  
        AccountMapper.build_entity(vault_entity,data, @config['MAIN_ASSET_NAME'])
      end

      def self.build_entity(entity, extra_data, asset_name)
        DataMapper.new(entity, extra_data, asset_name).build_entity
      end

      class DataMapper
        def initialize(entity, data, asset_name)
          @data = data
          @asset_name = asset_name
          @entity = entity

          @anchor_assets = Oblip::Repository::AnchorAssets.all
          # anchor_asset_codes = anchor_assets.map { |asset| asset.code }
        end

        def build_entity
          Entity::Vault.new(
            id: @entity ? @entity.id : nil,
            blockchain_source_fund_id: @entity ? @entity.blockchain_source_fund_id : nil, 
            initial_native_funds: @entity ? @entity.initial_native_funds : nil,
            initial_operation_cost: @entity ? @entity.initial_operation_cost : nil,
            is_paid: @entity ? @entity.is_paid : nil,
            address: address,
            salt: @entity ? @entity.salt : nil,
            sequence_number: seq_number,
            assets: assets
          )
        end

        private
        
        def seq_number
          @data['sequence']
        end
        
        def address
          @data['account_id']
        end

        def salt
          @data['salt'] ? @data['salt'] : nil
        end

        def assets
          balances = @data['balances']
          balances.map do |a|
            name = ''
            type = ''
            issuer = ''
            asset_code = ''
            asset_type = a['asset_type']
           
            case asset_type
            when 'native'
              name = 'Lumens'
              type =  Entity::VaultAsset::Types::NATIVE # Native
              asset_code = 'XLM'
            when 'credit_alphanum4'

              asset_code = a['asset_code']

              selected_asset = @anchor_assets.select { |asset| asset.code == asset_code }

              if selected_asset
                selected_asset = selected_asset[0]
                type = Entity::VaultAsset::Types::RECOGNIZED_ANCHOR_ASSET
                name = selected_asset.name
                issuer = a['asset_issuer']
              else
                name = asset_code
                type = Entity::VaultAsset::Types::OTHER
                issuer = a['asset_issuer']
              end

            else
              name = asset_code
              type = Entity::VaultAsset::Types::OTHER
              issuer = a['asset_issuer']
            end

            Entity::VaultAsset.new(
              id: nil,
              vault_id: nil,
              name: name,
              code: asset_code,
              balance: a['balance'].to_f,
              type: type,
              authorized: nil,
              issuer: issuer
            )

          end
        end

        # def seed
        #   @data['seed']
        # end

        # def xlm_balance
        #   balances =  @data['balances']
        #   @data['balances'].size > 1 ? balances[1]['balance'].to_f : balances[0]['balance'].to_f
        # end

        # def vault_balance
        #   balances = @data['balances']
        #   @data['balances'].size > 1 ? balances[0]['balance'].to_f : 0.0
        # end

      end
    end
  end
end
