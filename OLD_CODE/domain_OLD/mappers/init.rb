
# frozen_string_literal: true

folders = %w[horizon_mappers]

folders.each do |folder|
  require_relative "#{folder}/init.rb"
end