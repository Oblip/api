# frozen_string_literal: false

require 'dry-struct'


module Oblip
  module Entity
    # Domain entity object Blockchain Fund
    class BlockchainFund < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :amount_purchased, Types::Strict::Float.optional
      attribute :price_purchased, Types::Strict::Float.optional
      attribute :balance, Types::Strict::Float.optional
      attribute :incurred_fees, Types::Strict::Float.optional
      attribute :asset_name, Types::Strict::String.optional


      class SupportedAssets
        XLM = 'XLM'
        USDT = 'USDT'
      end
    end
  end
end
