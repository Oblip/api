# frozen_string_literal: false

require 'dry-struct'
require_relative 'anchor_asset'
require_relative 'oblip_account'
require_relative 'status'

module Oblip
  module Entity
    # Domain entity object for Escrow Fund
    class EscrowFund < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :anchor_asset, Types.Instance(AnchorAsset)
      attribute :oblip_account, Types.Instance(OblipAccount).optional
      attribute :funds_payable, Types::Strict::Float.optional
      attribute :funds_collected, Types::Strict::Float.optional
      attribute :balance, Types::Strict::Float.optional
      attribute :belongs_to_anchor, Types::Strict::Bool.optional
      attribute :is_locked, Types::Strict::Bool.optional
      attribute :status, Types.Instance(Status).optional
      
      module Status
        PENDING_FULL_COLLECTION = Entity::Status.new(id: 1, name: 'Pending Full Collection')
        COLLECTION_COMPLETED = Entity::Status.new(id: 2, name: 'Collection Completed')

        def self.find_id(id)
          case id
          when 1
            PENDING_FULL_COLLECTION
          when 2
            COLLECTION_COMPLETED
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end
    end
  end
end
