# frozen_string_literal: false

require 'dry-struct'
require_relative 'status.rb'

module Oblip
  module Entity
    # Domain entity object for Token Accounts
    class SerializedDate < Dry::Struct
    
      attribute :original_date, Types::Strict::DateTime.optional
      attribute :unix_time, Types::Strict::Integer.optional
      attribute :formatted_date, Types::Strict::String.optional

      def self.create(date)
        # TODO
      end
    end
  end
end
