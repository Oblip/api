# frozen_string_literal: false

require 'dry-struct'
require_relative 'oblip_type'

module Oblip
  module Entity
    # Domain entity object for Staff Account
    class StaffAccount < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :username, Types::Strict::String.optional
      attribute :email_address, Types::Strict::String.optional
      attribute :email_verified, Types::Strict::Bool.optional
      attribute :first_name, Types::Strict::String.optional
      attribute :middle_name, Types::Strict::String.optional
      attribute :last_name, Types::Strict::String.optional
      attribute :type, Types.Instance(OblipType).optional
      attribute :status, Types.Instance(Status).optional
      attribute :permissions, Types::Strict::String.optional # TODO: create a Permissions enity to handle this.
      attribute :photo_url, Types::Strict::String.optional

      module Type
        SUPER_ADMIN = OblipType.new(id: 100, name: 'Super Admin')

        def self.find_id(id)
          case id
          when 100
            SUPER_ADMIN
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      module Status
        ACTIVE = Entity::Status.new(id: 1, name: 'Active')
        INACTIVE = Entity::Status.new(id: 2, name: 'Inactive')

        def self.find_id(id)
          case id
          when 1
            ACTIVE
          when 2
            INACTIVE
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

    end
  end
end
