# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    # Domain entity object for Status
    class FinancialMethod < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :name, Types::Strict::String.optional

      BANK = FinancialMethod.new(
        id: 1,
        name: 'Bank'
      )

      def self.find(id)
        case id
        when 1
          BANK
        else
          raise StandardError.new("Invalid ID")
        end
      end
      
    end
  end
end
