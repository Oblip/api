# frozen_string_literal: false

require 'dry-struct'
require 'ostruct'

# require_relative '../.././lib/access_token.rb'

module Oblip
  module Entity
    # Domain entity object for Access Data
    class AccessMetaData < Dry::Struct
      attribute :token, Types::Strict::String.optional
      attribute :expires_on, Types::Strict::Integer.optional

      DEFAULT_EXPIRATION_WINDOW = AccessToken::ONE_HOUR / 2
      # DEFAULT_EXPIRATION_WINDOW = 60

      def self.create(oblip_account_entity)
        data = {
          id: oblip_account_entity.id,
          type: oblip_account_entity.type,
          country: oblip_account_entity.personal_details.country,
          base_anchor_asset_code: oblip_account_entity.base_anchor_asset.code,
          vault_id: oblip_account_entity.vault_id
        }

        content = AccessTokenContentRepresenter.new(OpenStruct.new(data))

        meta = AccessToken.create(content.to_json, DEFAULT_EXPIRATION_WINDOW)
        AccessMetaData.new(token: meta[:token], expires_on: meta[:expires_on])
      end
    end
  end
end
