# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    # Domain entity object for Anchor Assets
    class AnchorAsset < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :name, Types::Strict::String.optional
      attribute :code, Types::Strict::String.optional
      attribute :symbol, Types::Strict::String.optional
      attribute :issuer, Types::Strict::String.optional
      attribute :is_tethered, Types::Strict::Bool.optional
      # TODO: change this to money type.
      attribute :current_exchange_rate, Types::Strict::Float.optional
      attribute :volume, Types::Strict::Float.optional

      def self.prebuild(id)
        new(
          id: id,
          name: nil,
          code: nil,
          symbol: nil,
          issuer: nil,
          current_exchange_rate: nil,
          volume: nil
        )
      end
      
    end
  end
end
