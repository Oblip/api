# frozen_string_literal: false

require 'dry-struct'

require_relative 'oblip_account'

module Oblip
  module Entity
    # Domain entity object for Contact
    class Contact < Dry::Struct
      attribute :id, Types::Strict::String.optional
      attribute :owner_id, Types::Strict::String.optional
      attribute :contact, Types.Instance(OblipAccount).optional
    end
  end
end
