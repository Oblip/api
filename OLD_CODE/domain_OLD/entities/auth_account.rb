# frozen_string_literal: false

require 'dry-struct'

require_relative 'oblip_account'

module Oblip
  module Entity
    # Domain entity object for authenticated account
    class AuthAccount < Dry::Struct
      attribute :oblip_account, Types.Instance(OblipAccount).optional
      attribute :auth_token, Types::Strict::String.optional
    end
  end
end
