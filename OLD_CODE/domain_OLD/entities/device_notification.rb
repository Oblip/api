# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    class DeviceNotification < Dry::Struct
      attribute :title, Types::Strict::String
      attribute :message, Types::Strict::String
      # attribute :platform_type, Types::Strict::String
      # attribute :click_action, Types::Strict::String.optional
      attribute :type, Types::Strict::String
      attribute :data, Types::Strict::Hash.optional

      module Types
        TRANSACTION = 'type_transaction'
      end

      def to_hash

        notification_data =  {
          title: title,
          body: message,
          sound: 'default',
          largeIcon: 'oblip_icon',
          smallIcon: 'notification_icon_small',
          badge: 1, # TODO: check this again
          color: '#172B49'
        }

        { 
          android: {
            priority: "high"
          },
          priority: 10,
          data: {
            android: {
              priority: "high"
            },
            notification: notification_data,
            extras: data ? {type: type, payload: data} : {type: type}
          }
        }
      end

    end
  end
end