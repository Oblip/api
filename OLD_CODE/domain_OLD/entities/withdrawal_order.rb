# frozen_string_literal: false

require 'dry-struct'

require_relative 'status'
require_relative 'transaction'
require_relative 'oblip_account_bank'

module Oblip
  module Entity
    # Domain entity object for Status
    class WithdrawalOrder < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :oblip_account_id, Types::Strict::String.optional
      attribute :transaction, Types.Instance(Transaction)
      attribute :bank_account, Types.Instance(OblipAccountBank)
      attribute :status, Types.Instance(Status).optional

      def self.build(oblip_account_id:, transaction:, bank_account:)
        status = Status::WithdrawalOrder::UNDER_REVIEW

        new(
          id: nil,
          oblip_account_id: oblip_account_id,
          transaction: transaction,
          bank_account: bank_account,
          status: status
        )
      end
    end
  end
end
