# frozen_string_literal: false

require 'dry-struct'

require_relative 'status'
require_relative 'personal_detail'
require_relative 'oblip_type'
require_relative 'anchor_asset'
require_relative 'business'

module Oblip
  module Entity
    # Domain entity object for account
    class OblipAccount < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :phone_number, Types::Strict::String.optional
      attribute :is_phone_verified, Types::Strict::Bool.default(false) # TODO: review this
    
      attribute :status, Types.Instance(Entity::Status).optional
      attribute :type, Types.Instance(OblipType).optional
      attribute :personal_details, Types.Instance(PersonalDetail).optional
      attribute :business, Types.Instance(Business).optional
      attribute :base_anchor_asset, Types.Instance(AnchorAsset).optional
     
      attribute :kyc_level, Types::Strict::Integer.optional
      attribute :vault_id, Types::Strict::String.optional

      module Types
        PERSONAL = OblipType.new(id: 1, name: 'Personal')
        AGENT = OblipType.new(id: 2, name: 'Agent')
        BUSINESS = OblipType.new(id: 3, name: 'Business')
        ANCHOR = OblipType.new(id: 4, name: 'Anchor')

        def self.find(id)
          case id
          when 1
            PERSONAL
          when 2
            AGENT
          when 3
            BUSINESS
          when 4
            ANCHOR
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      module Status
        INCOMPLETE = Entity::Status.new(id: 1, name: 'Incomplete')
        ACTIVE = Entity::Status.new(id: 2, name: 'Active')
        DEACTIVATED = Entity::Status.new(id: 3, name: 'Deactivated')
        BANNED = Entity::Status.new(id: 4, name: 'Banned')

        def self.find(id)
          case id
          when 1
            INCOMPLETE
          when 2
            ACTIVE
          when 3
            DEACTIVATED
          when 4
            BANNED
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      def self.prebuild(id)
        new(
          id: id,
          phone_number: nil,
          is_phone_verified: false,
          status: nil,
          type: nil,
          personal_details: nil,
          business: nil,
          base_anchor_asset: nil,
          kyc_level: nil,
          vault_id: nil
        )
      end

      def self.create_new(phone_number:, base_anchor_asset:, type:, status:)
  
        new(
          id: nil,
          phone_number: phone_number,
          business: nil,
          is_phone_verified: false,
          status: status,
          type: type,
          personal_details: nil,
          base_anchor_asset: base_anchor_asset,
          kyc_level: 1,
          vault_id: nil
        )
      end     
    end
  end
end
