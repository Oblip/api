# frozen_string_literal: false

require 'dry-struct'
require 'stellar-base'

require_relative 'blockchain_fund'
require_relative 'anchor_asset'
require_relative 'oblip_type'
require_relative 'escrow_fund'

module Oblip
  module Entity
    # Domain entity object for Transactions
    class Transaction < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :anchor_asset, Types.Instance(AnchorAsset).optional
      # TODO; Change to Type Money later
      attribute :amount, Types::Strict::Float.optional
      attribute :anchor_fee_breakdown, Types.Instance(Fees).optional
      attribute :anchor_fee, Types::Coercible::Float.optional
      attribute :agent_fee, Types::Strict::Float.optional
      attribute :network_fee, Types::Strict::Float.optional
      attribute :anchor_escrow_fund, Types.Instance(EscrowFund).optional
      attribute :agent_escrow_fund, Types.Instance(EscrowFund).optional
      attribute :funding_pool, Types.Instance(BlockchainFund).optional
      attribute :exchange_rate, Types::Strict::Float.optional
      attribute :sender_account, Types.Instance(OblipAccount).optional
      attribute :recipient_account, Types.Instance(OblipAccount).optional
      attribute :requires_sender_signature, Types::Strict::Bool.optional
      attribute :sender_asset_balance, Types::Coercible::Float.optional
      attribute :recipient_asset_balance, Types::Coercible::Float.optional
      attribute :signed_tx_envelope, Types::Strict::String.optional
      attribute :unsigned_tx_envelope, Types::Strict::String.optional
      attribute :remarks, Types::Strict::String.optional
      attribute :type, Types.Instance(OblipType).optional
      attribute :vault_tx_hash, Types::Strict::String.optional
      attribute :status, Types.Instance(Status).optional
      attribute :created_at, Types::Strict::String.optional


      class Deposit
        # Agent Transactions
        AGENT_PERSONAL = { anchor: Fees.new(0,0), agent: Fees.new(0.005, 0) }
        # AGENT_BUSINESS = { anchor: Fees.new(0.005,0.1), agent: Fees.new(0, 0) }
        AGENT_AGENT = { anchor: Fees.new(0,1), agent: Fees.new(0.005, 0) }

        # Anchor Transactions
        ANCHOR_AGENT = { anchor: Fees.new(0, 1), agent: Fees.new(0,0) }
        ANCHOR_PERSONAL = { anchor: Fees.new(0.01, 1), agent: Fees.new(0,0) }
        ANCHOR_BUSINESS = { anchor: Fees.new(0.01, 1), agent: Fees.new(0,0) }

        def self.calculate_fees(asset, amount, fee)
          rate = asset.current_exchange_rate
          base_amount =  Entity::Transaction.calculate_base_amount(asset, amount)

          # calculates fees in USD (base asset)
          anchor_fee = (base_amount *  fee[:anchor][:percentage]) +  fee[:anchor][:fixed]
          agent_fee = (base_amount *  fee[:agent][:percentage]) +  fee[:agent][:fixed]
          
           # converts fees into choosen asset
           anchor_fee = OblipCoin.build(anchor_fee * rate).amount
           agent_fee = OblipCoin.build(agent_fee * rate).amount

           anchor_fee_breakdown = fee[:anchor].dup
           anchor_fee_breakdown[:fixed] = OblipCoin.build(anchor_fee_breakdown[:fixed] * rate).amount

            TxInvoice.new(amount,anchor_fee_breakdown, anchor_fee, agent_fee)
        end

        # TODO: refactor later?
        def self.get_fees(sender_type, recipient_type)
          oblip_types = Entity::OblipAccount::Types
          result = nil

          # agents
          if sender_type == oblip_types::AGENT && recipient_type == oblip_types::PERSONAL
            result = AGENT_PERSONAL
          elsif sender_type == oblip_types::AGENT && recipient_type == oblip_types::AGENT
            result = AGENT_AGENT
          end

          # anchor
          if sender_type == oblip_types::ANCHOR && recipient_type == oblip_types::AGENT
            result = ANCHOR_AGENT
          elsif sender_type == oblip_types::ANCHOR && recipient_type == oblip_types::PERSONAL
            result = ANCHOR_PERSONAL
          elsif sender_type == oblip_types::ANCHOR && recipient_type == oblip_types::BUSINESS
            result = ANCHOR_BUSINESS
          end

          if !result
            raise OException::PolicyRestriction
          end

          result
        end
      end

      class Payment
        # personal
        PERSONAL_PERSONAL = { anchor: Fees.new(0.015, 0.1), agent: Fees.new(0, 0) }
        PERSONAL_BUSINESS = { anchor: Fees.new(0.005, 0.1), agent: Fees.new(0,0) }
       
        # agent
        AGENT_BUSINESS = { anchor: Fees.new(0.005,0.1), agent: Fees.new(0, 0) }
        
        # businsess
        BUSINESS_PERSONAL = { anchor: Fees.new(0.01, 0.1), agent: Fees.new(0,0) }
        BUSINESS_BUSINESS = { anchor: Fees.new(0.005, 1), agent: Fees.new(0,0) }

        def self.calculate_fees(vault_source, asset, amount, fee)
          rate = asset.current_exchange_rate
          base_amount =  Entity::Transaction.calculate_base_amount(asset, amount)

          # calculates fees in USD (base asset)
          anchor_fee = (base_amount *  fee[:anchor][:percentage]) +  fee[:anchor][:fixed]
          agent_fee = (base_amount *  fee[:agent][:percentage]) +  fee[:agent][:fixed]

          anchor_fee += (vault_source.initial_operation_cost * rate) if !vault_source.is_paid

          # converts fees into choosen asset
          anchor_fee = OblipCoin.build(anchor_fee * rate).amount
          agent_fee = OblipCoin.build(agent_fee * rate).amount

          anchor_fee_breakdown = fee[:anchor].dup
           anchor_fee_breakdown[:fixed] = OblipCoin.build(anchor_fee_breakdown[:fixed] * rate).amount

            TxInvoice.new(amount,anchor_fee_breakdown, anchor_fee, agent_fee)
        end

        def self.get_fees (sender_type, recipient_type)
          oblip_types = Entity::OblipAccount::Types
          result = nil
          
          #personal
          if sender_type == oblip_types::PERSONAL && recipient_type == oblip_types::PERSONAL
            result = PERSONAL_PERSONAL
          elsif sender_type == oblip_types::PERSONAL && recipient_type == oblip_types::BUSINESS
            result = PERSONAL_BUSINESS
          end

          # agent
          if sender_type == oblip_types::AGENT && recipient_type == oblip_types::BUSINESS
            result = AGENT_BUSINESS
          end

          # business
          if sender_type == oblip_types::BUSINESS && recipient_type == oblip_types::PERSONAL
            result = BUSINESS_PERSONAL
          elsif sender_type == oblip_types::BUSINESS && recipient_type == oblip_types::BUSINESS
            result = BUSINESS_BUSINESS
          end

          if !result
            raise OException::PolicyRestriction
          end

          result
        end
      end

      class Withdrawal
        #agent
        AGENT_ANCHOR = { anchor: Fees.new(0, 2), agent: Fees.new(0,0) }

        # personal
        PERSONAL_AGENT = { anchor: Fees.new(0, 1), agent: Fees.new(0,0) }
        PERSONAL_ANCHOR = { anchor: Fees.new(0.02, 1), agent: Fees.new(0,0) }

        # business
        BUSINESS_AGENT = { anchor: Fees.new(0.005, 1), agent: Fees.new(0,0) }
        BUSINESS_ANCHOR = { anchor: Fees.new(0.01, 1) , agent: Fees.new(0,0) }


        def self.calculate_fees(vault_source, asset, amount, fee)
          rate = asset.current_exchange_rate
          base_amount =  Entity::Transaction.calculate_base_amount(asset, amount)
          
          # calculates fees in USD (base asset)
          anchor_fee = (base_amount *  fee[:anchor][:percentage]) +  fee[:anchor][:fixed]
          agent_fee = (base_amount*  fee[:agent][:percentage]) +  fee[:agent][:fixed]

          anchor_fee += vault_source.initial_operation_cost if !vault_source.is_paid

          # converts fees into choosen asset
          anchor_fee = OblipCoin.build(anchor_fee * rate).amount
          agent_fee = OblipCoin.build(agent_fee * rate).amount

          anchor_fee_breakdown = fee[:anchor].dup
           anchor_fee_breakdown[:fixed] = OblipCoin.build(anchor_fee_breakdown[:fixed] * rate).amount

            TxInvoice.new(amount,anchor_fee_breakdown, anchor_fee, agent_fee)
        end

        def self.get_fees (sender_type, recipient_type)
          oblip_types = Entity::OblipAccount::Types
          result = nil
          
          #personal
          if sender_type == oblip_types::PERSONAL && recipient_type == oblip_types::AGENT
            result = PERSONAL_AGENT
          elsif sender_type == oblip_types::PERSONAL && recipient_type == oblip_types::ANCHOR
            result = PERSONAL_ANCHOR
          end

          # agent
          if sender_type == oblip_types::AGENT && recipient_type == oblip_types::ANCHOR
            result = AGENT_ANCHOR
          end

          # business
          if sender_type == oblip_types::BUSINESS && recipient_type == oblip_types::AGENT
            result = BUSINESS_AGENT
          elsif sender_type == oblip_types::BUSINESS && recipient_type == oblip_types::ANCHOR
            result = BUSINESS_ANCHOR
          end

          if !result
            raise OException::PolicyRestriction
          end

          result
        end
      end

      module Status
        FAILED = Entity::Status.new(
          id: 1,
          name: 'Failed'
        )

        COMPLETED = Entity::Status.new(
          id: 2,
          name: 'Completed'
        )
        
        AWAITING_DEPOSIT = Entity::Status.new(
          id: 3,
          name: 'Awaiting Deposit'
        )

        CONFIRMATION_REVIEW = Entity::Status.new(
          id: 4,
          name: 'Confirmation Review'
        )
        
        AWAITING_WITHDRAWAL = Entity::Status.new(
          id: 5,
          name: 'Awaiting Withdrawal'
        )

        PROCESSING = Entity::Status.new(id: 6, name: 'Processing')
        
        def self.find(id)
          case id
          when 1
            FAILED
          when 2
            COMPLETED
          when 3
            AWAITING_DEPOSIT
          when 4
            CONFIRMATION_REVIEW # TODO change to: Deposit Submitted or something?
          when 5
            AWAITING_WITHDRAWAL
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      module Types
        DEPOSIT = OblipType.new(id: 1, name: 'Deposit')
        PAYMENT = OblipType.new(id: 2, name: 'Payment')
        WITHDRAWAL = OblipType.new(id: 3, name: 'Withdrawal')

        def self.find(id)
          case id
          when 1
            DEPOSIT
          when 2
            PAYMENT
          when 3
            WITHDRAWAL
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      # @param tx_data [Hash]{anchor_asset:, amount:, remarks:, status:, 
      #   tx_type:, sender_account:, recipient_account:, signed_tx_envelope:, vault_tx_hash:}
      def self.build(vault_source:, sender_type:, recipient_type:, tx_data:)

        invoice = calculate_fees(
          vault_source: vault_source,
          sender_type: sender_type,
          recipient_type: recipient_type,
          tx_data: tx_data
        )

        # network_fee = calculate_funding(tx_data[:signed_tx_envelope],
        #   tx_data[:anchor_asset].current_exchange_rate, funding_pool)
        
        new(
          id: nil,
          anchor_asset: tx_data[:anchor_asset],
          amount: tx_data[:amount].to_f,
          anchor_fee_breakdown: invoice[:anchor_fee_breakdown],
          anchor_fee: invoice[:anchor_fee].to_f,
          agent_fee: invoice[:agent_fee].to_f,
          exchange_rate: tx_data[:anchor_asset].current_exchange_rate,
          network_fee: nil,
          anchor_escrow_fund: nil,
          agent_escrow_fund: nil,
          funding_pool: nil,
          requires_sender_signature: tx_data[:requires_sender_signature],
          sender_account: tx_data[:sender_account],
          recipient_account: tx_data[:recipient_account],
          sender_asset_balance:  tx_data[:sender_asset_balance],
          recipient_asset_balance: tx_data[:recipient_asset_balance],
          remarks: tx_data[:remarks],
          type: tx_data[:tx_type],
          status: tx_data[:status],
          signed_tx_envelope: tx_data[:signed_tx_envelope],
          unsigned_tx_envelope: nil,
          vault_tx_hash: tx_data[:vault_tx_hash],
          created_at: nil
        )
      end


      # params @tx_data [Hash] { tx_type:, source_currency:, source_amount: ... }
      def self.calculate_fees(vault_source:, sender_type:, recipient_type:, tx_data:)
        oblip_types = Entity::OblipAccount::Types
        # base_amount = calculate_base_amount(tx_data[:anchor_asset], tx_data[:amount])

        case tx_data[:tx_type]
        when Entity::Transaction::Types::DEPOSIT
          fees = Deposit.get_fees(sender_type, recipient_type)
          Deposit.calculate_fees(tx_data[:anchor_asset], tx_data[:amount], fees)
        when Entity::Transaction::Types::PAYMENT
          fees = Payment.get_fees(sender_type, recipient_type)
          Payment.calculate_fees(vault_source, tx_data[:anchor_asset], tx_data[:amount], fees)
        when Entity::Transaction::Types::WITHDRAWAL
          fees = Withdrawal.get_fees(sender_type, recipient_type)
          Withdrawal.calculate_fees(vault_source,tx_data[:anchor_asset], tx_data[:amount], fees)
        else
          raise OException::PolicyRestriction
        end
      end

      def self.calculate_base_amount(anchor_asset, amount)
        OblipCoin.build(amount / anchor_asset.current_exchange_rate).amount
      end

      def add_signed_tx_envelope(signed_envelope)
        self.new(
          signed_tx_envelope: signed_envelope
        )
      end

      def add_unsigned_tx_envelope(unsigned_envelope)
        self.new(
          unsigned_tx_envelope: unsigned_envelope
        )
      end

      def calculate_transaction(sender_type, recipient_type)
        ##
        # The way it should be calculated are as follows: 
        ##
        # Agents -> Personal : Fee is deducted from amount
        # Agent -> Business : Fee is deducted on top of amount
        # Agent -> Agent : Fee is deducted from amount

        # Anchor -> anyone : Fee is deducted from amount

        # Personal A -> Personal B : Fee is deducted on top of amount
        # Personal -> Business : Fee is deducted on top of amount
        # Personal -> Agent : Fee is deducted on top of amount
        # Personal -> Anchor : Fee is deducted on top of amount

        # Business -> Personal : Fee is deducted on top of amount
        # Business -> Business : Fee is deducted on top of amount
        # Business -> Agent : Fee is deducted on top of amount
        # Business -> Anchor : Fee is deducted on top of amount
        ##

        # We don't charge an agent fee in the transaction, it's added as agent commission
        removed_amount = -(amount - anchor_fee)
        received_amount = (amount - anchor_fee)

        removed_amount = OblipCoin.build(removed_amount).amount
        received_amount = OblipCoin.build(received_amount).amount

        AmountTransacted.new(received_amount, removed_amount)
      end

      def self.apply_funding(tx_entity, fund_entity)
        decoded_envelope = Stellar::TransactionEnvelope.from_xdr(Base64.decode64(tx_entity.signed_tx_envelope))
        network_fee = decoded_envelope.tx.fee * 0.0000001 #(base lumens)
        amount_in_usd = network_fee * fund_entity.price_purchased
        exchanged_amount = amount_in_usd * tx_entity.exchange_rate

        tx_entity.new(
          funding_pool: fund_entity,
          network_fee: exchanged_amount
        )
      end
    end
  end
end
