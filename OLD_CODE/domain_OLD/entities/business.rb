# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    # Domain entity object for Business
    class Business < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :brand_name, Types::Strict::String.optional
      attribute :is_verified, Types::Strict::Bool.optional
      attribute :registration_name, Types::Strict::String.optional
      attribute :address, Types::Strict::String.optional
      attribute :oblip_account_id, Types::Coercible::String.optional
      attribute :logo_url, Types::Strict::String.optional
      attribute :created_at, Types::DateTime.optional
      attribute :updated_at, Types::DateTime.optional

    end
  end
end