# frozen_string_literal: false

require 'dry-struct'
require 'ostruct'

# require_relative '../../../lib/access_token.rb'
require_relative './access_meta_data.rb'

module Oblip
  module Entity
    # Domain entity object for Access Data
    class StaffAccessData < AccessMetaData

      DEFAULT_EXPIRATION_WINDOW = AccessToken::ONE_WEEK

      def self.create(staff_account_entity)
        data = {
          id: staff_account_entity.id,
          username: staff_account_entity.username,
          type: staff_account_entity.type,
          permissions: staff_account_entity.permissions,
        }

        content = Representer::Staff::AccessTokenContentRepresenter.new(OpenStruct.new(data))

        meta = AccessToken.create(content.to_json, DEFAULT_EXPIRATION_WINDOW)
        StaffAccessData.new(token: meta[:token], expires_on: meta[:expires_on])
      end
    end
  end
end
