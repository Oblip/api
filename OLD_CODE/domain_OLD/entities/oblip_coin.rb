# frozen_string_literal: false

require 'dry-struct'


module Oblip
  module Entity
    # Domain entity object for Money
    class OblipCoin < Dry::Struct
      attribute :amount, Types::Strict::Float

      def self.build(amount)
        new(
          amount: amount.truncate(7)
        )
      end
    end
  end
end
