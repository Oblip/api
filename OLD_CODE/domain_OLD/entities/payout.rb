# frozen_string_literal: false

require 'dry-struct'
require_relative 'status'
require_relative 'transaction'
require_relative 'oblip_type'
require_relative 'status'
require_relative 'anchor_asset'
require_relative 'oblip_account'
require_relative 'escrow_fund'

module Oblip
  module Entity
    # Domain entity object for Payout
    class Payout < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :deposit_transaction, Types.Instance(Transaction).optional
      attribute :escrow_fund, Types.Instance(EscrowFund).optional
      attribute :anchor_asset, Types.Instance(AnchorAsset).optional
      attribute :payee, Types.Instance(OblipAccount).optional
      attribute :amount, Types::Strict::Float.optional
      attribute :type, Types.Instance(OblipType).optional
      attribute :status, Types.Instance(Status).optional

      # TODO: add created_at, updated_at here

      module Types
        ANCHOR_PAYOUT = OblipType.new(id: 1, name: 'Anchor Payout')
        AGENT_PAYOUT = OblipType.new(id: 2, name: 'Agent Payout')
        BUSINESS_PAYOUT = OblipType.new(id: 3, name: 'Business Payout')

        def self.find_id(id)
          case id
          when 1
            ANCHOR
          when 2
            AGENT
          when 3
            BUSINESS
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      module Status
        PENDING_APPROVAL = Entity::Status.new(id: 1, name: 'Pending Approval')
        REJECTED = Entity::Status.new(id: 2, name: 'Rejected')
        PROCESSING = Entity::Status.new(id: 3, name: 'Processing')
        PAID_OUT = Entity::Status.new(id: 4, name: 'Paid Out')

        def find_id(id)
          case id
          when 1
            PENDING_APPROVAL
          when 2
            REJECTED
          when 3
            PROCESSING
          when 4
            PAID_OUT
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

    end
  end
end
