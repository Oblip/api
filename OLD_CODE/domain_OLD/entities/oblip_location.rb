# frozen_string_literal: false

require 'dry-struct'
require_relative 'oblip_account'

module Oblip
  module Entity
    # Domain entity object for Status
    class OblipLocation < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :oblip_account, Types.Instance(OblipAccount).optional
      attribute :name, Types::Strict::String.optional
      attribute :description, Types::Strict::String.optional
      attribute :address, Types::Strict::String.optional
      attribute :lat, Types::Strict::Float.optional
      attribute :lng, Types::Strict::Float.optional
    end
  end
end
