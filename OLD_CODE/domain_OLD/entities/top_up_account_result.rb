# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    # Domain entity object for Oblip Account Type
    class TopUpAccountResult < Dry::Struct

      TOP_UP_FEE = 1 # OLP

      attribute :tx_hash, Types::Strict::String.optional
      attribute :tx_id, Types::Strict::String.optional
      attribute :account_id, Types::Strict::String.optional
      attribute :amount, Types::Strict::Float.optional
      attribute :fee, Types::Strict::Float.optional
     
      def self.prebuild(account_id:, amount:)
        amount = amount - TOP_UP_FEE
        new(
          tx_hash: nil,
          account_id: account_id,
          amount: amount,
          fee: TOP_UP_FEE
        )
      end

      def self.build(prebuilt_entity, tx_hash:, tx_id:)
        prebuilt_entity.new(
          tx_hash: tx_hash,
          tx_id: tx_id
        )
      end
      
    end
  end
end
