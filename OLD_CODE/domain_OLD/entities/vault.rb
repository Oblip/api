# frozen_string_literal: false

require 'dry-struct'
require_relative 'vault_asset.rb'

module Oblip
  module Entity
    # Domain entity object for Token Accounts
    class Vault < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :address, Types::Strict::String.optional
      attribute :salt, Types::Strict::String.optional
      attribute :blockchain_source_fund_id, Types::Strict::Integer.optional
      attribute :initial_native_funds, Types::Strict::Float.optional
      attribute :initial_operation_cost, Types::Strict::Float.optional
      attribute :is_paid, Types::Strict::Bool.optional
      attribute :sequence_number, Types::Strict::String.optional
      attribute :assets, Types::Strict::Array.of(VaultAsset).optional

      NUM_SIGNERS = 3
      INITIAL_XLM_BALANCE = ((NUM_SIGNERS + 2) * 0.5) + 0.00006 # XLM
      # INITIAL_XLM_BALANCE = 3.5 # XLM

      def self.build_new(address, salt)
        new(
          id: nil,
          address: address,
          salt: salt,
          blockchain_source_fund_id: nil,
          initial_operation_cost: nil,
          initial_native_funds: INITIAL_XLM_BALANCE,
          is_paid: false,
          sequence_number: nil,
          assets: nil
        )
      end

      def self.init_funding(vault_entity, fund_entity)
        amount_in_usd = vault_entity.initial_native_funds * fund_entity.price_purchased
        incurred_fee_share = amount_in_usd * fund_entity.incurred_fees / (fund_entity.amount_purchased * fund_entity.price_purchased)
        initial_operation_cost = amount_in_usd + incurred_fee_share

        vault_entity.new(
          blockchain_source_fund_id: fund_entity.id,
          initial_operation_cost: initial_operation_cost
        )
      end
    end
  end
end
