# frozen_string_literal: false

require 'dry-struct'
require_relative 'anchor_asset'

module Oblip
  module Entity
    # Domain entity object for Token Accounts
    class Country < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :name, Types::Strict::String.optional
      attribute :code, Types::Strict::String.optional
      attribute :phone_code, Types::Strict::String.optional
      attribute :currency, Types::Strict::String.optional
      attribute :anchor_asset, Types.Instance(AnchorAsset).optional
      attribute :allow_cross_border_trade, Types::Strict::Bool.optional

      def self.build_with_id(id)
        new(
          id: id,
          code: nil,
          name:nil,
          phone_code: nil,
          currency: nil,
          anchor_asset: nil,
          allow_cross_border_trade: false
        )
      end
    end
  end
end
