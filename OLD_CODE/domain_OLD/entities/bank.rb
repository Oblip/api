# frozen_string_literal: false

require 'dry-struct'

require_relative 'country'

module Oblip
  module Entity
    # Domain entity object for Status
    class Bank < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :name, Types::Strict::String.optional
      attribute :company_name, Types::Strict::String.optional
      attribute :address, Types::Strict::String.optional
      attribute :logo_url, Types::Strict::String.optional
      attribute :swift_code, Types::Strict::String.optional
      attribute :routing_number, Types::Strict::String.optional
      attribute :country, Types.Instance(Country).optional
    
      def self.prebuild(id)
        new(
          id: id,
          name: nil,
          company_name: nil,
          address: nil,
          logo_url: nil,
          swift_code: nil,
          routing_number: nil,
          country: nil
        )
      end
    end
  end
end
