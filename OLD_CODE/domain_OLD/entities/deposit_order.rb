# frozen_string_literal: false

require 'dry-struct'

require_relative 'status'
require_relative 'transaction'
require_relative 'company_bank_account'
require_relative 'financial_method'

module Oblip
  module Entity
    # Domain entity object for Status
    class DepositOrder < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :oblip_account_id, Types::Strict::String.optional
      attribute :transaction, Types.Instance(Transaction)
      attribute :bank_account, Types.Instance(CompanyBankAccount)
      attribute :deposit_method, Types.Instance(FinancialMethod).optional
      attribute :status, Types.Instance(Status).optional
      attribute :created_at, Types::Time.optional
      attribute :updated_at, Types::Time.optional

      module Status
        UNCONFRIMED = Entity::Status.new(
          id: 1,
          name: 'Unconfirmed'
        )
        
        USER_CONFIRMED = Entity::Status.new(
          id: 2,
          name: 'User Confirmed'
        )
        
        COMPLETED = Entity::Status.new(
          id: 3,
          name: 'Completed'
        )
        
        FAILED = Entity::Status.new(
          id: 4,
          name: 'Failed'
        )

        def self.find(id)
          case id
          when 1
            UNCONFRIMED
          when 2
            USER_CONFIRMED
          when 3
            COMPLETED
          when 4
            FAILED
          else
            raise StandardError.new('Invalid ID')
          end
          
        end
      end

      def self.build(oblip_account_id:, transaction:, bank_account:, deposit_method:)
        status = Status::UNCONFRIMED
        new(
          id: nil,
          oblip_account_id: oblip_account_id,
          transaction: transaction,
          bank_account: bank_account,
          deposit_method: deposit_method,
          status: status,
          created_at: nil,
          updated_at: nil
        )
      end
    end
  end
end
