# frozen_string_literal: false

require 'dry-struct'
require_relative 'status.rb'
require_relative 'oblip_type'

module Oblip
  module Entity
    # Domain entity object for Asset
    class VaultAsset < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :vault_id, Types::Coercible::String.optional
      attribute :name, Types::Strict::String.optional
      attribute :code, Types::Strict::String.optional
      attribute :balance, Types::Strict::Float.optional
      attribute :type, Types.Instance(OblipType).optional
      attribute :authorized, Types::Strict::Bool.optional
      attribute :issuer, Types::Strict::String.optional

      module Types
        NATIVE = OblipType.new(id: 1, name: 'Native')
        RECOGNIZED_ANCHOR_ASSET = OblipType.new(id: 2, name: 'Recognized Anchor Asset')
        OTHER = OblipType.new(id: 3, name: 'Other')

        def self.find_id(id)
          case id
          when 1
              NATIVE
          when 2
            RECOGNIZED_ANCHOR_ASSET
          when 3
            OTHER
          else
            raise StandardError.new('INVALID ERROR')
          end
        end
      end
    end
  end
end
