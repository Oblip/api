# frozen_string_literal: false

require 'dry-struct'

require_relative 'bank'
require_relative 'status'

module Oblip
  module Entity
    # Domain entity object for Status
    class CompanyBankAccount < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :bank, Types.Instance(Bank).optional
      attribute :account_number, Types::Strict::String.optional
      attribute :status, Types.Instance(Status).optional


      def self.build(bank_id:, account_number: )
        status = Status::OblipAccountBank::UNVERIFIED
        new(
          id: nil,
          bank: Bank.prebuild(bank_id),
          account_number: account_number,
          status: status
        )
      end
    end
  end
end
