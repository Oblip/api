# frozen_string_literal: false

require 'dry-struct'

require_relative 'status'
require_relative 'bank'

module Oblip
  module Entity
    # Domain entity object for Status
    class OblipAccountBank < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :bank, Types.Instance(Bank).optional
      attribute :oblip_account_id, Types::Strict::String.optional
      attribute :account_number, Types::Strict::String.optional
      attribute :status, Types.Instance(Status).optional


      def self.build(bank_id:, oblip_account_id:, account_number: )
        status = Status::OblipAccountBank::UNVERIFIED
        new(
          id: nil,
          bank: Bank.prebuild(bank_id),
          oblip_account_id: oblip_account_id,
          account_number: account_number,
          status: status
        )
      end
    end
  end
end
