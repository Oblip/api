# frozen_string_literal: false

require 'dry-struct'
# require 'rbnacl'
require 'rbnacl'

require_relative 'status'
require_relative 'transaction'
require_relative 'oblip_account_bank'
require_relative 'oblip_type'

module Oblip
  module Entity
    # Domain entity object for Status
    class BankOrder < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      # attribute :ref_no, Types::Strict::String.optional
      attribute :transaction, Types.Instance(Transaction)
      attribute :bank_account, Types.Instance(OblipAccountBank)
      attribute :type, Types.Instance(OblipType).optional
      attribute :status, Types.Instance(Status).optional

      def self.build(transaction:, bank_account:, type_id:)
        # ref_no = RbNaCl::Random.random_bytes(32).unpack("N")[0].to_s
        status = Status::BankOrder::UNCONFRIMED
        type = OblipType.new(:bank_order, type_id)
        
        new(
          # ref_no: ref_no,
          transaction: transaction,
          bank_account: bank_account,
          type: type,
          status: status
        )
      end
    end
  end
end
