# frozen_string_literal: false

require 'dry-struct'

require_relative 'oblip_account'

module Oblip
  module Entity
    # Domain entity object for Contact
    class ContactSearchResult < Dry::Struct
      attribute :query, Types::Strict::String.optional
      attribute :is_in_contact_list, Types::Strict::Bool.optional
      attribute :result, Types.Instance(OblipAccount).optional
    end
  end
end
