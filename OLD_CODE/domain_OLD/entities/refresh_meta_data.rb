# frozen_string_literal: false

require 'dry-struct'
require_relative 'access_meta_data'

module Oblip
  module Entity
    # Domain entity object for Refresh Access Data
    class RefreshMetaData < Dry::Struct
      attribute :access_data, Types.Instance(AccessMetaData).optional
      attribute :refresh_token, Types::Strict::String.optional

      def self.create_refresh_token(client_id, secret)
        refresh_token =  "#{client_id}.#{secret}"
        new(refresh_token: refresh_token, access_data: nil)
      end
      
      def self.create_auth(client_id, secret, access_data)
        refresh_meta = create_refresh_token(client_id, secret)
        refresh_meta.new(access_data: access_data)
      end
    end
  end
end
