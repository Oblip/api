# frozen_string_literal: false

require 'dry-struct'
require_relative 'status.rb'
require_relative 'oblip_type.rb'
require 'date'

module Oblip
  module Entity
    # Domain entity object for ApiClient
    class ApiClient < Dry::Struct
      ANDROID = 'Android'
      IOS = 'iOS'
      
      attribute :id, Types::Coercible::String.optional
      attribute :oblip_account_id, Types::Strict::String.optional
      attribute :secret_code, Types::Strict::String.optional
      # attribute :expiry_date, Types::Strict::DateTime.optional # TODO: convert to SerializedDate
      attribute :public_key, Types::Strict::String.optional
      attribute :fcm_token, Types::Strict::String.optional
      attribute :verification_token, Types::Strict::String.optional
      attribute :is_verified, Types::Strict::Bool.optional
      attribute :last_attempted_verification, Types::Time.optional
      attribute :verification_attempts, Types::Strict::Integer.optional
      attribute :platform_type, Types::Strict::String.optional
      attribute :status, Types.Instance(Status).optional
      attribute :type, Types.Instance(OblipType).optional
      attribute :last_used, Types::Time.optional
      attribute :last_ip_address, Types::Strict::String.optional
      
      module Status
        HIBERNATED = Entity::Status.new(id: 1, name: 'Hibernate')
        ACTIVE = Entity::Status.new(id: 2, name: 'Active')
        DEACTIVATED = Entity::Status.new(id: 3, name: 'Deactivated')

        def self.find_id(id)
          case id
          when 1
            HIBERNATED
          when 2
            ACTIVE
          when 3
            DEACTIVATED
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      module Types
        USER = OblipType.new(id: 1, name: 'User')
        STAFF = OblipType.new(id: 2, name: 'Staff')
        DEVELOPER = OblipType.new(id: 3, name: 'Developer')

        def self.find_id(id)
          case id
          when 1
            USER
          when 2
            STAFF
          when 3
            DEVELOPER
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      def self.prebuild(public_key:, platform_type:, type:, fcm_token:, ip_address:)
        new(
          id: nil,
          oblip_account_id: nil,
          secret_code: nil,
          is_verified: false,
          verification_token: nil,
          last_attempted_verification: nil,
          verification_attempts: nil,
          public_key: public_key,
          fcm_token: fcm_token,
          platform_type: platform_type,
          status: Status::HIBERNATED,
          type: type,
          last_used: nil,
          last_ip_address: ip_address
        )
      end

      def is_secret_code_valid?(code)
        SecureDB.hash_content(code) == secret_code
      end

    end
  end
end
