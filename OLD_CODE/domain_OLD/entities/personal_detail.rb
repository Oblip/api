# frozen_string_literal: false

require 'dry-struct'

require_relative 'country'

module Oblip
  module Entity
    # Domain entity object for personal details
    class PersonalDetail < Dry::Struct
      attribute :id, Types::Coercible::String.optional
      attribute :first_name, Types::Strict::String.optional
      attribute :middle_name, Types::Strict::String.optional
      attribute :last_name, Types::Strict::String.optional
      attribute :gender, Types::Strict::String.optional
      attribute :birthday, Types::Strict::Date.optional
      attribute :country, Types.Instance(Country).optional
      attribute :photo_url, Types::Strict::String.optional

      def self.create_new(first_name:, middle_name:, last_name:, birthday:, country_id:)
        new(
          id: nil,
          first_name: first_name,
          middle_name: middle_name,
          last_name: last_name,
          gender: nil,
          birthday: Date.parse(birthday),
          country: Country.build_with_id(country_id),
          photo_url: nil
        )
      end

      def self.create_from_hash(input)
        new(
          id: nil,
          first_name: input['first_name'] || input[:first_name] || nil,
          middle_name: input['middle_name'] || input[:middle_name] || nil,
          last_name: input['last_name'] || input[:last_name] || nil,
          gender: input['gender'] || input[:gender] || nil,
          birthday: (( input['birthday'] || input[:birthday]) ? Date.parse((input['birthday'] || input[:birthday])) : nil),
          country: nil,
          photo_url:  input['photo_url'] || input[:photo_url] || nil
        )
      end
    end
  end
end
