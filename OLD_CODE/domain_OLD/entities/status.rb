# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    # Domain entity object for Status
    class Status < Dry::Struct
      attribute :id, Types::Strict::Integer.optional
      attribute :name, Types::Strict::String.optional


      class Device
        APP_INSTALLED = Status.new(
          id: 1,
          name: 'App Installed; not linked'
        )
        ACCOUNT_LINKED = Status.new(
          id: 2,
          name: 'Account Linked'
        )
        BLACKLISTED = Status.new(
          id: 3,
          name: 'Blacklisted'
        )

        def self.find(id)
          case id
          when 1
            APP_INSTALLED
          when 2
            ACCOUNT_LINKED
          when 3
            BLACKLISTED
          else 
            raise StandardError.new('Invalid ID')
          end
        end
      end

      # TODO: Remove (Depreciate)
      class Transaction
        FAILED = Status.new(
          id: 1,
          name: 'Failed'
        )

        COMPLETED = Status.new(
          id: 2,
          name: 'Completed'
        )
        
        AWAITING_DEPOSIT = Status.new(
          id: 3,
          name: 'Awaiting Deposit'
        )

        CONFIRMATION_REVIEW = Status.new(
          id: 4,
          name: 'Confirmation Review'
        )
        
        AWAITING_WITHDRAWAL = Status.new(
          id: 5,
          name: 'Awaiting Withdrawal'
        )

        PROCESSING = Status.new(id: 6, name: 'Processing')
        
        def self.find(id)
          case id
          when 1
            FAILED
          when 2
            COMPLETED
          when 3
            AWAITING_DEPOSIT
          when 4
            CONFIRMATION_REVIEW # TODO change to: Deposit Submitted or something?
          when 5
            AWAITING_WITHDRAWAL
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      class AgentCommission
        UNPAID = Status.new(
          id: 1,
          name: 'Unpaid'
        )

        APPROVED_PAYOUT = Status.new(
          id: 2,
          name: 'Approved Payout'
        )

        PAID = Status.new(
          id: 3,
          name: 'Paid'
        )

        def self.find(id)
          case id
          when 1
            UNPAID
          when 2
            APPROVED_PAYOUT
          when 3
            PAID
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      class TransactionEarning
        IN_ACCOUNT = Status.new(
          id: 1,
          name: 'In Account'
        )

        TRANSFERED = Status.new(
          id: 2,
          name: 'Transfered'
        )

        LIQUIDATED = Status.new(
          id: 2,
          name: 'Liquidated'
        )

        def self.find(id)
          case id
          when 1
            IN_ACCOUNT
          when 2
            TRANSFERED
          when 3
            LIQUIDATED
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      # TODO: remove (depreciated)
      class DepositOrder
        UNCONFRIMED = Status.new(
          id: 1,
          name: 'Unconfirmed'
        )
        USER_CONFIRMED = Status.new(
          id: 2,
          name: 'User Confirmed'
        )
        
        COMPLETED = Status.new(
          id: 3,
          name: 'Completed'
        )
        FAILED = Status.new(
          id: 4,
          name: 'Failed'
        )

        def self.find(id)
          case id
          when 1
            UNCONFRIMED
          when 2
            USER_CONFIRMED
          when 3
            COMPLETED
          when 4
            FAILED
          else
            raise StandardError.new('Invalid ID')
          end

        end
      end

      class WithdrawalOrder
        UNDER_REVIEW = Status.new(
          id: 1,
          name: 'Under Review'
        )
        PROCESSING = Status.new(
          id: 2,
          name: 'Processing'
        )
        COMPLETED = Status.new(
          id: 3,
          name: 'Completed'
        )
        FAILED = Status.new(
          id: 4,
          name: 'Failed'
        )

        def self.find(id)
          case id
          when 1
            UNDER_REVIEW
          when 2
            PROCESSING
          when 3
            COMPLETED
          when 4
            FAILED
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      class CompanyBankAccount
        ACTIVE = Status.new(
          id: 1,
          name: 'Active'
        )

        INACTIVE = Status.new(
          id: 1,
          name: 'Inactive'
        )

        def self.find(id)
          case id
          when 1
            ACTIVE
          when 2
            INACTIVE
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end

      class OblipAccountBank
        UNVERIFIED = Status.new(
          id: 1,
          name: 'Unverified'
        )

        VERIFIED = Status.new(
          id: 2,
          name: 'Verified'
        )

        DENIED = Status.new(
          id: 3,
          name: 'Denied'
        )

        def self.find(id)
          case id
          when 1
            UNVERIFIED
          when 2
            VERIFIED
          when 3
            DENIED
          else
            raise StandardError.new('Invalid ID')
          end
        end
      end


      STATUS_NAME = {
        transaction: {
          405 => 'Denied',
          100 => 'Pending Action',
          102 => 'Processing',
          200 => 'Completed'
        }
      }

      def self.build(object_sym, status_id)
        new(
          id: status_id,
          name: STATUS_NAME[object_sym][status_id]
        )
      end
    end
  end
end
