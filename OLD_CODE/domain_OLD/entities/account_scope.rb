# frozen_string_literal: false

require 'dry-struct'

require_relative 'oblip_account'

module Oblip
  module Entity
    # Domain entity object for authenticated account
    class AccountScope 
      
      module App
        ALL_PURPOSE = '<app_all_purpose>'
      end

      module Admin

      end
    end
  end
end
