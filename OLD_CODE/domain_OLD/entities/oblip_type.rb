# frozen_string_literal: false

require 'dry-struct'

module Oblip
  module Entity
    # Domain entity object for Oblip Account Type
    class OblipType < Dry::Struct

      TYPES = {
        oblip_account: {
          1 => 'Personal',
          2 => 'Agent',
          3 => 'Business',
          4 => 'Issuer',
          5 => 'Staff'
        },
        transaction: {
          1 => 'Deposit',
          2 => 'Payment',
          3 => 'Withdrawal',
          4 => 'Issuer Deposit'
        },
        bank_order: {
          1 => 'Deposit',
          2 => 'Withdrawal'
        },
        asset: {
          1 => 'Native',
          2 => 'Oblip Asset',
          3 => 'Other'
        }
      }

      attribute :id, Types::Strict::Integer.optional
      attribute :name, Types::Strict::String.optional

      def self.build(object_sym, type_id ) 
        new(
          id: type_id,
          name: TYPES[object_sym][type_id]
        )
      end
    end
  end
end
