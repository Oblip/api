# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for ApiClients
    class ApiClients
      def self.all
        Database::ApiClientOrm.all
          .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::ApiClientOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.is_credentials_valid?(client_id, secret_code)
        db_record = Database::ApiClientOrm.find(id: client_id)
        if db_record && db_record.is_secret_code_valid?(secret_code) &&
            db_record.is_verified
          rebuild_entity(db_record)
        else
          false
        end
      end

      def self.update_verification_flag(client_id, flag)
        db_record = Database::ApiClientOrm.find(id: client_id)
        db_record.is_verified = flag
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::ApiClientOrm.create(
          oblip_account_id: entity.oblip_account_id,
          secret_code: entity.secret_code,
          public_key: entity.public_key,
          fcm_token: entity.fcm_token,
          verification_token: entity.verification_token,
          platform_type: entity.platform_type,
          status: entity.status.id,
          type: entity.type.id,
          last_used: Time.now,
          last_ip_address: entity.last_ip_address
        )

        rebuild_entity(db_record)
      end

      def self.link_account(client_entity)
        db_record = Database::ApiClientOrm.find(id: client_entity.id)
        db_record.oblip_account_id = client_entity.oblip_account_id if client_entity.type == Entity::ApiClient::Types::USER
        # TODO: add staff_id and developer_id

        db_record.secret_code = client_entity.secret_code
        # db_record.expiry_date = client_entity.expiry_date
        db_record.status = client_entity.status.id
        db_record.save

        rebuild_entity(db_record)
      end
      

      def self.update_status(id, status)
        db_record = Database::ApiClientOrm.find(id: id)
        db_record.status = status.id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.save_verification_token(client_id, token)
        db_record = Database::ApiClientOrm.find(id: client_id)
        db_record.verification_token = token
        db_record.save
        
        rebuild_entity(db_record)
      end

      def self.update(entity)
        db_record = Database::ApiClientOrm.find(id: entity.id)
        oblip_account_id =  entity.oblip_account_id,
        #secret_code = entity.secret_code,
        # expiry_date = entity.expiry_date,
        public_key = entity.public_key,
        fcm_token =  entity.fcm_token,
        platform_type = entity.platform_type,
        status = entity.status.id,
        type = entity.type.id
        last_used = Time.now,
        last_ip_address = entity.last_ip_address
        rebuild_entity(db_record)
      end

      def self.update_last_seen(id, last_ip_address)
        db_record = Database::ApiClientOrm.find(id: id)
        db_record.last_used = Time.now
        db_record.last_ip_address = last_ip_address
        db_record.save

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        status = Entity::ApiClient::Status.find_id(db_record.status)
        type = Entity::ApiClient::Types.find_id(db_record.type)

        Entity::ApiClient.new(
          id: db_record.id,
          oblip_account_id: db_record.oblip_account_id,
          secret_code: db_record.secret_code,
          # expiry_date: db_record.expiry_date,
          public_key: db_record.public_key,
          last_attempted_verification: db_record.last_attempted_verification,
          verification_attempts: db_record.verification_attempts,
          platform_type: db_record.platform_type,
          fcm_token: db_record.fcm_token,
          verification_token: db_record.verification_token,
          is_verified: db_record.is_verified,
          status: status,
          type: type,
          last_used: db_record.last_used,
          last_ip_address: db_record.last_ip_address
        )
      end
    end
  end
end
