# frozen_string_literal: true

require 'rotp'

module Oblip
  module Repository
    # Repository for Staff Accounts
    class StaffAccounts
      def self.all
        Database::StaffAccountOrm.all.map do |db_account|
          rebuild_entity(db_account)
        end
      end

      def self.find_id(id)
        db_record = Database::StaffAccountOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_username(username)
        db_record = Database::StaffAccountOrm.find(username: username)
        rebuild_entity(db_record)
      end


      def self.credentials_valid?(username, password, otp)
        db_record = Database::StaffAccountOrm.find(username: username)
        
        raise StandardError, 'No username found' unless db_record
        raise StandardError, 'Password is Wrong' unless db_record.password?(password)

        issuer_type = ENV['RACK_ENV'] ? ENV['RACK_ENV'] : 'development'
        totp = ROTP::TOTP.new(db_record.otp_secret, issuer: "Oblip Staff #{issuer_type}")
        raise StandardError, 'Could not verify otp' unless totp.verify(otp)

        rebuild_entity(db_record)

      rescue StandardError => error
        puts error.message
        puts error.inspect
        puts error.backtrace
        nil
      end

      def self.super_admin_count
        Database::StaffAccountOrm.where(type: 100).count
      end

      def self.save_photo_url(id, url)
        db_record = Database::StaffAccountOrm.find(id: id)
        db_record.photo_url = url
        db_record.save

        rebuild_entity(db_record)
      end
      
      def self.create(entity, password, otp_secret = ROTP::Base32.random_base32)
        db_record = Database::StaffAccountOrm.create(
          username: entity.username,
          email_address: entity.email_address,
          password: password,
          otp_secret:otp_secret,
          first_name: entity.first_name,
          middle_name: entity.middle_name,
          last_name: entity.last_name,
          type: entity.type.id,
          email_verified: entity.email_verified,
          permissions: entity.permissions,
          status: entity.status.id,
        )

        rebuild_entity(db_record)
      end

      # rubocop:disable MethodLength
      def self.rebuild_entity(db_record)
        return nil unless db_record
        
        type = Entity::StaffAccount::Type.find_id(db_record.type)
        status = Entity::StaffAccount::Status.find_id(db_record.status)

        Entity::StaffAccount.new(
          id: db_record.id,
          username: db_record.username,
          email_address: db_record.email_address,
          email_verified: db_record.email_verified,
          first_name: db_record.first_name,
          middle_name: db_record.middle_name,
          last_name: db_record.last_name,
          permissions: db_record.permissions,
          type: type,
          status: status,
          photo_url: db_record.photo_url
        )
      end
      # rubocop:enable MethodLength
    end
  end
end
