# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Currencies
    class CompanyBankAccounts
      def self.all
        Database::CompanyBankAccountOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::CompanyBankAccountOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_by_bank(bank_id)
        db_record = Database::CompanyBankAccountOrm.find(bank_id: bank_id)
        rebuild_entity(db_record)
      end

      def self.find_many_by_country(country_id)
        bank_ids = Database::BankOrm.where(
          country_id: country_id).select_map(:id)
        db_records = Database::CompanyBankAccountOrm.where(bank_id: bank_ids).reverse(:created_at).all
        db_records.map { |record| rebuild_entity(record) }
      end


      def self.rebuild_entity(db_record)
        return nil unless db_record
        bank = Banks.find_id(db_record.bank_id)
        status = Entity::Status::CompanyBankAccount.find(db_record.status)
        
        Entity::CompanyBankAccount.new(
          id: db_record.id,
          account_number: db_record.account_number,
          bank: bank,
          status: status
        )
      end
    end
  end
end
