# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Oblip Locations
    class OblipLocations
      def self.all
        Database::OblipLocationOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::OblipLocationOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        oblip_account = OblipAccounts.find_id(db_record.oblip_account_id)

        Entity::OblipLocation.new(
          id: db_record.id,
          oblip_account: oblip_account,
          name: db_record.name,
          description: db_record.description,
          address: db_record.address,
          lat: db_record.lat,
          lng: db_record.lng
        )
      end
    end
  end
end
