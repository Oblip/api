# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Blockchain Funds
    class BlockchainFunds
      def self.all
        Database::BlockchainFundOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::BlockchainFundOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_asset_name(asset_name)
        records = Database::BlockchainFundOrm.where(asset_name: asset_name).all
        records.map { |record| rebuild_entity(record) }
      end

      def self.find_optimal_fund(asset_name, min_amount)
        db_record = Database::BlockchainFundOrm.filter{(balance >= min_amount)}.where(asset_name: asset_name).first
        rebuild_entity(db_record)
      end

      def self.update_balance(b_fund_id, amount_change)
        db_record = Database::BlockchainFundOrm.find(id: b_fund_id)
        db_record.balance += amount_change
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::BlockchainFundOrm.create(
          amount_purchased: entity.amount_purchased,
          price_purchased: entity.price_purchased,
          balance: entity.amount_purchased,
          incurred_fees: entity.incurred_fees,
          asset_name: asset_name,
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record
        
        Entity::BlockchainFund.new(
          id: db_record.id,
          amount_purchased: db_record.amount_purchased,
          price_purchased: db_record.price_purchased,
          balance: db_record.balance,
          incurred_fees: db_record.incurred_fees,
          asset_name: db_record.asset_name
        )
      end
    end
  end
end
