# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Contacts
    class Contacts
      def self.all
        Database::ContactOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::ContactOrm.find(id: id)
        rebuild_entity(db_record)
      end

      # TODO: remove? We don't need this
      def self.contact_exists?(account_id, other_account_id)
        db_record = Database::ContactOrm.find(
          owner_id: account_id,
          contact_id: other_account_id
        )

        rebuild_entity(db_record)
      end

      def self.create(owner_id, person_to_add_id)
        db_record = Database::ContactOrm.create(
          owner_id: owner_id,
          contact_id: person_to_add_id
        )   
        rebuild_entity(db_record)
      end

      def self.delete(id)
        db_record = Database::ContactOrm.find(id: id)
        entity = rebuild_entity(db_record)
        db_record.delete
        entity
      end

      def self.find_by_owner(owner_id, other_account_id)
        db_record = Database::ContactOrm.find(
          owner_id: owner_id, contact_id: other_account_id
        )
        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        contact = OblipAccounts.find_id(db_record.contact_id)

        Entity::Contact.new(
          id: db_record.id,
          owner_id: db_record.owner_id,
          contact: contact
        )
      end
    end
  end
end
