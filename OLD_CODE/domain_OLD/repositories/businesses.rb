# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Blockchain Funds
    class Businesses

      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0

      def self.all
        Database::BusinessOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::BusinessOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_all_businesses(offset, limit)
        db_records = Database::BusinessOrm.reverse(:created_at)
        .limit(limit ? limit : DEFAULT_LIMIT)
            .offset(offset ? offset : DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.find_by_oblip_account(id)
        db_record = Database::BusinessOrm.find(oblip_account_id: id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::BusinessOrm.create(
          brand_name: entity.brand_name,
          registration_name: entity.registration_name,
          address: entity.address,
          oblip_account_id: entity.oblip_account_id,
          logo_url: entity.logo_url
        )

        if(db_record)
          account_record = Database::OblipAccountOrm.find(id: entity.oblip_account_id)
          account_record.business_id = db_record.id
          account_record.save
        end

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record
        
        Entity::Business.new(
          id: db_record.id,
          brand_name: db_record.brand_name,
          is_verified: db_record.is_verified,
          registration_name: db_record.registration_name,
          address: db_record.address,
          oblip_account_id: db_record.oblip_account_id,
          logo_url: db_record.logo_url,
          created_at: db_record.created_at,
          updated_at: db_record.updated_at
        )
      end
    end
  end
end
