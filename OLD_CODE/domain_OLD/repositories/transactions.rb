# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Transactions
    class Transactions
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0

      def self.all
        Database::TransactionOrm.all.map do |db_record|
          rebuild_entity(db_record)
        end
      end

      def self.find_from_oblip_account(oblip_account_id:, offset:, limit:)
        db_records = Database::TransactionOrm.where(
          sender_account_id: oblip_account_id)
            .or(recipient_account_id: oblip_account_id).reverse(:created_at)
              .limit(limit ? limit : DEFAULT_LIMIT)
                .offset(offset ? offset : DEFAULT_OFFSET).all
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.find_between_oblip_accounts(first_id:, second_id:, offset:, limit:)
        # TODO: check this out - https://dba.stackexchange.com/questions/34266/selecting-where-two-columns-are-in-a-set
        db_records =  Database::TransactionOrm.where(sender_account_id: first_id,
          recipient_account_id: second_id).or(sender_account_id: second_id, recipient_account_id: first_id)
            .reverse(:created_at).limit(limit ? limit : DEFAULT_LIMIT)
              .offset(offset ? offset : DEFAULT_OFFSET).all
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.find_id(id)
        db_record = Database::TransactionOrm.find(id: id)
        rebuild_entity(db_record)
      end

      # REFACTOR
      def self.change_status(tx_id, status_entity)
        db_record = Database::TransactionOrm.find(id: tx_id)
        return nil unless db_record
        db_record.status = status_entity.id
        db_record.save
        rebuild_entity(db_record)
      end

      def self.save_vault_tx_data(tx_id, tx_hash, signed_tx_envelope, funding_pool, network_fee)
        db_record = Database::TransactionOrm.find(id: tx_id)
        return nil unless db_record
        db_record.vault_tx_hash = hash if tx_hash
        db_record.signed_tx_envelope = signed_tx_envelope if signed_tx_envelope
        db_record.network_fee_funding_id = funding_pool.id if funding_pool
        db_record.network_fee = network_fee if network_fee
        db_record.save
        rebuild_entity(db_record)
      end

      def self.save_sender_balance(tx_id, balance)
        db_record = Database::TransactionOrm.find(id: tx_id)
        db_record.sender_asset_balance = balance
        db_record.save

        rebuild_entity(db_record)
      end

      def self.save_anchor_escrow(tx_id, escrow)
        db_record = Database::TransactionOrm.find(id: tx_id)
        db_record.anchor_escrow_fund_id = escrow.id
        db_record.save

        rebuild_entity(db_record)
      end

      def update_remarks(tx_id, remarks)
        db_record = Database::TransactionOrm.find(id: tx_id)
        db_record.remarks = remarks
        db_record.save

        rebuild_entity(db_record)
      end

      def self.save_agent_escrow(tx_id, escrow)
        db_record = Database::TransactionOrm.find(id: tx_id)
        db_record.agent_escrow_fund_id = escrow.id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update(entity)
        db_record = Database::TransactionOrm.find(id: entity.id)
        db_record.network_fee = entity.network_fee if entity.network_fee
        db_record.network_fee_funding_id = entity.funding_pool ? entity.funding_pool.id : db_record.network_fee_funding_id
        db_record.vault_tx_hash = entity.vault_tx_hash if entity.vault_tx_hash
        db_record.signed_tx_envelope = entity.signed_tx_envelope if entity.signed_tx_envelope
        db_record.sender_asset_balance = entity.sender_asset_balance if entity.sender_asset_balance
        db_record.recipient_asset_balance = entity.recipient_asset_balance if entity.recipient_asset_balance
        db_record.remarks = entity.remarks if entity.remarks
        db_record.status = entity.status ? entity.status.id : db_record.status 
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::TransactionOrm.create(
          anchor_asset_code: entity.anchor_asset.code,
          amount: entity.amount,
          anchor_fee: entity.anchor_fee,
          agent_fee: entity.agent_fee,
          network_fee: entity.network_fee,
          network_fee_funding_id:  entity.funding_pool ? entity.funding_pool.id : nil,
          anchor_escrow_fund_id: entity.anchor_escrow_fund ? entity.anchor_escrow_fund.id : nil,
          agent_escrow_fund_id: entity.agent_escrow_fund ? entity.agent_escrow_fund.id : nil,
          exchange_rate: entity.exchange_rate,
          sender_account_id: entity.sender_account ? entity.sender_account.id : nil,
          recipient_account_id: entity.recipient_account ? entity.recipient_account.id : nil,
          sender_asset_balance: entity.sender_asset_balance,
          recipient_asset_balance: entity.recipient_asset_balance,
          type: entity.type.id,
          remarks: entity.remarks,
          signed_tx_envelope: entity.signed_tx_envelope,
          vault_tx_hash: entity.vault_tx_hash,
          status: entity.status.id
        )

        rebuild_entity(db_record)
      end
      
      # rubocop:disable Metrics/LineLength
      def self.rebuild_entity(db_record)
        return nil unless db_record

        status = Entity::Status::Transaction.find(db_record.status)
        anchor_asset = AnchorAssets.find_code(db_record.anchor_asset_code)
        sender_account = OblipAccounts.find_id(db_record.sender_account_id)
        recipient_account = OblipAccounts.find_id(db_record.recipient_account_id)
        funding_pool = BlockchainFunds.find_id(db_record.network_fee_funding_id)
        type = Entity::OblipType.build(:transaction, db_record.type)

        anchor_escrow_fund = EscrowFunds.find_id(db_record.anchor_escrow_fund_id)
        agent_escrow_fund = EscrowFunds.find_id(db_record.agent_escrow_fund_id)


        Entity::Transaction.new(
          id: db_record.id,
          anchor_asset: anchor_asset,
          amount: db_record.amount,
          anchor_fee_breakdown: nil,
          anchor_fee: db_record.anchor_fee,
          agent_fee: db_record.agent_fee,
          network_fee: db_record.network_fee,
          anchor_escrow_fund: anchor_escrow_fund,
          agent_escrow_fund: agent_escrow_fund,
          funding_pool: funding_pool,
          requires_sender_signature: nil,
          exchange_rate: db_record.exchange_rate,
          sender_account: sender_account,
          recipient_account: recipient_account,
          signed_tx_envelope: db_record.signed_tx_envelope,
          unsigned_tx_envelope: nil,
          sender_asset_balance: db_record.sender_asset_balance,
          recipient_asset_balance: db_record.recipient_asset_balance,
          remarks: db_record.remarks,
          type: type,
          vault_tx_hash: db_record.vault_tx_hash,
          status: status,
          created_at: db_record.created_at.strftime("%F %I:%M %p")
        )
      end
      # rubocop:enable Metrics/LineLength
    end
  end
end
