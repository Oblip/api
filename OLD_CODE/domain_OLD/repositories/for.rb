# frozen_string_literal: true

module Oblip
  module Repository
    For = {
      Entity::Country => Countries,
      Entity::Vault => Vaults,
      Entity::Status => Statuses,
      Entity::AnchorAsset => AnchorAssets,
      Entity::Bank => Banks,
      Entity::PersonalDetail => PersonalDetails,
      Entity::OblipAccount => OblipAccounts,
      Entity::Contact => Contacts,
      Entity::OblipAccountBank => OblipAccountBanks,
      Entity::OblipLocation => OblipLocations,
      Entity::Transaction => Transactions,
      Entity::CompanyBankAccount => CompanyBankAccounts,
      Entity::DepositOrder => DepositOrders,
      Entity::WithdrawalOrder => WithdrawalOrders,
      Entity::VaultAsset => VaultAssets
    }.freeze
  end
end
