# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Payouts
    class Payouts
      def self.all
        Database::PayoutOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::PayoutOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_oblip_account_id(id)
        db_record = Database::PayoutOrm.find(oblip_account_id: id)
        rebuild_entity(db_record)
      end

      def self.find_by_deposit_id(tx_id)
        db_record = Database::PayoutOrm.find(tx_deposit_id: tx_id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::PayoutOrm.create(
          tx_deposit_id: entity.deposit_transaction.id,
          oblip_account_id: entity.payee.id,
          anchor_asset_code: entity.anchor_asset.code,
          amount: entity.amount,
          type: entity.type.id,
          status: entity.status.id
        )

        rebuild_entity(db_record)
      end

      def self.update_amount(payout_id, amount_change)
        db_record = Database::PayoutOrm.find(id: payout_id)
        db_record.amount += amount_change
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_status(id, status)
        db_record = Database::PayoutOrm.find(id: id)
        db_record.status = status.id
        db_record.save

        rebuild_entity(db_record)
      end


      def self.rebuild_entity(db_record)
        return nil unless db_record

        status = Entity::Status::AgentCommission.find(db_record.status)
        deposit_transaction = Repository::Transactions.find_id(db_record.tx_deposit_id)
        payee = Repository::OblipAccounts.find_id(db_record.oblip_account_id)
        anchor_asset = Repository::AnchorAssets.find_code(db_record.anchor_asset_code)
        type = Entity::Payout::Types.find_id(db_record.type)

        Entity::AgentCommission.new(
          id: db_record.id,
          deposit_transaction: deposit_transaction,
          payee: payee,
          anchor_asset: anchor_asset,
          type: type,
          amount: db_record.amount,
          status: status
        )
      end
    end
  end
end
