# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Personal Details
    class PersonalDetails
      def self.all
        Database::PersonalDetailOrm.all.map do |db_account|
          rebuild_entity(db_account)
        end
      end

      def self.find_id(id)
        db_record = Database::PersonalDetailOrm.find(id: id)
        rebuild_entity(db_record)
      end

      # def self.find_by_account_id(id)
      #   account = Accounts.find_id(id)
      #   find_id(account.personal_detail_id)
      # end

      def self.delete(id)
        detail = Database::PersonalDetailOrm.find(id: id)
        detail.delete
      end

      # def self.update_by_account_id(account_id, entity)
      #   account = Accounts.find_id(account_id)
      #   update(account.personal_detail_id, entity)
      # end

      # rubocop:disable MethodLength
      def self.update(id, entity)
        detail = Database::PersonalDetailOrm.find(id: id)
        detail.first_name = entity.first_name || detail.first_name
        detail.middle_name = entity.middle_name || detail.middle_name
        detail.last_name = entity.last_name || detail.last_name
        detail.gender = entity.gender || detail.gender
        detail.birthday = (entity.birthday ? entity.birthday.to_s : nil) || detail.birthday
        detail.photo_url = entity.photo_url || detail.photo_url
        detail.save(raise_on_failure: true)

        rebuild_entity(detail)
      end
      # rubocop:enable MethodLength

      def self.create(entity)
        db_details = Database::PersonalDetailOrm.new(
          first_name: entity.first_name,
          middle_name: entity.middle_name,
          last_name: entity.last_name,
          gender: entity.gender,
          country_id: entity.country.id,
          birthday: entity.birthday.to_s,
          photo_url: entity.photo_url
        )

        db_details.save(raise_on_failure: true)

        rebuild_entity(db_details)
      end

      # rubocop:disable MethodLength
      def self.rebuild_entity(db_record)
        return nil unless db_record
        country_id = db_record.country_id
        country = db_record.country_id ? Countries.find_id(country_id) : nil
        dob = db_record.birthday ? Date.parse(db_record.birthday) : nil

        Entity::PersonalDetail.new(
          id: db_record.id,
          first_name: db_record.first_name,
          middle_name: db_record.middle_name,
          last_name: db_record.last_name,
          gender: db_record.gender,
          birthday: dob,
          country: country,
          photo_url: db_record.photo_url
        )
      end
      # rubocop:enable MethodLength
    end
  end
end
