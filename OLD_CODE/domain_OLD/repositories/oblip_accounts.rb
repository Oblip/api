# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Accounts
    class OblipAccounts
      VERIFY_PHONE_NUMBER = 'is_phone_verified'
      VERIFY_EMAIL_ADDRESS = 'is_email_verified'
      
      module Errors
        class InvalidValueError < StandardError; end
      end

      def self.all
        Database::OblipAccountOrm.all
        .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::OblipAccountOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.get_total_amount_transacted(account_id)
        result = Database::TransactionOrm.where(sender_account_id: account_id)
          .sum(:amount)
        result.nil? ? 0 : result
      end

      def self.get_all_contacts(account_id)
        owner = Database::OblipAccountOrm.find(id: account_id)
        owner.contacts.map { |record| Contacts.rebuild_entity(record) }
      end

      # def self.get_all_devices(account_id)
      #   owner = Database::OblipAccountOrm.find(id: account_id)
      #   owner.devices.map { |record| Devices.rebuild_entity(record) }
      # end

      def self.get_all_clients(account_id)
        owner = Database::OblipAccountOrm.find(id: account_id)
        owner.clients.map { |record| ApiClients.rebuild_entity(record) }
      end

      def self.get_all_bank_accounts(account_id)
        owner = Database::OblipAccountOrm.find(id: account_id)
        owner.banks.map { |record| OblipAccountBanks.rebuild_entity(record) }
      end

      def self.get_all_deposit_orders(account_id)
        db_records = Database::DepositOrderOrm.where(oblip_account_id: account_id)
          .reverse(:created_at).all

        db_records.map { |record| DepositOrders.rebuild_entity(record) }
      end

      def self.get_all_withdrawal_orders(account_id)
        db_records = Database::WithdrawalOrderOrm.where(oblip_account_id: account_id)
          .reverse(:created_at).all

        db_records.map { |record| WithdrawalOrders.rebuild_entity(record) }
      end

      def self.get_all_locations(account)
        account.locations.map { |record| OblipLocations.rebuild_entity(record) }
      end

      def self.get_personal_details(account_id)
        account = find_id(account_id)
        account.personal_details
      end

      def self.update_personal_details(account_id, personal_details_entity)
        account = find_id(account_id)
        PersonalDetails.update(account.personal_details.id, personal_details_entity)
      end

      def self.create_vault(account_id, vault_entity)
        vault = Repository::For[Entity::Vault].create(vault_entity)
        account = Database::OblipAccountOrm.find(id: account_id)
        account.vault_id = vault.id
        account.save
        vault
      end

      def self.find_vault(account_id)
        account = find_id(account_id)
        Vaults.find_id(account.vault_id)
      end

      def self.delete(id)
        account = Database::OblipAccountOrm.find(id: id)
        account.delete
      end

      def self.find_phone_number(phone_number)
        hashed = SecureDB.hash_content(phone_number)
        db_record = Database::OblipAccountOrm.find(phone_number_hash: hashed)
        rebuild_entity(db_record)
      end

      def self.add_business(id, business_id)
        db_record = Database::OblipAccountOrm.find(id: id)
        db_record.business_id = business_id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(detail_entity:, account_entity:, client_entity:)
        # Api.DB.transaction(prepare: 'create_oblip_account') do
        Api.DB.transaction do
          detail = PersonalDetails.create(detail_entity)
          account_entity = account_entity.new(personal_details: detail)
          account = create_account(account_entity)

          client_entity = client_entity.new(oblip_account_id: account.id)
          # link oblip account to device
          ApiClients.link_account(client_entity)
          account
        end
        # Api.DB.commit_prepared_transaction('create_oblip_account')
      end

      # rubocop:disable MethodLength
      def self.rebuild_entity(db_record)
        return nil unless db_record

        status = Entity::OblipAccount::Status.find(db_record.status)
        detail = PersonalDetails.find_id(db_record.personal_detail_id)
        type = Entity::OblipAccount::Types.find(db_record.type)

        base_anchor_asset = Repository::AnchorAssets.find_code(db_record.base_anchor_asset_code)
        business = Repository::Businesses.find_id(db_record.business_id)

        Entity::OblipAccount.new(
          id: db_record.id,
          status: status,
          type: type,
          business: business,
          personal_details: detail,
          is_phone_verified: db_record.is_phone_verified,
          phone_number: db_record.phone_number,
          kyc_level: db_record.kyc_level,
          base_anchor_asset: base_anchor_asset,
          vault_id: db_record.vault_id
        )
      end
      # rubocop:enable MethodLength

      private_class_method

      def self.create_account(entity)
        raise OException::DuplicationError if !find_phone_number(entity.phone_number).nil?
        
        db_account = Database::OblipAccountOrm.new(
          phone_number: entity.phone_number,
          type: entity.type.id,
          status: entity.status.id,
          base_anchor_asset_code: entity.base_anchor_asset.code,
          personal_detail_id: entity.personal_details.id
        )

        db_account.save(:raise_on_failure=>true)


        rebuild_entity(db_account)
      end
    end
  end
end
