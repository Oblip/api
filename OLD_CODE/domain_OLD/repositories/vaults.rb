# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Token Vaults
    class Vaults

      def self.find_id(id)
        db_record = Database::VaultOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_by_address(address)
        db_record = Database::VaultOrm.find(address: address)
        rebuild_entity(db_record)
      end

      def self.add_or_update_vault_and_assets_using_raw(vault_id, blockchain_data)
        update_sequence_number(vault_id, blockchain_data.sequence_number)
        update_paid_status(vault_id, blockchain_data.is_paid) if blockchain_data.is_paid
        
        blockchain_data.assets.map do |asset|
          exists = Vaults.find_vault_asset(vault_id, asset.code)
          VaultAssets.create(asset.new(vault_id: vault_id)) unless exists
          VaultAssets.update_balance(vault_id, asset.code, asset.balance) if exists
        end

        find_id(vault_id)
      end

      def self.update_sequence_number(vault_id, sequence_number)
        db_record = Database::VaultOrm.find(id: vault_id)
        db_record.sequence_number = sequence_number
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_paid_status(vault_id, paid_status)
        db_record = Database::VaultOrm.find(id: vault_id)
        db_record.is_paid = paid_status
        db_record.save

        rebuild_entity(db_record)
      end

      def self.find_all_assets(vault_id)
        db_records = Database::VaultAssetOrm.where(vault_id: vault_id).all
        db_records.map { |record| VaultAssets.rebuild_entity(record) }
      end

      def self.find_vault_asset(vault_id, asset_code)
        db_record = Database::VaultAssetOrm.find(vault_id: vault_id, code: asset_code)
        VaultAssets.rebuild_entity(db_record)
      end

      def self.delete(id)
        vault = Database::VaultOrm.find(id: id)
        vault.delete
      end

      def self.create(entity)
        db_record = Database::VaultOrm.new(
          address: entity.address,
          blockchain_source_fund_id: entity.blockchain_source_fund_id,
          initial_operation_cost: entity.initial_operation_cost,
          initial_native_funds: entity.initial_native_funds,
          is_paid: entity.is_paid,
          salt: entity.salt,
          sequence_number: entity.sequence_number
        )

        db_record.save

        rebuild_entity(db_record)
      end

     
      def self.rebuild_entity(db_record)
        return nil unless db_record
        
        assets = db_record.assets.map { |r| Repository::VaultAssets.rebuild_entity(r) }

        Entity::Vault.new(
          id: db_record.id,
          address: db_record.address,
          blockchain_source_fund_id: db_record.blockchain_source_fund_id,
          initial_operation_cost: db_record.initial_operation_cost,
          initial_native_funds: db_record.initial_native_funds,
          is_paid: db_record.is_paid,
          salt: db_record.salt,
          sequence_number: db_record.sequence_number,
          assets: assets
        )
      end

    end
  end
end
