# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Anchor Assets
    class AnchorAssets
      def self.all
        Database::AnchorAssetOrm.all
            .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::AnchorAssetOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_code(code)
        db_record = Database::AnchorAssetOrm.find(code: code)
        rebuild_entity(db_record)
      end

      def self.update_volume(asset_code, amount_change)
        db_record = Database::AnchorAssetOrm.find(code: asset_code)
        db_record.volume += amount_change
        db_record.save

        rebuild_entity(db_record)
      end

      # def self.find_by_country(code)
      #   db_record = Database::AnchorAssetOrm.find(country_code: code)
      #   rebuild_entity(db_record)
      # end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::AnchorAsset.new(
          id: db_record.id,
          code: db_record.code,
          name: db_record.name,
          symbol: db_record.symbol,
          issuer: db_record.issuer,
          is_tethered: db_record.is_tethered,
          current_exchange_rate: db_record.current_exchange_rate,
          volume: db_record.volume
        )
      end
    end
  end
end
