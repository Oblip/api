# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Currencies
    class OblipAccountBanks
      def self.all
        Database::OblipAccountBankOrm.all
        .map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::OblipAccountBankOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.account_exists?(oblip_account_id, bank_id, account_number)
        db_record = Database::OblipAccountBankOrm.find(
          oblip_account_id: oblip_account_id,
          bank_id: bank_id,
          account_number: account_number
        )

        rebuild_entity(db_record)
      end

      def self.unverified_count_by_oblip_account(oblip_account_id)
        Database::OblipAccountBankOrm.where(
          oblip_account_id: oblip_account_id,
          status: Entity::Status::OblipAccountBank::UNVERIFIED.id
        ).count
      end

      def self.update_status(bank_account_id, status_entity)
        db_record = Database::OblipAccountBankOrm.find(id: bank_account_id)
        db_record.status = status_entity.id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::OblipAccountBankOrm.create(
          bank_id: entity.bank.id,
          oblip_account_id: entity.oblip_account_id,
          account_number: entity.account_number,
          status: entity.status.id
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        bank = Banks.find_id(db_record.bank_id)
        status = Entity::Status::OblipAccountBank.find(db_record.status)

        Entity::OblipAccountBank.new(
          id: db_record.id,
          bank: bank,
          oblip_account_id: db_record.oblip_account_id,
          account_number: db_record.account_number,
          status: status
        )
      end
    end
  end
end
