# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Bank Orders
    class EscrowFunds
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0
      
      def self.all
        Database::EscrowFundOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::EscrowFundOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_all_account_escrow_funds(account_id, offset, limit)
        db_records = Database::EscrowFundOrm.where(oblip_account_id: account_id)
          .reverse(:created_at).limit(limit ? limit : DEFAULT_LIMIT)
            .offset(offset ? offset : DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.find_all_escrow_funds(offset, limit)
        db_records = Database::EscrowFundOrm.reverse(:created_at)
          .limit(limit ? limit : DEFAULT_LIMIT)
              .offset(offset ? offset : DEFAULT_OFFSET).all
        
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.find_account_open_escrow_fund(account_id, asset_code)
        db_record = Database::EscrowFundOrm.find(oblip_account_id: account_id,
          anchor_asset_code: asset_code, status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION.id,
          is_locked: false)

        rebuild_entity(db_record)
      end

      def self.find_open_anchor_escrow_fund(asset_code)
        db_record = Database::EscrowFundOrm.find(anchor_asset_code: asset_code, belongs_to_anchor: true,
          status: Entity::EscrowFund::Status::PENDING_FULL_COLLECTION.id, is_locked: false)
        
        rebuild_entity(db_record)
      end

      def self.update_funds_payable(escrow_id, amount_change)
        db_record = Database::EscrowFundOrm.find(id: escrow_id)
        db_record.funds_payable += amount_change
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_funds_collected(escrow_id, amount_change)
        db_record = Database::EscrowFundOrm.find(id: escrow_id)
        db_record.funds_collected += amount_change
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_balance(escrow_id, amount_change)
        db_record = Database::EscrowFundOrm.find(id: escrow_id)
        db_record.balance += amount_change
        db_record.save

        rebuild_entity(db_record)
      end

      def self.update_status(escrow_id, status_entity)
        db_record = Database::EscrowFundOrm.find(id: escrow_id)
        db_record.status = status_entity.id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::EscrowFundOrm.create(
          anchor_asset_code: entity.anchor_asset.code,
          oblip_account_id: entity.oblip_account ? entity.oblip_account.id : nil,
          funds_payable: entity.funds_payable,
          funds_collected: entity.funds_collected,
          balance: entity.balance,
          belongs_to_anchor: entity.belongs_to_anchor,
          is_locked: entity.is_locked,
          status: entity.status.id
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        anchor_asset = AnchorAssets.find_code(db_record.anchor_asset_code)
        oblip_account = OblipAccounts.find_id(db_record.oblip_account_id)
        status = Entity::EscrowFund::Status.find_id(db_record.status)
        
        Entity::EscrowFund.new(
          id: db_record.id,
          anchor_asset: anchor_asset,
          oblip_account: oblip_account,
          funds_payable: db_record.funds_payable,
          funds_collected: db_record.funds_collected,
          balance: db_record.balance,
          belongs_to_anchor: db_record.belongs_to_anchor,
          is_locked: db_record.is_locked,
          status: status
        )
      end
    end
  end
end
