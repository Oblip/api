# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Vault Assets
    class VaultAssets
      def self.all
        Database::VaultAssetOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::VaultAssetOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_code(vault_id, code)
        db_record = Database::VaultAssetOrm.find(vault_id: vault_id, code: code)
        rebuild_entity(db_record)
      end

      def self.update_balance(vault_id, code, new_amount)
        db_record = Database::VaultAssetOrm.find(vault_id: vault_id, code: code)
        db_record.balance = new_amount
        db_record.save
        
        rebuild_entity(db_record)
      end

      def self.update_authorization(vault_id, code, flag)
        db_record = Database::VaultAssetOrm.find(vault_id: vault_id, code: code)
        db_record.authorized = flag
        db_record.save

        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::VaultAssetOrm.create(
          vault_id: entity.vault_id,
          name: entity.name,
          code: entity.code,
          balance: entity.balance,
          type: entity.type.id,
          authorized: entity.authorized,
          issuer: entity.issuer
        )

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        type = Entity::VaultAsset::Types.find_id(db_record.type)

        Entity::VaultAsset.new(
          id: db_record.id,
          vault_id: db_record.vault_id,
          name: db_record.name,
          code: db_record.code,
          balance: db_record.balance,
          type: type,
          authorized: db_record.authorized,
          issuer: db_record.issuer
        )
      end
    end
  end
end
