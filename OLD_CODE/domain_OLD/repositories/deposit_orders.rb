# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Bank Orders
    class DepositOrders
      DEFAULT_LIMIT = 15
      DEFAULT_OFFSET = 0

      def self.all
        Database::DepositOrderOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::DepositOrderOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.get_total_account_orders(account_id)
        result = Database::DepositOrderOrm.where(oblip_account_id: account_id)
          .exclude(status: [Entity::DepositOrder::Status::COMPLETED.id,
            Entity::DepositOrder::Status::FAILED.id])
          .count
        result.nil? ? 0 : result
      end

      def self.find_by_transaction(tx_id)
        db_record = Database::DepositOrderOrm.find(transaction_id: tx_id)
        rebuild_entity(db_record)
      end

      def self.find_all_deposit_orders(offset, limit)
        db_records = Database::DepositOrderOrm.reverse(:created_at)
          .limit(limit ? limit : DEFAULT_LIMIT)
              .offset(offset ? offset : DEFAULT_OFFSET).all

        db_records.map { |record| rebuild_entity(record) }
      end

      def self.create(entity)
        db_record = Database::DepositOrderOrm.create(
          oblip_account_id: entity.oblip_account_id,
          transaction_id: entity.transaction.id,
          company_bank_account_id: entity.bank_account.id,
          deposit_method: entity.deposit_method.id,
          status: entity.status.id
        )

        rebuild_entity(db_record)
      end


      def self.change_status(id, status_entity)
        db_record = Database::DepositOrderOrm.find(id: id)
        db_record.status = status_entity.id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        transaction = Transactions.find_id(db_record.transaction_id)
        status = Entity::Status::DepositOrder.find(db_record.status)
        company_bank_account = CompanyBankAccounts.find_id(db_record.company_bank_account_id)
        deposit_method = Entity::FinancialMethod.find(db_record.deposit_method)

        Entity::DepositOrder.new(
          id: db_record.id,
          oblip_account_id: db_record.oblip_account_id,
          transaction: transaction,
          bank_account: company_bank_account,
          deposit_method:deposit_method,
          status: status,
          created_at: db_record.created_at,
          updated_at: db_record.updated_at
        )
      end
    end
  end
end
