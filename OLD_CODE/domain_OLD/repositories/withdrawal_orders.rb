# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Bank Orders
    class WithdrawalOrders
      def self.all
        Database::WithdrawalOrderOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::WithdrawalOrderOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.create(entity)
        db_record = Database::WithdrawalOrderOrm.create(
          oblip_account_id: entity.oblip_account_id,
          transaction_id: entity.transaction.id,
          oblip_account_bank_id: entity.bank_account.id,
          status: entity.status.id
        )
        rebuild_entity(db_record)
      end

      def self.update_status(id, status_entity)
        db_record = Database::WithdrawalOrderOrm.find(id: id)
        db_record.status = status_entity.id
        db_record.save

        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record
        
        transaction = Transactions.find_id(db_record.transaction_id)
        status = Entity::Status::WithdrawalOrder.find(db_record.status)
        oblip_account_bank = OblipAccountBanks.find_id(db_record.oblip_account_bank_id)

        Entity::WithdrawalOrder.new(
          id: db_record.id,
          oblip_account_id: db_record.oblip_account_id,
          transaction: transaction,
          bank_account: oblip_account_bank,
          status: status
        )
      end
    end
  end
end
