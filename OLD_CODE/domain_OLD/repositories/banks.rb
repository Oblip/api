# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Currencies
    class Banks
      def self.all
        Database::BankOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::BankOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_by_swift(code)
        db_record = Database::BankOrm.find(swift_code: code)
        rebuild_entity(db_record)
      end

      def self.find_many_by_country(country_id)
        db_records = Database::BankOrm.where(country_id: country_id).all
        db_records.map { |record| rebuild_entity(record) }
      end

      def self.find_by_routing(number)
        db_record = Database::BankOrm.find(routing_number: number)
        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        country = Countries.find_id(db_record.country_id)
        
        Entity::Bank.new(
          id: db_record.id,
          name: db_record.name,
          company_name: db_record.company_name,
          address: db_record.address,
          logo_url: db_record.logo_url,
          swift_code: db_record.swift_code,
          routing_number: db_record.routing_number,
          country: country
        )
      end
    end
  end
end
