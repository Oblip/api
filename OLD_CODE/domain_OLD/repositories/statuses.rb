# frozen_string_literal: true

module Oblip
  module Repository
    # Repository for Countries
    class Statuses
      def self.all
        Database::StatusOrm.all.map { |db_account| rebuild_entity(db_account) }
      end

      def self.find_id(id)
        db_record = Database::StatusOrm.find(id: id)
        rebuild_entity(db_record)
      end

      def self.find_code(code)
        db_record = Database::StatusOrm.find(code: code)
        rebuild_entity(db_record)
      end

      def self.rebuild_entity(db_record)
        return nil unless db_record

        Entity::Status.new(
          id: db_record.id,
          name: db_record.name,
        )
      end
    end
  end
end
