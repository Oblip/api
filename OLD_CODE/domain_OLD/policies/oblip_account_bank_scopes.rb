# frozen_string_literal: true

module Oblip
  # Policy to determine if account can manage bank accounts
  class OblipAccountBankPolicy
    class AccountScope
      def initialize(auth_account, bank_accounts)
        @auth_account = auth_account
        @bank_accounts = bank_accounts
      end
    end
  end
end