# frozen_string_literal: true

module Oblip
  # Policy to determine if an account can interact with a particular client
  class ApiClientPolicy
    def initialize(account_id, client)
      @account_id = account_id
      @client = client
    end

    def can_view?
      account_is_owner?
    end

    def can_update?
      account_is_owner?
    end

    private

    def account_is_owner?
      @client.oblip_account_id == @account_id
    end
  end
end