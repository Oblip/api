# frozen_string_literal: true

module Oblip
  # Policy to determine if an account can make a certain transaction
  class TransactionPolicy
    def initialize(auth_token, current_kyc_level, transaction_entity)
      @auth_token = auth_token
      @tx_entity = transaction_entity
      @kyc_level = current_kyc_level
    end

    def can_create_transaction?(total_amount_transacted)
      raise OException::PolicyRestriction unless unlimited_transactions_allowed?(total_amount_transacted)
    end

    private

    def unlimited_transactions_allowed?(total_amount_transacted)
      if @kyc_level == 1
        total = @tx_entity.amount + total_amount_transacted
        total > 100 ? false : true # allow only if total is less than or equal to 100
      else
        true # no limits
      end
    end


  end
end