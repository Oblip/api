# frozen_string_literal: true

module Oblip
  class EscrowFundPolicy
    class AccountScope
      def initialize(auth_account, params)
        @current_account = auth_account
        
        @offset = params['offset']
        @limit = params['limit']

      end

      def all_viewable
        if @current_account and (@current_account['type']['id'] >= Entity::StaffAccount::Type::SUPER_ADMIN.id)
          FindEscrowFunds.call(
            auth_account: @current_account,
            offset: @offset,
            limit: @limit
          )
        else
          FindOblipAccountEscrowFunds.call(
            auth_account: @current_account,
            offset: @offset,
            limit: @limit
          )
        end
      end

    end
  end
end