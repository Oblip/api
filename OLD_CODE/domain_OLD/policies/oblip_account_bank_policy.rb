# frozen_string_literal: true

module Oblip
  # Policy to determine if an account can interact with a particular oblip account bank
  class OblipAccountBankPolicy
    def initialize(auth_account, bank_accounts)
      @account_id = auth_account['id']
      @bank_accounts = bank_accounts
    end

    def can_view?
      account_is_owner?
    end

    def can_add_more?
      count = @bank_accounts.count do |x| 
        x.status.id == Entity::Status::OblipAccountBank::UNVERIFIED.id
      end
      count < 2
    end

    def is_a_duplicate?(bank_id, account_number)
      count = @bank_accounts.count do |x| 
        x.bank.id == bank_id && x.account_number == account_number
      end
      count > 0
    end



    private

    def account_is_owner?
      @bank_accounts[0].oblip_account_id == @account_id
    end
  end
end