# frozen_string_literal: true

module Oblip
  # Policy to determine if an account can interact with a particular fund account
  class FundAccountPolicy
    def initialize(account_id, target_fund)
      @account_id = account_id
      @target_fund = target_fund
    end

    def can_view?
      account_is_owner?
    end

    private

    def account_is_owner?
      @target_fund.oblip_account_id == @account_id
    end
  end
end