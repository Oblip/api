# frozen_string_literal: true

module Oblip
  # Policy for Deposit Orders
  class DepositPolicy
    def initialize(auth_account)
      @auth_account = auth_account
    end

    def can_create_deposit_order?(total_pending_orders)
      raise OException::PolicyRestriction if reached_allowed_limit?(total_pending_orders)
    end

    private

    def reached_allowed_limit?(pending_orders)
      pending_orders >= 2 ? true: false
    end
  end
end