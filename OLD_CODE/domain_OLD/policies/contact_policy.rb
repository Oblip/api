# frozen_string_literal: true

module Oblip
  # Policy to determine if an account can interact with a particular contact
  class ContactPolicy
    def initialize(auth_account, contact)
      @auth_account = auth_account
      @contact = contact
    end

    def can_view?
      account_is_owner?
    end

    def can_delete?
      account_is_owner?
    end

    def can_update?
      account_is_owner?
    end

    private

    def account_is_owner?
      @contact.owner_id == @auth_account['id']
    end
  end
end