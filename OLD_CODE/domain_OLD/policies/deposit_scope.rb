# frozen_string_literal: true

module Oblip
  class DepositPolicy
    class AccountScope
      def initialize(auth_account, params)
        @current_account = auth_account
        
        @offset = params['offset']
        @limit = params['limit']
        @id = params['id']

      end

      def all_viewable
        if @current_account and (@current_account['type']['id'] >= Entity::StaffAccount::Type::SUPER_ADMIN.id)
          FindDepositOrders.call(
            auth_account: @current_account,
            offset: @offset,
            limit: @limit
          )
        else
          FindDepositOrdersByAccount.call(
            auth_account: @current_account
            # TODO: add limit and offset here
          )
        end
      end

      def self.viewable?(current_account, deposit)
        current_account and  (deposit.oblip_account_id == current_account['id'] or
          (current_account['type']['id'] >= Entity::StaffAccount::Type::SUPER_ADMIN.id))
      end

    end
  end
end