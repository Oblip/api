# # frozen_string_literal: true

# require_relative 'spec_helper.rb'
# require 'stellar-base'

# horizon = Oblip::Horizon::Api.new(Oblip::Api.config)

# # NOTE: doesn't test exactly what failed
# def test_withdrawal_order(horizon, user_data, source_amount, bank_account, fee_type, should_fail = false)
#   req_headers = { 
#     'CONTENT_TYPE' => 'application/json',
#     'HTTP_AUTHORIZATION' => "Bearer #{user_data[:auth_tokens]['access_data']['token']}"
#   }

#   asset = Oblip::Repository::AnchorAssets.find_code(bank_account['bank']['country']['currency'])

#   2.times do |n|
#     user_data[:vault] = Oblip::Repository::Vaults.find_id(user_data[:vault].id)
#     quote = get_quote(
#       headers: req_headers,
#       asset: asset,
#       amount: source_amount,
#       recipient_account_id: nil,
#       type_id: Oblip::Entity::Transaction::Types::WITHDRAWAL.id
#     )

#     # tx_amount = Oblip::Entity::Transaction.calculate_olp_amount(bank_account.bank.currency,
#     #   source_amount)

#     tx_fees = Oblip::Entity::Transaction::Withdrawal.calculate_fees(user_data[:vault], asset, source_amount, fee_type)

#     # signed_envelope = horizon.create_olp_signed_payment_envelope(
#     #   from_account: user_data[:master],
#     #   to_account: horizon.anchor, 
#     #   signer: user_data[:signer],
#     #   amount: source_amount,
#     #   fee: quote['anchor_fee']
#     # )

#     signed_envelope = horizon.create_payment_envelope(
#       asset_code: asset.code,
#       amount: source_amount,
#       fee: quote['anchor_fee'],
#       should_sign: true,
#       keys: {
#         from_account: user_data[:master],
#         to_account: horizon.anchor,
#         signer: user_data[:signer]
#       }
#     )
#     data = {
#       bank_account_id: bank_account['id'],
#       amount: source_amount,
#       signed_tx_envelope: signed_envelope
#     }

#     before_funding_pool = Oblip::Repository::BlockchainFunds.all.first
    
#     post 'api/v1/withdrawals', data.to_json, req_headers

#     if should_fail
#       _(last_response.status != 201).must_equal true
#     else
#       _(last_response.status).must_equal 201 # created

#       tx_result = JSON.parse(last_response.body)

#       after_funding_pool = Oblip::Repository::BlockchainFunds.all.first
  
#       _(tx_result['bank_account']['account_number']).must_equal bank_account['account_number']
#       _(tx_result['transaction']['type']['id']).must_equal Oblip::Entity::Transaction::Types::WITHDRAWAL.id
#       _(tx_result['transaction']['amount']).must_equal quote['amount']
#       _(tx_result['transaction']['anchor_fee']).must_equal tx_fees[:anchor_fee]
#       _(tx_result['transaction']['agent_fee']).must_equal tx_fees[:agent_fee]
#       _(tx_result['transaction']['amount']).must_equal source_amount

#       _(after_funding_pool.balance).must_equal before_funding_pool.balance - tx_result['transaction']['network_fee']
#     end
#   end
# end

# describe 'Test Deposit Handling' do
#   include Rack::Test::Methods

#   before do
#     wipe_database

#     intialize_blockchain_fund_seeds(DATA[:blockchain_funds])
#   end

#   it 'HAPPY: should get a list of withdrawal' do
#     user_data = setup_account(horizon,DATA[:api_clients][0], DATA[:oblip_accounts][0], 100)
#     access_token = user_data[:auth_tokens]['access_data']['token']
    
#     req_headers = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
#     }
    
#     bk_account_seed = DATA[:bank_accounts][0]
#     bk_account_seed['account_number'] = '0939123943294'
    
#     bank_account = setup_bank_account(access_token, bk_account_seed)

#     # this creates two withdrawal orders
#     test_withdrawal_order(horizon, user_data, 20, bank_account,
#       Oblip::Entity::Transaction::Withdrawal::PERSONAL_ANCHOR)
    
#     get 'api/v1/withdrawals', nil, req_headers
#     _(last_response.status).must_equal 200
    
#     result = JSON.parse(last_response.body)
#     _(result['withdrawal_orders'].size).must_equal 2

#   end

#   describe 'Test Withdrawal Order Handling for Personal Account' do
#     it 'HAPPY: should create a withdrawal order' do
#       user_data = setup_account(horizon,DATA[:api_clients][0], DATA[:oblip_accounts][0], 100)
#       access_token = user_data[:auth_tokens]['access_data']['token']
      
#       bk_account_seed = DATA[:bank_accounts][1]
#       bk_account_seed['account_number'] = '0939123943294'
      
#       bank_account = setup_bank_account(access_token, bk_account_seed)

#       # this creates two withdrawal orders
#       test_withdrawal_order(horizon, user_data, 20, bank_account,
#         Oblip::Entity::Transaction::Withdrawal::PERSONAL_ANCHOR)
#     end

#     it 'SAD: should not allow withdrawal while being underfunded' do
#       user_data = setup_account(horizon,DATA[:api_clients][0], DATA[:oblip_accounts][0])
#       access_token = user_data[:auth_tokens]['access_data']['token']
      
#       bk_account_seed = DATA[:bank_accounts][0]
#       bk_account_seed['account_number'] = '0939123943293'
#       bank_account = setup_bank_account(access_token,bk_account_seed)

#       # this creates two withdrawal orders
#       test_withdrawal_order(horizon, user_data, 90, bank_account,
#         Oblip::Entity::Transaction::Withdrawal::PERSONAL_ANCHOR, true)
#     end
#   end

#   describe 'Test Withdrawal Order Handling for Agent Account' do
#     it 'HAPPY: should create a withdrawal order' do
#       user_data = setup_account(horizon,DATA[:api_clients][3], DATA[:oblip_accounts][3], 120)
#       access_token = user_data[:auth_tokens]['access_data']['token']
#       bk_account_seed = DATA[:bank_accounts][0]
#       bk_account_seed['account_number'] = '0939123913394'
#       bank_account = setup_bank_account(access_token,bk_account_seed)

#       # this creates two withdrawal orders
#       test_withdrawal_order(horizon, user_data, 20, bank_account,
#         Oblip::Entity::Transaction::Withdrawal::AGENT_ANCHOR)
#     end

#     it 'SAD: should not allow withdrawal while being underfunded' do
#       user_data = setup_account(horizon,DATA[:api_clients][3], DATA[:oblip_accounts][3])
#       access_token = user_data[:auth_tokens]['access_data']['token']
#       bk_account_seed = DATA[:bank_accounts][1]
#       bk_account_seed['account_number'] = '1939123943293'
#       bank_account = setup_bank_account(access_token,bk_account_seed)

#       # this creates two withdrawal orders
#       test_withdrawal_order(horizon, user_data, 90, bank_account,
#         Oblip::Entity::Transaction::Withdrawal::AGENT_ANCHOR, true)
#     end
#   end

#   describe 'Test Withdrawal Order Handling for a Business Account' do
#     it 'HAPPY: should create a withdrawal order' do
#       user_data = setup_account(horizon,DATA[:api_clients][6], DATA[:oblip_accounts][6], 120)
#       access_token = user_data[:auth_tokens]['access_data']['token']
#       bk_account_seed = DATA[:bank_accounts][0]
#       bk_account_seed['account_number'] = '0934123913394'
#       bank_account = setup_bank_account(access_token,bk_account_seed)

#       # this creates two withdrawal orders
#       test_withdrawal_order(horizon, user_data, 20, bank_account,
#         Oblip::Entity::Transaction::Withdrawal::BUSINESS_ANCHOR)
#     end

#     it 'SAD: should not allow withdrawal while being underfunded' do
#       user_data = setup_account(horizon,DATA[:api_clients][6], DATA[:oblip_accounts][6])
#       access_token = user_data[:auth_tokens]['access_data']['token']
#       bk_account_seed = DATA[:bank_accounts][1]
#       bk_account_seed['account_number'] = '1239123943293'
#       bank_account = setup_bank_account(access_token,bk_account_seed)

#       # this creates two withdrawal orders
#       test_withdrawal_order(horizon, user_data, 90, bank_account,
#         Oblip::Entity::Transaction::Withdrawal::BUSINESS_ANCHOR, true)
#     end
#   end

# end
