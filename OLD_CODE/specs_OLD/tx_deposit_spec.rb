# frozen_string_literal: true

require_relative 'spec_helper.rb'
horizon = Oblip::Horizon::Api.new(Oblip::Api.config)

def test_deposit_order(access_token, deposit_data, fee_type, expect_success = true)
  calc_class = Oblip::Entity::Transaction::Deposit
  
  asset = Oblip::Repository::AnchorAssets.find_code('BZD')

  # tx_amount = Oblip::Entity::Transaction.calculate_olp_amount(currency, deposit_data['amount'])
  tx_fees = calc_class.calculate_fees(asset, deposit_data['amount'], fee_type)

  headers = { 
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }

  data = {
    company_bank_id: deposit_data['company_bank_id'],
    amount: deposit_data['amount']
  }

  post 'api/v1/deposits', data.to_json, headers
  
  if expect_success
    _(last_response.status).must_equal 201

    result = JSON.parse(last_response.body)

    _(result['bank_account']['id']).must_equal deposit_data['company_bank_id']
    _(result['transaction']['amount']).must_equal deposit_data['amount']
    _(result['transaction']['anchor_fee']).must_equal tx_fees[:anchor_fee]
    _(result['transaction']['agent_fee']).must_equal tx_fees[:agent_fee]

    result
  else 
    nil
  end
end

describe 'Test Deposit Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    intialize_blockchain_fund_seeds(DATA[:blockchain_funds])
  end

  it 'HAPPY: should find a  single deposit order using the ID' do
    user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = user_data[:auth_tokens]
    access_token = auth_tokens['access_data']['token']
    req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # creates the deposit order
    result = test_deposit_order(access_token,DATA[:deposits][0],
      Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)

    get "api/v1/deposits/#{result['id']}", nil, req_headers
    _(last_response.status).must_equal 200

    response = JSON.parse(last_response.body)

    _(response['id']).must_equal result['id']
  end

  it 'SAD: should not allow an unauthorized user to access another user deposit order' do
    user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = user_data[:auth_tokens]
    access_token = auth_tokens['access_data']['token']

    other_data = setup_account(horizon, DATA[:api_clients][2], DATA[:oblip_accounts][2])
    other_auth_tokens = other_data[:auth_tokens]
    other_access_token = other_auth_tokens['access_data']['token']

    other_req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{other_access_token}"
    }

    # creates the deposit order
    result = test_deposit_order(access_token,DATA[:deposits][1],
      Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)

    get "api/v1/deposits/#{result['id']}", nil, other_req_headers
    _(last_response.status).must_equal 404

  end

  it 'HAPPY: should get a list of deposits' do

    user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = user_data[:auth_tokens]
    access_token = auth_tokens['access_data']['token']
    req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    2.times do |n|
      test_deposit_order(access_token, DATA[:deposits][n],
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)
    end

    get 'api/v1/deposits', nil, req_headers
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)
    _(result['deposit_orders'].size > 0).must_equal true

    result
  end

  describe 'Test all account types creating a Deposit order' do
    it 'HAPPY: should create a deposit order of a personal account' do
      user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      deposit_data =  DATA[:deposits][0]
      
      test_deposit_order(access_token, deposit_data,
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)
    end

    it 'HAPPY: should create a deposit order of a business account' do
      user_data = setup_account(horizon, DATA[:api_clients][6], DATA[:oblip_accounts][6])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      deposit_data =  DATA[:deposits][1]
      
      test_deposit_order(access_token, deposit_data,
        Oblip::Entity::Transaction::Deposit::ANCHOR_BUSINESS)
    end

    it 'HAPPY: should create a deposit order of a agent account' do
      user_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      deposit_data =  DATA[:deposits][2]
      
      test_deposit_order(access_token, deposit_data,
        Oblip::Entity::Transaction::Deposit::ANCHOR_AGENT)
    end
  end

  describe 'Test Deposit Orders Staff Handling' do
    before :each do
      # create super admin
      @super_admin_data = DATA[:staff_accounts]['super_admin']
      auth_data = Oblip::Service::Staff::CreateSuperAdmin.new
        .call(@super_admin_data).value!

      access_data = auth_data[:access_data]

      @req_headers = { 
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_data.token}"
      }
      
    end

    it 'HAPPY: should allow a staff to find a  single deposit order using the ID' do
      user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']
  
      # creates the deposit order
      result = test_deposit_order(access_token,DATA[:deposits][0],
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)
  
      get "api/v1/deposits/#{result['id']}", nil, @req_headers
      _(last_response.status).must_equal 200
  
      response = JSON.parse(last_response.body)
  
      _(response['id']).must_equal result['id']
    end

    it 'HAPPY: should get all deposit orders by pagination' do
      user_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      # creates more than one deposit order
      # TODO: create deposits of different users to test
      2.times do |x|
        deposit_data = DATA[:deposits][x]
        test_deposit_order(access_token, deposit_data,
          Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)
      end

      get 'api/v1/deposits', nil, @req_headers
      _(last_response.status).must_equal 200

      result = JSON.parse(last_response.body)
      _(result['deposit_orders'].size > 0).must_equal true
    end

    it 'HAPPY: should allow a super admin to approve a deposit order' do
      user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      deposit_data =  DATA[:deposits][0]
      
      order_results = test_deposit_order(access_token, deposit_data,
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)

      post "api/v1/deposits/#{order_results['id']}/status/approve", nil, @req_headers
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)
      
      _(result['id']).must_equal order_results['id']
      _(result['status']['id']).must_equal Oblip::Entity::DepositOrder::Status::COMPLETED.id
      _(result['transaction']['status']['id']).must_equal Oblip::Entity::Status::Transaction::COMPLETED.id
    end

    it 'HAPPY: should allow a user to confirm a deposit order' do
      user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      deposit_data =  DATA[:deposits][0]

      order_results = test_deposit_order(access_token, deposit_data,
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)

      headers = { 
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      post "api/v1/deposits/#{order_results['id']}/status/confirm", nil, headers
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)
      _(result['status']['id']).must_equal Oblip::Entity::DepositOrder::Status::USER_CONFIRMED.id
      _(result['transaction']['status']['id']).must_equal Oblip::Entity::Transaction::Status::CONFIRMATION_REVIEW.id
    end

    it 'SAD: should not allow new order creation if user has more than two pending orders' do
      user_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][0])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      # create two pending deposit orders
      2.times do |n|
        deposit_data =  DATA[:deposits][n]
        test_deposit_order(access_token, deposit_data,
          Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)
      end

      # attempt to create one more
      test_deposit_order(access_token, DATA[:deposits][2],
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL, false)
      
      _(last_response.status).must_equal 400
    end

    it 'SAD: should not allow a user to confirm a deposit order which is not theirs' do
      user_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
      auth_tokens = user_data[:auth_tokens]
      access_token = auth_tokens['access_data']['token']

      deposit_data =  DATA[:deposits][0]

      order_results = test_deposit_order(access_token, deposit_data,
        Oblip::Entity::Transaction::Deposit::ANCHOR_PERSONAL)


      user_data1 = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])
      auth_tokens1 = user_data1[:auth_tokens]
      access_token1 = auth_tokens1['access_data']['token']

      headers = { 
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token1}"
      }

      post "api/v1/deposits/#{order_results['id']}/status/confirm", nil, headers
      _(last_response.status).must_equal 404
      result = JSON.parse(last_response.body)
      
    end
  end
end