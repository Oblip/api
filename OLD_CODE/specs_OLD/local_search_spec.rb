# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Search Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = new_data[:auth_tokens]

    other_data = create_oblip_account(DATA[:api_clients][1], DATA[:oblip_accounts][1])
    other_auth_tokens = other_data[:auth_tokens]


    @first_req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }

    second_req_headers = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{other_auth_tokens['access_data']['token']}"
    }

    get 'api/v1/oblip_account', nil, second_req_headers
    @second_account = JSON.parse(last_response.body)
  end

  it 'HAPPY: should successfully find a contact by phone number' do
    phone_number = @second_account['phone_number']
  
    get "api/v1/search/contacts/#{phone_number}", nil, @first_req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['result']['id']).must_equal @second_account['id']
  end

  it 'SAD: not find any search results' do
    phone_number = '+8860900418910'

    get "api/v1/search/contacts/#{phone_number}", nil, @first_req_headers
    _(last_response.status).must_equal 404

  end

  it 'HAPPY: it should find an account in a users contact list' do
    # add contact
    post 'api/v1/contacts', { contact_account_id: @second_account['id'] }.to_json,
      @first_req_headers
    _(last_response.status).must_equal 201

    phone_number = @second_account['phone_number']

    get "api/v1/search/contacts/#{phone_number}", nil,  @first_req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['result']['id']).must_equal @second_account['id']
    _(result['is_in_contact_list']).must_equal true
  end

  it 'SAD: should check for invalid phone number' do
    phone_number = 'xxxxx'

    get "api/v1/search/contacts/#{phone_number}", nil,  @first_req_headers
    _(last_response.status).must_equal 400
  end
end