# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'stellar-base'

horizon = Oblip::Horizon::Api.new(Oblip::Api.config)
issuer_api_token = Oblip::Api.config['OBLIP_ISSUER_API_TOKEN']

def create_transaction(horizon, sender_data, recipient_data, amount, tx_type_id, calc_class, fee_type)
  headers = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{sender_data[:auth_tokens]['access_data']['token']}"
  }

  # should give us different anchor fees
  2.times do |n|

    sender_data[:vault] = Oblip::Repository::Vaults.find_id(sender_data[:vault].id)

    asset = Oblip::Repository::AnchorAssets.find_code('BZD')

    # tx_amount = Oblip::Entity::Transaction.calculate_olp_amount(currency, amount)

    if calc_class == Oblip::Entity::Transaction::Deposit
      tx_fees = calc_class.calculate_fees(asset, amount, fee_type)
    else
      tx_fees = calc_class.calculate_fees(sender_data[:vault], asset, amount, fee_type)
    end

    quote = get_quote(
      headers: headers,
      asset: asset,
      amount: amount,
      recipient_account_id: recipient_data ? recipient_data[:account]['id'] : nil,
      type_id: tx_type_id
    )

    signed_envelope = horizon.create_payment_envelope(
      asset_code: asset.code,
      amount: quote['amount'],
      fee: quote['anchor_fee'],
      should_sign: quote['requires_sender_signature'],
      keys: {
        from_account: sender_data[:master],
        to_account: recipient_data ? recipient_data[:master] : horizon.anchor,
        signer: sender_data[:signer]
      }
    )

    data = {
      asset_code: asset.code,
      amount: amount,
      recipient_account_id: recipient_data ? recipient_data[:account]['id'] : nil,
      remarks: 'Test Note',
      type_id: tx_type_id, 
      signed_tx_envelope: signed_envelope
    }

    post 'api/v1/transactions', data.to_json, headers
    JSON.parse(last_response.body)
  end
end

describe 'Test Escrow Funds Handling' do
  include Rack::Test::Methods
  
  before :each do
    wipe_database
    intialize_blockchain_fund_seeds(DATA[:blockchain_funds])
  end
  
  it 'HAPPY: should get all escrow funds of an agent account' do
    # create agent
    agent_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3], 310)
    
    # create personal recipient
    recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])

    tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
    calc_class = Oblip::Entity::Transaction::Deposit
   
    # create agent deposit transaction to a personal account
    create_transaction(horizon, agent_data, recipient_data, 20,
      tx_type_id, calc_class, calc_class::AGENT_PERSONAL)
    
    # prepare request headers
    headers = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{agent_data[:auth_tokens]['access_data']['token']}"
    }

    # get escrows
    get 'api/v1/escrow_funds', nil, headers
    _(last_response.status).must_equal 200 # created

    results = JSON.parse(last_response.body)

    _(results['escrow_funds'].size > 0).must_equal true
  end

  it 'HAPPY: should get a specific escrow fund of an agent' do
    # create agent
    agent_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3], 310)
    
    # create personal recipient
    recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])

    tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
    calc_class = Oblip::Entity::Transaction::Deposit
   
    # create agent deposit transaction to a personal account
    create_transaction(horizon, agent_data, recipient_data, 20,
      tx_type_id, calc_class, calc_class::AGENT_PERSONAL)
    
    # prepare request headers
    headers = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{agent_data[:auth_tokens]['access_data']['token']}"
    }

    # get escrows
    get 'api/v1/escrow_funds', nil, headers
    results = JSON.parse(last_response.body)

    escrow_data = results['escrow_funds'][0]

    get "api/v1/escrow_funds/#{escrow_data['id']}", nil, headers
    _(last_response.status).must_equal 200 # created
    single_escrow_data = JSON.parse(last_response.body)

    _(single_escrow_data['id']).must_equal escrow_data['id']
  end

  it 'SAD: should not allow an unauthorized person to access a escrow fund of another user' do
    # create agent
    agent_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3], 310)
    
    # create personal recipient
    recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])

    tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
    calc_class = Oblip::Entity::Transaction::Deposit
   
    # create agent deposit transaction to a personal account
    create_transaction(horizon, agent_data, recipient_data, 20,
      tx_type_id, calc_class, calc_class::AGENT_PERSONAL)


    # create another account
    other_account = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
    
    # prepare request headers
    headers = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{agent_data[:auth_tokens]['access_data']['token']}"
    }

    other_headers = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{other_account[:auth_tokens]['access_data']['token']}"
    }


    # get escrows
    get 'api/v1/escrow_funds', nil, headers
    results = JSON.parse(last_response.body)

    escrow_data = results['escrow_funds'][0]

    get "api/v1/escrow_funds/#{escrow_data['id']}", nil, other_headers
    _(last_response.status).must_equal 404 # not found (unauthorized)
  end

  describe 'Staff Hanlding of escrow funds in the system' do
    before :each do
      @super_admin_data = DATA[:staff_accounts]['super_admin']

      auth_data = Oblip::Service::Staff::CreateSuperAdmin.new
        .call(@super_admin_data).value!

      @headers = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{auth_data[:access_data].token}"
      }

      # create agent
      agent_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3], 310)
      
      # create personal recipient
      recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])

      tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
      calc_class = Oblip::Entity::Transaction::Deposit
    
      # create agent deposit transaction to a personal account
      create_transaction(horizon, agent_data, recipient_data, 20,
        tx_type_id, calc_class, calc_class::AGENT_PERSONAL)
    
    end

    it 'HAPPY: should get all escrow funds' do
      get 'api/v1/escrow_funds', nil, @headers
      _(last_response.status).must_equal 200 # created

      results = JSON.parse(last_response.body)
  
      _(results['escrow_funds'].size > 0).must_equal true
    end
  end
end