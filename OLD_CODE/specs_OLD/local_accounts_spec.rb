# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should get a logged in oblip account data' do
    account_seed = DATA[:oblip_accounts][0]
    new_data = create_oblip_account(DATA[:api_clients][0], account_seed)
    auth_tokens = new_data[:auth_tokens]
    
    req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }


    get 'api/v1/oblip_account', nil, req_headers
    _(last_response.status).must_equal 200

    result = JSON.parse last_response.body
    _(result['phone_number']).must_equal account_seed['phone_number']
  end

  it 'SAD: should return not found while trying to access an oblip account without authorization' do
    get '/api/v1/oblip_account'

    _(last_response.status).must_equal 404
  end

  describe 'Creating New Accounts' do
    it 'HAPPY: should be able to create new oblip_account and return an auth_tokens' do
      account_seed = DATA[:oblip_accounts][1]
      result = create_oblip_account(DATA[:api_clients][1], account_seed)
      _(last_response.status).must_equal 201


      _(result[:auth_tokens]['refresh_token'].nil?).must_equal false
      _(result[:auth_tokens]['access_data'].nil?).must_equal false
    end

    it 'SAD: it should not allow to create an account with the same phone number' do
      first_account = DATA[:oblip_accounts][2]
      create_oblip_account(DATA[:api_clients][2], first_account)

      second_account = DATA[:oblip_accounts][3]
      second_account['phone_number'] = first_account['phone_number']
      
      result =  create_oblip_account(DATA[:api_clients][3], second_account)
      _(last_response.status).must_equal 422 # cannot process
    end
  end

  it 'SAD: should not allow to create an account without a verified client' do
    account_seed = DATA[:oblip_accounts][3]
    create_oblip_account(DATA[:api_clients][3], account_seed, false)
    _(last_response.status).must_equal 401 # unauthorized
  end
end
