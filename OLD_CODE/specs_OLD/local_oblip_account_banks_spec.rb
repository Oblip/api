# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Bank Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    @oblip_account_seed = DATA[:oblip_accounts][0]
    user_data = create_oblip_account(DATA[:api_clients][0], @oblip_account_seed)
    @auth_tokens = user_data[:auth_tokens]

    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{@auth_tokens['access_data']['token']}"
    }

     # get logged_in user account data
     get 'api/v1/oblip_account', nil, @req_headers
     @user_account = JSON.parse(last_response.body)
  end

  it 'HAPPY: should get a list of user bank accounts' do

    DATA[:bank_accounts].each do |r|
      setup_bank_account(@auth_tokens['access_data']['token'],r)
    end

    get "api/v1/oblip_account/banks", nil, @req_headers

    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)
  
    _(result['bank_accounts'].size > 0).must_equal true
    _(result['bank_accounts'][0]['account_number']).must_equal DATA[:bank_accounts][0]['account_number']
  end

  describe 'Adding a new Oblip User Bank Account' do
    before :each do
      @first_bank_account = DATA[:bank_accounts][0]
    end

    it 'HAPPY: should add a new bank account' do
      # post 'api/v1/oblip_account/banks', @first_bank_account.to_json, @req_headers 
      setup_bank_account(@auth_tokens['access_data']['token'], @first_bank_account, false)
      _(last_response.status).must_equal 201

      result = JSON.parse(last_response.body)

      _(result['account_number']).must_equal @first_bank_account['account_number']
    end

    it 'SAD: should not allow for duplicate accounts' do
      setup_bank_account(@auth_tokens['access_data']['token'], @first_bank_account, false)
      _(last_response.status).must_equal 201

      # duplicate
      setup_bank_account(@auth_tokens['access_data']['token'], @first_bank_account, false)
      _(last_response.status).must_equal 400
    end

    it 'SAD: should not allow for more than two unverified accounts' do
      DATA[:bank_accounts].each do |r|
        setup_bank_account(@auth_tokens['access_data']['token'],r, false)
      end

      # not allowed
      @first_bank_account['account_number'] = '5039292929292'
      setup_bank_account(@auth_tokens['access_data']['token'], @first_bank_account, false)
      _(last_response.status).must_equal 400
    end
  end
end