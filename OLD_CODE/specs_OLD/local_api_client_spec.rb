# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Api Clients' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should get a mobile client' do
    client_seed = DATA[:api_clients][0]
    user_data = create_oblip_account(client_seed, DATA[:oblip_accounts][0])
    auth_tokens = user_data[:auth_tokens]
    new_client_data = user_data[:client]
    
    req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }

    get "api/v1/clients/mobile/#{new_client_data[:result]['id']}", nil, req_headers
    _(last_response.status).must_equal 200
    created = JSON.parse(last_response.body)

    _(created['id']).must_equal new_client_data[:result]['id']
    _(created['platform_type']).must_equal client_seed['platform_type']
    _(created['last_ip_address']).must_equal client_seed['ip_address']
  end

  it 'HAPPY: should register a new mobile client' do
    client_seed = DATA[:api_clients][0]
    client_data = register_mobile_client(client_seed)
    _(last_response.status).must_equal 201

    _(client_data[:result]['type']['id']).must_equal client_seed['type']
  end

  it 'HAPPY: should update a client data about last seen' do
    account_seed = DATA[:oblip_accounts][1]
    account = create_oblip_account(DATA[:api_clients][1], account_seed)
    auth_tokens = account[:auth_tokens]

    req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }
    
    patch "api/v1/clients/mobile/#{account[:client][:result]['id']}", nil,  req_headers
    _(last_response.status).must_equal 200

  end

  # TODO
  # it 'HAPPY: should update a device' do
  #   device_data = DATA[:devices][0]
  #   user_data = create_oblip_account(device_data, DATA[:oblip_accounts][0])
  #   user = user_data[:account]
  #   device = user_data[:device][:data]

  #   req_headers = { 
  #     'CONTENT_TYPE' => 'application/json',
  #     'HTTP_AUTHORIZATION' => "Bearer #{user.auth_token}"
  #   }
  #   ip_address = '127.5.0.1'
  #   data = {
  #     ip_address: ip_address
  #   }

  #   put "api/v1/devices/#{device.id}", data.to_json, req_headers
  #   _(last_response.status).must_equal 200
  #   created = JSON.parse(last_response.body)

  #   _(created['id']).must_equal device.id
  #   _(created['last_ip_address']).must_equal ip_address
  # end

  # describe 'Adding a new device' do
  #   before :each do
  #     @req_header = { 'CONTENT_TYPE' => 'application/json' }
  #     @device_data = DATA[:devices][1]
  #   end

  #   it 'HAPPY: should add a new device' do
  #     post 'api/v1/devices', @device_data.to_json, @req_header
  #     _(last_response.status).must_equal 201

  #     created = JSON.parse(last_response.body)

  #     _(created['device_unique_id']).must_equal @device_data['device_unique_id']
  #     _(created['last_ip_address']).must_equal @device_data['ip_address']
  #   end
  # end

end
