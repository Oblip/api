# # frozen_string_literal: true

# require_relative 'spec_helper.rb'

# describe 'Test Account PIN Code Handling' do
#   include Rack::Test::Methods

#   before :each do
#     wipe_database

#     @logged_in_account = Oblip::CreateOblipAccount.new.call(
#       email: DATA[:oblip_accounts][0]['email'],
#       password: DATA[:oblip_accounts][0]['password'],
#       first_name: DATA[:oblip_accounts][0]['first_name'],
#       middle_name: DATA[:oblip_accounts][0]['middle_name'],
#       last_name: DATA[:oblip_accounts][0]['last_name'],
#       type: DATA[:oblip_accounts][0]['type'],
#       config: Oblip::Api.config
#     )

#     @logged_in_account = @logged_in_account.value.message

#     @req_headers = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{@logged_in_account.auth_token}"
#     }
#   end

#   it 'HAPPY: should update the a logged in user\'s PIN code' do
#     pin_code = '068329'

#     data = {
#       pin_code: pin_code
#     }

#     put 'api/v1/oblip_account/pin_code', data.to_json, @req_headers
#     _(last_response.status).must_equal 200
#     result = JSON.parse(last_response.body)
#   end

#   it 'SAD: should fail updating the a logged in user\'s PIN code' do
#     pin_code = '0683'

#     data = {
#       pin_code: pin_code
#     }

#     put 'api/v1/oblip_account/pin_code', data.to_json, @req_headers
#     _(last_response.status).must_equal 400
#   end

# end