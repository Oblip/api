# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'stellar-base'

horizon = Oblip::Horizon::Api.new(Oblip::Api.config)
issuer_api_token = Oblip::Api.config['OBLIP_ISSUER_API_TOKEN']

def test_transaction(horizon, sender_data, recipient_data, amount, tx_type_id, calc_class, fee_type)
  headers = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{sender_data[:auth_tokens]['access_data']['token']}"
  }

  # should give us different anchor fees
  2.times do |n|

    sender_data[:vault] = Oblip::Repository::Vaults.find_id(sender_data[:vault].id)

    asset = Oblip::Repository::AnchorAssets.find_code('BZD')

    # tx_amount = Oblip::Entity::Transaction.calculate_olp_amount(currency, amount)

    if calc_class == Oblip::Entity::Transaction::Deposit
      tx_fees = calc_class.calculate_fees(asset, amount, fee_type)
    else
      tx_fees = calc_class.calculate_fees(sender_data[:vault], asset, amount, fee_type)
    end

    quote = get_quote(
      headers: headers,
      asset: asset,
      amount: amount,
      recipient_account_id: recipient_data ? recipient_data[:account]['id'] : nil,
      type_id: tx_type_id
    )

    signed_envelope = horizon.create_payment_envelope(
      asset_code: asset.code,
      amount: quote['amount'],
      fee: quote['anchor_fee'],
      should_sign: quote['requires_sender_signature'],
      keys: {
        from_account: sender_data[:master],
        to_account: recipient_data ? recipient_data[:master] : horizon.anchor,
        signer: sender_data[:signer]
      }
    )

    data = {
      asset_code: asset.code,
      amount: amount,
      recipient_account_id: recipient_data ? recipient_data[:account]['id'] : nil,
      remarks: 'Test Note',
      type_id: tx_type_id, 
      signed_tx_envelope: signed_envelope
    }

    post 'api/v1/transactions', data.to_json, headers
    _(last_response.status).must_equal 201 # created
    tx_result = JSON.parse(last_response.body)

    # puts "From: #{sender_data[:master].address}"
    # puts "TO: #{recipient_data ? recipient_data[:master].address : horizon.anchor.address}"
    # puts "Transaction Hash: #{tx_result['vault_tx_hash']}"

    _(tx_result['anchor_asset']['code']).must_equal asset.code
    _(tx_result['type']['id']).must_equal  tx_type_id
    _(tx_result['amount']).must_equal amount
    _(tx_result['anchor_fee']).must_equal tx_fees[:anchor_fee]
    _(tx_result['agent_fee']).must_equal tx_fees[:agent_fee]
  end
end

describe 'Test Transaction Handling' do
  include Rack::Test::Methods
  
  before :each do
    wipe_database
    intialize_blockchain_fund_seeds(DATA[:blockchain_funds])
  end

  describe 'Testing Transactions Between accounts' do
    # personal
    describe 'Test Transaction From a Personal Account to other accounts' do
      before :each do
        @sender_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0], 110)
      end

      describe 'Test Payment Transactions' do
        before :each do
          @tx_type_id =  Oblip::Entity::Transaction::Types::PAYMENT.id
          @calc_class = Oblip::Entity::Transaction::Payment
        end

        it 'HAPPY: should create two payment transactions between two personal accounts' do
          recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])
          test_transaction(horizon, @sender_data, recipient_data, 20,
            @tx_type_id, @calc_class, @calc_class::PERSONAL_PERSONAL)
        end

        it 'HAPPY: should test two payment transaction between a personal account and a business' do
          recipient_data = setup_account(horizon, DATA[:api_clients][2], DATA[:oblip_accounts][5])

          test_transaction(horizon, @sender_data, recipient_data, 30,
            @tx_type_id, @calc_class, @calc_class::PERSONAL_BUSINESS)
        end
      end

      describe 'Test Withdrawal Transactions' do
        before :each do
          @tx_type_id =  Oblip::Entity::Transaction::Types::WITHDRAWAL.id
          @calc_class = Oblip::Entity::Transaction::Withdrawal
        end
        it 'HAPPY: should test two withdrawal transaction between a personal account and an agent' do
          recipient_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])

          test_transaction(horizon, @sender_data, recipient_data, 10,
            @tx_type_id, @calc_class, @calc_class::PERSONAL_AGENT)
        end
      end
    end

    # agent
    describe 'Test Transactions from a Agent Account to other accounts' do
      before :each do
        @agent_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3], 310)
      end

      describe 'Test Payment Transactions' do
        before :each do
          @tx_type_id =  Oblip::Entity::Transaction::Types::PAYMENT.id
          @calc_class = Oblip::Entity::Transaction::Payment
        end

        it 'HAPPY: should test two payment transactions between an agent account and a business' do
          recipient_data = setup_account(horizon, DATA[:api_clients][2], DATA[:oblip_accounts][5])

          test_transaction(horizon, @agent_data, recipient_data, 10,
            @tx_type_id, @calc_class, @calc_class::AGENT_BUSINESS)
        end
      end

      describe 'Test Deposit Transactions' do
        before :each do
          @tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
          @calc_class = Oblip::Entity::Transaction::Deposit
        end

        it 'HAPPY: should test two deposit transaction between a agent account and a personal account' do
          recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])
          
          test_transaction(horizon, @agent_data, recipient_data, 20,
            @tx_type_id, @calc_class, @calc_class::AGENT_PERSONAL)
        end

        it 'HAPPY: should test two deposit transactions between two agent accounts' do
          recipient_data = setup_account(horizon, DATA[:api_clients][4], DATA[:oblip_accounts][4])

          test_transaction(horizon, @agent_data, recipient_data, 10,
            @tx_type_id, @calc_class, @calc_class::AGENT_AGENT)
        end
      end
    end

    

    # business 
    describe 'Test Transactions from a Business Account to other accounts' do
      before :each do
        @sender_data = setup_account(horizon, DATA[:api_clients][5], DATA[:oblip_accounts][5], 210)
      end

      describe 'Test Payment Transactions' do
        before :each do
          @tx_type_id =  Oblip::Entity::Transaction::Types::PAYMENT.id
          @calc_class = Oblip::Entity::Transaction::Payment
        end

        it 'HAPPY: should test two payment transaction quote between two business accounts' do
          recipient_data = setup_account(horizon, DATA[:api_clients][6], DATA[:oblip_accounts][6])
          
          test_transaction(horizon, @sender_data, recipient_data, 50,
            @tx_type_id, @calc_class, @calc_class::BUSINESS_BUSINESS)
        end

        it 'HAPPY: should test two payment transaction between a business account and personal' do
          recipient_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
          
          test_transaction(horizon, @sender_data, recipient_data, 10,
            @tx_type_id, @calc_class, @calc_class::BUSINESS_PERSONAL)
        end
      end

      describe 'Test Withdrawal Transactions' do
        before :each do
          @tx_type_id =  Oblip::Entity::Transaction::Types::WITHDRAWAL.id
          @calc_class = Oblip::Entity::Transaction::Withdrawal
        end

        it 'HAPPY: should test two withdrawal transactions between a business account and an agent' do
          recipient_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])
          
          test_transaction(horizon, @sender_data, recipient_data, 10,
            @tx_type_id, @calc_class, @calc_class::BUSINESS_AGENT)
        end
      end
    end
  end
end




########## THE FOLLOWING IS OLD CODE:: ##############

# describe 'Test Personal Transaction Handling' do
#   include Rack::Test::Methods

#   before do
#     wipe_database

#     # initialize test blockchain funds
#     intialize_blockchain_fund_seeds(DATA[:blockchain_funds])

#     @sender_account = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])[:account]
#     # @sender_keypair = Stellar::KeyPair.from_seed('SAB5TTSY3GNXVZ2QBFBC2EXWECSGS7ZNN2IRZ2BGHQS434I67GUSMPKQ')
    
#     @sender_master = Stellar::KeyPair.random
#     @sender_signer = Stellar::KeyPair.random

#     @sender_vault = Oblip::CreateVault.new.call(
#       auth_account: @sender_account.oblip_account,
#       salt: 'xxx',
#       address:  @sender_master.address,
#       config: Oblip::Api.config
#     )

#     @sender_vault = @sender_vault.value!.message

#     # lets configure the new account
#     horizon.config_new_account(@sender_master, @sender_signer)
#     asset_code = Oblip::Api.config['MAIN_ASSET_CODE'] 

#     # we need to trust hte account
#     horizon.allow_trust(@sender_master.address, asset_code)

#     @recipient_account = create_oblip_account(DATA[:api_clients][1], DATA[:oblip_accounts][1])[:account]
#     # @recipient_master = Stellar::KeyPair.from_seed('SDBGHE6BDUZUIKL5BIWKHKHFZ55LVSY4KOYE6UE4MEDC7Z5QKTYKXNZK')
#     @recipient_master = Stellar::KeyPair.random
#     @recipient_signer = Stellar::KeyPair.random
#     Oblip::CreateVault.new.call(
#       auth_account: @recipient_account.oblip_account,
#       address: @recipient_master.address,
#       salt: 'xxx',
#       config: Oblip::Api.config
#     )
    
#     horizon.config_new_account(@recipient_master, @recipient_signer)
   
#     # again, we need to trust the account
#     asset_code = Oblip::Api.config['MAIN_ASSET_CODE'] 
#     horizon.allow_trust(@recipient_master.address, asset_code)
    
#     # @recipient_fund =  @recipient_account.oblip_account.default_fund_id
#     @req_headers = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{@sender_account.auth_token}"
#     }

#     @recipient_req_headers = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{@recipient_account.auth_token}"
#     }
#   end

#   it 'HAPPY: should create a payment transaction between two users for the first time' do
#     funding_amount = 5
    
#     topup_amount = 100
#     issuer_req_header = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
#     }

#     data = {
#       recipient_id: @sender_account.oblip_account.id,
#       amount: topup_amount
#     }

#     post 'api/v1/topup', data.to_json, issuer_req_header
    

#     # tx_hash = horizon.submit_payment_by(signed_envelope)

#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 2

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2 #payment
#     }

#     post 'api/v1/quote', quote_data.to_json, @req_headers
#     q_result = JSON.parse(last_response.body)

#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: @sender_master ,
#       to_account: @recipient_master, 
#       signer: @sender_signer,
#       amount: q_result['tx_amount'],
#       fee: q_result['anchor_fee']
#     )
    

#     notes = 'Here is $2 :)'

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: notes,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2, # payment
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, @req_headers
#     _(last_response.status).must_equal 201 # created
#     tx_result = JSON.parse(last_response.body)
#     anchor_fee = tx_result['tx_amount'] * Oblip::Entity::Transaction::ANCHOR_PERCENT_FEES[:payment]
#     anchor_fee += @sender_vault.initial_operation_cost # added for the first transaction
    
#     _(tx_result['recipient_fund']['id']).must_equal @recipient_account.oblip_account.default_fund_id
#     _(tx_result['notes']).must_equal notes
#     _(tx_result['tx_amount']).must_equal q_result['tx_amount']
#     _(tx_result['anchor_fee']).must_equal anchor_fee
#     _(tx_result['type']['id']).must_equal 2 # payment
#     _(tx_result['status']['id']).must_equal Oblip::Entity::Status::Transaction::COMPLETED.id

#     recipient_fund_account_id =  @recipient_account.oblip_account.default_fund_id
#     get "api/v1/fund_accounts/#{recipient_fund_account_id}", nil, @recipient_req_headers
#     fund_result = JSON.parse(last_response.body)

#     _(fund_result['balance']).must_equal q_result['tx_amount']
#   end

#   it 'HAPPY: should create two different payment transaction between two users and test the correct anchor fees' do
    
#     # topping up here
#     topup_amount = 100
#     issuer_req_header = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
#     }

#     data = {
#       recipient_id: @sender_account.oblip_account.id,
#       amount: topup_amount
#     }

#     post 'api/v1/topup', data.to_json, issuer_req_header
    
#     # first transaction

#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 30

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2 #payment
#     }

#     post 'api/v1/quote', quote_data.to_json, @req_headers
#     q_result = JSON.parse(last_response.body)

#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: @sender_master ,
#       to_account: @recipient_master, 
#       signer: @sender_signer,
#       amount: q_result['tx_amount'],
#       fee: q_result['anchor_fee']
#     )
    

#     notes = 'Here is $30 :)'

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: notes,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2, # payment
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, @req_headers
#     _(last_response.status).must_equal 201 # created

#     tx_result = JSON.parse(last_response.body)
#     anchor_fee = tx_result['tx_amount'] * Oblip::Entity::Transaction::ANCHOR_PERCENT_FEES[:payment]
#     anchor_fee += @sender_vault.initial_operation_cost # added for the first transaction

#     # charged for the initial vault cost
#     _(tx_result['anchor_fee']).must_equal anchor_fee

#     # second transaction

#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 50

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2 #payment
#     }

#     post 'api/v1/quote', quote_data.to_json, @req_headers
#     q_result = JSON.parse(last_response.body)

#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: @sender_master ,
#       to_account: @recipient_master, 
#       signer: @sender_signer,
#       amount: q_result['tx_amount'],
#       fee: q_result['anchor_fee']
#     )
    

#     notes = 'Here is $50'

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: notes,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2, # payment
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, @req_headers
#     _(last_response.status).must_equal 201 # created


#     tx_result = JSON.parse(last_response.body)
#     anchor_fee = tx_result['tx_amount'] * Oblip::Entity::Transaction::ANCHOR_PERCENT_FEES[:payment]


#     # not charged for the initial vault cost
#     _(tx_result['anchor_fee']).must_equal anchor_fee

#   end

#   it 'SAD: should attempt a transaction > 100 with KYC level 1' do
#     # funding this account:
    
#     topup_amount = 120
#     issuer_req_header = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
#     }

#     data = {
#       recipient_id: @sender_account.oblip_account.id,
#       amount: topup_amount
#     }

#     post 'api/v1/topup', data.to_json, issuer_req_header

#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 205

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2 #payment
#     }

#     post 'api/v1/quote', quote_data.to_json, @req_headers
#     q_result = JSON.parse(last_response.body)

#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: @sender_master ,
#       to_account: @recipient_master, 
#       signer: @sender_signer,
#       amount: q_result['tx_amount'],
#       fee: q_result['anchor_fee']
#     )

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: '',
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2, # payment
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, @req_headers
#     _(last_response.status).must_equal 400 # bad request (policy restriction)

#   end

#   it 'SAD: should attempt a p2p transaction with a zero balance account' do
#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 2

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2 #payment
#     }

#     post 'api/v1/quote', quote_data.to_json, @req_headers
#     result = JSON.parse(last_response.body)

#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: @sender_master ,
#       to_account: @recipient_master,
#       signer: @sender_signer,
#       amount: result['tx_amount'],
#       fee: result['anchor_fee'],
#     )
    
#     notes = 'Here is $2 :)'

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: notes,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2, # payment
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, @req_headers
#     _(last_response.status).must_equal 422 # cannot process

#   end

#   it 'HAPPY: should create a transaction between an agent and a personal account' do
#     # creating agent account
#     agent_account =  create_oblip_account(DATA[:api_clients][3], DATA[:oblip_accounts][3])[:account]
#     agent_master = Stellar::KeyPair.random
#     agent_signer = Stellar::KeyPair.random

#     agent_vault = Oblip::CreateVault.new.call(
#       auth_account: agent_account.oblip_account,
#       salt: 'xxx',
#       address:  agent_master.address,
#       config: Oblip::Api.config
#     )

    
#     horizon.config_new_account(agent_master, agent_signer)
#     # horizon.trust_issuer(agent_master)
#     horizon.allow_trust(agent_master.address, Oblip::Api.config['MAIN_ASSET_CODE'])

#     agent_req_headers = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{agent_account.auth_token}"
#     }

#     # topping up agent
#     topup_amount = 100
#     issuer_req_header = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
#     }

#     data = {
#       recipient_id: agent_account.oblip_account.id,
#       amount: topup_amount # oblip native currency
#     }

#     post 'api/v1/topup', data.to_json, issuer_req_header

#     # now lets get quote (prepare to make a transaction)
#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 50

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 1 # agent deposit
#     }

#     post 'api/v1/quote', quote_data.to_json, agent_req_headers
#     q_result = JSON.parse(last_response.body)

#     # create a signed envelope manually (usually this is done in the client)
#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: agent_master,
#       to_account: @recipient_master, 
#       signer: agent_signer,
#       amount: q_result['tx_amount'],
#       fee: q_result['anchor_fee']
#     )

#     notes = 'Deposit via Agent'

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: notes,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 1, # agent deposit
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, agent_req_headers
#     _(last_response.status).must_equal 201 # created
#     tx_result = JSON.parse(last_response.body)
    
#     _(tx_result['tx_amount']).must_equal q_result['tx_amount']
#     _(tx_result['anchor_fee']).must_equal 0
#     _(tx_result['agent_fee'] > 0).must_equal true
    
#     commission = Oblip::Repository::AgentCommissions
#       .find_by_reference_tx(tx_result['id'])

#     _(commission.amount).must_equal tx_result['agent_fee']
    
#   end

#   it 'HAPPY: should get one transaction by transaction id' do

#     # Topping up and account to make a transaction
#     topup_amount = 20
#     issuer_req_header = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
#     }

#     data = {
#       recipient_id: @sender_account.oblip_account.id,
#       amount: topup_amount
#     }

#     post 'api/v1/topup', data.to_json, issuer_req_header
#     _(last_response.status).must_equal 201 # created

#     # Getting a Quote
#     bzd_currency = Oblip::Repository::For[Oblip::Entity::Currency].find_code('BZD')
#     bzd_amount = 2

#     quote_data = {
#       source_currency_id: bzd_currency.id,
#       source_amount: bzd_amount,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2 #payment
#     }

#     post 'api/v1/quote', quote_data.to_json, @req_headers
#     q_result = JSON.parse(last_response.body)

#     signed_envelope = horizon.create_olp_signed_payment_envelope(
#       from_account: @sender_master ,
#       to_account: @recipient_master, 
#       signer: @sender_signer,
#       amount: q_result['tx_amount'],
#       fee: q_result['anchor_fee']
#     )
    
#     # Making a transaction

#     notes = 'Here is $2 :)'

#     data = {
#       source_currency_id: bzd_currency.id, # BZD
#       source_amount: bzd_amount,
#       notes: notes,
#       recipient_account_id: @recipient_account.oblip_account.id,
#       type_id: 2, # payment
#       signed_tx_envelope: signed_envelope
#     }

#     post 'api/v1/transactions', data.to_json, @req_headers
#     _(last_response.status).must_equal 201 # created
#     tx_result = JSON.parse(last_response.body)

#     get "api/v1/transactions/#{tx_result['id']}", nil, @req_headers
#     _(last_response.status).must_equal 200 # ok
#     tx_data = JSON.parse(last_response.body)

#     _(tx_data['id']).must_equal tx_result['id']
    
#   end

#   describe 'Test Retrieving Many Transactions Handling' do
#     before do
#       # Topping up an account to make a transaction
#       topup_amount = 100
#       issuer_req_header = { 
#         'CONTENT_TYPE' => 'application/json',
#         'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
#       }

#       data = {
#         recipient_id: @sender_account.oblip_account.id,
#         amount: topup_amount
#       }

#       post 'api/v1/topup', data.to_json, issuer_req_header

#       DATA[:transactions].each do |tx_data|
#         quote_data = {
#           source_currency_id: tx_data['source_currency_id'],
#           source_amount: tx_data['source_amount'],
#           recipient_account_id: @recipient_account.oblip_account.id,
#           type_id: tx_data['type_id']
#         }

#         post 'api/v1/quote', quote_data.to_json, @req_headers
#         q_result = JSON.parse(last_response.body)

#         signed_envelope = horizon.create_olp_signed_payment_envelope(
#           from_account: @sender_master ,
#           to_account: @recipient_master, 
#           signer: @sender_signer,
#           amount: q_result['tx_amount'],
#           fee: q_result['anchor_fee']
#         )

#         # Making a transaction

#         notes = 'Test Note'
    
#         data = {
#           source_currency_id: tx_data['source_currency_id'], # BZD
#           source_amount: tx_data['source_amount'],
#           notes: notes,
#           recipient_account_id: @recipient_account.oblip_account.id,
#           type_id:  tx_data['type_id'], # payment
#           signed_tx_envelope: signed_envelope
#         }
    
#         post 'api/v1/transactions', data.to_json, @req_headers
#         tx_result = JSON.parse(last_response.body)
#       end

#     end

#     it 'HAPPY: should get a list of transactions of a logged in user' do
#       get 'api/v1/transactions', nil, @req_headers
#       _(last_response.status).must_equal 200 # ok
#       result = JSON.parse(last_response.body)
#       _(result['transactions'].size > 0).must_equal true
#     end

#     it 'HAPPY: should get only the first two transactions of a logged in user' do
#       get 'api/v1/transactions?limit=2', nil, @req_headers
#       _(last_response.status).must_equal 200 # ok

#       result = JSON.parse(last_response.body)
#       _(result['transactions'].size == 2).must_equal true
#     end

#     it 'HAPPY: should get only get the 2nd transaction of a logged in user' do
#       get 'api/v1/transactions?offset=1&limit=1', nil, @req_headers
#       _(last_response.status).must_equal 200 # ok

#       result = JSON.parse(last_response.body)
#       _(result['transactions'].size == 1).must_equal true
#     end

#     it 'HAPPY: should get transactions between a two oblip accounts' do
#       profile_id = @recipient_account.oblip_account.id
#       get "api/v1/profile/#{profile_id}/transactions", nil, @req_headers
#       _(last_response.status).must_equal 200 # ok
#       result = JSON.parse(last_response.body)
#       _(result['transactions'].size > 0).must_equal true
      
#       # TODO: need more tests here
#     end

#     it 'HAPPY: should get transactions of a fund account' do
#       fund_account_id = @sender_account.oblip_account.default_fund_id
#       get "api/v1/fund_accounts/#{fund_account_id}/transactions", nil, @req_headers
#       _(last_response.status).must_equal 200 # ok
#       result = JSON.parse(last_response.body)
#       _(result['transactions'].size > 0).must_equal true
      
#       # TODO: need more tests here
#     end
#   end
# end