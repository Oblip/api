# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'minitest/autorun'
require 'minitest/rg'
require 'yaml'

require_relative 'test_load_all'

load 'Rakefile'

# rubocop:disable Metrics/AbcSize
def wipe_database
  Oblip::Database::StaffAccountOrm.dataset.destroy
  Oblip::Database::PayoutOrm.dataset.destroy
  Oblip::Database::VaultAssetOrm.dataset.destroy
  Oblip::Database::WithdrawalOrderOrm.dataset.destroy
  Oblip::Database::DepositOrderOrm.dataset.destroy
  Oblip::Database::TransactionOrm.dataset.destroy
  Oblip::Database::EscrowFundOrm.dataset.destroy
  Oblip::Database::OblipLocationOrm.dataset.destroy
  Oblip::Database::OblipAccountBankOrm.dataset.destroy
  Oblip::Database::ContactOrm.dataset.destroy
  Oblip::Database::ApiClientOrm.dataset.destroy
  Oblip::Database::OblipAccountOrm.dataset.destroy
  Oblip::Database::BusinessOrm.dataset.destroy
  Oblip::Database::PersonalDetailOrm.dataset.destroy
  Oblip::Database::VaultOrm.dataset.destroy
  Oblip::Database::BlockchainFundOrm.dataset.destroy
end
# rubocop:enable Metrics/AbcSize

# seed files
account_seed_file = File.read('infrastructure/database/seeds/account_seeds.yml')
transaction_seed_file = File.read('infrastructure/database/seeds/transaction_seeds.yml')
api_client_seed_file = File.read('infrastructure/database/seeds/api_client_seeds.yml')
bank_account_seed_file = File.read('infrastructure/database/seeds/oblip_account_bank_seeds.yml')
deposit_file = File.read('infrastructure/database/seeds/deposit_seeds.yml')
blockchain_funds_file = File.read('infrastructure/database/seeds/blockchain_fund_seeds.yml')
staff_file = File.read('infrastructure/database/seeds/staff_account_seeds.yml')
business_seed_file =File.read('infrastructure/database/seeds/business_seeds.yml')

DATA = {
  oblip_accounts: YAML.safe_load(account_seed_file),
  transactions: YAML.safe_load(transaction_seed_file),
  api_clients: YAML.safe_load(api_client_seed_file),
  bank_accounts: YAML.safe_load(bank_account_seed_file),
  deposits: YAML.safe_load(deposit_file),
  blockchain_funds: YAML.safe_load(blockchain_funds_file),
  staff_accounts: YAML.safe_load(staff_file),
  businesses: YAML.safe_load(business_seed_file)
}.freeze

def intialize_blockchain_fund_seeds(data)
  data.each do |record|
    Oblip::Database::BlockchainFundOrm.create(
      amount_purchased: record['amount_purchased'],
      balance: record['balance'],
      price_purchased: record['price_purchased'],
      incurred_fees: record['incurred_fees'],
      asset_name: record['asset_name']
    )
  end
end

def register_mobile_client(client_data, verification_flag = false)
  client_keypair = VerifyRequest.generate_keypair
  client_data['public_key'] = client_keypair[:verify_key]
  headers = {'CONTENT_TYPE' => 'application/json'}

  post 'api/v1/clients/mobile', client_data.to_json, headers
  result = JSON.parse(last_response.body)
  # _(last_response.status).must_equal 201

  if verification_flag
    Oblip::Repository::ApiClients
      .update_verification_flag(result['id'], true)
    
    result['is_verified'] = true
  end

  {
    keypair: client_keypair,
    result: result
  }
end

def create_oblip_account(client_data, account_data, verification_flag = true)
  client = register_mobile_client(client_data, verification_flag)

  # verify_request = VerifyRequest.new(device_keypair.verify_key)
  sign_request = SignRequest.new(client[:keypair][:signing_key])
  signed = sign_request.sign(account_data)
  headers = {'CONTENT_TYPE' => 'application/json'}

  new_account_data = {
    client_id: client[:result]['id'],
    data: account_data,
    signature: signed[:signature]
  }

  post 'api/v1/oblip_account', new_account_data.to_json, headers
  result = JSON.parse(last_response.body)

  {
    client: client,
    auth_tokens: result
  }
end

def test_topup_account(oblip_account_id, amount)
  issuer_api_token = Oblip::Api.config['OBLIP_ISSUER_API_TOKEN']
  issuer_req_header = { 
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
  }

  data = {
    recipient_id: oblip_account_id,
    amount: amount
  }

  post 'api/v1/topup', data.to_json, issuer_req_header
end

def get_quote(asset:, amount:, recipient_account_id:, type_id:, headers:)
  quote_data = {
    asset_code: asset.code,
    amount: amount,
    recipient_account_id: recipient_account_id,
    type_id: type_id
  }

  post 'api/v1/quote', quote_data.to_json, headers
  JSON.parse(last_response.body)
end

def setup_account(horizon, device_data, oblip_account_data, fund_amount = 0)
  oblip_data =  create_oblip_account(device_data, oblip_account_data)
  auth_tokens = oblip_data[:auth_tokens]
  # puts auth_tokens
  access_token = auth_tokens['access_data']['token']
  client = oblip_data[:client]

  req_headers = {
    'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }

  get 'api/v1/oblip_account', nil, req_headers
  user_data = JSON.parse(last_response.body)

  master = Stellar::KeyPair.random
  signer = Stellar::KeyPair.random

  data = {
    address: master.address,
    salt: SecureDB.new_salt
  }

  post 'api/v1/vault', data.to_json, req_headers
  vault_data = JSON.parse(last_response.body)

  # lets configure the new account
  horizon.config_new_account('BZD', master, signer)
  #asset_code = Oblip::Api.config['MAIN_ASSET_CODE'] 

  # we need to trust the account
  h = horizon.allow_trust(master.address, 'BZD')

  # create/update assets
  accountMapper = Oblip::Horizon::AccountMapper.new(Oblip::Api.config)
  blockchain_data = accountMapper.load(master.address)
  
  vault = Oblip::Repository::Vaults.add_or_update_vault_and_assets_using_raw(
    vault_data['id'],
    blockchain_data
  )

  # fund account
  test_topup_account(user_data['id'], fund_amount) if fund_amount > 0

  {
    account: user_data,
    auth_tokens: auth_tokens,
    client: client,
    vault: vault, # will be an object not hash
    master: master,
    signer: signer
  } 
end

def create_business(access_token,  business_seed)
  req_headers = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }
  
  post 'api/v1/oblip_account/business', business_seed.to_json, req_headers
  JSON.parse(last_response.body)
end

def setup_bank_account(access_token, bk_acc_seed, should_verify = true)
  req_headers = { 
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }

  data = {
    bank_id: bk_acc_seed['bank_id'],
    account_number: bk_acc_seed['account_number']
  }

  post 'api/v1/oblip_account/banks', data.to_json, req_headers
  bank_account = JSON.parse(last_response.body)

  if should_verify
     # now let's fake verify the bank account
    Oblip::Repository::OblipAccountBanks.update_status(bank_account['id'],
      Oblip::Entity::Status::OblipAccountBank::VERIFIED)

    bank_account['status']['id'] = Oblip::Entity::Status::OblipAccountBank::VERIFIED.id
    bank_account['status']['name'] = Oblip::Entity::Status::OblipAccountBank::VERIFIED.name
  end

  bank_account
end