# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'phonelib'

describe 'Test Bank Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    account_seed = DATA[:oblip_accounts][0]
    @country_code = Phonelib.parse(account_seed['phone_number']).country

    new_data = create_oblip_account(DATA[:api_clients][0], account_seed)
    auth_tokens = new_data[:auth_tokens]


    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }
  end

  it 'HAPPY: should get all the company banks accounts of a specific country' do

    get 'api/v1/company_bank_accounts', nil, @req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['accounts'].size > 0).must_equal true
    _(result['accounts'][0]['bank']['country']['code']).must_equal @country_code
  end

  # TODO: add test for specific bank using id

  
end