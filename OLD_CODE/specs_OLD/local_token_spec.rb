# frozen_string_literal: true
$VERBOSE = nil

require_relative 'spec_helper.rb'

def test_token_spec (new_data)
  
    refresh_token = new_data[:auth_tokens]['refresh_token']

    data = {
      refresh_token: refresh_token
    }

    sign_request = SignRequest.new(new_data[:client][:keypair][:signing_key])
    signed = sign_request.sign(data)
    headers = {'CONTENT_TYPE' => 'application/json'}

    data = {
      data: data,
      signature: signed[:signature]
    }

    post 'api/v1/token', data.to_json, headers
end


describe 'Tests for handling Api Clients' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  # it 'HAPPY: not allow to get a new access token <= 30 minutes' do
  #   new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
  #   test_token_spec(new_data)

  #   _(last_response.status).must_equal 400 # bad request (PolicyRestriction)
  # end

  it 'HAPPY: should get a new access token if last issused token has expired' do
    old_expiration_window = Oblip::Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW
    Oblip::Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW = 1
    new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    sleep(3)
    test_token_spec(new_data)
    _(last_response.status).must_equal 201
    Oblip::Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW = old_expiration_window
  end
end
