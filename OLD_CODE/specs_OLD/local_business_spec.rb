# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Business Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    user_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][5])
    auth_tokens = user_data[:auth_tokens]
    @token = auth_tokens['access_data']['token']
  end

  it 'HAPPY: should get an account\'s business info' do
    seed = DATA[:businesses][0]
    create_business(@token, seed)
    _(last_response.status).must_equal 201


    req_header = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{@token}"
    }

    get 'api/v1/oblip_account/business', nil, req_header
    business_data = JSON.parse(last_response.body)


    _(business_data['brand_name']).must_equal seed['brand_name']
    _(business_data['registration_name']).must_equal seed['registration_name']
    _(business_data['address']).must_equal seed['address']
  end

  it 'SAD: should not allow a non-business or non-agent oblip account type create a business' do
    user_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = user_data[:auth_tokens]
    token = auth_tokens['access_data']['token']

    seed = DATA[:businesses][0]
    create_business(token, seed)
    _(last_response.status).must_equal 400
  end

  it 'HAPPY: should create a new business' do

    base64 = File.open('infrastructure/database/seeds/image.jpg', 'rb') do |img|
      Base64.strict_encode64(img.read)
    end

    seed = DATA[:businesses][0]
    seed['logo_base64'] = base64

    business_data  = create_business(@token, seed)
    _(last_response.status).must_equal 201

    req_header = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{@token}"
    }
  
    get 'api/v1/oblip_account', nil, req_header
    account = JSON.parse(last_response.body)

    _(account['business']['id']).must_equal business_data['id']
    _(business_data['brand_name']).must_equal seed['brand_name']
    _(business_data['registration_name']).must_equal seed['registration_name']
    _(business_data['address']).must_equal seed['address']
  end
end