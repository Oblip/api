# frozen_string_literal: true

# run pry -r <path/to/this/file>
require './init.rb'
require 'rack/test'

# rubocop:disable Style/MixinUsage
include Rack::Test::Methods
# rubocop:enable Style/MixinUsage

def app
  Oblip::Api
end
