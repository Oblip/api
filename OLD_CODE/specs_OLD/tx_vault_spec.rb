# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'stellar-base'

horizon = Oblip::Horizon::Api.new(Oblip::Api.config)

describe 'Test Account Vault Handling' do
  include Rack::Test::Methods

  before do
    wipe_database

    # initialize test blockchain funds
    intialize_blockchain_fund_seeds(DATA[:blockchain_funds])

    user_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = user_data[:auth_tokens]
    
    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }
  end

  it 'HAPPY: should create a logged in user\'s account vault' do
    keypair = Stellar::KeyPair.random
    data = {
      address:  keypair.address,
      salt: SecureDB.new_salt
    }

    post 'api/v1/vault', data.to_json, @req_headers
    _(last_response.status).must_equal 201

  end

  it 'HAPPY: should authorize an asset issued by Oblip' do
    keypair = Stellar::KeyPair.random
    signer = Stellar::KeyPair.random

    data = {
      address:  keypair.address,
      salt: 'xxx'
    }
    post 'api/v1/vault', data.to_json, @req_headers

    asset_code = 'BZD'

    horizon.config_new_account(asset_code, keypair, signer)
   
    
    data = {
      asset_code: asset_code
    }

    post 'api/v1/vault/authorize_asset', data.to_json, @req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['code']).must_equal asset_code
  end

  it 'HAPPY: should get the logged in user\'s account vault' do
    keypair = Stellar::KeyPair.random
    salt = SecureDB.new_salt
    data = {
      address:  keypair.address,
      salt: salt
    }

    # created a new vault
    post 'api/v1/vault', data.to_json, @req_headers
    _(last_response.status).must_equal 201
    new_vault = JSON.parse(last_response.body)

    get 'api/v1/vault', nil,  @req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['id']).must_equal new_vault['id']
    _(result['address']).must_equal new_vault['address']
    _(result['salt']).must_equal salt
    _(result['initial_native_funds']).must_equal Oblip::Entity::Vault::INITIAL_XLM_BALANCE
    _(result['sequence_number']).must_equal new_vault['sequence_number']
    _(result['assets'].size > 0).must_equal true
    _(result['assets'][0]['type']['id']).must_equal 1 # native (XLM)
  end

  it 'HAPPY: should get all the assets of a vault' do
    account = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3], 110)

    auth_tokens = account[:auth_tokens]

    req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }

    get 'api/v1/vault/assets', nil, req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['assets'].size > 0).must_equal true
  end
end