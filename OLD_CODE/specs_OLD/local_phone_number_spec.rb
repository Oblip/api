# # frozen_string_literal: true

# require_relative 'spec_helper.rb'

# describe 'Test Account Phone Number Handling' do
#   include Rack::Test::Methods

#   before :each do
#     wipe_database

#     @logged_in_account = Oblip::CreateOblipAccount.new.call(
#       email: DATA[:oblip_accounts][0]['email'],
#       password: DATA[:oblip_accounts][0]['password'],
#       first_name: DATA[:oblip_accounts][0]['first_name'],
#       middle_name: DATA[:oblip_accounts][0]['middle_name'],
#       last_name: DATA[:oblip_accounts][0]['last_name'],
#       type: DATA[:oblip_accounts][0]['type'],
#       config: Oblip::Api.config
#     )

#     @logged_in_account = @logged_in_account.value.message

#     @req_headers = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{@logged_in_account.auth_token}"
#     }
#   end

#   it 'HAPPY: should save the phone number of the logged in Oblip Account' do
#     phone_number = '+886900418916'
#     phone_code = '+886'
#     # last_record = Oblip::Database::OblipAccountOrm.last
#     # last_record.phone_number = phone_number
#     # last_record.save

#     Oblip::SavePhoneNumber.new.call(
#       config: Oblip::Api.config,
#       auth_account: @logged_in_account.oblip_account,
#       phone_number: phone_number
#     )

#     get 'api/v1/oblip_account/phone_number', nil, @req_headers
#     _(last_response.status).must_equal 200
#     result = JSON.parse(last_response.body)

#     _(result['phone_number']).must_equal phone_number
#     _(result['country']['phone_code']).must_equal '+886'
#   end

#   it 'SAD: it should now allow access to resource without a proper auth token' do
#     @req_headers['HTTP_AUTHORIZATION'] = 'xwerwerewwrwerwe'
#     get 'api/v1/oblip_account/phone_number', nil, @req_headers
#     _(last_response.status).must_equal 404
#   end
  
#   it 'HAPPY: should update the user\'s phone number, phone number_token and country' do
#     phone_number = '+886900418916'
#     phone_code = '+886'
    
#     data = {
#       phone_number: phone_number
#     }

#     put 'api/v1/oblip_account/phone_number', data.to_json, @req_headers
#     _(last_response.status).must_equal 200
#     result = JSON.parse(last_response.body)

#     _(result['phone_number']).must_equal phone_number
#     _(result['personal_details']['country']['phone_code']).must_equal '+886'

   
#   end
# end