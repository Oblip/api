# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Sending SMS Code Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
    @req_headers = { 
      'CONTENT_TYPE' => 'application/json'
    }
  end

  it 'should send a sms code to a phone number' do
    phone_number = '+886900418916'
    phone_code = '+886'

    data = {
      phone_number: phone_number
    }

    client = register_mobile_client(DATA[:api_clients][0])
    sign_request = SignRequest.new(client[:keypair][:signing_key])
    signed = sign_request.sign(data)
    
    payload = {
      data: data,
      signature: signed[:signature],
      client_id: client[:result]['id']
    }

    post 'api/v1/sms_manager/send_verification_code', payload.to_json, @req_headers
    _(last_response.status).must_equal 200
    
    result = JSON.parse(last_response.body)
  end
end
