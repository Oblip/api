# frozen_string_literal: true

require_relative 'spec_helper.rb'

horizon = Oblip::Horizon::Api.new(Oblip::Api.config)

issuer_api_token = Oblip::Api.config['OBLIP_ISSUER_API_TOKEN']

describe 'Test Profile Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    intialize_blockchain_fund_seeds(DATA[:blockchain_funds])

    @agent_data = setup_account(horizon,DATA[:api_clients][3], DATA[:oblip_accounts][3])

    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{issuer_api_token.strip}"
    }
  end

  it 'HAPPY: should allow the issuer to directly top-up a user\'s account' do
    amount = 10
    
    # TODO: add asset_code to topup
    data = {
      recipient_id: @agent_data[:account]['id'],
      amount: amount
    }

    post 'api/v1/topup', data.to_json, @req_headers
    _(last_response.status).must_equal 201 # created
    tx_result = JSON.parse(last_response.body)

    _(tx_result['recipient_account']['id']).must_equal @agent_data[:account]['id']
    # _(tx_result['recipient_fund']['balance']).must_equal amount - 1 # 1 OLP fee
    _(tx_result['type']['id']).must_equal Oblip::Entity::Transaction::Types::DEPOSIT.id
    _(tx_result['anchor_asset']['code']).must_equal 'BZD'
    _(tx_result['status']['id']).must_equal 2 # completed
  end
end
