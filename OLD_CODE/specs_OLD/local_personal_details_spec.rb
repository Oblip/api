# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'base64'

describe 'Test Account Personal Detail Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = new_data[:auth_tokens]

    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }
  end

  it 'HAPPY: should be able to get the personal details of a new Oblip Account' do
    get 'api/v1/oblip_account/personal_details', nil, @req_headers
    _(last_response.status).must_equal 200

    last_record = Oblip::Database::PersonalDetailOrm.last
    result = JSON.parse(last_response.body)

    _(result['id']).must_equal last_record.id
    _(result['first_name']).must_equal last_record.first_name
    _(result['last_name']).must_equal last_record.last_name
  end

  it 'SAD: it should now allow access to resource without a proper auth token' do
    @req_headers['HTTP_AUTHORIZATION'] = 'xwerwerewwrwerwe'
    get 'api/v1/oblip_account/personal_details', nil, @req_headers
    _(last_response.status).must_equal 404
  end

  it 'HAPPY: should update a logged in user\'s personal details' do
    new_data = {
      birthday: '1994-05-25',
      gender: 'male'
    }

    put 'api/v1/oblip_account/personal_details', new_data.to_json, @req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    last_record = Oblip::Database::PersonalDetailOrm.last
    result = JSON.parse(last_response.body)

    _(result['id']).must_equal last_record.id
    _(result['first_name']).must_equal last_record.first_name
    _(result['last_name']).must_equal last_record.last_name
    _(result['birthday']).must_equal last_record.birthday
    _(result['gender']).must_equal last_record.gender
  end


  it 'HAPPY: should upload and save a photo' do
    base64 = File.open('infrastructure/database/seeds/image.jpg', 'rb') do |img|
        Base64.strict_encode64(img.read)
    end

    data = {
      base64_file: base64
    }

    post 'api/v1/oblip_account/personal_details/account_photo', data.to_json, @req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    # deletes the file from the cloud
    url = URI.parse(result['photo_url'])
    parts = url.path.split('/')
    key = 'account_photos/' + parts[parts.size - 1]

    Oblip::CloudStorage.delete(bucket: :app, name: key )
  end
 
end
