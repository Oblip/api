# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Profile Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    first_user_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    first_auth_tokens = first_user_data[:auth_tokens]


    second_user_data = create_oblip_account(DATA[:api_clients][1], DATA[:oblip_accounts][1])
    second_auth_tokens = second_user_data[:auth_tokens]

    @first_req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{first_auth_tokens['access_data']['token']}"
    }

    @second_req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{second_auth_tokens['access_data']['token']}"
    }

    get 'api/v1/oblip_account', nil, @second_req_headers
    @second_account = JSON.parse(last_response.body)
  end


  it 'HAPPY: should get a profile basic information' do

    id = @second_account['id']

    get "api/v1/profile/#{id}", nil, @first_req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['account']['id']).must_equal @second_account['id']
  end

  
end