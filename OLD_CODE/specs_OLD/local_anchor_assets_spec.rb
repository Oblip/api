# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Currencies Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    auth_tokens = new_data[:auth_tokens]
    
    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{auth_tokens['access_data']['token']}"
    }

  end


  it 'HAPPY: should get all anchor assets from our database' do
    get 'api/v1/anchor_assets', nil, @req_headers
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)
    _(result['assets'].size > 0).must_equal true
  end
end
