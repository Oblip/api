# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'stellar-base'

horizon = Oblip::Horizon::Api.new(Oblip::Api.config)

def test_quote(sender_data, recipient_data, amount, tx_type_id, calc_class, fee_type)
  headers = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{sender_data[:auth_tokens]['access_data']['token']}"
  }

  # should give us different anchor fees
  2.times do |n|
    # let's assume we made 1 transaction and paid the vault cost and we are going for 2nd tx
    sender_data[:vault] = Oblip::Repository::Vaults.update_paid_status(sender_data[:vault].id, true) if n == 1

    # bzd_amount = 20
    asset= Oblip::Repository::AnchorAssets.find_code('BZD')

    if calc_class == Oblip::Entity::Transaction::Deposit
      tx_fees = calc_class.calculate_fees(asset, amount, fee_type)
    else
      tx_fees = calc_class.calculate_fees(sender_data[:vault], asset, amount, fee_type)
    end
   
    data = {
      asset_code: asset.code, # BZD
      amount: amount,
      recipient_account_id:  recipient_data ? recipient_data[:account]['id'] : nil,
      type_id: tx_type_id
    }

    post 'api/v1/quote', data.to_json, headers

    _(last_response.status).must_equal 200 # created
    result = JSON.parse(last_response.body)

    _(result['amount']).must_equal amount
    _(result['type']['id']).must_equal  tx_type_id
    _(result['anchor_fee_breakdown'].nil?).must_equal false
    _(result['unsigned_tx_envelope'].nil?).must_equal false
    _(result['anchor_fee']).must_equal tx_fees[:anchor_fee]
    _(result['agent_fee']).must_equal tx_fees[:agent_fee]
  end
end

describe 'Test Quote Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    intialize_blockchain_fund_seeds(DATA[:blockchain_funds])
  end

  # personal
  describe 'Test Transaction Quotes from a Personal Account to other accounts' do
    before :each do
      @sender_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
    end

    describe 'Test Payment Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::PAYMENT.id
        @calc_class = Oblip::Entity::Transaction::Payment
      end
      it 'HAPPY: should test two payment transaction quote between two personal accounts' do
        recipient_data = setup_account(horizon, DATA[:api_clients][1], DATA[:oblip_accounts][1])
        
        test_quote(@sender_data, recipient_data, 20,
          @tx_type_id, @calc_class, @calc_class::PERSONAL_PERSONAL)
      end
  
      it 'HAPPY: should test two payment transaction between a personal account and a business' do
        recipient_data = setup_account(horizon, DATA[:api_clients][2], DATA[:oblip_accounts][5])
        
        test_quote(@sender_data, recipient_data, 10,
          @tx_type_id,  @calc_class, @calc_class::PERSONAL_BUSINESS)
      end
    end

    describe 'Test Withdrawal Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::WITHDRAWAL.id
        @calc_class = Oblip::Entity::Transaction::Withdrawal
      end
      it 'HAPPY: should test two withdrawal transaction quotes between a personal account and an anchor' do
        
        test_quote(@sender_data, nil, 200,
          @tx_type_id, @calc_class, @calc_class::PERSONAL_ANCHOR)
      end

      it 'HAPPY: should test two withdrawal transaction quotes between a personal account and an agent' do
        recipient_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])

        test_quote(@sender_data, recipient_data, 130,
          @tx_type_id,@calc_class, @calc_class::PERSONAL_AGENT)
      end
    end
  end

  # agent
  describe 'Test Transaction Quotes from a Agent Account to other accounts' do
    before :each do
      @agent_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])
    end

    describe 'Test Payment Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::PAYMENT.id
        @calc_class = Oblip::Entity::Transaction::Payment
      end
  
      it 'HAPPY: should test two payment transaction between an agent account and a business' do
        recipient_data = setup_account(horizon, DATA[:api_clients][2], DATA[:oblip_accounts][5])
        
        test_quote(@agent_data, recipient_data, 10,
          @tx_type_id, @calc_class, @calc_class::AGENT_BUSINESS)
      end
    end

    describe 'Test Withdrawal Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::WITHDRAWAL.id
        @calc_class = Oblip::Entity::Transaction::Withdrawal
      end
      it 'HAPPY: should test two withdrawal transaction quotes between a agent account and an anchor' do
        
        test_quote(@agent_data, nil, 100,
          @tx_type_id, @calc_class, @calc_class::AGENT_ANCHOR)
      end
    end

    describe 'Test Deposit Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
        @calc_class = Oblip::Entity::Transaction::Deposit
      end
      it 'HAPPY: should test two deposit transaction quotes between a agent account and a personal account' do
        recipient_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])

        test_quote(@agent_data, recipient_data, 20,
          @tx_type_id, @calc_class, @calc_class::AGENT_PERSONAL)
      end

      it 'HAPPY: should test two deposit transaction quotes between two agent accounts' do
        recipient_data = setup_account(horizon, DATA[:api_clients][4], DATA[:oblip_accounts][4])

        test_quote(@agent_data, recipient_data, 100,
          @tx_type_id, @calc_class, @calc_class::AGENT_AGENT)
      end
    end
  end

  # business
  describe 'Test Transaction Quotes from a Business Account to other accounts' do
    before :each do
      @sender_data = setup_account(horizon, DATA[:api_clients][5], DATA[:oblip_accounts][5])
    end

    describe 'Test Payment Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::PAYMENT.id
        @calc_class = Oblip::Entity::Transaction::Payment
      end
      it 'HAPPY: should test two payment transaction quote between two business accounts' do
        recipient_data = setup_account(horizon, DATA[:api_clients][6], DATA[:oblip_accounts][6])
        
        test_quote(@sender_data, recipient_data, 2000,
          @tx_type_id, @calc_class, @calc_class::BUSINESS_BUSINESS)
      end
  
      it 'HAPPY: should test two payment transaction between a business account and personal' do
        recipient_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][0])
        
        test_quote(@sender_data, recipient_data, 10,
          @tx_type_id,  @calc_class, @calc_class::BUSINESS_PERSONAL)
      end
    end

    describe 'Test Withdrawal Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::WITHDRAWAL.id
        @calc_class = Oblip::Entity::Transaction::Withdrawal
      end
      it 'HAPPY: should test two withdrawal transaction quotes between a business account and an anchor' do
        
        test_quote(@sender_data, nil, 3000,
          @tx_type_id, @calc_class, @calc_class::BUSINESS_ANCHOR)
      end

      it 'HAPPY: should test two withdrawal transaction quotes between a business account and an agent' do
        recipient_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])

        test_quote(@sender_data, recipient_data, 150,
          @tx_type_id,@calc_class, @calc_class::BUSINESS_AGENT)
      end
    end
  end

  # anchor
  describe 'Test Transaction Quotes from an Anchor Account to other accounts' do
  
    describe 'Test Deposit Transaction Quote' do
      before :each do
        @tx_type_id =  Oblip::Entity::Transaction::Types::DEPOSIT.id
        @calc_class = Oblip::Entity::Transaction::Deposit
      end
      it 'HAPPY: should test two deposit transaction quotes between an anchor and an agent account' do
        recipient_data = setup_account(horizon, DATA[:api_clients][3], DATA[:oblip_accounts][3])

        test_quote(recipient_data, recipient_data, 200,
          @tx_type_id, @calc_class, @calc_class::ANCHOR_AGENT)
      end

      it 'HAPPY: should test two deposit transaction quotes between an anchor and a personal account' do
        recipient_data = setup_account(horizon, DATA[:api_clients][0], DATA[:oblip_accounts][1])

        test_quote(recipient_data, recipient_data, 100,
          @tx_type_id, @calc_class, @calc_class::ANCHOR_PERSONAL)
      end

      it 'HAPPY: should test two deposit transaction quotes between an anchor and a business account' do
        recipient_data = setup_account(horizon, DATA[:api_clients][5], DATA[:oblip_accounts][6])

        test_quote(recipient_data, recipient_data, 1000,
          @tx_type_id, @calc_class, @calc_class::ANCHOR_BUSINESS)
      end

    end
  end
 
end