# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Fund Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    first_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
    @first_auth_tokens = first_data[:auth_tokens]

    second_data = create_oblip_account(DATA[:api_clients][1], DATA[:oblip_accounts][1])
    @second_auth_tokens = second_data[:auth_tokens]

    @first_req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{@first_auth_tokens['access_data']['token']}"
    }

    @second_req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{@second_auth_tokens['access_data']['token']}"
    }

    get 'api/v1/oblip_account', nil, @first_req_headers
    @first_account = result = JSON.parse(last_response.body)

    get 'api/v1/oblip_account', nil, @second_req_headers
    @second_account = result = JSON.parse(last_response.body)
  end

  it 'HAPPY: get all contacts of a logged in user' do
    post 'api/v1/contacts', {'contact_account_id' => @second_account['id']}.to_json,
      @first_req_headers

    _(last_response.status).must_equal 201
    
    get 'api/v1/contacts', nil, @first_req_headers
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)
    _(result['contacts'].size > 0).must_equal true
    _(result['contacts'][0]['contact'].nil?).must_equal false
  end

  it 'HAPPY: it should add a new contact' do
    data ={
      contact_account_id: @second_account['id']
    }

    post "api/v1/contacts", data.to_json,  @first_req_headers
    _(last_response.status).must_equal 201
    result = JSON.parse(last_response.body)

    _(result['owner_id']).must_equal @first_account['id']
    _(result['contact']['id']).must_equal @second_account['id']
  end

  it 'SAD: should not allow duplicate contact' do
    data ={
      contact_account_id: @second_account['id']
    }

    post "api/v1/contacts", data.to_json,  @first_req_headers
    post "api/v1/contacts", data.to_json,  @first_req_headers

    _(last_response.status).must_equal 400
  end

  it 'HAPPY: should delete a contact of a logged in user' do
    # add a contact first
    data ={
      contact_account_id: @second_account['id']
    }

    post "api/v1/contacts", data.to_json,  @first_req_headers
    _(last_response.status).must_equal 201
    new_contact =JSON.parse(last_response.body)

    # then, delete it
    delete "api/v1/contacts/#{new_contact['id']}", nil,  @first_req_headers
    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)
    
    _(result['id']).must_equal new_contact['id']
    _(result['owner_id']).must_equal @first_account['id']
    _(result['contact']['id']).must_equal @second_account['id']
  end
end