# # frozen_string_literal: true

# require_relative 'spec_helper.rb'

# def test_verify_email(req_header, check_invalid = false, invalidCode = false) 
#   last_account = Oblip::Database::OblipAccountOrm.first
#   email_payload = ShortCodeToken.payload(last_account.email_verification_token)
#   code = invalidCode ? 'nope' : email_payload['code']

#   post_data = {'email_code' => code}
#   post 'api/v1/account_verification/email', post_data.to_json, req_header
  
#   # expects the Auth Token to be invalid
#   if(check_invalid)
#     _(last_response.status).must_equal 404
#   else
#      # expects the Email code to not be correct
#     if(invalidCode)
#       _(last_response.status).must_equal 400
#     else
#       # everything should be awesome here.
#       _(last_response.status).must_equal 200
#       result = JSON.parse(last_response.body)

#       _(result['id']).must_equal last_account.id
#       _(result['email']).must_equal last_account.email
#       _(result['is_email_verified']).must_equal true
#     end
#   end
# end

# def test_verify_phone_number(req_header, account, check_invalid = false, invalidCode = false)
#   phone_number = '+886900418916'
#   Oblip::SavePhoneNumber.new.call(
#     config: Oblip::Api.config,
#     auth_account: account,
#     phone_number: phone_number
#   )

#   last_account = Oblip::Database::OblipAccountOrm.first
#   phone_token_payload = ShortCodeToken.payload(last_account.phone_verification_token)
#   code = invalidCode ? 'nope' :  phone_token_payload['code']

#   post_data = {'sms_code' => code }
#   post 'api/v1/account_verification/phone_number', post_data.to_json, req_header
  
#   # expects the Auth Token to be invalid
#   if(check_invalid)
#     _(last_response.status).must_equal 404
#   else
#     # expects the SMS code to not be correct
#     if(invalidCode)
#       _(last_response.status).must_equal 401
#     else
#       # everything should be awesome here.
#       _(last_response.status).must_equal 200
#       result = JSON.parse(last_response.body)

#       _(result['id']).must_equal last_account.id
#       # _(result['email']).must_equal last_account.email
#       _(result['is_phone_verified']).must_equal true
#     end
#   end
# end

# describe 'Test Account Verification Handling' do
#   include Rack::Test::Methods
#   before :each do
#     wipe_database
#     @logged_in_account = Oblip::CreateOblipAccount.new.call(
#       phone_number: DATA[:oblip_accounts][0]['phone_number'],
#       first_name: DATA[:oblip_accounts][0]['first_name'],
#       middle_name: DATA[:oblip_accounts][0]['middle_name'],
#       last_name: DATA[:oblip_accounts][0]['last_name'],
#       birthday:  DATA[:oblip_accounts][0]['birthday'],
#       type: DATA[:oblip_accounts][0]['type'],
#       config: Oblip::Api.config
#     )

#     @logged_in_account = @logged_in_account.value.message

#     @req_header = { 
#       'CONTENT_TYPE' => 'application/json',
#       'HTTP_AUTHORIZATION' => "Bearer #{@logged_in_account.auth_token}"
#     }
#   end

#   it 'HAPPY: should verify the email address of the user' do
#     test_verify_email(@req_header)
#   end

#   it 'SAD: should fail verifying the email of a logged in user' do
#     test_verify_email(@req_header, false, true)
#   end
  

#   it 'SAD: should not allow email verification without a valid auth token' do
#     @req_header['HTTP_AUTHORIZATION'] = 'xwerwerewwrwerwe'
#     test_verify_email(@req_header, true)
#   end

#   it 'HAPPY: should verify the phone number of the logged in user' do
#     test_verify_phone_number(@req_header, @logged_in_account.oblip_account)
#   end

#   # it 'SAD: should fail verifying the phone number of a logged in user' do
#   #   test_verify_phone_number(
#   #       @req_header, 
#   #       @logged_in_account.oblip_account, 
#   #       false, 
#   #       true
#   #     )
#   # end

#   # it 'SAD: should not allow phone verification without a valid auth token' do
#   #   @req_header['HTTP_AUTHORIZATION'] = 'xwerwerewwrwerwe'
#   #   test_verify_phone_number(
#   #       @req_header, 
#   #       @logged_in_account.oblip_account, 
#   #       true
#   #     )
#   # end

#   # it 'HAPPY: should successfully verify the pin code of a logged in user' do
#   #   pin_code = '666662'
#   #   Oblip::SaveSecurePIN.call(
#   #     auth_account: @logged_in_account.oblip_account,
#   #     pin: pin_code
#   #   )

#   #   post_data = {'pin_code' => pin_code }
#   #   post 'api/v1/account_verification/pin_code', post_data.to_json, @req_header
#   #   _(last_response.status).must_equal 200
#   # end

#   # it 'SAD: should fail verifying the pin code of a logged in user' do
#   #   pin_code = '666662'
#   #   Oblip::SaveSecurePIN.call(
#   #     auth_account: @logged_in_account.oblip_account,
#   #     pin: pin_code
#   #   )

#   #   pin_code = '594021'

#   #   post_data = {'pin_code' => pin_code }
#   #   post 'api/v1/account_verification/pin_code', post_data.to_json, @req_header
#   #   _(last_response.status).must_equal 400
#   # end

#   # it 'SAD: should not allow pin code verification without a valid auth token' do
#   #   pin_code = '666662'
#   #   Oblip::SaveSecurePIN.call(
#   #     auth_account: @logged_in_account.oblip_account,
#   #     pin: pin_code
#   #   )

#   #   @req_header['HTTP_AUTHORIZATION'] = 'xwerwerewwrwerwe'

#   #   post_data = {'pin_code' => pin_code }
#   #   post 'api/v1/account_verification/pin_code', post_data.to_json, @req_header
#   #   _(last_response.status).must_equal 404
#   # end
# end
