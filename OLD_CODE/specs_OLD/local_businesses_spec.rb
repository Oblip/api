# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Businesses Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    @super_admin_data = DATA[:staff_accounts]['super_admin']

    auth_data = Oblip::Service::Staff::CreateSuperAdmin.new
      .call(@super_admin_data).value!

    access_data = auth_data[:access_data]

    @req_headers = { 
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_data.token}"
    }

  end

  it 'HAPPY: should allow staff to get a list of businesses' do
    accounts = [DATA[:oblip_accounts][5], DATA[:oblip_accounts][6]]

    2.times do |n|
      user_data = create_oblip_account(DATA[:api_clients][n], accounts[n])
      token = user_data[:auth_tokens]['access_data']['token']

      business_seed = DATA[:businesses][n]
      # creates the business of course :p
      create_business(token, business_seed)
    end

    get 'api/v1/businesses', nil, @req_headers
    _(last_response.status).must_equal 200

    data = JSON.parse(last_response.body)

    _(data['businesses'].size == 2).must_equal true
  end
end