# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'rotp'

describe 'Test Staff Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database

    # create super admin account

    @super_admin_data = DATA[:staff_accounts]['super_admin']

    auth_data = Oblip::Service::Staff::CreateSuperAdmin.new
      .call(@super_admin_data).value!
    
    @otp_secret = auth_data[:otp_secret]
  end

  it 'HAPPY: should test OTP SECRET of super admin' do
    _(@otp_secret.nil?).must_equal false
  end

  it 'HAPPY: should authenticate super admin staff' do
    signing_key = Oblip::Api.config['ALLOWED_APPLICATION_SIGNING_KEY']
    sign_request = SignRequest.new(signing_key)

    totp = ROTP::TOTP.new(@otp_secret, issuer: "Oblip Staff")


    data = {
      username: @super_admin_data['username'],
      password: @super_admin_data['password'],
      otp: totp.now
    }
    
    signed = sign_request.sign(data)
    headers = {'CONTENT_TYPE' => 'application/json'}

    auth_body = {
      data: data,
      signature: signed[:signature]
    }

    post 'api/v1/staff_accounts/auth', auth_body.to_json, headers
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)
    _(result['account'].nil?).must_equal false
    _(result['access_data'].nil?).must_equal false
    _(result['account']['username']).must_equal @super_admin_data['username']
    _(result['account']['type']['id']).must_equal Oblip::Entity::StaffAccount::Type::SUPER_ADMIN.id
  end

end