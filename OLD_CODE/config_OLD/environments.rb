# frozen_string_literal: true

require 'roda'
require 'econfig'
require_relative '../lib/secure_db.rb'
require_relative '../lib/access_token.rb'
require_relative '../lib/refresh_token.rb'
require_relative '../infrastructure/sendgrid/email_notification.rb'
# require_relative '../lib/secure_message.rb'
require_relative '../infrastructure/aws/cloud_storage.rb'

module Oblip
  # Configuration for the API
  class Api < Roda
    plugin :environments

    extend Econfig::Shortcut
    Econfig.env = environment.to_s
    Econfig.root = '.'

    configure :development, :test do
      # Allows running reload! in pry to restart entire app
      def self.reload!
        exec 'pry -r ./spec/test_load_all'
      end
    end

    # configure :development, :test do
    #   ENV['DATABASE_URL'] = 'sqlite://' + config.DB_FILENAME
    #   puts "DB NAME: #{config.FUCK}, DB_URL: #{ENV['DATABASE_URL']}"
    # end

    configure :development, :test do
      # Example of string: "postgres://YourUserName:YourPassword@YourHost:5432/YourDatabase";
      ENV['DATABASE_URL'] = "postgres://#{config.RDS_USERNAME}:#{config.RDS_PASSWORD}@#{config.RDS_HOSTNAME}:#{config.RDS_PORT}/#{config.RDS_DB_NAME}"
    end

    configure :production, :staging do
      # Example of string: "postgres://YourUserName:YourPassword@YourHost:5432/YourDatabase";
      ENV['DATABASE_URL'] = "postgres://#{ENV['RDS_USERNAME']}:#{ENV['RDS_PASSWORD']}@#{ENV['RDS_HOSTNAME']}:#{ENV['RDS_PORT']}/#{ENV['RDS_DB_NAME']}"
    end

    # For all runnable environments
    configure do
      require 'sequel'
      OBLIP_DB = Sequel.connect(ENV['DATABASE_URL'])

      # rubocop:disable Naming/MethodName
      def self.DB
        OBLIP_DB
      end
      # rubocop:enable Naming/MethodName
      CloudStorage.setup(config)
      EmailNotification.setup(config)
      SecureDB.setup(config.DB_KEY)
      AccessToken.setup(config.ACCESS_KEY)
      RefreshToken.setup(config.REFRESH_KEY)
      # SecureMessage.setup(config.MSG_KEY)
    end
  end
end
