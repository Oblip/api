# frozen_string_literal: false

require 'rest-client'
require 'stellar-base'

class SetupBlockchainAccounts
  module Networks
    TEST = "https://horizon-testnet.stellar.org"
    PUBLIC = "https://horizon.stellar.org"
  end

  module InitialBalances
    ZHENFU = 100
    FUEL_STATION = 2000 # blockchain funds
    PRIVATE_VAULT = 6
  end

  def initialize(network_url)
    @network_url = network_url
  end

  def setup(anchor_seed)
    anchor = Stellar::KeyPair.from_seed(anchor_seed)
    keys = generate_keys
    zhenfu = keys[:zhenfu]

    # steps
    configure_anchor(anchor,keys)
    create_accounts(anchor, keys)
    configure_fuel_station(keys)
    configure_zhenfu_assets(anchor, zhenfu)
    
    keys[:anchor] = anchor
    
    print_key_info(keys)

    puts "\n\nDONE!"

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def get_account_data (address)
    account_res= RestClient.get "#{@network_url}/accounts/#{address}"
    account_data = JSON.parse(account_res.body)
  end

  def submit_transaction(xdr_envelope)
    response = RestClient.post "#{@network_url}/transactions", { tx: xdr_envelope }
    JSON.parse(response.body)

  rescue RestClient::ExceptionWithResponse => e
    puts e.response
  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  private

  def print_key_info(keys)
    puts "\n\n================================"
    puts "            Keys                "
    puts "================================\n\n"

    keys.each do |name, keypair|
      puts "Key: #{name}"
      puts "Address:  #{keypair.address}"
      puts "Secret: #{keypair.seed}"
      puts "\n"
    end

    puts "\n================================\n\n"
  end

  def configure_zhenfu_assets (anchor, zhenfu)
    zhenfu_data = get_account_data(zhenfu.address)
    anchor_data = get_account_data(anchor.address)

    # static: We will trust three currencies for now: BZD, USD, TWD
    trust_bzd = Stellar::Transaction.change_trust({
      account:    zhenfu,
      sequence:  zhenfu_data['sequence'].to_i + 1,
      fee: 600,
      line: Stellar::Asset.alphanum4('BZD',  anchor)
    })

    trust_usd = Stellar::Transaction.change_trust({
      account:    zhenfu,
      sequence:  zhenfu_data['sequence'].to_i + 1,
      fee: 600,
      line: Stellar::Asset.alphanum4('USD',  anchor)
    })

    trust_twd = Stellar::Transaction.change_trust({
      account:    zhenfu,
      sequence:  zhenfu_data['sequence'].to_i + 1,
      fee: 600,
      line: Stellar::Asset.alphanum4('TWD',  anchor)
    })

    allow_bzd = Stellar::Transaction.allow_trust({
      trustor: zhenfu,
      account: anchor,
      asset: [:alphanum4,'BZD', anchor],
      authorize: true,
      fee: 600,
      sequence:  anchor_data['sequence'].to_i + 1,
    })

    allow_usd = Stellar::Transaction.allow_trust({
      trustor: zhenfu,
      account: anchor,
      asset: [:alphanum4,'USD', anchor],
      authorize: true,
      fee: 600,
      sequence:  anchor_data['sequence'].to_i + 1,
    })

    allow_twd = Stellar::Transaction.allow_trust({
      trustor: zhenfu,
      account: anchor,
      asset: [:alphanum4,'TWD', anchor],
      authorize: true,
      fee: 600,
      sequence:  anchor_data['sequence'].to_i + 1,
    })

    bundled_ops = trust_bzd.merge(allow_bzd)
      .merge(trust_usd).merge(allow_usd)
      .merge(trust_twd).merge(allow_twd)

    xdr_envelope = bundled_ops.to_envelope(zhenfu, anchor).to_xdr(:base64)
    data = submit_transaction(xdr_envelope)

    puts "Tx Hash : #{data['hash']}"
    puts "Configure Zhenfu Assets: DONE"

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def configure_fuel_station(keys)
    fuel_station = keys[:fuel_station]
    account_data = get_account_data(fuel_station.address)

    config_tx = Stellar::Transaction.set_options({
      account: fuel_station,
      fee: 100,
      sequence: account_data['sequence'].to_i + 1,
      master_weight: 4,
      high_threshold: 4,
      med_threshold: 2,
      low_threshold: 1,
      signer: Stellar::Signer.new(key: Stellar::SignerKey
        .new(:signer_key_type_ed25519, keys[:mama].raw_public_key),
         weight: 2)
    })

    xdr_envelope = config_tx.to_envelope(fuel_station).to_xdr(:base64)
    submit_transaction(xdr_envelope)

    puts "Configure Fuel Station: DONE"

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def configure_anchor(anchor, keys)
    account_data = get_account_data(anchor.address)

    config_tx = Stellar::Transaction.set_options({
      account: anchor,
      fee: 200,
      sequence: account_data['sequence'].to_i + 1,
      set: [Stellar::AccountFlags.auth_required_flag, Stellar::AccountFlags.auth_revocable_flag],
      master_weight: 4,
      high_threshold: 4,
      med_threshold: 2,
      low_threshold: 1,
      signer: Stellar::Signer.new(key: Stellar::SignerKey
        .new(:signer_key_type_ed25519, keys[:credit_issuer].raw_public_key),
         weight: 2)
    })

    baba_signer_tx = Stellar::Transaction.set_options({
      account: anchor,
      fee: 200,
      sequence: account_data['sequence'].to_i + 1,
      signer: Stellar::Signer.new(key: Stellar::SignerKey
        .new(:signer_key_type_ed25519, keys[:baba].raw_public_key),
         weight: 1)
    })

    config_bundle = config_tx.merge(baba_signer_tx)

    xdr_envelope = config_bundle.to_envelope(anchor).to_xdr(:base64)
    submit_transaction(xdr_envelope)

    puts "Configure Anchor: DONE!"

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def create_accounts(anchor, keys)
    account_data = get_account_data(anchor.address)

    zhenfu_tx = Stellar::Transaction.create_account({
      account: anchor,
      destination: keys[:zhenfu],
      fee: 300,
      sequence: account_data['sequence'].to_i + 1,
      starting_balance: InitialBalances::ZHENFU
    })

    fuel_station_tx = Stellar::Transaction.create_account({
      account: anchor,
      destination: keys[:fuel_station],
      fee: 300,
      sequence: account_data['sequence'].to_i + 1,
      starting_balance: InitialBalances::FUEL_STATION
    })

    private_vault_tx = Stellar::Transaction.create_account({
      account: anchor,
      destination: keys[:private_vault],
      fee: 300,
      sequence: account_data['sequence'].to_i + 1,
      starting_balance: InitialBalances::PRIVATE_VAULT
    })

    bundled_tx = zhenfu_tx.merge(fuel_station_tx).merge(private_vault_tx)
    xdr_envelope = bundled_tx.to_envelope(anchor).to_xdr(:base64)

    submit_transaction(xdr_envelope)
    puts "Create Accounts: DONE!\n"

  rescue StandardError => error
    puts "Create Accounts: ERROR!\n"
    puts error.message
    puts error.inspect
    puts error.backtrace
  end
  

  def generate_keys
    {
      credit_issuer: Stellar::KeyPair.random, # signer
      baba: Stellar::KeyPair.random, # signer
      mama: Stellar::KeyPair.random, # signer
      zhenfu: Stellar::KeyPair.random, # acccount
      fuel_station: Stellar::KeyPair.random, # account
      private_vault: Stellar::KeyPair.random # account
    }
  end
end