# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'minitest/autorun'
require 'minitest/rg'
require 'minitest/pride'

require 'yaml'
require 'securerandom'

require_relative 'test_load_all'

# helpers
require_relative 'helpers/utils'

load 'Rakefile'

# rubocop:disable Metrics/AbcSize
def wipe_database
  Oblip::Database::AgentCommissionOrm.dataset.destroy
  Oblip::Database::PayoutOrm.dataset.destroy
  Oblip::Database::BankAccountRequestOrm.dataset.destroy
  Oblip::Database::TransactionOrm.dataset.destroy
  Oblip::Database::BankAccountOrm.dataset.destroy
  Oblip::Database::RegisteredBankOrm.dataset.destroy
  Oblip::Database::BusinessOrm.dataset.destroy
  Oblip::Database::CheckingAccountCredOrm.dataset.destroy
  Oblip::Database::ClientOrm.dataset.destroy
  Oblip::Database::AccountOrm.dataset.destroy
  Oblip::Database::AdminAccountOrm.dataset.destroy
end

# seed files
client_seeds = File.read('infrastructure/database/seeds/client_seeds.yml')
CLIENT_SEEDS = YAML.safe_load(client_seeds)
CLIENT_SEEDS.freeze

account_seeds = File.read('infrastructure/database/seeds/account_seeds.yml')
ACCOUNT_SEEDS = YAML.safe_load(account_seeds)
ACCOUNT_SEEDS.freeze

business_seeds = File.read('infrastructure/database/seeds/business_seeds.yml')
BUSINESS_SEEDS = YAML.safe_load(business_seeds)
BUSINESS_SEEDS.freeze

admin_account_seeds = File.read('infrastructure/database/seeds/admin_account_seeds.yml')
ADMIN_ACCOUNT_SEEDS = YAML.safe_load(admin_account_seeds)
ADMIN_ACCOUNT_SEEDS.freeze

registered_bank_seeds = File.read('infrastructure/database/seeds/registered_bank_seeds.yml')
REGISTERED_BANK_SEEDS = YAML.safe_load(registered_bank_seeds)
REGISTERED_BANK_SEEDS.freeze

# helper functions

def test_token_spec (client_data, refresh_token, type = nil, business_id = nil)

  data = {
    refresh_token: refresh_token,
    type: type,
    business_id: business_id
  }

  sign_request = SignRequest.new(client_data[:keypair][:signing_key])
  signature = sign_request.sign(data)
  headers = {'CONTENT_TYPE' => 'application/json'}

  data = {
    data: data,
    signature: signature
  }

  post 'v1/token', data.to_json, headers
  _(last_response.status).must_equal 201

  JSON.parse(last_response.body)
end

# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/AbcSize
def create_client(client_seed, verification_flag = false)
  client_keypair = VerifyRequest.generate_keypair
  client_seed['verifier_key'] = client_keypair[:verify_key]

  wrapper_keypair = PublicEncryption.generate_keypair
  client_seed['wrapper_key'] = wrapper_keypair[:public_key]

  post 'v1/clients', client_seed.to_json,
       'CONTENT_TYPE' => 'application/json'
  result = JSON.parse(last_response.body)

  if verification_flag
    result = Oblip::Repository::Clients
             .update_verification_flag(result['id'], true)
    representer = Oblip::ClientRepresenter.new(result)
    result = JSON.parse(representer.to_json)
  end
  { keypair: client_keypair, wrapper_keypair: wrapper_keypair, result: result }
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/AbcSize

# Creates a new client and account
# @returns client_data { keypair: client_keypair, result: result }
def create_account(client_seed, account_seed, verification_flag = true)
  client_data = create_client(client_seed, verification_flag)
  signature = SignRequest.new(client_data[:keypair][:signing_key])
                         .sign(account_seed)
  headers = { 'CONTENT_TYPE' => 'application/json' }
  payload = {
    client_id: client_data[:result]['id'],
    data: account_seed, signature: signature
  }
  post 'v1/accounts', payload.to_json, headers
  client_data
end

def create_admin_account(admin_seed)
  entity = Oblip::Entity::AdminAccount.build(admin_seed)
  password = admin_seed['password']
  account = Oblip::Repository::AdminAccounts.create(entity, password)
  account
end

def setup_admin(admin_account_seed)

  # create admin
  # admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
  account = create_admin_account(admin_account_seed)
  email = admin_account_seed['email_address']
  password = admin_account_seed['password']

  admin_auth = auth_admin_account(email, password)
  access_token = admin_auth['access']['token']

  header = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }

  {
    account: account,
    header: header
  }
end

def create_bank_account(bank_id:,acct_number:,name:,header:)
  payload = {
    registered_bank_id: bank_id,
    account_number: acct_number,
    account_name: name
  }

  # create bank account for logged-in user
  post 'v1/bank_accounts', payload.to_json, header
  _(last_response.status).must_equal 201
  bank_account = JSON.parse(last_response.body)
end

def get_registered_banks(header)
  # get a list of registered banks
  get '/v1/registered_banks', nil, header
  _(last_response.status).must_equal 200

  registered_banks = JSON.parse(last_response.body)['registered_banks']
  registered_banks
end

def setup_business(user, business_seed, admin)
  business_seed['account_id'] = user[:account]['id']
  business_seed['username'] = random_username

  post 'v1/admin/businesses', business_seed.to_json, admin[:header]
  _(last_response.status).must_equal 201
  business = JSON.parse(last_response.body)

  # authenticate business
  business_id = business['id']
  access_type = 'business'
  refresh_token = user[:tokens]['refresh']['token']
  token_data = test_token_spec(user[:client], refresh_token, access_type, business_id)

  business_access = token_data['token']
  business_header = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{business_access}"
  }

  # create checking account
  post 'v1/account/checking_account', nil, business_header
  _(last_response.status).must_equal 201

  checking_account = JSON.parse(last_response.body)

  {
    account: business,
    header: business_header,
    checking_account: checking_account
  }
end

def setup_user(client_seed, account_seed)
 client = create_account(client_seed, account_seed)
  _(last_response.status).must_equal 201

  auth_account = JSON.parse(last_response.body)
  access_token = auth_account['tokens']['access']['token']

  header = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }

  post 'v1/account/checking_account', nil, header
  _(last_response.status).must_equal 201

  checking_account = JSON.parse(last_response.body)

  {
    account: auth_account['account'],
    tokens: auth_account['tokens'],
    client: client,
    header: header,
    checking_account: checking_account
  }
end

def auth_admin_account(email, password)
  payload = {
    email_address: email,
    password: password
  }

  header = { 'CONTENT_TYPE' => 'application/json' }

  post 'v1/admin/auth', payload.to_json, header
  _(last_response.status).must_equal 200
  JSON.parse(last_response.body)
end

# rubocop:disable MethodLength, AbcSize
def create_registered_banks
  # create admin
  admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
  create_admin_account(admin_account_seed)
  email = admin_account_seed['email_address']
  password = admin_account_seed['password']

  admin_auth = auth_admin_account(email, password)
  access_token = admin_auth['access']['token']

  header = {
    'CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  }

  # create registered banks
  REGISTERED_BANK_SEEDS.each do |seed|
    post 'v1/registered_banks', seed.to_json, header
    _(last_response.status).must_equal 201
  end
end
# rubocop:enable MethodLength, AbcSize
