# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Bank Account Request through /admin Spec' do
  include Rack::Test::Methods

  before :each do
    wipe_database
    create_registered_banks
  end

  it 'HAPPY: should get a list of all bank account requests' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    account_seed_one['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    payloads = [
      {
        amount: 100,
        # registered_bank_id: bank['id'],
        asset: 'BZD'
      },
      {
        amount: 200,
        # registered_bank_id: bank['id'],
        asset: 'BZD'
      }
    ]

    all_requests = []
    # create bank account requests
    payloads.each do |v|
      post "/v1/bank_accounts/#{bank_account['id']}/deposits", v.to_json, header
      _(last_response.status).must_equal 201
      request = results = JSON.parse(last_response.body)
      all_requests.push(request)
    end

    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    admin_access_token = admin_auth['access']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
    }

    # get a list of bank account requests as an admin
    get '/v1/admin/bank_account_requests', nil, admin_header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)
    _(results['bank_account_requests'].size == 2).must_equal true


    # attempt to get a list of bank account requests as user
    get '/v1/admin/bank_account_requests', nil, header
    _(last_response.status).must_equal 403

    # try to get a list of requests by type verified
    get '/v1/admin/bank_account_requests?status=verified', nil, admin_header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)
    _(results['bank_account_requests'].size.zero?).must_equal true

    # lets verify a request
    put "/v1/admin/bank_account_requests/#{all_requests[0]['id']}/status", 
      {status: 'verified'}.to_json, admin_header
    _(last_response.status).must_equal 200

    # try to get a list of requests by type verified
    get '/v1/admin/bank_account_requests?status=verified', nil, admin_header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['bank_account_requests'].size == 1).must_equal true

    # try to a get a list of requests by a invalid type
    get '/v1/admin/bank_account_requests?status=shit', nil, admin_header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['bank_account_requests'].size.zero?).must_equal true
  end

  it 'HAPPY: should allow an admin user to verify a deposit request' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    account_seed_one['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    amount = 200
    asset = 'BZD'
    payload = {
      amount: amount,
      # registered_bank_id: bank['id'],
      asset: asset
    }

    post "/v1/bank_accounts/#{bank_account['id']}/deposits", payload.to_json, header
    _(last_response.status).must_equal 201

    request = JSON.parse(last_response.body)

    status = 'user_confirmed'

    status_payload = {
      status: status
    }

    # let's attempt to confirm the request
    put "/v1/bank_accounts/#{bank_account['id']}/deposits/#{request['id']}/status", status_payload.to_json, header
    _(last_response.status).must_equal 200

    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    admin_access_token = admin_auth['access']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
    }

    admin_status = 'verified'
    admin_status_payload = {
      status: admin_status
    }

    # let's attempt to verify the request
    put "/v1/admin/bank_account_requests/#{request['id']}/status", admin_status_payload.to_json, admin_header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['status']['code']).must_equal 'verified'
  end
end
