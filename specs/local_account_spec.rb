# frozen_string_literal: true

require_relative 'spec_helper.rb'
require 'phonelib'

describe 'Test Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end



  # rubocop:disable Metrics/LineLength
  describe 'Creating new account' do
    it 'HAPPY: should create a new account' do
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username

      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      result = JSON.parse(last_response.body)

      _(result['account'].nil?).must_equal false
      _(result['tokens'].nil?).must_equal false
      _(result['tokens']['refresh'].nil?).must_equal false
      _(result['tokens']['access'].nil?).must_equal false

      # _(result['account']['secret_code'].nil?).must_equal false
      _(result['account']['profile']['name']['first_name']).must_equal account_seed['first_name']
      _(result['account']['profile']['name']['middle_name'].nil?).must_equal account_seed['middle_name'] == ''
      _(result['account']['profile']['name']['last_name']).must_equal account_seed['last_name']
      _(result['account']['phone_number']).must_equal Phonelib.parse(account_seed['phone_number']).to_s
      _(result['account']['id'].nil?).must_equal false
      _(result['account']['type']).must_equal 'personal'

      # check client
      client_entity = Oblip::Repository::Clients.find_id(client[:result]['id'])

      _(client_entity.account_id).must_equal result['account']['id']
      _(client_entity.secret_code.nil?).must_equal false
    end
    # rubocop:enable Metrics/LineLength

    it 'HAPPY: it should convert a valid username to small letters' do
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = 'REGGIE'

      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201

      result = JSON.parse(last_response.body)
      _(result['account']['username']).must_equal 'reggie'
    end

    it 'SAD: should not allow an invalid username' do
      
      # first test
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = 'reggie.escobar'

      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 400 # bad request

      # second test
      account_seed = ACCOUNT_SEEDS[1]
      account_seed['username'] = 'ReGGie!@Escobar'

      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 400 # bad request
    end

    it 'SAD: should not create an account with any reserved usernames' do
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = Oblip::Horizon::Api.fund[:username]
      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 400

      account_seed['username'] = 'oblip'
      client = create_account(CLIENT_SEEDS[1], account_seed)
      _(last_response.status).must_equal 400
    end

    it 'HAPPY: should get a personal account information' do
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username
      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      account = JSON.parse(last_response.body)

      account_id = account['account']['id']
      access_token = account['tokens']['access']['token']

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      get '/v1/account', nil, header
      _(last_response.status).must_equal 200

      result = JSON.parse(last_response.body)

      _(result['id']).must_equal account_id
    end

    it 'HAPPY: should get single personal account information' do
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username
      client = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      account = JSON.parse(last_response.body)

      account_id = account['account']['id']
      access_token = account['tokens']['access']['token']

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      get "/v1/accounts/#{account_id}", nil, header
      _(last_response.status).must_equal 200

      result = JSON.parse(last_response.body)

      _(result['id']).must_equal account_id
    end

    it 'HAPPY: should get single personal account while logged in as a business' do
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username
      client_data = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      account = JSON.parse(last_response.body)

      account_id = account['account']['id']
      user_refresh_token = account['tokens']['refresh']['token']

      # header = {
      #   'CONTENT_TYPE' => 'application/json',
      #   'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      # }

       # create admin
      admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
      create_admin_account(admin_account_seed)
      email = admin_account_seed['email_address']
      password = admin_account_seed['password']

      admin_auth = auth_admin_account(email, password)
      admin_access_token = admin_auth['access']['token']

      admin_header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
      }

      # create first business
      business_seed = BUSINESS_SEEDS[1]
      business_seed['username'] = random_username
      business_seed['account_id'] = account_id

      post 'v1/admin/businesses', business_seed.to_json, admin_header
      _(last_response.status).must_equal 201

      business = JSON.parse(last_response.body)

      type = 'business'
      business_id = business['id']

      access = test_token_spec(client_data, user_refresh_token, type, business_id)

      business_header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access['token']}"
      }

      get "/v1/accounts/#{account_id}", nil, business_header
      _(last_response.status).must_equal 200

      result = JSON.parse(last_response.body)

      _(result['id']).must_equal account_id
    end

    it 'SAD: should not be allowed to create an account with an existing username' do
      username = 'prodoxx'

      # first account
      account_seed1 = ACCOUNT_SEEDS[0]
      account_seed1['username'] = username

      # second account
      account_seed2 = ACCOUNT_SEEDS[1]
      account_seed2['username'] = username

      create_account(CLIENT_SEEDS[0], account_seed1)
      _(last_response.status).must_equal 201

      oblip_auth = JSON.parse(last_response.body)

      create_account(CLIENT_SEEDS[1], account_seed2)
      _(last_response.status).must_equal 400
    end

    it 'SAD: should now create an account with a username from a business that exists' do
      username = 'abc900'

      # attempting to create a business with the same username
      # create admin
      admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
      create_admin_account(admin_account_seed)
      email = admin_account_seed['email_address']
      password = admin_account_seed['password']

      admin_auth = auth_admin_account(email, password)
      access_token = admin_auth['access']['token']

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      # create oblip account
      account_seed = ACCOUNT_SEEDS[1]
      account_seed['username'] = random_username
      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201

      oblip_auth = JSON.parse(last_response.body)

      # create first business
      business_seed = BUSINESS_SEEDS[0]
      business_seed['account_id'] = oblip_auth['account']['id']
      business_seed['username'] = username

      post 'v1/admin/businesses', business_seed.to_json, header
      _(last_response.status).must_equal 201

      account_seed1 = ACCOUNT_SEEDS[0]
      account_seed1['username'] = username

      create_account(CLIENT_SEEDS[1], account_seed1)
      _(last_response.status).must_equal 400
    end
  end

  describe 'Updating existing account handling' do
    it 'HAPPY: should update a single field (email) of an existing account' do
      # creates an account
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username

      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      auth_account = JSON.parse(last_response.body)
      access_token = auth_account['tokens']['access']['token']

      # updates email
      email = 'reggie.escobar94@gmail.com'
      payload = { email: email }
      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      put 'v1/account', payload.to_json, header
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)

      _(result['email']).must_equal email
    end

    it 'HAPPY: should update multiple fields (street_name, city, state, country' do
      # creates an account
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username

      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      auth_account = JSON.parse(last_response.body)
      access_token = auth_account['tokens']['access']['token']

      # update multiple fields
      street_name = '780 Calderon Ave.'
      city = 'Mountain View'
      state = 'California'
      country_id = 2 # US
      zip_code = '9449'

      payload = {
        street_name: street_name,
        city: city,
        state: state,
        country_id: country_id,
        zip_code: zip_code
      }

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      put 'v1/account', payload.to_json, header
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)

      _(result['profile']['address']['city']).must_equal city
      _(result['profile']['address']['state']).must_equal state
      _(result['profile']['address']['street_name']).must_equal street_name
      _(result['profile']['address']['zip_code']).must_equal zip_code
      _(result['profile']['address']['country']['id']).must_equal country_id
    end

    it 'SAD: should not allow updating a single (photo_url) restricted fields' do
      # creates an account
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username

      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      auth_account = JSON.parse(last_response.body)
      access_token = auth_account['tokens']['access']['token']

      photo_url = 'http://somewebpage.com/photo.jpg'
      payload = { photo_url: photo_url }

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      put 'v1/account', payload.to_json, header
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)

      _(result['photo_url'] != photo_url).must_equal true
    end

    it 'SAD: should not allow updating a single (phone_number) restricted fields' do
      # creates an account
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username

      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      auth_account = JSON.parse(last_response.body)
      access_token = auth_account['tokens']['access']['token']

      phone_number = '+5016620652'
      payload = { phone_number: phone_number }

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      put 'v1/account', payload.to_json, header
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)

      _(result['phone_number'] != phone_number).must_equal true
    end

    it 'SAD: should ignore updating restricted fields and only allow
       allowed fields' do
      # creates an account
      account_seed = ACCOUNT_SEEDS[0]
      account_seed['username'] = random_username

      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201
      auth_account = JSON.parse(last_response.body)
      access_token = auth_account['tokens']['access']['token']

      phone_number = '+5016620652'
      street_name = '780 Calderon Ave.'
      payload = { phone_number: phone_number, street_name: street_name }

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      put 'v1/account', payload.to_json, header
      _(last_response.status).must_equal 200
      result = JSON.parse(last_response.body)

      _(result['profile']['address']['street_name']).must_equal street_name
      _(result['phone_number'] != phone_number).must_equal true
    end
  end
end
