# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Businesses' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end
  
  it 'HAPPY: Ensure the business official name is being stored and retived' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    result = JSON.parse(last_response.body)
    _(result['legal_name'].to_s.length > 0).must_equal true
  end

  it 'SAD: should not allow an invalid username for business account' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['account_id'] = oblip_auth['account']['id']
    business_seed['username'] = 'yserri./pal'

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 400
  end

  it 'HAPPY: it should convert a valid username to small letters for business account' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['account_id'] = oblip_auth['account']['id']
    business_seed['username'] = 'YSERRI'

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    result = JSON.parse(last_response.body)
    _(result['username']).must_equal 'yserri'
  end

  it 'HAPPY: should get business data with a business access token' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['account_id'] = oblip_auth['account']['id']
    business_seed['username'] = random_username

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    business = JSON.parse(last_response.body)
    business_id = business['id']
    access_type = 'business'
    token_data = test_token_spec(client_data, refresh_token, access_type, business_id)

    business_access = token_data['token']
    business_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{business_access}"
    }

    get 'v1/account', nil, business_header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['id']).must_equal business_id
  end

  it 'HAPPY: should get all the businesses who are agents' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    get 'v1/businesses?is_agent=true', nil, header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['businesses'].size == 1).must_equal true
  end

  it 'HAPPY: should get a single business' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    business_created = JSON.parse(last_response.body)

    get "v1/businesses/#{business_created['id']}", nil, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['id']).must_equal business_created['id']
    _(result['name']).must_equal business_created['name']
    _(result['account_id']).must_equal business_created['account_id']
  end

  it 'HAPPY: should get the first 15 businesses that belong to a user' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    one_user_access = oblip_auth['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    user_one_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{one_user_access}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # second user
    second_account_seed = ACCOUNT_SEEDS[0]
    second_account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[1], second_account_seed)
    _(last_response.status).must_equal 201

    second_oblip_auth = JSON.parse(last_response.body)

    # create third business
    business_seed = BUSINESS_SEEDS[2]
    business_seed['username'] = random_username
    business_seed['account_id'] = second_oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    get 'v1/businesses', nil, user_one_header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['businesses'].size == 2).must_equal true
    _(results['businesses'][0]['name']).must_equal BUSINESS_SEEDS[0]['name']
  end

  it 'HAPPY: should get the first 15 businesses that belong to a user as a business' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    one_user_access = oblip_auth['tokens']['access']['token']
    refresh_token = oblip_auth['tokens']['refresh']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    user_one_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{one_user_access}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201


    # authenticate 2nd business 

    business = JSON.parse(last_response.body)
    business_id = business['id']
    access_type = 'business'
    token_data = test_token_spec(client_data, refresh_token, access_type, business_id)

    business_access = token_data['token']
    business_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{business_access}"
    }


    # second user
    second_account_seed = ACCOUNT_SEEDS[0]
    second_account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[1], second_account_seed)
    _(last_response.status).must_equal 201

    second_oblip_auth = JSON.parse(last_response.body)

    # create third business
    business_seed = BUSINESS_SEEDS[2]
    business_seed['username'] = random_username
    business_seed['account_id'] = second_oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    get 'v1/businesses', nil, business_header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['businesses'].size == 2).must_equal true
    _(results['businesses'][0]['name']).must_equal BUSINESS_SEEDS[0]['name']
  end

  it 'HAPPY: should get a list of all near by businesses' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    my_lat = 37.408917
    my_lng = -122.095597

    # within 1 mile
    get "v1/businesses?nearby=true&lat=#{my_lat}&lng=#{my_lng}&distance=1",
        nil, header

    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['businesses'].size == 1).must_equal true
    _(result['businesses'][0]['distance'].nil?).must_equal false

    # within 10 mile
    get "v1/businesses?nearby=true&lat=#{my_lat}&lng=#{my_lng}&distance=10",
        nil, header

    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['businesses'].size == 2).must_equal true
    _(result['businesses'][0]['distance'].nil?).must_equal false
  end

  it 'HAPPY: should get a list of all near by agents' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    my_lat = 37.408917
    my_lng = -122.095597

    # within 1 mile
    get "v1/businesses?is_agent=true&nearby=true&lat=#{my_lat}&lng=#{my_lng}&distance=1",
        nil, header

    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['businesses'].size.zero?).must_equal true

    # within 10 mile
    get "v1/businesses?is_agent=true&nearby=true&lat=#{my_lat}&lng=#{my_lng}&distance=10",
        nil, header

    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['businesses'].size == 1).must_equal true
    _(result['businesses'][0]['distance'].nil?).must_equal false
  end

  it 'SAD: should try to get a list of all near by businesses but fail' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    my_lat = 37.408917
    my_lng = -122.095597

    # without lng param
    get "v1/businesses?nearby=true&lat=#{my_lat}",
        nil, header
 
    _(last_response.status).must_equal 400 # bad request

    # without lng param
    get "v1/businesses?nearby=true&lng=#{my_lng}",
        nil, header

    _(last_response.status).must_equal 400 # bad request

    # without both lat & lng
    get 'v1/businesses?nearby=true',
        nil, header

    _(last_response.status).must_equal 400 # bad request

    # without both lat but no value
    get 'v1/businesses?nearby=true&lat=',
        nil, header

    _(last_response.status).must_equal 400 # bad request
  end

  it 'HAPPY: should edit an existing business data' do

    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    business_data = JSON.parse(last_response.body)

    # edit business
    business_id = business_data['id']
    new_name = 'My Store'
    username = 'prodoxx'
    payload = {
      is_agent: true,
      name: new_name,
      username: username
    }

    put "v1/admin/businesses/#{business_id}", payload.to_json, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['name'] != business_data['name']).must_equal true
    _(result['username'] != business_data['username']).must_equal true
    _(result['is_agent'] != business_data['is_agent']).must_equal true
    _(result['account_id']).must_equal business_data['account_id']
  end
end
