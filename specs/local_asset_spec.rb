# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should get a list of available assets' do
     # create oblip account
     account_seed_one = ACCOUNT_SEEDS[0]
     account_seed_one['username'] = random_username

     create_account(CLIENT_SEEDS[0], account_seed_one)
     _(last_response.status).must_equal 201
 
     auth_account = JSON.parse(last_response.body)
     access_token = auth_account['tokens']['access']['token']
 
     header = {
       'CONTENT_TYPE' => 'application/json',
       'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
     }

     get 'v1/available_assets', nil, header
     _(last_response.status).must_equal 200

     result = JSON.parse(last_response.body)
     _(result['assets'].size > 0).must_equal true
  end
end
