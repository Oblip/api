# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Registered Bank Spec' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should get a list of registered banks' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create registered banks
    REGISTERED_BANK_SEEDS.each do |seed|
      post 'v1/registered_banks', seed.to_json, header
      _(last_response.status).must_equal 201
    end

    get 'v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['registered_banks'].empty?).must_equal false
  end

  it 'HAPPY: should create a new registered bank' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    bank_seed = REGISTERED_BANK_SEEDS[0]

    post 'v1/registered_banks', bank_seed.to_json, header
    _(last_response.status).must_equal 201

    result = JSON.parse(last_response.body)

    _(result['name']).must_equal bank_seed['name']
    _(result['account_number']).must_equal bank_seed['account_number']
    _(result['account_name']).must_equal bank_seed['account_name']
    _(result['balance']).must_equal 0.0
    _(result['country']['id']).must_equal bank_seed['country_id']
  end

  it 'SAD: only admin should create a new registered bank' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    account_seed_one['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']


    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    bank_seed = REGISTERED_BANK_SEEDS[0]

    post 'v1/registered_banks', bank_seed.to_json, header
    _(last_response.status).must_equal 401

    modified_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => 'Bearer shit'
    }

    # try to create without a valid token
    post 'v1/registered_banks', bank_seed.to_json, header
    _(last_response.status).must_equal 401

  end
end
