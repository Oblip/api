# frozen_string_literal: true

def random_username(n = 10)
  charset = Array('a'..'z') + Array(0..9)
  Array.new(n) { charset.sample }.join
end

def random_identifier
  SecureRandom.urlsafe_base64(32)
end
