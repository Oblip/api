# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Bank Account Spec' do
  include Rack::Test::Methods

  before :each do
    wipe_database
    create_registered_banks
  end

  it 'SAD: should not be allowed to create a duplicate business bank account' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    admin_access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    user_refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
    }

    # create  business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    business = JSON.parse(last_response.body)

    # get business access token
    type = 'business'
    business_id = business['id']
    access = test_token_spec(client_data, user_refresh_token, type, business_id)
    business_token = access['token']

    business_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{business_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, business_header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567894'
    account_name = business['name']

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in business account
    post 'v1/bank_accounts', payload.to_json, business_header
    _(last_response.status).must_equal 201

    # create second bank account
    bank = registered_banks[0]
    account_number = '1234567894'
    account_name = business['name']

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    post 'v1/bank_accounts', payload.to_json, business_header
    _(last_response.status).must_equal 400

  end

  it 'HAPPY: should get a list of bank accounts as an admin user' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header_one = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header_one
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header_one
    _(last_response.status).must_equal 201

    # create another oblip user
    second_account_seed = ACCOUNT_SEEDS[2]

    create_account(CLIENT_SEEDS[1], second_account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header_two = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    bank = registered_banks[1]
    account_number = '7234567849'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header_two
    _(last_response.status).must_equal 201

    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    header_admin = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    get 'v1/bank_accounts', nil, header_admin
    _(last_response.status).must_equal 200

    admin_results = JSON.parse(last_response.body)

    _(admin_results['bank_accounts'].size == 2).must_equal true

    # let user one try to get the list of bank accounts
    get 'v1/bank_accounts', nil, header_one
    _(last_response.status).must_equal 200

    user_one_results = JSON.parse(last_response.body)

    _(user_one_results['bank_accounts'].size == 1).must_equal true
  end

  it 'HAPPY: should get a single bank account' do
    # create oblip account
    account_seed = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # get single bank account data
    get "v1/bank_accounts/#{bank_account['id']}", nil, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['registered_bank']['id']).must_equal bank['id']
    _(result['account_number']).must_equal account_number
    _(result['username']).must_equal auth_account['account']['username']
    # _(result['account_id']).must_equal auth_account['account']['id']
    _(result['status']['code']).must_equal 'processing'

    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    header_admin = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get single bank account data as an admin
    get "v1/bank_accounts/#{bank_account['id']}", nil, header_admin
    _(last_response.status).must_equal 200

    # create oblip account 2
    second_account_seed = ACCOUNT_SEEDS[2]
    create_account(CLIENT_SEEDS[2], second_account_seed)
    _(last_response.status).must_equal 201

    second_auth_account = JSON.parse(last_response.body)
    second_access_token = second_auth_account['tokens']['access']['token']

    second_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{second_access_token}"
    }

    # attempt to get the bank info by another user
    get "v1/bank_accounts/#{bank_account['id']}", nil, second_header
    _(last_response.status).must_equal 404
  end

  it 'HAPPY: should get a list of bank accounts of logged in user' do
    # create oblip account
    account_seed = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'
    
    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    get 'v1/bank_accounts', nil, header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['bank_accounts'].size == 1).must_equal true
  end

  it 'HAPPY: should be able to create a new business bank account' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    admin_access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    user_refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
    }

    # create  business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    business = JSON.parse(last_response.body)

    # get business access token
    type = 'business'
    business_id = business['id']
    access = test_token_spec(client_data, user_refresh_token, type, business_id)
    business_token = access['token']

    business_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{business_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, business_header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567890'
    account_name = business['name']

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in business account
    post 'v1/bank_accounts', payload.to_json, business_header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    _(bank_account['registered_bank']['id']).must_equal bank['id']
    _(bank_account['account_number']).must_equal account_number
    _(bank_account['account_name']).must_equal account_name
    _(bank_account['is_partner']).must_equal true
    _(bank_account['username']).must_equal business['username']
    _(bank_account['status']['code']).must_equal 'processing'
  end

  it 'HAPPY: should be able create a new personal bank account' do
    # create oblip account
    account_seed = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)


    _(bank_account['registered_bank']['id']).must_equal bank['id']
    _(bank_account['account_number']).must_equal account_number
    _(bank_account['account_name']).must_equal account_name
    _(bank_account['is_partner']).must_equal false # by default
    _(bank_account['username']).must_equal auth_account['account']['username']
    # _(bank_account['account_id']).must_equal auth_account['account']['id']
    _(bank_account['status']['code']).must_equal 'processing'
  end

  it 'HAPPY: should allow an admin to verify a bank account' do
    # create oblip account
    account_seed = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    header_admin = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # scenario with verified status
    payload = {
      status: 'verified'
    }

    put "v1/bank_accounts/#{bank_account['id']}/status", payload.to_json, header_admin
    _(last_response.status).must_equal 200

    bank_account = JSON.parse(last_response.body)

    _(bank_account['status']['code']).must_equal 'verified'

    # scenario with unable to verify status
    comment = 'Your full name does not match the bank account provided'
    payload = {
      status: 'unable_to_verify',
      comment: comment
    }

    put "v1/bank_accounts/#{bank_account['id']}/status", payload.to_json, header_admin
    _(last_response.status).must_equal 200

    bank_account = JSON.parse(last_response.body)

    _(bank_account['status']['code']).must_equal 'unable_to_verify'
    _(bank_account['status']['message']).must_equal comment
  end

  it 'SAD: should not allow a user to verify a bank account' do
    # create oblip account
    account_seed = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200
    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    payload = {
      status: 'verified'
    }

    put "v1/bank_accounts/#{bank_account['id']}/status", payload.to_json, header
    _(last_response.status).must_equal 401
  end

  # it 'HAPPY: should get a list of deposit requests' do
  #   # create oblip account
  #   account_seed_one = ACCOUNT_SEEDS[0]
  #   create_account(CLIENT_SEEDS[0], account_seed_one)
  #   _(last_response.status).must_equal 201

  #   auth_account = JSON.parse(last_response.body)
  #   access_token = auth_account['tokens']['access']['token']

  #   header = {
  #     'CONTENT_TYPE' => 'application/json',
  #     'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  #   }

  #   # get a list of registered banks
  #   get '/v1/registered_banks', nil, header
  #   _(last_response.status).must_equal 200

  #   registered_banks = JSON.parse(last_response.body)['registered_banks']

  #   bank = registered_banks[0]
  #   account_number = '1234567899'
  #   account_name = 'Reggie Escobar'

  #   payload = {
  #     registered_bank_id: bank['id'],
  #     account_number: account_number,
  #     account_name: account_name
  #   }

  #   # create bank account for logged-in user
  #   post 'v1/bank_accounts', payload.to_json, header
  #   _(last_response.status).must_equal 201

  #   bank_account = JSON.parse(last_response.body)

  #   # lets create a deposit 
  #   payloads = [
  #     {
  #       amount: 100,
  #       registered_bank_id: bank['id'],
  #       asset: 'BZD'
  #     },
  #     {
  #       amount: 200,
  #       registered_bank_id: bank['id'],
  #       asset: 'BZD'
  #     }
  #   ]

  #   all_requests = []
  #   # create bank account requests
  #   payloads.each do |v|
  #     post "/v1/bank_accounts/#{bank_account['id']}/deposits", v.to_json, header
  #     _(last_response.status).must_equal 201
  #     request = JSON.parse(last_response.body)
  #     all_requests.push(request)
  #   end

  #   # get a list of deposit requests
  #   get "/v1/bank_accounts/#{bank_account['id']}/deposits", nil, header
  #   _(last_response.status).must_equal 200

  #   results = JSON.parse(last_response.body)

  #   _(results['bank_account_requests'].size == 2).must_equal true

  #   # create oblip account 2
  #   second_account_seed = ACCOUNT_SEEDS[2]
  #   create_account(CLIENT_SEEDS[2], second_account_seed)
  #   _(last_response.status).must_equal 201

  #   second_auth_account = JSON.parse(last_response.body)
  #   second_access_token = second_auth_account['tokens']['access']['token']

  #   second_header = {
  #     'CONTENT_TYPE' => 'application/json',
  #     'HTTP_AUTHORIZATION' => "Bearer #{second_access_token}"
  #   }

  #   # attempt to get the deposit request as a different user
  #   get "/v1/bank_accounts/#{bank_account['id']}/deposits", nil, second_header
  #   _(last_response.status).must_equal 403
  # end
end
