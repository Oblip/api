# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Checking Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'SAD: should handle getting a business checking account that does not exist' do
     # create admin
     admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
     create_admin_account(admin_account_seed)
     email = admin_account_seed['email_address']
     password = admin_account_seed['password']
 
     admin_auth = auth_admin_account(email, password)
     access_token = admin_auth['access']['token']
 
     # create oblip account
     account_seed = ACCOUNT_SEEDS[1]
     client_data = create_account(CLIENT_SEEDS[0], account_seed)
     _(last_response.status).must_equal 201
 
     oblip_auth = JSON.parse(last_response.body)
     refresh_token = oblip_auth['tokens']['refresh']['token']
 
     admin_header = {
       'CONTENT_TYPE' => 'application/json',
       'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
     }
 
     # create first business
     business_seed = BUSINESS_SEEDS[0]
     business_seed['username'] = random_username
     business_seed['account_id'] = oblip_auth['account']['id']
 
     post 'v1/admin/businesses', business_seed.to_json, admin_header
     _(last_response.status).must_equal 201
 
     business = JSON.parse(last_response.body)
     business_id = business['id']
     access_type = 'business'
     token_data = test_token_spec(client_data, refresh_token, access_type, business_id)
     
     business_access = token_data['token']
     business_header = {
       'CONTENT_TYPE' => 'application/json',
       'HTTP_AUTHORIZATION' => "Bearer #{business_access}"
     }

     get 'v1/account/checking_account', nil, business_header
     _(last_response.status).must_equal 400
  end

  it 'HAPPY: should get a business checking account' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    refresh_token = oblip_auth['tokens']['refresh']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    business = JSON.parse(last_response.body)
    business_id = business['id']
    access_type = 'business'
    token_data = test_token_spec(client_data, refresh_token, access_type, business_id)
    
    business_access = token_data['token']
    business_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{business_access}"
    }

    post 'v1/account/checking_account', nil, business_header
    _(last_response.status).must_equal 201

    account_signer = JSON.parse(last_response.body)
    checking_account_id = account_signer['account']['checking_account_id']

    get 'v1/account/checking_account', nil, business_header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['id']).must_equal checking_account_id
  end

  it 'HAPPY: should get a personal checking account of a user' do
    # create account
    account_seed = ACCOUNT_SEEDS[1]
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # creates blockchain account
    post 'v1/account/checking_account', nil, header
    _(last_response.status).must_equal 201

    # gets the blockchain account
    get 'v1/account/checking_account', nil, header
    _(last_response.status).must_equal 200

    checking_account = JSON.parse(last_response.body)

    _(checking_account['id'].nil?).must_equal false
    _(checking_account['funds'].size >= 1).must_equal true
  end

  describe 'Creating a new checking account' do
    it 'HAPPY: should create a checking account for a personal account' do
      # create account
      account_seed = ACCOUNT_SEEDS[0]
      create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201

      auth_account = JSON.parse(last_response.body)
      access_token = auth_account['tokens']['access']['token']

      header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      post 'v1/account/checking_account', nil, header
      _(last_response.status).must_equal 201

      account_signer = JSON.parse(last_response.body)

      _(account_signer['account'].nil?).must_equal false
      _(account_signer['account']['checking_account_id'].nil?).must_equal false
      _(account_signer['signer_seed'].nil?).must_equal false
      _(account_signer['wrapper_public_key'].nil?).must_equal false
    end

    it 'HAPPY: should create a checking account for a business account' do
      # create admin
      admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
      create_admin_account(admin_account_seed)
      email = admin_account_seed['email_address']
      password = admin_account_seed['password']

      admin_auth = auth_admin_account(email, password)
      access_token = admin_auth['access']['token']

      # create oblip account
      account_seed = ACCOUNT_SEEDS[1]
      client_data = create_account(CLIENT_SEEDS[0], account_seed)
      _(last_response.status).must_equal 201

      oblip_auth = JSON.parse(last_response.body)
      refresh_token = oblip_auth['tokens']['refresh']['token']

      admin_header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
      }

      # create first business
      business_seed = BUSINESS_SEEDS[0]
      business_seed['account_id'] = oblip_auth['account']['id']

      post 'v1/admin/businesses', business_seed.to_json, admin_header
      _(last_response.status).must_equal 201

      business = JSON.parse(last_response.body)
      business_id = business['id']
      access_type = 'business'
      token_data = test_token_spec(client_data, refresh_token, access_type, business_id)
      
      business_access = token_data['token']
      business_header = {
        'CONTENT_TYPE' => 'application/json',
        'HTTP_AUTHORIZATION' => "Bearer #{business_access}"
      }

      post 'v1/account/checking_account', nil, business_header
      _(last_response.status).must_equal 201

      account_signer = JSON.parse(last_response.body)

      _(account_signer['account'].nil?).must_equal false
      _(account_signer['account']['checking_account_id'].nil?).must_equal false
      _(account_signer['signer_seed'].nil?).must_equal false
      _(account_signer['wrapper_public_key'].nil?).must_equal false
    end
  end
end
