# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Admin User Account Handling' do
  include Rack::Test::Methods

  before do
    wipe_database
  end

  it 'HAPPY: should find an oblip account as an admin' do
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[0]
    admin = create_admin_account(admin_account_seed)

    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }
 
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    user_auth = JSON.parse(last_response.body)
    username = user_auth['account']['username']

    # test finding an account
    get "/v1/admin/account_management/#{username}", nil, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['username']).must_equal username
  end
end
