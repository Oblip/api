# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Businesses' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should create a new business' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    result = JSON.parse(last_response.body)

    _(result['id'].nil?).must_equal false
    _(result['name']).must_equal business_seed['name']
    _(result['username']).must_equal business_seed['username']
    _(result['street']).must_equal business_seed['street']
    _(result['city']).must_equal business_seed['city']
    _(result['country']['id']).must_equal business_seed['country_id']
    _(result['lat']).must_equal business_seed['lat']
    _(result['lng']).must_equal business_seed['lng']
    _(result['is_agent']).must_equal business_seed['is_agent']
    _(result['account_id']).must_equal business_seed['account_id']
    _(result['type']).must_equal 'business'
  end

  it 'SAD: should try to create a business with an existing username and fail' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    username = 'yepii55'

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = username

    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = username

    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 400
  end

  it 'HAPPY: should get the first 15 busineses' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201

    oblip_auth = JSON.parse(last_response.body)
    one_user_access = oblip_auth['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    user_one_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{one_user_access}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[0]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # create second business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    # second user
    second_account_seed = ACCOUNT_SEEDS[0]
    second_account_seed['username'] = random_username
    create_account(CLIENT_SEEDS[1], second_account_seed)
    _(last_response.status).must_equal 201

    second_oblip_auth = JSON.parse(last_response.body)

    # create third business
    business_seed = BUSINESS_SEEDS[2]
    business_seed['username'] = random_username
    business_seed['account_id'] = second_oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, header
    _(last_response.status).must_equal 201

    get 'v1/admin/businesses', nil, header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['businesses'].size == 3).must_equal true
    _(results['businesses'][0]['name']).must_equal BUSINESS_SEEDS[2]['name']
  end
end
