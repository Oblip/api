# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for Account Federation' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should get bulk account data using a list of checking account ids' do

    data = []

    ACCOUNT_SEEDS.each_with_index do |item, index|
      client = CLIENT_SEEDS[index]
      item['username'] = random_username
      data.push(setup_user(client, item))
    end

    checking_accounts = data.map do |item|
      item[:checking_account]['account']['checking_account_id']
    end

    payload = {
      checking_accounts: checking_accounts
    }

    post '/v1/account_federation', payload.to_json, data[0][:header]
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['accounts'].size > 0).must_equal true
    _(results['accounts'][0]['checking_account_id'].nil?).must_equal false
  end
end
