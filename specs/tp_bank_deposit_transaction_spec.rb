# frozen_string_literal: true

require_relative 'spec_helper.rb'

def create_deposit_request(bank_account:,bank_id:,amount:,asset:,header:)
  payload = {
    registered_bank_id: bank_id,
    amount: amount,
    asset: asset
  }

  post "/v1/bank_accounts/#{bank_account['id']}/deposits", payload.to_json, header
  _(last_response.status).must_equal 201

  request = JSON.parse(last_response.body)
  request
end

def verify_deposit(bank_account:, request:, admin_header:)
  admin_status = 'verified'
  admin_status_payload = {
    status: admin_status
  }

  # let's attempt to verify the request
  put "/v1/bank_accounts/#{bank_account['id']}/deposits/#{request['id']}/status", admin_status_payload.to_json, admin_header
  _(last_response.status).must_equal 200
end

def update_tx_id(bank_account:, request:, tx_id:, admin_header:)
  payload = {
    transaction_id: tx_id
  }

  # let's attempt to update the tx_id
  put "/v1/bank_accounts/#{bank_account['id']}/deposits/#{request['id']}/transaction", payload.to_json, admin_header
  _(last_response.status).must_equal 200
  request = JSON.parse(last_response.body)
  request
end

describe 'Test Bank Deposit Transaction Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
    create_registered_banks

    @admin = setup_admin(ADMIN_ACCOUNT_SEEDS[1])
    Shoryuken.worker_executor = Shoryuken::Worker::InlineExecutor
  end

  after do
    Shoryuken.worker_executor = Shoryuken::Worker::DefaultExecutor
  end

  it 'HAPPY: should create a bank deposit transaction' do

    # pre-setup
    seed1 = ACCOUNT_SEEDS[0]
    seed1['username'] = random_username

    user = setup_user(CLIENT_SEEDS[0], seed1)
    registered_banks = get_registered_banks(user[:header])
    bank_account = create_bank_account(
      bank_id: registered_banks[0]['id'],
      acct_number: '1234567899',
      name: 'Reggie Escobar',
      header: user[:header]
    )

    request = create_deposit_request(
      bank_account: bank_account,
      bank_id: registered_banks[0]['id'],
      amount: 100,
      asset: 'BZD',
      header: user[:header]
    )

    verify_deposit(
      bank_account: bank_account,
      request: request,
      admin_header: @admin[:header]
    )

    # prepare transaction
    message = "Bank deposit ref no. #{request['ref_no']}"
    type = 'bank_deposit'
    envl_payload = {
      to: user[:account]['username'],
      amount: request['amount'],
      asset: request['asset'],
      message: message,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, @admin[:header]
    _(last_response.status).must_equal 200
    envl_data = JSON.parse(last_response.body)

    identifier =  random_identifier

    tx_payload = {
      envelope: envl_data['envelope'],
      type: type,
      message: message,
      identifier: identifier
    }

    # create transaction
    post '/v1/transactions', tx_payload.to_json, @admin[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(2)

    # get transactions
    get 'v1/transactions', nil, user[:header]
    _(last_response.status).must_equal 200
    
    results = JSON.parse(last_response.body)
    
    _(results['transactions'].size == 1).must_equal true

    tx = results['transactions'][0]

    _(tx['tx_hash'].nil?).must_equal false
    _(tx['from_internal']).must_equal Oblip::Horizon::Api.fund[:username]
    _(tx['to_internal']).must_equal user[:account]['username']
    _(tx['message']).must_equal message
    # _(tx['paging_token'].nil?).must_equal false
    # _(tx['network_fee'].nil?).must_equal false
    _(tx['amount']).must_equal request['amount'].to_s
    _(tx['asset']).must_equal request['asset']
    _(tx['type']).must_equal type

    # update transaction id on deposit request
    result = update_tx_id(
      bank_account: bank_account,
      request: request,
      tx_id: tx['id'],
      admin_header: @admin[:header]
    )

    _(result['transaction_id']).must_equal tx['id']
  end

  it 'SAD: it should not allow a non-admin to make a bank transactions' do
    # pre-setup
    seed1 = ACCOUNT_SEEDS[0]
    seed1['username'] = random_username
    user = setup_user(CLIENT_SEEDS[0], seed1)

		seed2 = ACCOUNT_SEEDS[1]
		seed2['username'] = random_username

    user_2 = setup_user(CLIENT_SEEDS[1], seed2)
    registered_banks = get_registered_banks(user[:header])
    bank_account = create_bank_account(
      bank_id: registered_banks[0]['id'],
      acct_number: '1234567899',
      name: 'Reggie Escobar',
      header: user[:header]
    )

    request = create_deposit_request(
      bank_account: bank_account,
      bank_id: registered_banks[0]['id'],
      amount: 100,
      asset: 'BZD',
      header: user[:header]
    )

    verify_deposit(
      bank_account: bank_account,
      request: request,
      admin_header: @admin[:header]
    )

    # prepare transaction
    message = "Bank deposit ref no. #{request['ref_no']}"
    type = 'bank_deposit'

    envl_payload = {
      to: user_2[:account]['username'],
      amount: request['amount'],
      asset: request['asset']
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, user[:header]
    _(last_response.status).must_equal 200
    envl_data = JSON.parse(last_response.body)

    identifier = random_identifier

    tx_payload = {
      envelope: envl_data['envelope'],
      type: type,
      message: message,
      identifier: identifier
    }

    # attempt to create deposit transaction
    post '/v1/transactions', tx_payload.to_json, user[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(2)

    get '/v1/transactions', nil, user[:header]
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['transactions'].size == 0).must_equal true
  end

  it 'SAD: should not allow any user to specify an invalid type' do
    # pre-setup
    user = setup_user(CLIENT_SEEDS[0], ACCOUNT_SEEDS[0])
    user_2 = setup_user(CLIENT_SEEDS[1], ACCOUNT_SEEDS[1])
    registered_banks = get_registered_banks(user[:header])
    bank_account = create_bank_account(
      bank_id: registered_banks[0]['id'],
      acct_number: '1234567899',
      name: 'Reggie Escobar',
      header: user[:header]
    )

    request = create_deposit_request(
      bank_account: bank_account,
      bank_id: registered_banks[0]['id'],
      amount: 100,
      asset: 'BZD',
      header: user[:header]
    )

    verify_deposit(
      bank_account: bank_account,
      request: request,
      admin_header: @admin[:header]
    )

    # prepare transaction
    message = "Bank deposit ref no. #{request['ref_no']}"
    type = 'absolute_shit'
    
    envl_payload = {
      to: user_2[:account]['username'],
      amount: request['amount'],
      asset: request['asset'],
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, user[:header]
    _(last_response.status).must_equal 200
    envl_data = JSON.parse(last_response.body)

    tx_payload = {
      envelope: envl_data['envelope'],
      type: type,
      message: message,
      identifier: random_identifier
    }

    # attempt to create deposit transaction
    post '/v1/transactions', tx_payload.to_json, user[:header]
    _(last_response.status).must_equal 202

    sleep(2)

    get 'v1/transactions', nil, user[:header]
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['transactions'].size == 0).must_equal true
  end
end
