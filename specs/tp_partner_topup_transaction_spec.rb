# frozen_string_literal: true

require_relative 'spec_helper.rb'


describe 'Test Partner Topup Transaction Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
    create_registered_banks

    @admin = setup_admin(ADMIN_ACCOUNT_SEEDS[1])
    Shoryuken.worker_executor = Shoryuken::Worker::InlineExecutor
  end

  after do
    Shoryuken.worker_executor = Shoryuken::Worker::DefaultExecutor
  end

  it 'SAD: should not allow partner topup transaction > 1389.16' do
    # create a personal account
    seed1 = ACCOUNT_SEEDS[0]
    seed1['username'] = random_username
    first_user = setup_user(CLIENT_SEEDS[0], seed1)

    # create a personal account
    seed2 = ACCOUNT_SEEDS[1]
    seed2['username'] = random_username
    second_user = setup_user(CLIENT_SEEDS[1], seed2)

    # create partner business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business = setup_business(first_user, business_seed, @admin)

    # =======================
    # we are going to cheat here (TODO: refactor later to add the real flow)
    # We are going to do a bank deposit to fill the business with money
    # ======================

    # prepare bank deposit transaction
    deposit_amount = 14000
    deposit_asset = 'BZD'
    message = "Bank deposit"
    type = 'bank_deposit'

    envl_payload = {
      to: business[:account]['username'],
      amount: deposit_amount,
      asset: deposit_asset,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, @admin[:header]
    _(last_response.status).must_equal 200
    envl_data = JSON.parse(last_response.body)

    identifier =  random_identifier
    # identifier = random_username(32)

    tx_payload = {
      envelope: envl_data['envelope'],
      type: type,
      message: message,
      identifier: identifier
    }

    # create bank deposit transaction
    post '/v1/transactions', tx_payload.to_json, @admin[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(3)

    # get transactions (make sure nothing went wrong)
    get 'v1/transactions', nil, business[:header]
    _(last_response.status).must_equal 200

    tx_results = JSON.parse(last_response.body)
    puts "Transaction Size: #{tx_results['transactions'].size}"
    _(tx_results['transactions'].size == 1).must_equal true

    # ====================
    # We are going to test the partner topup transaction
    # ====================

    # prepare partner topup transaction
    amount = 1390
    asset = 'BZD'
    message = "Partner topup"
    type = 'partner_topup'

    envl_payload = {
      to: second_user[:account]['username'],
      amount: amount,
      asset: asset,
      message: message,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, business[:header]
    _(last_response.status).must_equal 200

    envl_data = JSON.parse(last_response.body)

    # we need to decrypt the message
    wrapper_public_key = business[:checking_account]['wrapper_public_key']
    cipher, nonce = business[:checking_account]['signer_seed'].split('.')

    # decrypt message to get seed
    signer_seed = PublicEncryption.decrypt(cipher, nonce,
      wrapper_public_key, first_user[:client][:wrapper_keypair][:private_key])

    # we need to sign it.
    signed_envelope = Oblip::Horizon::Api.sign_envelope(envl_data['envelope'],
      signer_seed)

    identifier =  random_identifier
    tx_payload = {
      envelope: signed_envelope,
      type: type,
      message: message,
      identifier: identifier
    }

    # create partner topup transaction
    post '/v1/transactions', tx_payload.to_json, business[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(3)

    # get transactions (make sure nothing went wrong)
    get 'v1/transactions', nil, second_user[:header]
    _(last_response.status).must_equal 200

    tx_results = JSON.parse(last_response.body)
    _(tx_results['transactions'].size == 0).must_equal true
  end

  it 'SAD: should not be able to do a partner topup transaction with a low balance' do
    # create a personal account
    seed1 = ACCOUNT_SEEDS[0]
    seed1['username'] = random_username
    first_user = setup_user(CLIENT_SEEDS[0], seed1)

    # create a personal account
    seed2 = ACCOUNT_SEEDS[1]
    seed2['username'] = random_username
    second_user = setup_user(CLIENT_SEEDS[1], seed2)

    # create partner business
    business_seed = BUSINESS_SEEDS[1]
    business = setup_business(first_user, business_seed, @admin)

    # ====================
    # We are going to test the partner topup transaction with a zero balance
    # ====================

    # prepare partner topup transaction
    amount = 50
    asset = 'BZD'
    message = "Partner topup"
    type = 'partner_topup'

    envl_payload = {
      to: second_user[:account]['username'],
      amount: amount,
      asset: asset,
      message: message,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, business[:header]
    _(last_response.status).must_equal 200

    envl_data = JSON.parse(last_response.body)

    # we need to decrypt the message
    wrapper_public_key = business[:checking_account]['wrapper_public_key']
    cipher, nonce = business[:checking_account]['signer_seed'].split('.')

    # decrypt message to get seed
    signer_seed = PublicEncryption.decrypt(cipher, nonce,
      wrapper_public_key, first_user[:client][:wrapper_keypair][:private_key])

    # we need to sign it.
    signed_envelope = Oblip::Horizon::Api.sign_envelope(envl_data['envelope'],
      signer_seed)

    identifier =  random_identifier
    tx_payload = {
      envelope: signed_envelope,
      type: type,
      message: message,
      identifier: identifier
    }

    # create partner topup transaction
    post '/v1/transactions', tx_payload.to_json, business[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(2)

    # get transactions (make sure nothing went wrong)
    get 'v1/transactions', nil, second_user[:header]
    _(last_response.status).must_equal 200

    tx_results = JSON.parse(last_response.body)
    _(tx_results['transactions'].size == 0).must_equal true
  end

  it 'HAPPY: should create a partner topup transaction' do
    # create a personal account
    seed1 = ACCOUNT_SEEDS[0]
    seed1['username'] = random_username
    first_user = setup_user(CLIENT_SEEDS[0], seed1)

    # create a personal account
    seed2 = ACCOUNT_SEEDS[1]
    seed2['username'] = random_username
    second_user = setup_user(CLIENT_SEEDS[1], seed2)

    # create partner business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['username'] = random_username
    business = setup_business(first_user, business_seed, @admin)

    # =======================
    # we are going to cheat here (TODO: refactor later to add the real flow)
    # We are going to do a bank deposit to fill the business with money
    # ======================

    # prepare bank deposit transaction
    deposit_amount = 100
    deposit_asset = 'BZD'
    message = "Bank deposit"
    type = 'bank_deposit'

    envl_payload = {
      to: business[:account]['username'],
      amount: deposit_amount,
      asset: deposit_asset,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, @admin[:header]
    _(last_response.status).must_equal 200
    envl_data = JSON.parse(last_response.body)

    identifier =  random_identifier
    # identifier = random_username(32)

    tx_payload = {
      envelope: envl_data['envelope'],
      type: type,
      message: message,
      identifier: identifier
    }

    # create bank deposit transaction
    post '/v1/transactions', tx_payload.to_json, @admin[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(3)

    # get transactions (make sure nothing went wrong)
    get 'v1/transactions', nil, business[:header]
    _(last_response.status).must_equal 200

    tx_results = JSON.parse(last_response.body)
    puts "Transaction Size: #{tx_results['transactions'].size}"
    _(tx_results['transactions'].size == 1).must_equal true

    # ====================
    # We are going to test the partner topup transaction
    # ====================

    # prepare partner topup transaction
    amount = 50
    asset = 'BZD'
    message = "Partner topup"
    type = 'partner_topup'

    envl_payload = {
      to: second_user[:account]['username'],
      amount: amount,
      asset: asset,
      message: message,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, business[:header]
    _(last_response.status).must_equal 200

    envl_data = JSON.parse(last_response.body)

    # we need to decrypt the message
    wrapper_public_key = business[:checking_account]['wrapper_public_key']
    cipher, nonce = business[:checking_account]['signer_seed'].split('.')

    # decrypt message to get seed
    signer_seed = PublicEncryption.decrypt(cipher, nonce,
      wrapper_public_key, first_user[:client][:wrapper_keypair][:private_key])

    # we need to sign it.
    signed_envelope = Oblip::Horizon::Api.sign_envelope(envl_data['envelope'],
      signer_seed)

    identifier =  random_identifier
    tx_payload = {
      envelope: signed_envelope,
      type: type,
      message: message,
      identifier: identifier
    }

    # create partner topup transaction
    post '/v1/transactions', tx_payload.to_json, business[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(3)

    # get transactions (make sure nothing went wrong)
    get 'v1/transactions', nil, second_user[:header]
    _(last_response.status).must_equal 200

    tx_results = JSON.parse(last_response.body)
    _(tx_results['transactions'].size == 1).must_equal true

    transaction = tx_results['transactions'][0]

    _(transaction['to_internal']).must_equal second_user[:account]['username']
    _(transaction['type']).must_equal type
    _(transaction['amount']).must_equal amount.to_f.to_s
    _(transaction['asset']).must_equal asset
    _(transaction['message']).must_equal message
  end

  it 'SAD: should not be able to do a partner topup transaction with a low balance' do
    # create a personal account
    seed1 = ACCOUNT_SEEDS[0]
    seed1['username'] = random_username
    first_user = setup_user(CLIENT_SEEDS[0], seed1)

    # create a personal account
    seed2 = ACCOUNT_SEEDS[1]
    seed2['username'] = random_username
    second_user = setup_user(CLIENT_SEEDS[1], seed2)

    # create partner business
    business_seed = BUSINESS_SEEDS[1]
    business = setup_business(first_user, business_seed, @admin)

    # ====================
    # We are going to test the partner topup transaction with a zero balance
    # ====================

    # prepare partner topup transaction
    amount = 50
    asset = 'BZD'
    message = "Partner topup"
    type = 'partner_topup'

    envl_payload = {
      to: second_user[:account]['username'],
      amount: amount,
      asset: asset,
      message: message,
      type: type
    }

    # create a transaction envelope
    post '/v1/transaction_envelope', envl_payload.to_json, business[:header]
    _(last_response.status).must_equal 200

    envl_data = JSON.parse(last_response.body)

    # we need to decrypt the message
    wrapper_public_key = business[:checking_account]['wrapper_public_key']
    cipher, nonce = business[:checking_account]['signer_seed'].split('.')

    # decrypt message to get seed
    signer_seed = PublicEncryption.decrypt(cipher, nonce,
      wrapper_public_key, first_user[:client][:wrapper_keypair][:private_key])

    # we need to sign it.
    signed_envelope = Oblip::Horizon::Api.sign_envelope(envl_data['envelope'],
      signer_seed)

    identifier =  random_identifier
    tx_payload = {
      envelope: signed_envelope,
      type: type,
      message: message,
      identifier: identifier
    }

    # create partner topup transaction
    post '/v1/transactions', tx_payload.to_json, business[:header]
    _(last_response.status).must_equal 202

    # wait a bit
    sleep(2)

    # get transactions (make sure nothing went wrong)
    get 'v1/transactions', nil, second_user[:header]
    _(last_response.status).must_equal 200

    tx_results = JSON.parse(last_response.body)
    _(tx_results['transactions'].size == 0).must_equal true
  end
end
