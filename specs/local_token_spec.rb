# frozen_string_literal: true
$VERBOSE = nil

require_relative 'spec_helper.rb'

describe 'Tests for handling Api Clients' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should refresh an account access token' do
    # create account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed, true)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    refresh_token = auth_account['tokens']['refresh']['token']

    access =test_token_spec(client_data, refresh_token )

    _(access['token'].nil?).must_equal false
  end

  it 'HAPPY: should refresh a business access token' do
    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    admin_access_token = admin_auth['access']['token']

    # create oblip account
    account_seed = ACCOUNT_SEEDS[1]
    account_seed['username'] = random_username
    client_data = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201
    oblip_auth = JSON.parse(last_response.body)

    user_refresh_token = oblip_auth['tokens']['refresh']['token']
    
    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
    }

    # create first business
    business_seed = BUSINESS_SEEDS[1]
    business_seed['account_id'] = oblip_auth['account']['id']

    post 'v1/admin/businesses', business_seed.to_json, admin_header
    _(last_response.status).must_equal 201

    business = JSON.parse(last_response.body)

    type = 'business'
    business_id = business['id']
    
    access = test_token_spec(client_data, user_refresh_token, type, business_id)
    _(access['token'].nil?).must_equal false
  end

  # it 'HAPPY: not allow to get a new access token <= 30 minutes' do
  #   new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
  #   test_token_spec(new_data)

  #   _(last_response.status).must_equal 400 # bad request (PolicyRestriction)
  # end

  # it 'HAPPY: should get a new access token if last issused token has expired' do
  #   old_expiration_window = Oblip::Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW
  #   Oblip::Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW = 1
  #   new_data = create_oblip_account(DATA[:api_clients][0], DATA[:oblip_accounts][0])
  #   sleep(3)
  #   test_token_spec(new_data)
  #   _(last_response.status).must_equal 201
  #   Oblip::Entity::AccessMetaData::DEFAULT_EXPIRATION_WINDOW = old_expiration_window
  # end
end
