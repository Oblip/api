# frozen_string_literal: true

require_relative 'spec_helper.rb'

def send_phone_verification_code(client)
  phone_number = '5016620652'

  # create and sign payload
  data = { phone_number: phone_number }
  signer = SignRequest.new(client[:keypair][:signing_key])
  signature = signer.sign(data)

  client_id = client[:result]['id']
  payload = { data: data, signature: signature }

  headers = { 'CONTENT_TYPE' => 'application/json' }
  post "v1/clients/#{client_id}/send_verification",
       payload.to_json, headers
end

describe 'Tests for handling Clients' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should dynamically create a new client' do
    data = create_client(CLIENT_SEEDS[0])
    _(last_response.status).must_equal 201

    result = data[:result]

    _(result['verifier_key']).must_equal data[:keypair][:verify_key]
    _(result['is_verified']).must_equal false
    _(result['fcm_token']).must_equal CLIENT_SEEDS[0]['fcm_token']
  end

  it 'HAPPY: should update a the fcm token of a client' do
    client = create_account(CLIENT_SEEDS[0], ACCOUNT_SEEDS[0])
    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    fake_token = '123456789awqsxtrcdkeloqiasdfasdnfkadsnfklajfalssercdkarid'
    data = {
      fcm_token: fake_token
    }

    signer = SignRequest.new(client[:keypair][:signing_key])
    signature = signer.sign(data)

    client_id = client[:result]['id']
    payload = { data: data, signature: signature }

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    put "v1/clients/#{client_id}", payload.to_json, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)
    _(result['id']).must_equal client_id
    _(result['fcm_token']).must_equal fake_token
  end

  it 'HAPPY: should send a phone verification to the user and save the token' do
    client = create_client(CLIENT_SEEDS[0])

    send_phone_verification_code(client)
    client_id = client[:result]['id']

    _(last_response.status).must_equal 200

    db_record = Oblip::Database::ClientOrm.find(id: client_id)
    _(db_record.verification_token.nil?).must_equal false
  end

  it 'HAPPY: should verify phone number with the correct code' do
    client = create_client(CLIENT_SEEDS[1])
    send_phone_verification_code(client)
    client_id = client[:result]['id']

    _(last_response.status).must_equal 200
    db_record = Oblip::Database::ClientOrm.find(id: client_id)

    v_token = db_record.verification_token
    code_data = ShortCodeToken.payload(v_token)
    code = code_data['code']

    data = { code: code }
    signer = SignRequest.new(client[:keypair][:signing_key])
    signature = signer.sign(data)
    headers = { 'CONTENT_TYPE' => 'application/json' }
    payload = { data: data, signature: signature }

    post "v1/clients/#{client_id}/verify",
         payload.to_json, headers

    _(last_response.status).must_equal 200
    result = JSON.parse(last_response.body)

    _(result['is_verified']).must_equal true
  end

  it 'SAD: should not verify phone number given wrong code' do
    client = create_client(CLIENT_SEEDS[0])
    send_phone_verification_code(client)
    client_id = client[:result]['id']

    _(last_response.status).must_equal 200

    data = { code: '50104' }
    signer = SignRequest.new(client[:keypair][:signing_key])
    signature = signer.sign(data)
    headers = { 'CONTENT_TYPE' => 'application/json' }
    payload = { data: data, signature: signature }

    post "v1/clients/#{client_id}/verify",
         payload.to_json, headers

    _(last_response.status).must_equal 400
  end
end
