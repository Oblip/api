# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Tests for handling Bank Account Request Spec' do
  include Rack::Test::Methods

  before :each do
    wipe_database
    create_registered_banks
  end

  it 'HAPPY: should get a single bank account request' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    payloads = [
      {
        amount: 100,
        asset: 'BZD'
      },
      {
        amount: 200,
        asset: 'BZD'
      }
    ]

    requests = []

    # create bank account requests
    payloads.each do |v|
      post "/v1/bank_accounts/#{bank_account['id']}/deposits", v.to_json, header
      _(last_response.status).must_equal 201

      requests.push(JSON.parse(last_response.body))
    end

    # get a list of deposits (default)
    get "/v1/bank_accounts/#{bank_account['id']}/requests/#{requests[0]['id']}", nil, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['id']).must_equal requests[0]['id']
    _(result['bank_account']['id']).must_equal bank_account['id']
  end

  it 'HAPPY: should get a list of all bank account requests' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    payloads = [
      {
        amount: 100,
        asset: 'BZD'
      },
      {
        amount: 200,
        asset: 'BZD'
      }
    ]

    # create bank account requests
    payloads.each do |v|
      post "/v1/bank_accounts/#{bank_account['id']}/deposits", v.to_json, header
      _(last_response.status).must_equal 201
    end

    # get a list of deposits (default)
    get "/v1/bank_accounts/#{bank_account['id']}/requests", nil, header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['bank_account_requests'].size == 2).must_equal true
    _(results['bank_account_requests'][0]['bank_account']['id']).must_equal bank_account['id']
  end

  it 'HAPPY: should get a list of all bank account deposit requests' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    payloads = [
      {
        amount: 100,
        asset: 'BZD'
      },
      {
        amount: 200,
        asset: 'BZD'
      }
    ]

    # create bank account requests
    payloads.each do |v|
      post "/v1/bank_accounts/#{bank_account['id']}/deposits", v.to_json, header
      _(last_response.status).must_equal 201
    end

    # get a list of deposits (default)
    get "/v1/bank_accounts/#{bank_account['id']}/deposits", nil, header
    _(last_response.status).must_equal 200

    results = JSON.parse(last_response.body)

    _(results['bank_account_requests'].size == 2).must_equal true
  end

  it 'HAPPY: should be able to create a bank account deposit request' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    amount = 100
    asset = 'BZD'
    payload = {
      registered_bank_id: bank['id'],
      amount: amount,
      asset: asset
    }

    post "/v1/bank_accounts/#{bank_account['id']}/deposits", payload.to_json, header
    _(last_response.status).must_equal 201

    result = JSON.parse(last_response.body)

    _(result['amount']).must_equal amount
    _(result['asset']).must_equal asset
    _(result['bank_account']['id']).must_equal bank_account['id']
    _(result['registered_bank']['id']).must_equal bank['id']
    _(result['type']).must_equal 'deposit'
    _(result['status']['code']).must_equal 'unconfirmed'
    _(result['ref_no'].nil?).must_equal false
    _(result['created_at'].nil?).must_equal false
  end
  

  it 'SAD: should not allow a user (non-admin) to verify his/her own deposit request' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'
    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    amount = 200
    asset = 'BZD'
    payload = {
      registered_bank_id: bank['id'],
      amount: amount,
      asset: asset
    }

    post "/v1/bank_accounts/#{bank_account['id']}/deposits", payload.to_json, header
    _(last_response.status).must_equal 201

    request = JSON.parse(last_response.body)

    status = 'verified'
    status_payload = {
      status: status
    }

    # let's attempt to verify the request
    put "/v1/bank_accounts/#{bank_account['id']}/deposits/#{request['id']}/status", status_payload.to_json, header
    _(last_response.status).must_equal 403 # foribiden 

  end

  it 'HAPPY: should allow an admin user to change the status of a deposit request' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    amount = 200
    asset = 'BZD'
    payload = {
      registered_bank_id: bank['id'],
      amount: amount,
      asset: asset
    }

    post "/v1/bank_accounts/#{bank_account['id']}/deposits", payload.to_json, header
    _(last_response.status).must_equal 201

    request = JSON.parse(last_response.body)

    status = 'user_confirmed'

    status_payload = {
      status: status
    }

    # let's attempt to confirm the request
    put "/v1/bank_accounts/#{bank_account['id']}/deposits/#{request['id']}/status", status_payload.to_json, header
    _(last_response.status).must_equal 200


    # create admin
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[1]
    create_admin_account(admin_account_seed)
    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    admin_auth = auth_admin_account(email, password)
    admin_access_token = admin_auth['access']['token']

    admin_header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{admin_access_token}"
    }

    admin_status = 'verified'
    admin_status_payload = {
      status: admin_status
    }

    # let's attempt to verify the request
    put "/v1/bank_accounts/#{bank_account['id']}/deposits/#{request['id']}/status", admin_status_payload.to_json, admin_header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['status']['code']).must_equal 'verified'
  end

  it 'HAPPY: should be able to allow a user to confirm a deposit request' do
    # create oblip account
    account_seed_one = ACCOUNT_SEEDS[0]
    create_account(CLIENT_SEEDS[0], account_seed_one)
    _(last_response.status).must_equal 201

    auth_account = JSON.parse(last_response.body)
    access_token = auth_account['tokens']['access']['token']

    header = {
      'CONTENT_TYPE' => 'application/json',
      'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
    }

    # get a list of registered banks
    get '/v1/registered_banks', nil, header
    _(last_response.status).must_equal 200

    registered_banks = JSON.parse(last_response.body)['registered_banks']

    bank = registered_banks[0]
    account_number = '1234567899'
    account_name = 'Reggie Escobar'

    payload = {
      registered_bank_id: bank['id'],
      account_number: account_number,
      account_name: account_name
    }

    # create bank account for logged-in user
    post 'v1/bank_accounts', payload.to_json, header
    _(last_response.status).must_equal 201

    bank_account = JSON.parse(last_response.body)

    # lets create a deposit 
    amount = 200
    asset = 'BZD'
    payload = {
      registered_bank_id: bank['id'],
      amount: amount,
      asset: asset
    }

    post "/v1/bank_accounts/#{bank_account['id']}/deposits", payload.to_json, header
    _(last_response.status).must_equal 201

    request = JSON.parse(last_response.body)

    status = 'user_confirmed'
    status_payload = {
      status: status
    }

    # let's confirm the request
    put "/v1/bank_accounts/#{bank_account['id']}/requests/#{request['id']}/status",
      status_payload.to_json, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['id']).must_equal request['id']
    _(result['status']['code']).must_equal status
  end

  

  # it 'HAPPY: should be able to create a bank account withdrawal request' do
  #   # create oblip account
  #   account_seed_one = ACCOUNT_SEEDS[0]
  #   create_account(CLIENT_SEEDS[0], account_seed_one)
  #   _(last_response.status).must_equal 201

  #   auth_account = JSON.parse(last_response.body)
  #   access_token = auth_account['tokens']['access']['token']

  #   header = {
  #     'CONTENT_TYPE' => 'application/json',
  #     'HTTP_AUTHORIZATION' => "Bearer #{access_token}"
  #   }

  #   # get a list of registered banks
  #   get '/v1/registered_banks', nil, header
  #   _(last_response.status).must_equal 200

  #   registered_banks = JSON.parse(last_response.body)['registered_banks']

  #   bank = registered_banks[0]
  #   account_number = '1234567899'

  #   payload = {
  #     registered_bank_id: bank['id'],
  #     account_number: account_number
  #   }

  #   # create bank account for logged-in user
  #   post 'v1/bank_accounts', payload.to_json, header
  #   _(last_response.status).must_equal 201

  #   bank_account = JSON.parse(last_response.body)

  #   # lets create a deposit 
  #   amount = 100
  #   asset = 'BZD'
  #   payload = {
  #     amount: amount,
  #     asset: asset
  #   }

  #   post "/v1/bank_accounts/#{bank_account['id']}/withdrawal", payload.to_json, header
  #   _(last_response.status).must_equal 201

  #   result = JSON.parse(last_response.body)

  #   _(result['amount']).must_equal amount
  #   _(result['asset']).must_equal asset
  #   _(result['type']).must_equal 'deposit'
  #   _(result['status']['code']).must_equal 'unconfirmed'
  #   _(result['ref_no'].nil?).must_equal false
  # end
end
