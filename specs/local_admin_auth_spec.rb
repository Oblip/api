# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Admin Account Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  it 'HAPPY: should authenticate an admin account' do
    admin_account_seed = ADMIN_ACCOUNT_SEEDS[0]
    account = create_admin_account(admin_account_seed)
    # payload = {
    #   email_address: admin_account_seed['email_address'],
    #   password: admin_account_seed['password']
    # }

    # header = { 'CONTENT_TYPE' => 'application/json' }

    # post 'v1/admin/auth', payload.to_json, header
    # _(last_response.status).must_equal 200

    # result = JSON.parse(last_response.body)

    email = admin_account_seed['email_address']
    password = admin_account_seed['password']

    result = auth_admin_account(email, password)

    _(result['account'].nil?).must_equal false
    _(result['account']['full_name']).must_equal account.full_name
    _(result['account']['type']).must_equal account.type
    _(result['account']['email_address']).must_equal account.email_address
    _(result['access'].nil?).must_equal false
    _(result['access']['token'].nil?).must_equal false
    _(result['access']['expires_on'].nil?).must_equal false
  end
end

