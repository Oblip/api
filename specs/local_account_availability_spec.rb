# frozen_string_literal: true

require_relative 'spec_helper.rb'

describe 'Test Account Availability Handling' do
  include Rack::Test::Methods

  before :each do
    wipe_database
  end

  after :each do
    wipe_database
  end

  it 'HAPPY: should get the basic data of a user' do
    account_seed = ACCOUNT_SEEDS[0]
    account_seed['username'] = random_username
    client = create_account(CLIENT_SEEDS[0], account_seed)
    _(last_response.status).must_equal 201
    auth_account = JSON.parse(last_response.body)
    account = auth_account['account']

    header = {
      'CONTENT_TYPE' => 'application/json'
    }

    get "v1/account_availability/#{account['username']}", nil, header
    _(last_response.status).must_equal 200

    result = JSON.parse(last_response.body)

    _(result['username']).must_equal account['username']
    _(result['name'].nil?).must_equal false
    _(result['type'].nil?).must_equal false
    _(result['is_agent'].nil?).must_equal false
  end

end
