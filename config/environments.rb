# frozen_string_literal: true

require 'roda'
require 'econfig'
require_relative '../lib/secure_db.rb'
require_relative '../lib/access_token.rb'
require_relative '../lib/refresh_token.rb'
require_relative '../infrastructure/horizon/api.rb'
require_relative '../infrastructure/twilio/api.rb'

module Oblip
  # Configuration for the API
  class Api < Roda
    plugin :environments

    extend Econfig::Shortcut
    Econfig.env = environment.to_s
    Econfig.root = '.'

    configure :development, :test do
      # Allows running reload! in pry to restart entire app
      def self.reload!
        exec 'pry -r ./spec/test_load_all'
      end
    end

    configure :development, :app_test, :test do
      # Example:postgres://YourUserName:YourPassword@YourHost:5432/YourDatabase
      ENV['DATABASE_URL'] = config.DATABASE_URL
    end

    configure :production, :staging do
      # Example:postgres://YourUserName:YourPassword@YourHost:5432/YourDatabase
    end

    # For all runnable environments
    configure do
      require 'sequel'

      Sequel.default_timezone = :utc

      begin
        retries ||= 0
        OBLIP_DB = Sequel.connect(ENV['DATABASE_URL'])
      rescue StandardError => e
        puts 'Could not connect to db. Will retry in 10 seconds'
        sleep(10)
        retry if (retries += 1) < 10
        raise StandardError, e.message
      end

      # rubocop:disable Naming/MethodName
      def self.DB
        OBLIP_DB
      end

      # rubocop:enable Naming/MethodName
      SecureDB.setup(config.DB_KEY)
      AccessToken.setup(config.ACCESS_KEY)
      RefreshToken.setup(config.REFRESH_KEY)

      Oblip::Horizon::Api.setup(
        network_url: config.HORIZON_NETWORK_URL,
        funding_acct: {
          address: config.FUNDING_ACCOUNT_ADDRESS,
          signer_seed: config.FUNDING_SIGNER_SEED
        },
        anchor_acct: {
          address: config.ANCHOR_ACCOUNT_ADDRESS,
          signer_seed: config.ANCHOR_SIGNER_SEED
        }
      )

      Oblip::TwilioIntegration::Api.setup(config)
    end
  end
end
