# frozen_string_literal: true

require_relative 'load_all'
require 'http'
require 'econfig'
require 'shoryuken'

# Shoryuken worker class to process transactions in parallel
class TransactionWorker
  extend Econfig::Shortcut
  Econfig.env = ENV['RACK_ENV'] || 'development'
  Econfig.root = File.expand_path('..', File.dirname(__FILE__))

  include ResultRepresenter

  require_relative 'test_helper' if ENV['RACK_ENV'] == 'test'

  Shoryuken.sqs_client = Aws::SQS::Client.new(
    access_key_id: config.AWS_ACCESS_KEY_ID,
    secret_access_key: config.AWS_SECRET_ACCESS_KEY,
    region: config.AWS_REGION
  )

  include Shoryuken::Worker
  shoryuken_options queue: config.TRANSACTION_QUEUE_URL, auto_delete: true

  def perform(sqs_msg, request_json)
    request = JSON.parse(request_json)
    identifier = request['data']['identifier']
    result = Oblip::CreateTransaction.new.call(request)

    output = represent_response(result, Oblip::TransactionRepresenter)
    
    puts output
    if ENV['RACK_ENV'] != 'test'
      sqs_msg.delete
      publish(identifier, output)
    end
  end

  private

  def publish(channel, message)
    puts "Posting to /#{channel}: #{message}"
    HTTP.headers(content_type: 'application/json')
        .post(
          "#{TransactionWorker.config.API_URL}/faye",
          body: {
            channel: "/#{channel}",
            data: JSON.parse(message)
          }.to_json
        )
  end
end
