# frozen_string_literal: false

module Oblip
  # A set of custom general exception error classes
  module Exceptions
    # Raised when an action requires authorization but non exists.
    class Unauthorized < StandardError
      def message(msg = 'You\'re not authorized.')
        msg
      end
    end

    # Raised when the expected value is invalid
    class InvalidValue < StandardError
      def message(msg = 'Invalid value supplied')
        msg
      end
    end

    # Raised when there is a missing value
    class MissingValue < StandardError
      def message(msg = 'Missing values')
        msg
      end
    end

    # Raised when a value that is duplicate is found
    class Duplication < StandardError
      def initialize(msg = 'Duplication found')
        @msg = msg
      end

      def message
        @msg
      end
    end

     # Raised when the resource does not exist
     class NoResource < StandardError
      def message(msg = 'No resource')
        msg
      end
    end

  end
end
