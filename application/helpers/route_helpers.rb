# frozen_string_literal: true

module Oblip
  # Web Api
  class Api < Roda
    # Represent HTTP response for result
    # Parameters:
    #   - result: Result object with #message to represent
    #   - success_representer: representer class if result is success
    #                          #to_json called if result is failure
    #   - (optional) block to execute before success representation
    # Returns: Json representation of success/failure message
    def represent_response(result, success_representer)
      r = result
      if result.success? && !success_representer.nil?
        r.value!.message = nil if r.success? && success_representer.nil?
        data_result = handle_success(result, success_representer)
        yield if block_given?
        data_result
      else
        handle_alternative(result)
      end
    end

    def handle_success(result, success_representer)
      http_response = HttpResponseRepresenter.new(result.value!)
      response.status = http_response.http_code
      success_representer.new(result.value!.message).to_json
    end

    def handle_alternative(result)
      value = result.success ? result.value! : result.failure
      http_response = HttpResponseRepresenter.new(value)
      response.status = http_response.http_code
      http_response.to_json
    end

    # Extracts sub-resource path from request
    # Parameters: HTTP request (Roda request object)
    # Returns: folder path (string)
    def folder_name_from(request)
      path = request.remaining_path
      path.empty? ? '' : path[1..-1]
    end
  end
end
