# frozen_string_literal: true

require 'ostruct'

module Oblip
  # Web Api
  class Api < Roda
    # rubocop:disable MethodLength, AbcSize
    def verify_request_identity(routing)
      raise StandardError unless routing.headers['AUTHORIZATION']

      scheme, auth_token = routing.headers['AUTHORIZATION'].split(' ')
      account_payload = AccessToken.payload(auth_token)
      inv_rq_iden = RequestIdentity.new 400, 'INVALID_AUTH_TYPE'
      return inv_rq_iden, nil unless scheme.match?(/^Bearer$/i)

      account_data = JsonRequestBody.parse_symbolize(account_payload)
      account = OpenStruct.new(account_data)
      RequestIdentity.new 200, 'VALID_TOKEN', account
    rescue AccessToken::InvalidTokenError
      RequestIdentity.new 401, 'UNAUTHORIZED', nil
    rescue AccessToken::ExpiredTokenError
      RequestIdentity.new 401, 'EXPIRED_TOKEN', nil
    rescue StandardError
      RequestIdentity.new 400, 'BAD_REQUEST', nil
    end
    # rubocop:enable MethodLength, AbcSize
  end
end
