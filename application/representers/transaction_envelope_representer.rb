# frozen_string_literal: true

module Oblip
  # Represents information of a transaction envelope for API rendering
  class TransactionEnvelopeRepresenter < Roar::Decorator
    include Roar::JSON
    property :envelope
  end
end
