# frozen_string_literal: true

require_relative 'asset_representer'

module Oblip
  # Represents a collection of available assets for API output
  class AvailableAssetsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :assets, extend: AssetRepresenter
  end
end
