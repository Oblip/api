# frozen_string_literal: true

module Oblip
  # Represents information a code name for API output
  class CodeNameRepresenter < Roar::Decorator
    include Roar::JSON

    property :code
    property :name
    property :message
  end
end
