# frozen_string_literal: true

require_relative 'registered_bank_representer'
require_relative 'code_name_representer'
require_relative 'bank_account_representer'

module Oblip
  # Represents information of a bank account for API output
  class BankAccountRequestRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :bank_account, extend: BankAccountRepresenter, class: Struct
    property :registered_bank, extend: RegisteredBankRepresenter, class: Struct
    property :ref_no
    property :amount
    property :asset
    property :type
    property :transaction_id
    property :status, extend: CodeNameRepresenter, class: Struct
    property :created_at
  end
end
