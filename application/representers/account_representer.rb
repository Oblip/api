# frozen_string_literal: true

require_relative 'country_representer'

module Oblip
  # Represents the info of Account for API rendering
  class AccountRepresenter < Roar::Decorator
    include Roar::JSON
    
    # Represents the name for API rendering
    class NameRepresenter < Roar::Decorator
      include Roar::JSON

      property :first_name
      property :middle_name
      property :last_name
    end

    # Represents the address for API rendering
    class AddressRepresenter < Roar::Decorator
      include Roar::JSON

      property :street_name
      property :city
      property :state
      property :zip_code
      property :country, extend: CountryRepresenter, class: OpenStruct
    end

    # Represents the profile for API rendering
    class ProfileRepresenter < Roar::Decorator
      include Roar::JSON

      property :name, extend: NameRepresenter, class: OpenStruct
      property :address, extend: AddressRepresenter, class: OpenStruct
      property :dob
      property :photo_url
    end

    property :id
    property :username
    property :phone_number
    property :email
    property :checking_account_id
    # property :secret_code
    property :profile, extend: ProfileRepresenter, class: OpenStruct
    property :type
  end
end
