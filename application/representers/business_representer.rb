# frozen_string_literal: true

require_relative 'country_representer'

module Oblip
  # Represents information of a business for API output
  class BusinessRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :legal_name
    property :username
    property :street
    property :city
    property :country, extend: CountryRepresenter, class: OpenStruct
    property :lat
    property :lng
    property :is_agent
    property :distance
    property :account_id
    property :checking_account_id
    property :type
  end
end
