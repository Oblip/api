# frozen_string_literal: true

module Oblip
  # Representers the info of Account for API rendering
  class CountryRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :code
    property :phone_code
    property :currency
  end
end
