# frozen_string_literal: true

require_relative 'status_representer'

module Oblip
  # Represents non-confidential information of client  for API output
  class ClientRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :account_id
    property :verifier_key
    property :wrapper_key
    property :fcm_token
    property :is_verified
  end
end
