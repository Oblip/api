# frozen_string_literal: true

require_relative 'asset_representer'

module Oblip
  # Represents a collection of businesses for API output
  class FundRepresenter < Roar::Decorator
    include Roar::JSON

    property :balance
    property :asset, extend: AssetRepresenter, class: OpenStruct
  end
end
