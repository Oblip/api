# frozen_string_literal: true

module Oblip
  # Represents a collection of payout for API output
  class PayoutRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :amount
    property :payout_ref
    property :username
    property :is_paid
    property :paid_date
  end
end
