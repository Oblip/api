# frozen_string_literal: true

require_relative 'account_representer'

module Oblip
  # Represents non-confidential information of client  for API output
  class AccountSignerRepresenter < Roar::Decorator
    include Roar::JSON

    property :account, extend: AccountRepresenter, class: OpenStruct
    property :signer_seed
    property :wrapper_public_key
  end
end
