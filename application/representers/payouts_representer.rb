# frozen_string_literal: true

require_relative 'payout_representer'

module Oblip
  # Represents a collection of payouts for API output
  class PayoutsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :payouts, extend: PayoutRepresenter
  end
end
