# frozen_string_literal: true

require_relative 'account_representer'
require_relative 'tokens_representer'

module Oblip
  # Representers the info of Account for API rendering
  class AuthAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :account, extend: AccountRepresenter, class: OpenStruct
    property :tokens, extend: TokensRepresenter, class: OpenStruct
  end
end
