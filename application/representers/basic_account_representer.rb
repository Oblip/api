# frozen_string_literal: true

module Oblip
  # Represents an asset for API output
  class BasicAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :username
    property :name
    property :type
    property :is_agent
  end
end
