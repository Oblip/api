# frozen_string_literal: true

require_relative 'registered_bank_representer'

module Oblip
  # Represents a collection of registered banks for API output
  class RegisteredBanksRepresenter < Roar::Decorator
    include Roar::JSON

    collection :registered_banks, extend: RegisteredBankRepresenter
  end
end
