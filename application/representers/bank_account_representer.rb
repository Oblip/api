# frozen_string_literal: true

require_relative 'registered_bank_representer'
require_relative 'code_name_representer'

module Oblip
  # Represents information of a bank account for API output
  class BankAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :username
    property :registered_bank, extend: RegisteredBankRepresenter, class: OpenStruct
    property :account_name
    property :account_number
    property :is_partner
    property :status, extend: CodeNameRepresenter, class: Struct
  end
end
