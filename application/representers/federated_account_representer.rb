# frozen_string_literal: true

module Oblip
  # Represents an asset for API output
  class FederatedAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :username
    property :name
    property :type
    property :checking_account_id
  end
end
