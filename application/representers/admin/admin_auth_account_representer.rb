# frozen_string_literal: true

require_relative '../token_representer.rb'
require_relative 'admin_account_representer.rb'

module Oblip
  module Admin
    # Represents information of AdminAccount for API output
    class AdminAuthAccountRepresenter < Roar::Decorator
      include Roar::JSON

      property :account, extend: AdminAccountRepresenter, class: OpenStruct
      property :access, extend: TokenRepresenter, class: OpenStruct
    end
  end
end
