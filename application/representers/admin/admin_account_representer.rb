# frozen_string_literal: true

module Oblip
  module Admin
    # Represents information of AdminAccount for API output
    class AdminAccountRepresenter < Roar::Decorator
      include Roar::JSON

      property :id
      property :full_name
      property :email_address
      property :type
    end
  end
end
