# frozen_string_literal: true

require 'roar/decorator'
require 'roar/json'

require_relative 'admin/init.rb'

Dir.glob("#{File.dirname(__FILE__)}/*.rb").each do |file|
  require file
end
