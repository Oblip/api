# frozen_string_literal: true

require_relative 'federated_account_representer'

module Oblip
  # Represents a collection of federated accounts for API output
  class FederatedAccountsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :accounts, extend: FederatedAccountRepresenter
  end
end
