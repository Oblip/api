# frozen_string_literal: true

module Oblip
  # Representer a token for API rendering
  class TokenRepresenter < Roar::Decorator
    include Roar::JSON

    property :token
    property :expires_on
  end
end
