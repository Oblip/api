# frozen_string_literal: true

require_relative 'bank_account_request_representer'

module Oblip
  # Represents a collection of businesses for API output
  class BankAccountRequestsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :bank_account_requests, extend: BankAccountRequestRepresenter
  end
end
