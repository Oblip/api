# frozen_string_literal: true

module Oblip
  # Represents a transaction for API output
  class TransactionRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :tx_hash
    property :from_internal
    property :to_internal
    property :message
    # property :paging_token
    # property :network_fee
    property :amount
    property :asset
    property :type
  end
end
