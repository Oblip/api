# frozen_string_literal: true

require_relative 'fund_representer'

module Oblip
  # Represents non-confidential information of client  for API output
  class CheckingAccountRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    collection :funds, extend: FundRepresenter
  end
end
