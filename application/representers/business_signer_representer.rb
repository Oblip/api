# frozen_string_literal: true

require_relative 'business_representer'

module Oblip
  # Represents business signer information for API output
  class BusinessSignerRepresenter < Roar::Decorator
    include Roar::JSON

    property :account, extend: BusinessRepresenter, class: OpenStruct
    property :signer_seed
    property :wrapper_public_key
  end
end
