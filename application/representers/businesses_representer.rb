# frozen_string_literal: true

require_relative 'business_representer'

module Oblip
  # Represents a collection of businesses for API output
  class BusinessesRepresenter < Roar::Decorator
    include Roar::JSON

    collection :businesses, extend: BusinessRepresenter
  end
end
