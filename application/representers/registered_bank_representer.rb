# frozen_string_literal: true

require_relative 'country_representer'

module Oblip
  # Represents information of a business for API output
  class RegisteredBankRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :name
    property :account_number
    property :account_name
    property :country, extend: CountryRepresenter, class: OpenStruct
    property :balance
  end
end
