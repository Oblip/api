# frozen_string_literal: true

require_relative 'bank_account_representer'

module Oblip
  # Represents a collection of businesses for API output
  class BankAccountsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :bank_accounts, extend: BankAccountRepresenter
  end
end
