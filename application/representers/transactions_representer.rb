# frozen_string_literal: true

require_relative 'transaction_representer'

module Oblip
  # Represents a collection of transactions for API output
  class TransactionsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :transactions, extend: TransactionRepresenter
  end
end
