# frozen_string_literal: true

module Oblip
  # Represents an asset for API output
  class AssetRepresenter < Roar::Decorator
    include Roar::JSON

    property :asset_code
    property :asset_type
    property :asset_issuer
    property :is_authorized
    property :asset_name
  end
end
