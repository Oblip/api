# frozen_string_literal: true

require_relative 'token_representer'

module Oblip
  # Representers the refresh and access token for API rendering
  class TokensRepresenter < Roar::Decorator
    include Roar::JSON

    property :refresh, extend: TokenRepresenter, class: OpenStruct
    property :access, extend: TokenRepresenter, class: OpenStruct
  end
end
