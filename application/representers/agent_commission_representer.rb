# frozen_string_literal: true

require_relative 'transaction_representer'

module Oblip
  # Represents information of a bank account for API output
  class AgentCommissionRepresenter < Roar::Decorator
    include Roar::JSON

    property :id
    property :transaction, extend: TransactionRepresenter, class: OpenStruct
    property :username
    property :amount
    property :asset
    property :payout
  end
end
