# frozen_string_literal: true

require_relative 'agent_commission_representer'

module Oblip
  # Represents a collection of agent commissions for API output
  class AgentCommissionsRepresenter < Roar::Decorator
    include Roar::JSON

    collection :agent_commissions, extend: AgentCommissionRepresenter
  end
end
