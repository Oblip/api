# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to update bank account status in database
  # Usage:
  #   result = UpdateBankAccountStatus.call(requst_identity:, data:)
  #   result.success?
  class UpdateBankAccountStatus
    include Dry::Transaction

    step :verify_auth
    step :check_required_data
    step :validate_status
    step :verify_bank_account

    def verify_auth(request_identity:, data:)
      if request_identity.code != 200 ||
         request_identity.data&.type != 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      data = JsonRequestBody.symbolize(data)

      Success(data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def check_required_data(data)
      raise Exceptions::MissingValue unless data.key?(:bank_account_id)
      raise Exceptions::MissingValue unless data.key?(:status)

      Success(data)
    rescue Exceptions::MissingValue
      Failure(Result.new(:bad_request, 'Missing required data'))
    end

    def validate_status(data)
      status = data[:status]
      comment = data[:comment]
      entity = Entity::BankAccount::Status.build(status, comment)
      raise Exceptions::InvalidValue unless entity

      Success(data)
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid status'))
    end

    def verify_bank_account(data)
      id = data[:bank_account_id]
      status = data[:status]
      comment = data[:comment]
      bank_account = Repository::BankAccounts
                     .update_status(id, status, comment)
      
      send_notification(bank_account)
      Success(Result.new(:ok, bank_account))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    private

    def send_notification(entity)
      if ENV['RACK_ENV'] != 'test'
        fcm = FCM.new(Api.config['FCM_SERVER_KEY'])
        identity = Repository::Usernames.find_username(entity.username)
        clients = Repository::Clients.find_many_by_account(identity.master_account_id)
        fcm_tokens = clients.map { |client| client.fcm_token }

        content = notification_content(entity.status)

        options = Entity::DeviceNotification.new(
          title: content[:title],
          message: content[:message],
          type: Entity::DeviceNotification::Types::BANK_ACCOUNT,
          data: JSON.parse(BankAccountRepresenter.new(entity).to_json)
        )

        fcm.send(fcm_tokens, options.to_hash)
        fcm = nil
      end
    end

    # TODO: Move to bank account entity
    def notification_content(status)
      notif_title = 'Bank Account'
      notif_message = "Your bank account status changed to '#{status.name}'."

      { title: notif_title, message: notif_message }
    end
  end
end
