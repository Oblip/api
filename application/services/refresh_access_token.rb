# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to verify and refresh access token
  # Usage:
  #   result = RefreshAccessToken.call(signature:, ip_address:, data:)
  #   result.success?
  class RefreshAccessToken
    include Dry::Transaction

    step :check_params
    step :verify_credentials
    step :validate_request
    step :generate_token

    def check_params(data:, signature:)
      msg = 'Missing or invalid values provided.'
      raise Exceptions::MissingValue, msg unless data || signature
      raise Exceptions::MissingValue, msg unless data['refresh_token']
      # raise Exceptions::MissingValue, msg unless data['type']

      if data['type'] == 'business'
        raise Exceptions::MissingValue, msg unless data['business_id']
      end

      Success(data: data, signature: signature)
    rescue Exceptions::MissingValue => e
      Failure(Result.new(:bad_request, 'Invalid payload'))
    end

    def verify_credentials(data:, signature:)
      client_id, secret_code = data['refresh_token'].split('.')
      client = Repository::Clients.credentials_valid?(client_id, secret_code)

      raise Exceptions::Unauthorized unless client

      Success(data: data, client: client, signature: signature)
    rescue Exceptions::Unauthorized => e
      puts 'Client does not exit'
      Failure(Result.new(:unauthorized, e.message))
    end

    def validate_request(data:, client:, signature:)
      request_verifier = VerifyRequest.new(client.verifier_key)
      signed_refresh_token = request_verifier.parse(signature, data.to_json)

      Success(client: client, data: data)
    rescue VerifyRequest::SignatureVerificationFailed
      puts 'We could not verify sig'
      Failure(Result.new(:unauthorized, 'You\'re not authorized.'))
    end

    def generate_token(client:, data:)
      access_token = nil

      case data['type']
      when 'personal'
        access_token = generate_personal_token(client)
      when 'business'
        access_token = generate_business_token(client, data['business_id'])
      else
        access_token = generate_personal_token(client)
      end

      Success(Result.new(:created, access_token))
    end

    private 

    def generate_business_token(client, business_id)
      business = Repository::Businesses.find_id(business_id)
      access_token = Entity::Token.create_access_token(business, client)
      access_token
    end

    def verify_business_ownership(client, business_id)
      business = Repository::Businesses.find_id(business_id)
      (business.account_id == client.account_id)
    end
    
    def generate_personal_token(client)
      account = Repository::Accounts.find_id(client.account_id)
      access_token = Entity::Token.create_access_token(account, client)
      access_token
    end
  end
end
