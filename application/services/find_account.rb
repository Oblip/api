# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a account in the database
  # Usage:
  #   result = FindAccount.new.call(request_identity)
  #   result.success?
  class FindAccount
    include Dry::Transaction

    step :verify_auth
    step :find_account

    def verify_auth(request_identity)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success(request_identity.data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def find_account(auth_account)
      account = nil
      case auth_account.type
      when 'personal'
        account = Repository::Accounts.find_id(auth_account.account_id)
      when 'business'
        account = Repository::Businesses.find_id(auth_account.account_id)
      else
        account = Repository::Accounts.find_id(auth_account.account_id)
      end

      Success(Result.new(:ok, account))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
