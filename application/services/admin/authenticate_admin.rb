# frozen_string_literal: true

require 'dry/transaction'
require 'ostruct'

module Oblip
  module Admin
    # Service to authenticate an admin account
    # Usage:
    #   result = AuthenticateAdmin.new.call(email_address:, password:)
    #   result.success?
    class AuthenticateAdmin
      include Dry::Transaction

      step :validate_credentials
      step :generate_tokens

      # TODO: allow only whitelisted applications

      def validate_credentials(email_address:, password:)
        account = Repository::AdminAccounts
                  .credentials_valid?(email_address, password)
        raise Exceptions::Unauthorized unless account

        Success(account)
      rescue Exceptions::Unauthorized
        Failure(Result.new(:unauthorized, 'Unauthorized'))
      rescue StandardError
        Failure(Result.new(:internal_error, 'Something went wrong.'))
      end

      # rubocop:disable Metrics/AbcSize
      # rubocop:disable Metrics/MethodLength
      def generate_tokens(account)
        # TODO: refactor this to be more mantainable.
        token_data = {
          id: account.id, full_name: account.full_name,
          email_address: account.email_address, type: account.type
        }

        # TODO: refactor this crap:
        token = AccessToken.create(token_data.to_json,
                                   AccessToken::ONE_HOUR / 2)
        token = Token.new(token[:token], token[:expires_on])
        Success(Result.new(:ok, AdminAuthAccount.new(account, token)))
      rescue StandardError => error
        puts error.message
        puts error.inspect
        puts error.backtrace
        Failure(Result.new(:internal_error, 'Something went wrong.'))
      end
      # rubocop:enable Metrics/AbcSize
      # rubocop:enable Metrics/MethodLength
    end
  end
end
