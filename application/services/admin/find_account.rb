# frozen_string_literal: true

require 'dry/transaction'
require 'ostruct'

module Oblip
  module Admin
    # Service to find an account in the database
    # Usage:
    #   result = AuthenticateAdmin.new.call(email_address:, password:)
    #   result.success?
    class FindAccount
      include Dry::Transaction

      step :verify_auth
      step :find_account

      def verify_auth(request_identity:, username:)
        raise Exceptions::Unauthorized unless request_identity.data['type'] == 'admin'
  
        Success(username)
      rescue Exceptions::Unauthorized
        Failure(Result.new(:unauthorized, request_identity.message))
      rescue StandardError
        Failure(Result.new(:internal_error, 'Something went wrong.'))
      end

      def find_account(username)
        identity = Repository::Usernames.find_username(username)
        account = nil
        case identity.type
        when 'personal'
          account = Repository::Accounts.find_id(identity.reference_id)
        when 'business'
          account = Repository::Businesses.find_id(identity.reference_id)
        end

        Success(Result.new(:ok, account))

      rescue StandardError
        Failure(Result.new(:internal_error, 'Something went wrong.'))
      end
    end
  end
end