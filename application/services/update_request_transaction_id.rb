# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to update add a transaction id for a request in the db
  # Usage:
  #   result = UpdateequestTransactionId.new.call(request_identity:, req_id:, tx_id:)
  #   result.success?
  class UpdateRequestTransactionId
    include Dry::Transaction

    step :verify_auth
    step :update

    def verify_auth(request_identity:, req_id:, tx_id:)
      raise Exceptions::Unauthorized if request_identity.code != 200
      raise Exceptions::Unauthorized if request_identity.data['type'] != 'admin'

      Success(req_id: req_id, tx_id: tx_id)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def update(req_id:, tx_id:)
      result = Repository::BankAccountRequests
                .update_transaction_id(req_id, tx_id)
      Success(Result.new(:ok, result))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
