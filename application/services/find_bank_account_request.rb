# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a single bank account request in the database
  # Usage:
  #   result = FindBankAccountRequest.new.call(request_identity:, bank_account_id, request_id:)
  #   result.success?
  class FindBankAccountRequest
    include Dry::Transaction

    step :verify_auth
    step :verify_ownership
    step :verify_request
    step :output_request
    
    def verify_auth(request_identity:, bank_account_id:, request_id:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      auth_account = request_identity.data
      ba_id = bank_account_id
      rq_id = request_id

      Success(auth_account: auth_account, ba_id: ba_id, rq_id: rq_id)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def verify_ownership(auth_account:, ba_id:, rq_id:)
      bank_account_id = ba_id
      is_owner = true
      if bank_account_id
        bank_account = Repository::BankAccounts.find_id(bank_account_id)
        is_owner = bank_account.username == auth_account.username
      end

      raise Exceptions::Unauthorized unless
        (!bank_account_id.nil? && is_owner) || auth_account.type == 'admin'

      Success(ba: bank_account, rq_id: rq_id)
    rescue Exceptions::Unauthorized
      msg = 'You don\'t have the rights to view this content'
      Failure(Result.new(:forbidden, msg))
    end

    def verify_request(ba:, rq_id:)
      request = Repository::BankAccountRequests.find_id(rq_id)
      raise Exceptions::NoResource unless request

      rq_ba_id = request.bank_account_id
      raise Exceptions::Unauthorized unless rq_ba_id == ba.id

      Success(request)
    rescue Exceptions::Unauthorized
      msg = 'You don\'t have the rights to view this content'
      Failure(Result.new(:unauthorized, msg))
    rescue Exceptions::NoResource
      Failure(Result.new(:not_found, 'Resource not found'))
    end

    def output_request(request)
      Success(Result.new(:ok, request))
    rescue StandardError 
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
