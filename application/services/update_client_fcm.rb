# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to change a client's fcm token in the database
  # Usage:
  #   result = UpdateRequestStatus.new.call(request_identity:, data:)
  #   result.success?
  class UpdateClientFcm
    include Dry::Transaction

    step :verify_auth
    step :verify_request
    step :check_policy
    step :update_fcm
 

    def verify_auth(request_identity:, data:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      Success(data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def verify_request(payload)
      client_id = payload['client_id']
      signature = payload['signature']
      data = payload['data']
      client = Repository::Clients.find_id(client_id)
      raise Exceptions::Unauthorized unless client

      request_verifier = VerifyRequest.new(client.verifier_key)
      data = request_verifier.parse(signature, data.to_json)

      Success(client: client, data: data)
    rescue Exceptions::Unauthorized,
           VerifyRequest::SignatureVerificationFailed => e
      Failure(Result.new(:unauthorized, e.message))
    end

    def check_policy(client:, data:)
      fcm = data[:fcm_token]
      raise Exceptions::InvalidValue unless !fcm.nil?
      raise Exceptions::InvalidValue unless fcm.size > 30
     
      Success(client_id: client.id, token: fcm)
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid or missing fcm token'))
    end

    def update_fcm(client_id:, token:)
      client = Repository::Clients.update_fcm_token(client_id, token)

      Success(Result.new(:ok, client))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
