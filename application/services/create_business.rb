# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to create business in the database
  # Usage:
  #   result = CreateBusiness.new.call(request_identity:, data:)
  #   result.success?
  class CreateBusiness
    include Dry::Transaction

    step :verify_auth
    step :check_username_exists
    step :check_valid_username
    step :create_entity
    step :create_business

    private

    def verify_auth(request_identity:, data:)
      if request_identity.code != 200 ||
         request_identity.data.type != 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      Success(data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def check_username_exists(data)
      username = Repository::Usernames.find_username(data['username'])
      raise Exceptions::Duplication.new('Username already exists.') if username
      
      Success(data)
    rescue Exceptions::Duplication => error
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:bad_request, error.message))
    end

    def check_valid_username(data)
      data['username'] = AccountPolicy.transform_username(data['username'])
      raise Exceptions::InvalidValue unless AccountPolicy.valid_username?(data['username'])

      Success(data)
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Username is invalid'))
    end

    def create_entity(data)
      business = Entity::Business.build_from_hash(data)
      Success(business)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def create_business(data)
      business = Repository::Businesses.create(data)
      Success(Result.new(:created, business))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
