# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a bank account in the database
  # Usage:
  #   result = FindBankAccount.new.call(request_identity:, id:)
  #   result.success?
  class FindBankAccount
    include Dry::Transaction

    step :verify_auth
    step :find_bank_account
    step :check_access

    def verify_auth(request_identity:, id:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success(auth_account: request_identity.data, id: id)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def find_bank_account(auth_account:, id:)
      bank_account = Repository::BankAccounts.find_id(id)
      raise Exceptions::NoResource unless bank_account

      Success(auth_account: auth_account, bank_account: bank_account)

    rescue Exceptions::NoResource
      Failure(Result.new(:not_found, 'Resource not found'))
    end

    def check_access(auth_account:, bank_account:)
      if auth_account&.username != bank_account.username &&
         auth_account&.type != 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      Success(Result.new(:ok, bank_account))
    rescue Exceptions::Unauthorized
      Failure(Result.new(:not_found, 'Resource not found.'))
    end
  end
end
