# frozen_string_literal: true

require 'dry-monads'
# require 'phonelib'
# require_relative '../../lib/securable.rb'
require 'ostruct'

module Oblip
  # Service to create a transaction envelope
  # Usage:
  #   result = CreateTransactionEnvelope.new.call(request_identity:, data:)
  #   result.success?
  class CreateTransactionEnvelope
    include Dry::Transaction
    include Securable

    step :verify_auth
    # step :validate_message # TODO
    # step :validate_asset # TODO
    step :identify_sender
    step :identify_receiver
    step :create_envelope

    private

    def verify_auth(request_identity:, data:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      Success(auth_account: request_identity.data, data: data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def identify_sender(auth_account:, data:)
      account = nil
      is_admin = auth_account.type == 'admin'

      case auth_account.type
      when 'personal'
        account = Repository::Accounts.find_id(auth_account.account_id)
      when 'business'
        account = Repository::Businesses.find_id(auth_account.account_id)
      end

      Success(sender: account, is_admin: is_admin, data: data)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def identify_receiver(sender:, is_admin:, data:)
      to_internal = data['to']
      receiver = Repository::Usernames.find_username(to_internal)
      raise Exceptions::NoResource unless receiver

      account = nil
      case receiver.type
      when 'personal'
        account = Repository::Accounts.find_id(receiver.reference_id)
      when 'business'
        account = Repository::Businesses.find_id(receiver.reference_id)
      end

      Success(sender: sender, receiver: account, is_admin: is_admin, data: data)
    rescue Exceptions::NoResource => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:bad_request, 'Invalid value.'))
    end

    # def validate_asset(input)
    #   fund_address = Horizon::Api.fund[:account].address
    #   mapper = Horizon::Account(fund_address)
    #   fund_data = mapper.load
    #   asset = input[:data]['asset']
    #   result = fund_data.assets.select { |n| n.asset_code == asset }
      
    #   raise Exceptions::InvalidValue unless result.size > 0

    #   Success(input)
    # rescue Exceptions::InvalidValue
    #   Failure(Result.new(:bad_request, 'Invalid value'))
    # end

    def create_envelope(input)
      fund_address = Horizon::Api.fund[:account].address
      from = input[:is_admin] ? fund_address : input[:sender].checking_account_id
      to = input[:receiver].checking_account_id

      envelope = Horizon::Api.create_payment_envelope(
        from: from, to: to,
        amount: input[:data]['amount'],
        asset: input[:data]['asset']
      )

      result = OpenStruct.new(envelope: envelope)
      Success(Result.new(:ok, result))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    # def create_memo(input)
    #   from_user = input[:is_admin] ? 'funding-bot' : input[:sender].username
    #   to_user = input[:receiver].username

    #   random_key = SecureDB.generate_key

    #   memo_payload = {
    #     from: from_user,
    #     to: to_user,
    #     message: input[:data]['message']
    #   }.to_json
      
    #   encrypted_memo = Base64.strict_encode64(base_encrypt(memo_payload, random_key))
    #   s_key = ''
    #   s_key = Base64.strict_encode64(base_encrypt(random_key, input[:sender].secret_code)) if !input[:is_admin]
    #   r_key = Base64.strict_encode64(base_encrypt(random_key, input[:receiver].secret_code))

    #   divider = s_key != '' ? ';' : ''
    #   keys = s_key + divider + r_key

    #   memo = encrypted_memo + '.' + keys
    #   puts memo
    #   memo
    # end
  end
end
