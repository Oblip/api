# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all registered banks in the database
  # Usage:
  #   result = FindRegisteredBanks.new.call(request_identity:)
  #   result.success?
  class FindRegisteredBanks
    include Dry::Transaction

    step :verify_auth
    step :find_all_registered_banks

    def verify_auth(request_identity:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success()
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def find_all_registered_banks
      banks = Repository::RegisteredBanks.all

      registered_banks = OpenStruct.new(registered_banks: banks)

      Success(Result.new(:ok, registered_banks))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
