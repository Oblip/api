# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to create a bank account in the database
  # Usage:
  #   result = CreateBankAccount.new.call(request_identity:, data:)
  #   result.success?
  class CreateBankAccount
    include Dry::Transaction

    step :verify_auth
    step :create_entity
    step :check_values
    step :check_account_exists
    step :create_bank_account

    def verify_auth(request_identity:, data:)
      if request_identity.code != 200 ||
         request_identity.data['type'] == 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      Success(auth_account: request_identity.data, data: data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    # rubocop:disable MethodLength
    def create_entity(auth_account:, data:)
      bank_id = data['registered_bank_id']
      is_partner = (auth_account.type == 'business')

      entity = Entity::BankAccount.new(
        id: nil,
        username: auth_account.username,
        registered_bank: Entity::RegisteredBank.build_id(bank_id),
        account_number: data['account_number'],
        account_name: data['account_name'],
        is_partner: is_partner,
        status_code: nil,
        status_comment: nil
      )

      Success(entity)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
    # rubocop:enable MethodLength

    def check_values(entity)
      # TODO: move to policies
      checks = []
      checks.push(entity.account_number.nil?)
      checks.push(entity.account_name.nil?)
      checks.push(entity.registered_bank.id.nil?)

      raise Exceptions::MissingValue if checks.include?(true)

      Success(entity)
    rescue Exceptions::MissingValue
      Failure(Result.new(:bad_request, 'Missing values.'))
    end

    def check_account_exists(entity)
      it_exists = Repository::BankAccounts.bank_account_exists?(
        entity.username, entity.registered_bank.id, entity.account_number
      )

      raise Exceptions::Duplication.new(
        'Bank Account already exists.') if it_exists

      Success(entity)
    rescue Exceptions::Duplication => error
      Failure(Result.new(:bad_request, error.message))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def create_bank_account(entity)
      bank_account = Repository::BankAccounts.create(entity)

      if ENV['RACK_ENV'] != 'test'
        bank_account.notify_verification(
          Api.config['BANK_AGENT_WEBHOOK_URL']) if bank_account
      end

      Success(Result.new(:created, bank_account))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
