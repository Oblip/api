# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to send a OTP verification code to a phone number
  # Usage:
  #   result = SendPhoneVerification.new.call(config:, phone_number:)
  #   result.success?
  class SendPhoneVerification
    include Dry::Transaction

    step :init
    step :validate_request
    step :validate_phone_number
    step :generate_code
    step :save_verification_token
    step :generate_msg
    step :send_sms

    private

    def init(client_id:, signature:, data:)
      @client_id = client_id
      @signature = signature
      @data = data

      Success('Passed')
    end

    def validate_request
      client = Repository::Clients.find_id(@client_id)
      raise Exceptions::Unauthorized unless client
     
      request_verifier = VerifyRequest.new(client.verifier_key)
      @data = request_verifier.parse(@signature, @data.to_json)
      Success('Passed')
    rescue Exceptions::Unauthorized,
           VerifyRequest::SignatureVerificationFailed => e
      Failure(Result.new(:unauthorized, e.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def generate_code
      code_result = ShortCodeToken.create
      @code = code_result[:code]
      @token_data = code_result[:token_data]

      Success('Passed')
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def validate_phone_number
      @phone_number = Phonelib.parse(@data[:phone_number])
      error_msg = 'Invalid phone number'
      raise Exceptions::InvalidValue, error_msg unless @phone_number.valid?

      Success('Passed')
    rescue Exceptions::InvalidValue => e
      Failure(Result.new(:bad_request, e.message))
    end

    def save_verification_token
      Repository::Clients
        .save_verification_token(@client_id, @token_data[:token])
      Success('Passed')
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def generate_msg
      message = "Oblip verification code: #{@code}. You are binding this "
      message += ' phone number to your Oblip account. Valid for 20 minutes.'

      Success(message)
    end

    def send_sms(message)
      prod = ENV['RACK_ENV'] == 'production'
      stag = ENV['RACK_ENV'] == 'staging'
      if prod || stag
        TwilioIntegration::Api.send_message(
          to:  @phone_number.to_s,
          message: message
        )
        # SMS::Api.new(@config).send_message(phone_number: @phone_number.to_s,
        #                                    type: SMS::TYPE_TRANSACTIONAL,
        #                                    message: message)
      end
      Success(Result.new(:ok, '4-digit code was sent successfully'))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
