# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to change a bank account request status in the database
  # Usage:
  #   result = UpdateRequestStatus.new.call(request_identity:, data:)
  #   result.success?
  class UpdateRequestStatus
    include Dry::Transaction

    step :verify_auth
    step :verify_ownership
    step :verify_valid_status
    step :verify_status_policy
    step :change_status


    def verify_auth(request_identity:, data:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      Success(auth_account: request_identity.data, data: data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    # TODO: fix rubocop issue
    # rubocop:disable AbcSize, MethodLength
    def verify_ownership(auth_account:, data:)
      request = Repository::BankAccountRequests.find_id(data['request_id'])
      bank_account = Repository::BankAccounts.find_id(request.bank_account_id)

      if auth_account.username != bank_account.username &&
         auth_account.type != 'admin'
        raise Exceptions::Unauthorized
      end

      Success(auth_account: auth_account, data: data, bank_account: bank_account)
    rescue Exceptions::Unauthorized => error
      puts error.inspect
      puts error.backtrace
      msg = 'You don\'t have the rights to take this action'
      Failure(Result.new(:forbidden, msg))
    end
    # rubocop:enable AbcSize, MethodLength

    def verify_valid_status(auth_account:, data:, bank_account:)
      status = data['status']
      valid_status = Entity::BankAccountRequest::Status.key?(status.to_sym)
      raise Exceptions::InvalidValue unless valid_status

      Success(auth_account: auth_account, data: data, bank_account: bank_account)
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid status'))
    end

    def verify_status_policy(auth_account:, data:, bank_account:)
      status = data['status'].to_sym
      if %i[verified unable_to_verify].include?(status) &&
         auth_account.type != 'admin'
        raise Exceptions::Unauthorized
      end

      Success(data: data, bank_account: bank_account)
    rescue Exceptions::Unauthorized
      msg = 'You don\'t have the rights to take this action'
      Failure(Result.new(:forbidden, msg))
    end

    def change_status(data:, bank_account:)
      status = data['status']
      comment = data['comment']
      id = data['request_id']
      result = Repository::BankAccountRequests
               .update_status(id, status, comment)

      case result.status.code
      when 'user_confirmed'
        notify_to_discord(result)
      when 'verified'
        send_notification(bank_account.username, result)
      when 'unable_to_verify'
        send_notification(bank_account.username, result)
      end

      Success(Result.new(:ok, result))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    private

    def notify_to_discord(entity)
      if ENV['RACK_ENV'] != 'test'
        entity.notify_confirmation(
          Api.config['BANK_AGENT_WEBHOOK_URL']) if entity
      end
    end

    def send_notification(username, entity)
      if ENV['RACK_ENV'] != 'test'
        fcm = FCM.new(Api.config['FCM_SERVER_KEY'])
        identity = Repository::Usernames.find_username(username)
        clients = Repository::Clients.find_many_by_account(identity.master_account_id)
        fcm_tokens = clients.map { |client| client.fcm_token }

        content = notification_content(entity)

        options = Entity::DeviceNotification.new(
          title: content[:title],
          message: content[:message],
          type: Entity::DeviceNotification::Types::BANK_ACCOUNT,
          data: JSON.parse(BankAccountRequestRepresenter.new(entity).to_json)
        )

        fcm.send(fcm_tokens, options.to_hash)
        fcm = nil
      end
    end

    # TODO: Move to bank account entity
    def notification_content(entity)
      notif_title = "Banking Request Ref #: #{entity.ref_no}"
      notif_message = "Your banking request changed status to '#{entity.status.name}'."

      { title: notif_title, message: notif_message }
    end
  end
end
