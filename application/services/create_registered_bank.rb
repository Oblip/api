# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to create a registered bank in the database
  # Usage:
  #   result = CreateRegisteredBank.new.call(request_identity:, data:)
  #   result.success?
  class CreateRegisteredBank
    include Dry::Transaction

    step :verify_auth
    step :create_entity
    step :create_registered_bank

    def verify_auth(request_identity:, data:)
      if request_identity.code != 200 ||
         request_identity.data['type'] != 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      Success(data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    # rubocop:disable MethodLength
    def create_entity(data)
      entity = Entity::RegisteredBank.new(
        id: nil,
        name: data['name'],
        account_number: data['account_number'],
        account_name: data['account_name'],
        balance: nil,
        country: Entity::Country.build_id(data['country_id'])
      )

      Success(entity)
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
    # rubocop:enable MethodLength

    def create_registered_bank(entity)
      bank = Repository::RegisteredBanks.create(entity)

      Success(Result.new(:created, bank))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
