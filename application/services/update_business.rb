# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to update a business in the database
  # Usage:
  #   result = UpdateBusiness.new.call(auth_account:, data:)
  #   result.success?
  class UpdateBusiness
    include Dry::Transaction

    step :verify_auth
    step :create_entity
    step :update

    def verify_auth(request_identity:, data:)
      raise Exceptions::Unauthorized if request_identity.code != 200
      raise Exceptions::Unauthorized if request_identity.data['type'] != 'admin'

      Success(data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def create_entity(data)
      business = Entity::Business.build_from_hash(data)

      Success(business)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def update(business_entity)
      business = Repository::Businesses.update(business_entity)
      Success(Result.new(:ok, business))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace

      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
