# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'
require 'uri'

module Oblip
  # Service to create an account in the database
  # Usage:
  #   result = CreateAccount.new.call(client_id:, data:, signature:)
  #   result.success?
  class CreateAccount
    include Dry::Transaction

    step :validate_request
    step :check_client_verification
    step :check_valid_username
    step :check_username_exists
    step :format_phone_number
    step :check_account_existence
    step :create_account
    step :save_client_account_reference
    step :save_secret
    step :generate_tokens

    private

    def validate_request(client_id:, data:, signature:)
      client = Repository::Clients.find_id(client_id)
      raise Exceptions::Unauthorized unless client

      request_verifier = VerifyRequest.new(client.verifier_key)
      data = request_verifier.parse(signature, data.to_json)
      Success(client: client, data: data)
    rescue Exceptions::Unauthorized,
           VerifyRequest::SignatureVerificationFailed => e
      Failure(Result.new(:unauthorized, e.message))
    end

    def check_client_verification(client:, data:)
      raise Exceptions::Unauthorized unless client.is_verified

      Success(client: client, data: data)
    rescue Exceptions::Unauthorized => e
      Failure(Result.new(:unauthorized, e.message))
    end

    def check_valid_username(client:, data:)
      data[:username] = AccountPolicy.transform_username(data[:username])
      raise Exceptions::InvalidValue unless AccountPolicy.valid_username?(data[:username])

      Success(client: client, data: data)
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Username is invalid'))
    end

    def check_username_exists(client:, data:)
      username = Repository::Usernames.find_username(data[:username])
      raise Exceptions::Duplication.new('Username already exists.') if username

      # TODO: move to policy with list of reserved words
      if [Horizon::Api.fund[:username], 'oblip'].include? data[:username]
        raise Exceptions::Duplication.new('Username already exists.')
      end

      Success(client: client, data: data)
    rescue Exceptions::Duplication => e
      Failure(Result.new(:bad_request, e.message))
    end

    def format_phone_number(client:, data:)
      phone_number = Phonelib.parse(data[:phone_number])
      data[:phone_number] = phone_number.to_s

      Success(client: client, data: data)
    end

    def check_account_existence(client:, data:)
      account = Repository::Accounts.find_phone_number(data[:phone_number])
      raise Exceptions::Duplication, 'Account already exists.' if account

      Success(client: client, data: data)
    rescue Exceptions::Duplication => e
      Failure(Result.new(:bad_request, e.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def create_account(client:, data:)
      entity = Entity::Account.build_hash(data)
      account = Repository::Accounts.create(entity)

      Success(account: account, client: client)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def save_client_account_reference(account:, client:)
      new_client = Repository::Clients.save_account(client.id, account.id)
      Success(account: account, client: new_client)
    end

    def save_secret(account:, client:)
      secret = SecureDB.generate_key
      client = Repository::Clients.save_secret(client.id, secret)
      Success(account: account, client: client, secret: secret)
    end

    def generate_tokens(account:, client:, secret:)
      refresh_token = Entity::Token.create_refresh_token(client.id, secret)
      access_token = Entity::Token.create_access_token(account, client)
      tokens = OpenStruct.new(refresh: refresh_token, access: access_token)
      Success(Result.new(:created, AuthAccount.new(account, tokens)))
    end
  end
end
