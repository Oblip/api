# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find an account's businesses in the database
  # Usage:
  #   result = FindAccountBusinesses.new.call(request_identity:)
  #   result.success?
  class FindAccountBusinesses
    include Dry::Transaction

    step :verify_auth
    step :find_businesses

    def verify_auth(request_identity:)
      raise Exceptions::Unauthorized if request_identity.code != 200
      raise Exceptions::Unauthorized if request_identity.data.type == 'admin'

      Success(request_identity.data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    # TODO: refactor for rubocop
    def find_businesses(auth_account)

      results = case auth_account.type
                when 'personal'
                  Repository::Businesses
                .find_by_account(auth_account.account_id)
                when 'business'
                  business = Repository::Businesses.find_id(auth_account.account_id)
                  Repository::Businesses
                    .find_by_account(business.account_id)

                  # businesses.select do |item|
                  #   item.id != auth_account.account_id
                  # end
                else
                  Repository::Businesses
                    .find_by_account(auth_account.account_id)
                end

      Success(Result.new(:ok, Businesses.new(results)))
    rescue Exceptions::MissingValue
      Failure(Result.new(:bad_request, 'Missing required parameter(s)'))
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid parameter(s) value'))
    end
  end
end
