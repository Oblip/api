# frozen_string_literal: true

require 'dry-monads'
require 'dry/transaction'
require 'fcm'
require_relative '../../lib/helpers.rb'

module Oblip
  # Service to create a transaction and save the the db
  # Usage:
  #   result = CreateTransaction.new.call(request_identity:, data:)
  #   result.success?
  class CreateTransaction
    include Dry::Transaction

    # step :verify_auth
    step :resolve_message
    step :identify_sender
    step :verify_action
    step :collect_info
    step :create_transaction
    step :save_metadata
    step :save_commissions
    step :process_notification

    # def verify_auth(request_identity:, data:)
    #   raise Exceptions::Unauthorized unless request_identity.code == 200

    #   Success(auth_account: request_identity.data, data: data)
    # rescue Exceptions::Unauthorized
    #   Failure(Result.new(:unauthorized, request_identity.message))
    # rescue StandardError
    #   Failure(Result.new(:internal_error, 'Something went wrong.'))
    # end

    def resolve_message(payload)

      # TODO: update the services without OpenStruct
      auth_account = OpenStruct.new(payload['auth_account'])
      data = payload['data']

      Success(auth_account: auth_account, data: data)

    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def identify_sender(auth_account:, data:)
      @is_admin = false
      account = nil
      @is_admin = auth_account.type == 'admin'
      if(!@is_admin)
        case auth_account.type
        when 'personal'
          account = Repository::Accounts.find_id(auth_account.account_id)
        when 'business'
          account = Repository::Businesses.find_id(auth_account.account_id)
        end
      end

      Success(sender: account, data: data)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def verify_action(input)
      types = Entity::Transaction::Type
      types = types.keys.map(&:to_s)
      type = input[:data]['type']

      raise Exceptions::InvalidValue unless types.include?(type)

      admin_allowed_types = types.select { |i| i[/bank_*/] }

      # TODO: check if not partner cannot use type partner_*

      # TODO: refactor this
      if !@is_admin
        raise Exceptions::Unauthorized if admin_allowed_types.include?(type)
      else
        raise Exceptions::Unauthorized if !admin_allowed_types.include?(type)
      end

      Success(input)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:forbidden,
        'You don\'t have the right to take this action'))
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid value'))
    end

    def collect_info(input)
      data = input[:data]
      envelope = data['envelope']
      unwraped_envl = Horizon::Api.unwrap_xdr_envelope(envelope)

      raise Exceptions::InvalidValue unless unwraped_envl

      payment_op = unwraped_envl.tx.operations[0].body.value
      asset_data = payment_op.asset.value

      to_address = unwraped_envl.tx.operations[0].body.value.destination.value
      to_address = Stellar::Util::StrKey.check_encode(:account_id, to_address)

      # from_address = unwraped_envl.tx.operations[0].source_account.value
      # from_address = Stellar::Util::StrKey.check_encode(:account_id, from_address)

      receiver = Repository::Usernames.find_checking_account(to_address)
      raise Exceptions::NoResource unless receiver

      # raise Exception::NoResource unless from_address == sender_checking_id

      signed_envelope = Horizon::Api.funder_sign_envelope(envelope)
      asset_code = asset_data.asset_code.delete("\x00")

      tx_amount = (payment_op.amount * 0.0000001).round(2)

      # TODO: refactor
      if data['type'] == Entity::Transaction::Type.partner_topup
        raise Entity::AgentCommission::Exception::NotAvailable unless tx_amount < 1389.16 # highest value for commission
      end

      tx_data = {
        message: data['message'],
        type: data['type'],
        amount: tx_amount,
        asset: asset_code,
        from: @is_admin ? Horizon::Api.fund[:username] : input[:sender].username,
        to: receiver.username,
        signed_envelope: signed_envelope
      }

      Success(tx_data)

    rescue Entity::AgentCommission::Exception::NotAvailable
      Failure(Result.new(:bad_request, "You're not allowed to fund more than $1,389 BZD"))
    rescue Exceptions::InvalidValue => error
      puts error.message
      Failure(Result.new(:bad_request, ''))
    rescue Exceptions::NoResource => error
      puts error.message
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:bad_request, 'Invalid value'))
    rescue StandardError => error
      puts error.message
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def create_transaction(tx_payload)
      tx_hash = Horizon::Api.create_transaction(tx_payload[:signed_envelope])
      # tx_result = Horizon::Api::load_single_transaction(tx_hash)

      entity = Entity::Transaction.new(
        id: nil,
        tx_hash: tx_hash,
        paging_token: nil,
        network_fee: nil,
        message: tx_payload[:message],
        type: tx_payload[:type],
        asset: tx_payload[:asset],
        amount: tx_payload[:amount].to_s,
        from_internal: tx_payload[:from],
        to_internal: tx_payload[:to]
      )

      Success(entity)
    rescue Horizon::Api::Errors::InvalidEnvelope => error
      puts error.message
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:bad_request, 'Unable to process transaction.'))
    rescue Horizon::Api::Errors::TransactionError => error
      puts error.message
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:bad_request, 'Unable to process transaction.'))
    rescue Horizon::Api::Errors::FetchValueError => error
      puts error.message
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:bad_request, 'Unable to process transaction.'))
    rescue StandardError => error
      puts error.message
      # puts error.inspect
      # puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def save_metadata(entity)
      result = Repository::Transactions.create(entity)

      Success(result)
    rescue StandardError => error
      puts error.message
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def save_commissions(tx_result)
      if tx_result.type == Entity::Transaction::Type.partner_topup
        create_commission(tx_result)
      end
      Success(tx_result)
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def process_notification(result)
      send_notification(result)

      Success(Result.new(:created, result))
    rescue StandardError => error
      puts error.message
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    private

    def create_commission(tx_result)
      commission = Entity::AgentCommission
        .calculate_commission(tx_result.amount.to_f)
      available_payout = Repository::Payouts
        .find_available_by_username(tx_result.from_internal)

      if !available_payout
        # Create a new payout
        available_payout = Repository::Payouts.create(Entity::Payout.new(
          id: nil,
          amount: "0",
          payout_ref: generate_ref,
          username: tx_result.from_internal,
          is_paid: false,
          paid_date: nil
        ))
      end

      # Create commission record
      com_entity = Repository::AgentCommissions.create(Entity::AgentCommission.new(
        id: nil,
        transaction_id: tx_result.id,
        username: tx_result.from_internal,
        amount: commission.to_s,
        asset: 'BZD',
        payout_id: available_payout.id
      ))

      # Update payout
      Repository::Payouts.update_amount(available_payout.id, commission) 
      com_entity
    end


    def send_notification(entity)
      if ENV['RACK_ENV'] != 'test'
        sender = get_identity_by_username(entity.from_internal)

        fcm = FCM.new(Api.config['FCM_SERVER_KEY'])

        reciever = Repository::Usernames.find_username(entity.to_internal)
        account_id = get_account_id(reciever)
        clients = Repository::Clients.find_many_by_account(account_id)

        fcm_tokens = clients.map { |client| client.fcm_token }

        content = notification_content(sender.name, entity.amount,
                                       entity.asset)

        options = Entity::DeviceNotification.new(
          title: content[:title],
          message: content[:message],
          type: Entity::DeviceNotification::Types::TRANSACTION,
          data: JSON.parse(TransactionRepresenter.new(entity).to_json)
        )

        fcm.send(fcm_tokens, options.to_hash)
        fcm = nil
      end
    end

    def get_identity_by_username(username)
      if @is_admin
        sender = Entity::Username.build_fund_identity()
      else
        sender = Repository::Usernames.find_username(username)
      end
    end

    def notification_content(sender_name, amount, asset)
        notif_title = 'Transaction'
        notif_message = "#{sender_name} sent you "
        notif_message += "#{amount} "
        notif_message += "#{asset}"

        { title: notif_title, message: notif_message }
    end

    def get_account_id(identity)
      account_id = nil
      case identity.type
      when 'personal'
        account_id = identity.reference_id
      when 'business'
        business = Repository::Businesses.find_id(identity.reference_id)
        account_id = business.account_id
      end
      account_id
    end
  end
end
