# frozen_string_literal: true

require 'dry-monads'
require 'ostruct'

module Oblip
  # Service to publish a job to the transaction worker
  # Usage:
  #   result = PublishTransaction.new.call(request_identity:, data:)
  #   result.success?
  class PublishTransaction
    include Dry::Transaction

    step :verify_auth
    step :check_identitfier
    step :publish_job

    def verify_auth(request_identity:, data:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      Success(auth_account: request_identity.data, data: data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def check_identitfier(auth_account:, data:)
      raise Exceptions::MissingValue unless 
        data['identifier'] && data['identifier'].size > 6

      Success(auth_account: auth_account, data: data)
    rescue Exceptions::MissingValue
      Failure(Result.new(:bad_request, 'Missing an identifier'))
    end

    def publish_job(auth_account:, data:)
      payload = {
        auth_account: {
          account_id: auth_account.account_id,
          type: auth_account.type
        },
        data: data
      }

      TransactionWorker.perform_async(payload.to_json)

      Success(Result.new(:processing, 'We are processing the transaction'))
    end
  end
end
