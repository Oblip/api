# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to create a bank account request in the database
  # Usage:
  #   result = CreateBankAccountRequest.new.call(request_identity:, data:)
  #   result.success?
  class CreateBankAccountRequest
    include Dry::Transaction

    step :verify_auth
    step :verify_ownership
    step :create_entity
    step :create_request

    def verify_auth(request_identity:, data:)
      if request_identity.code != 200 ||
         request_identity.data['type'] == 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      Success(auth_account: request_identity.data, data: data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def verify_ownership(auth_account:, data:)
      bank_account = Repository::BankAccounts.find_id(data['bank_account_id'])
      is_owner = bank_account.username == auth_account.username

      raise Exceptions::Unauthorized unless is_owner

      # append registered bank id to data directory
      data['registered_bank_id'] = bank_account.registered_bank.id

      Success(data)
    rescue Exceptions::Unauthorized
      msg = 'You don\'t have the rights to take this action.'
      Failure(Result.new(:forbidden, msg))
    end

    def create_entity(data)
      valid_type = Entity::BankAccountRequest::Type.key?(data['type'].to_sym)
      raise Exceptions::InvalidValue unless valid_type

      check_required_data(data)

      entity = Entity::BankAccountRequest.build_from_hash(data)

      Success(entity)
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid data passed'))
    rescue Exceptions::MissingValue
      Failure(Result.new(:bad_request, 'Missing data'))
    end

    def create_request(entity)
      request = Repository::BankAccountRequests.create(entity)

      Success(Result.new(:created, request))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    private

    def check_required_data(data)
      raise Exceptions::MissingValue unless data['amount']
      raise Exceptions::MissingValue unless data['asset']

      assets = Entity::Asset::AvailableAssets.values

      raise Exceptions::InvalidValue unless assets.include?(data['asset'])

    end
  end
end
