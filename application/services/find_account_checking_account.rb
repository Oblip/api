# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a checking account of a logged in user
  # Usage:
  #   result = FindAccountCheckingAccount.new.call(request_identity:)
  #   result.success?
  class FindAccountCheckingAccount
    include Dry::Transaction

    step :verify_auth
    step :find_checking_account

    def verify_auth(request_identity:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success(auth_account: request_identity.data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def find_checking_account(auth_account:)
      account = nil
      case auth_account.type
      when 'personal'
        account = Repository::Accounts.find_id(auth_account.account_id)
      when 'business'
        account = Repository::Businesses.find_id(auth_account.account_id)
      else
        account = Repository::Accounts.find_id(auth_account.account_id)
      end

      raise Exceptions::NoResource unless account.checking_account_id

      horizon_account = Horizon::Account
                        .new(account.checking_account_id)

      checking_account = horizon_account.load

      Success(Result.new(:ok, checking_account))

    rescue Exceptions::NoResource
      Failure(Result.new(:bad_request, 'Checking account does not exist.'))
    end
  end
end
