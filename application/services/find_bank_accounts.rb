# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all bank accounts in the database
  # Usage:
  #   result = FindBankAccounts.new.call(request_identity:, params:)
  #   result.success?
  class FindBankAccounts
    include Dry::Transaction

    step :verify_auth
    step :find_bank_accounts

    def verify_auth(request_identity:, params:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      params = JsonRequestBody.symbolize(params)

      Success(auth_account: request_identity.data, params: params)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    # rubocop:disable Metrics/MethodLength
    def find_bank_accounts(auth_account:, params:)
      results = nil
      if auth_account&.type == 'admin' # TODO: set access type in domain
        results = Repository::BankAccounts
                  .find_all_bank_accounts(nil, params[:offst], params[:limit])
      else
        results = Repository::BankAccounts
                  .find_all_bank_accounts(auth_account.username, params[:offst],
                                          params[:limit])
      end

      bank_accounts = OpenStruct.new(bank_accounts: results)
      Success(Result.new(:ok, bank_accounts))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
    # rubocop:enable Metrics/MethodLength
  end
end
