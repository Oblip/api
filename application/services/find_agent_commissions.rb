# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find agent commissions of a logged in user or admin
  # Usage:
  #   result = FindAgentCommissions.new.call(request_identity:, params:)
  #   result.success?
  class FindAgentCommissions
    include Dry::Transaction

    step :verify_auth
    step :find_agent_commissions

    def verify_auth(request_identity:, params:)
      raise Exceptions::Unauthorized if request_identity.code != 200
      raise Exceptions::Unauthorized unless request_identity.data.is_agent || request_identity.data.type === 'admin'

      params = JsonRequestBody.symbolize(params)

      Success(auth_account: request_identity.data, params: params)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end


    def find_agent_commissions(auth_account:, params:)
      if auth_account&.type == 'admin' # TODO: set access type in domain
        results = Repository::AgentCommissions
                  .find_all(nil, params[:offst], params[:limit])
      else
        results = Repository::AgentCommissions
                  .find_all(auth_account.username, params[:offst],
                                          params[:limit])
      end

      commissions = OpenStruct.new(agent_commissions: results)
      Success(Result.new(:ok, commissions))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
