# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find payouts of a logged in partner or admin
  # Usage:
  #   result = FindPayouts.new.call(request_identity:, params:)
  #   result.success?
  class FindPayouts
    include Dry::Transaction

    step :verify_auth
    step :find_agent_commissions

    def verify_auth(request_identity:, params:)
      raise Exceptions::Unauthorized if request_identity.code != 200
      raise Exceptions::Unauthorized unless request_identity.data.is_agent || request_identity.data.type === 'admin'

      params = JsonRequestBody.symbolize(params)

      Success(auth_account: request_identity.data, params: params)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end


    def find_agent_commissions(auth_account:, params:)
      if auth_account&.type == 'admin' # TODO: set access type in domain
        results = Repository::Payouts
                  .find_all(nil, params[:offst], params[:limit])
      else
        results = Repository::Payouts
                  .find_all(auth_account.username, params[:offst],
                                          params[:limit])
      end

      payouts = OpenStruct.new(payouts: results)
      Success(Result.new(:ok, payouts))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
