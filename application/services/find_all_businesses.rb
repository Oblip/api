# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all businesses in the database
  # Usage:
  #   result = FindAllBusinesses.new.call(request_identity:, params:)
  #   result.success?
  class FindAllBusinesses
    include Dry::Transaction

    step :verify_auth
    step :find_businesses

    def verify_auth(request_identity:, params:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      params = JsonRequestBody.symbolize(params)

      Success(params)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    # TODO: refactor for rubocop
    # rubocop:disable Metrics/MethodLength
    def find_businesses(params)
      results = nil
      if params[:nearby]
        check_required_geolocate_params(params)
        results = Repository::Businesses.find_all_nearby(params: params)
      else
        results = Repository::Businesses
                  .find_all_businesses(params[:is_agent], params[:offset],
                                       params[:limit])
      end

      Success(Result.new(:ok, Businesses.new(results)))
    rescue Exceptions::MissingValue
      Failure(Result.new(:bad_request, 'Missing required parameter(s)'))
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'Invalid parameter(s) value'))
    end
    # rubocop:enable Metrics/MethodLength

    private

    def check_required_geolocate_params(params)
      raise Exceptions::MissingValue unless params.key?(:lat)
      raise Exceptions::MissingValue unless params.key?(:lng)

      raise Exceptions::InvalidValue unless params[:lat].class != Float
      raise Exceptions::InvalidValue unless params[:lng].class != Float
    end
  end
end
