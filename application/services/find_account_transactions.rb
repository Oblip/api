# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find account transactions in the database
  # Usage:
  #   result = FindAccountTransactions.new.call(request_identity: params:)
  #   result.success?
  class FindAccountTransactions
    include Dry::Transaction

    step :verify_auth
    step :find_transactions
    
    def verify_auth(request_identity:, params:)
      if request_identity.code != 200 ||
         request_identity.data.type == 'admin' # TODO: change to enum
        raise Exceptions::Unauthorized
      end

      params = JsonRequestBody.symbolize(params)

      Success(username: request_identity.data['username'], params: params)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError  => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def find_transactions(username:, params:)
      results = Repository::Transactions.find_from_account(
        username, params[:offset], params[:limit]
      )

      Success(Result.new(:ok, Transactions.new(results)))
    rescue StandardError  => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
