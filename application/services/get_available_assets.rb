# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all available in the database
  # Usage:
  #   result = GetAvailableAssets.new.call(request_identity:)
  #   result.success?
  class GetAvailableAssets
    include Dry::Transaction

    step :verify_auth
    step :get_assets

    def verify_auth(request_identity:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success()
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError => error
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def get_assets
      assets = Entity::Asset::AvailableAssets.values.map do |asset|
        Entity::Asset.new(
          asset_code: asset,
          asset_type: nil,
          asset_issuer: Horizon::Api.anchor[:account].address,
          is_authorized: nil
        )
      end

      Success(Result.new(:ok, Assets.new(assets)))

    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end