# frozen_string_literal: true

require 'dry-monads'
require 'bip_mnemonic'

module Oblip
  # Service to create an checking auth_account in the blockchain
  # Usage:
  #   result = CreateCheckingAccount.new.call(request_identity:)
  #   result.success?
  class CreateCheckingAccount
    include Dry::Transaction

    step :verify_auth
    step :check_duplicates
    step :generate_secret
    step :generate_seed
    step :create_checking_account
    step :set_asset_permissions
    step :config_checking_account
    step :encrypt_secrets
    step :save_creds
    step :save_checking_account_id
    step :generate_cipher
    step :output_result

    def verify_auth(request_identity:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success(auth_account: request_identity.data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def check_duplicates(auth_account:)
      has_duplicate = nil

      case auth_account.type
      when 'personal'
        has_duplicate = personal_duplicate?(auth_account.account_id)
      when 'business'
        has_duplicate = business_duplicate?(auth_account.account_id)
      else
        has_duplicate = personal_duplicate?(auth_account.account_id)
      end

      msg = 'A checking account already exists.'

      raise Exceptions::Duplication msg if has_duplicate

      Success(auth_account: auth_account)
    rescue Exceptions::Duplication => error
      puts error.message
      Failure(Result.new(:bad_request, error.message))
    end

    def generate_secret(auth_account:)
      mnemonic = BipMnemonic.to_mnemonic(bits: 256)
      salt = SecureDB.new_salt
      secret = mnemonic + '|' + salt
      Success(auth_account: auth_account, secret: secret)
    end

    def generate_seed(auth_account:, secret:)
      mnemonic, salt = secret.split('|')
      seed = SecureDB.hash_password32(salt, mnemonic)

      Success(auth_account: auth_account, secret: secret, seed: seed)
    end

    def create_checking_account(auth_account:, secret:, seed:)
      master = Horizon::Api.keypair_from_raw(seed)
      starting_balance = Entity::CheckingAccount.starting_balance

      Horizon::Api.create_account(master.address, starting_balance)

      Success(auth_account: auth_account, secret: secret, master: master)
    end

    def set_asset_permissions(input)
      assets = Entity::Asset::AvailableAssets

      assets.values.each do |asset|
        Horizon::Api::trust_issuer(input[:master].seed, asset)
        Horizon::Api::allow_trust(input[:master].address, asset)
      end

      Success(input)
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def config_checking_account(auth_account:, secret:, master:)
      thres = { master: 10, high: 10, med: 5, low: 3 }
      Horizon::Api.change_thresholds(master.seed,
                                     master: thres[:master], high: thres[:high],
                                     med: thres[:med], low: thres[:low])

      signer = Horizon::Api.add_signer(master.seed, thres[:med])

      Success(auth_account: auth_account, secret: secret, master: master,
              signer: signer)
    end

    def encrypt_secrets(auth_account:, secret:, master:, signer:)
      lock_public = Api.config['MASTER_CHECKING_PUBLIC_KEY']

      new_keys = PublicEncryption.generate_keypair
      cipher = PublicEncryption.simple_encrypt(secret, lock_public,
                                        new_keys[:private_key])

      creds = Entity::CheckingAccountCred.new(
        id: nil, checking_account_id: master.address,
        secret: cipher, wrapper_key: new_keys[:public_key]
      )

      Success(auth_account: auth_account, signer: signer, creds: creds)
    end

    def save_creds(auth_account:, signer:, creds:)
      creds = Repository::CheckingAccountCreds.create(creds)

      Success(auth_account: auth_account, signer: signer, creds: creds)
    end

    def save_checking_account_id(auth_account:, signer:, creds:)

      account = case auth_account.type
                when 'personal'
                  account = save_personal_checking_account(
                    auth_account.account_id, creds.checking_account_id
                  )
                when 'business'
                  account = save_business_checking_account(
                    auth_account.account_id, creds.checking_account_id
                  )
                else
                  account = save_personal_checking_account(
                    auth_account.account_id, creds.checking_account_id)
                end
      
      Success(client_id: auth_account.client_id, signer: signer,
              account: account)
      
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end

    def generate_cipher(client_id:, signer:, account:)
      sender = PublicEncryption.generate_keypair
      client = Repository::Clients.find_id(client_id)

      cipher_data = PublicEncryption.encrypt(signer.seed, client.wrapper_key,
                                             sender[:private_key])
      cipher = cipher_data[:encrypted_message] + '.' + cipher_data[:nonce]

      Success(account: account, cipher: cipher, sender: sender)

    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def output_result(account:, cipher:, sender:)
      Success(Result.new(:created, AccountSigner.new(account, cipher,
                         sender[:public_key])))
    end

    private

    def save_business_checking_account(business_id, checking_account_id)
      business = Repository::Businesses
                 .set_checking_account(business_id,
                                       checking_account_id)
      business
    end

    def save_personal_checking_account(account_id, checking_account_id)
      account = Repository::Accounts
                .set_checking_account(account_id,
                                      checking_account_id)
      account
    end

    def personal_duplicate?(account_id)
      account = Repository::Accounts.find_id(account_id)
      !account.checking_account_id.nil?
    end

    def business_duplicate?(business_id)
      business = Repository::Businesses.find_id(business_id)
      !business.checking_account_id.nil?
    end
  end
end
