# frozen_string_literal: true

module Oblip
  # Service to add a client to our database
  # Usage:
  #   result = CreateClient.call(verifier_key:, wrapper_key:, fcm_token:)
  #   result.success?
  module CreateClient
    extend Dry::Monads::Result::Mixin

    def self.call(verifier_key:, wrapper_key:, fcm_token:)
      entity = Entity::Client.prebuild(wrapper_key: wrapper_key,
                                       fcm_token: fcm_token,
                                       verifier_key: verifier_key)
      client = Repository::Clients.create(entity)

      Success(Result.new(:created, client))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong. Try again.'))
    end
  end
end
