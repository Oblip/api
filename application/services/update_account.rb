# frozen_string_literal: true

require 'dry-monads'
require 'phonelib'

module Oblip
  # Service to update an account in the database
  # Usage:
  #   result = UpdateAccount.new.call(auth_account:, data:)
  #   result.success?
  class UpdateAccount
    include Dry::Transaction

    step :verify_auth
    step :create_entity
    step :update

    def verify_auth(request_identity:, data:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success(auth_account: request_identity.data, data: data)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    # rubocop:disable Metrics/MethodLength
    # rubocop:disable Metrics/AbcSize
    def create_entity(auth_account:, data:)
      name = Entity::Account::Name.new(
        first_name: data['first_name'],
        middle_name: data['middle_name'],
        last_name: data['last_name']
      )

      address = Entity::Account::Address.new(
        street_name: data['street_name'],
        city: data['city'],
        zip_code: data['zip_code'],
        state: data['state'],
        country: Entity::Country.build_id(data['country_id'])
      )

      profile = Entity::Account::Profile.new(
        name: name,
        address: address,
        dob: data['dob'],
        photo_url: nil
      )

      account = Entity::Account.new(
        id: auth_account.account_id,
        username: nil,
        phone_number: nil,
        email: data['email'],
        profile: profile,
        checking_account_id: nil,
        secret_code: nil
      )

      Success(account)
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Metrics/AbcSize

    def update(account_entity)
      account = Repository::Accounts.update(account_entity)
      Success(Result.new(:ok, account))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace

      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
