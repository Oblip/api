# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find a business in the database
  # Usage:
  #   result = FindBusiness.new.call(request_identity:, id:)
  #   result.success?
  class FindBusiness
    include Dry::Transaction

    step :verify_auth
    step :find_business

    def verify_auth(request_identity:, id:)
      raise Exceptions::Unauthorized if request_identity.code != 200

      Success(id)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    def find_business(id)
      business = Repository::Businesses.find_id(id)
      raise Exceptions::NoResource unless business

      Success(Result.new(:ok, business))

    rescue Exceptions::NoResource
      Failure(Result.new(:not_found, 'Resource does not exist'))
    end
  end
end