# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to get a list of federated accounts in the database
  # Usage:
  #   result = FindFederatedAccounts.new.call(request_identity:, ck_account_list:)
  #   result.success?
  class FindFederatedAccounts
    include Dry::Transaction

    step :verify_auth
    step :find_accounts

    def verify_auth(request_identity:, ck_account_list:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      Success(ck_account_list)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end

    def find_accounts(list)
      raise Exceptions::InvalidValue unless list.size <= 15

      fund = Horizon::Api.fund
      accounts = Repository::Usernames.find_all_checking_account(list)
      if list.include?(fund[:account].address)
        entity = Entity::Username.build_fund_identity()
        accounts.push(entity)
      end
      Success(Result.new(:ok, Accounts.new(accounts)))
    rescue Exceptions::InvalidValue
      Failure(Result.new(:bad_request, 'List supplied exceeded the limit'))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
