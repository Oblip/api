# frozen_string_literal: false

require 'dry-monads'
require 'dry/transaction'

require_relative 'admin/init.rb'

Dir.glob("#{File.dirname(__FILE__)}/*.rb").each do |file|
  require file
end
