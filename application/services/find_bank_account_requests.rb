# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to find all bank account requests in the database
  # Usage:
  #   result = FindBankAccountRequests.new.call(request_identity:, params:)
  #   result.success?
  class FindBankAccountRequests
    include Dry::Transaction

    step :verify_auth
    step :verify_ownership
    step :find_bank_account_requests

    def verify_auth(request_identity:, params:)
      raise Exceptions::Unauthorized unless request_identity.code == 200

      params = JsonRequestBody.symbolize(params)

      Success(auth_account: request_identity.data, params: params)
    rescue Exceptions::Unauthorized
      Failure(Result.new(:unauthorized, request_identity.message))
    end

    # TODO: move to policy
    def verify_ownership(auth_account:, params:)
      bank_account_id = params[:bank_account_id]
      is_owner = true
      if bank_account_id
        bank_account = Repository::BankAccounts.find_id(bank_account_id)
        is_owner = bank_account.username == auth_account.username
      end

      raise Exceptions::Unauthorized unless
        (!bank_account_id.nil? && is_owner) || auth_account.type == 'admin'

      Success(params)
    rescue Exceptions::Unauthorized
      msg = 'You don\'t have the rights to take this action.'
      Failure(Result.new(:forbidden, msg))
    end


    def find_bank_account_requests(params)
      results = Repository::BankAccountRequests.find_all_requests(
        params[:bank_account_id], params[:type], params[:status],
        params[:offset], params[:limit]
      )

      requests = OpenStruct.new(bank_account_requests: results)
      Success(Result.new(:ok, requests))
    rescue StandardError
      Failure(Result.new(:internal_error, 'Something went wrong'))
    end
  end
end
