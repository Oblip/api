# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to verify phone number from sms token
  # Usage:
  #   result = VerifyPhoneNumber.call(client_id: xx, data:, signature:)
  #   result.success?
  class VerifyPhoneNumber
    include Dry::Transaction

    step :check_client
    step :validate_request
    step :verify_code

    def check_client(client_id:, data:, signature:)
      client = Repository::Clients.find_id(client_id)
      raise Exceptions::Unauthorized unless client

      Success(client: client, data: data, signature: signature)
    rescue Exceptions::Unauthorized => e
      Failure(Result.new(:unauthorized, e.message))
    end

    def validate_request(client:, data:, signature:)
      request_verifier = VerifyRequest.new(client.verifier_key)
      data = request_verifier.parse(signature, data.to_json)
      Success(client: client, code: data[:code])
    rescue VerifyRequest::SignatureVerificationFailed => e
      puts e.inspect
      puts e.backtrace
      Failure(Result.new(:unauthorized, e.message))
    end

    def verify_code(client:, code:)
      payload = ShortCodeToken.payload(client.verification_token)
      is_correct_code = payload['code'] == code
      if not_dev? && is_correct_code
        code_verified(client)
      elsif not_dev? && !is_correct_code
        Failure(Result.new(:bad_request, 'Verification code is invalid'))
      else
        code_verified(client)
      end
    end

    private

    def not_dev?
      ENV['RACK_ENV'] == 'production' ||
        ENV['RACK_ENV'] == 'staging' ||
        ENV['RACK_ENV'] == 'test'
        
    end

    def code_verified(client)
      client = Repository::Clients
               .update_verification_flag(client.id, true)
      Success(Result.new(:ok, client))
    end
  end
end
