# frozen_string_literal: true

require 'dry-monads'

module Oblip
  # Service to check account availability in the database
  # Usage:
  #   result = AccountAvailability.new.call(username)
  #   result.success?
  class AccountAvailability
    include Dry::Transaction

    step :check_availability

    def check_availability(username)
      account = Repository::Usernames.find_username(username)
      raise Exceptions::NoResource unless account

      Success(Result.new(:ok, account))
    rescue Exceptions::NoResource
      Failure(Result.new(:not_found, 'Username does not exist'))
    rescue StandardError => error
      puts error.inspect
      puts error.backtrace
      Failure(Result.new(:internal_error, 'Something went wrong.'))
    end
  end
end
