# frozen_string_literal: true

module Oblip
  # API endpoints for businesses
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('available_assets') do |routing|
      routing.is do
        routing.get do
          results = GetAvailableAssets.new.call(
            request_identity: @request_identity
          )

          represent_response(results, AvailableAssetsRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
    # rubocop:enable Metrics/BlockLength
  end
end
