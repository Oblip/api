# frozen_string_literal: true

module Oblip
  # API endpoints for businesses
  class Api < Roda
    plugin :all_verbs

    route('account_availability') do |routing|
      # /v1/account_availability/:username
      routing.on String do |username|
        routing.is do
          routing.get do
            result = AccountAvailability.new.call(username)
            
            if ENV['RACK_ENV'] == 'development' || ENV['RACK_ENV'] == 'staging'
              response["Access-Control-Allow-Origin"] = "*"
            end

            represent_response(result, BasicAccountRepresenter)
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
  end
end
