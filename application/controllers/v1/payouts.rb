# frozen_string_literal: true

module Oblip
  # API endpoints for agent commissions
  class Api < Roda
    plugin :all_verbs

    route('payouts') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params

          results = FindPayouts.new.call(
            request_identity: @request_identity,
            params: parameters
          )

          represent_response(results, PayoutsRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
