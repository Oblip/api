# frozen_string_literal: true

module Oblip
  # API endpoints for authentication for admin accounts
  class Api < Roda
    plugin :all_verbs

    route('account_management', 'admin') do |routing|
      routing.is do
        routing.get do
          # TODO:
        end
      end

      routing.on String do |username|
        routing.is do
          routing.get do
            result = Admin::FindAccount.new.call(
              request_identity: @request_identity,
              username: username
            )
            entity = result.value_or(nil)&.message
            case entity&.type
            when 'personal'
              represent_response(result, AccountRepresenter)
            when 'business'
              represent_response(result, BusinessRepresenter)
            else
              represent_response(result, AccountRepresenter)
            end
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
  end
end
