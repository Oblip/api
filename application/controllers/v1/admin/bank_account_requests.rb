# frozen_string_literal: true

module Oblip
  # API endpoints for authentication for admin accounts
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('bank_account_requests', 'admin') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params

          results = FindBankAccountRequests.new.call(
            request_identity: @request_identity,
            params: parameters
          )

          represent_response(results, BankAccountRequestsRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
      # /v1/admin/bank_account_requests/:request_id
      routing.on String do |request_id|

        routing.is do
          routing.get do
            # TODO: 
          end
        end

        # /v1/admin/bank_account_requests/:request_id/status
        routing.on('status') do
          routing.is do
            routing.put do
              body = JSON.parse(routing.body.read)
              body['request_id'] = request_id

              result = UpdateRequestStatus.new.call(
                request_identity: @request_identity,
                data: body
              )

              represent_response(result, BankAccountRequestRepresenter)
            rescue StandardError => error
              puts error.message
              puts error.inspect
              puts error.backtrace
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something went wrong.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end
        end
      end
    end
  end
end
