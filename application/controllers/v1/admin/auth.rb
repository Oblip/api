# frozen_string_literal: true

module Oblip
  # API endpoints for authentication for admin accounts
  class Api < Roda
    plugin :all_verbs

    route('auth', 'admin') do |routing|
      routing.is do
        routing.post do
          data = JSON.parse(routing.body.read)

          result = Admin::AuthenticateAdmin.new.call(
            email_address: data['email_address'],
            password: data['password']
          )

          represent_response(result, Admin::AdminAuthAccountRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end

