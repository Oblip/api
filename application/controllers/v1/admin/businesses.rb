# frozen_string_literal: true

module Oblip
  # API endpoints for businesses
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('businesses', 'admin') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params

          results = FindAllBusinesses.new.call(
            request_identity: @request_identity,
            params: parameters
          )

          represent_response(results, BusinessesRepresenter)

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          body = JSON.parse(routing.body.read)

          result = CreateBusiness.new.call(
            request_identity: @request_identity,
            data: body
          )

          represent_response(result, BusinessRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      routing.on String do |id|
        routing.is do
          routing.get do

            result = FindBusiness.new.call(
              request_identity: @request_identity,
              id: id
            )

            represent_response(result, BusinessRepresenter)
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end

          routing.put do
            body = JSON.parse(routing.body.read)
            body['id'] = id
            result = UpdateBusiness.new.call(
              request_identity: @request_identity,
              data: body
            )

            represent_response(result, BusinessRepresenter)
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
  end
end
