# frozen_string_literal: true

module Oblip
  # Routes for granting access tokens
  class Api < Roda
    plugin :all_verbs
    route('token') do |routing|
      routing.is do
        routing.post do
          req_body = JSON.parse(routing.body.read)
          signature = req_body['signature']
          data = req_body['data']
          # ip_address = routing.env['REMOTE_ADDR']

          result = RefreshAccessToken.new.call(
            signature: signature,
            data: data
          )

          represent_response(result, TokenRepresenter)

        rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something does not seem right.')
            )
            response.status = http_response.http_code
            http_response.to_json
        end
      end
    end
  end
end
