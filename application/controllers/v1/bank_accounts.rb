# frozen_string_literal: true

module Oblip
  # API endpoints for businesses
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('bank_accounts') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params

          results = FindBankAccounts.new.call(
            request_identity: @request_identity,
            params: parameters
          )

          represent_response(results, BankAccountsRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          body = JSON.parse(routing.body.read)
          result = CreateBankAccount.new.call(
            request_identity: @request_identity,
            data: body
          )

          represent_response(result, BankAccountRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      # v1/bank_accounts/:id
      routing.on String do |id|
        routing.is do
          routing.get do
            result = FindBankAccount.new.call(
              request_identity: @request_identity,
              id: id
            )

            represent_response(result, BankAccountRepresenter)
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end

        # v1/bank_accounts/:id/requests
        routing.on('requests') do
          routing.is do
            routing.get do
              parameters = routing.params
              parameters['bank_account_id'] = id

              results = FindBankAccountRequests.new.call(
                request_identity: @request_identity,
                params: parameters
              )

              represent_response(results, BankAccountRequestsRepresenter)
            rescue StandardError
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something went wrong.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end

          # /v1/bank_accounts/:id/requests/:request_id
          routing.on String do |request_id|
            routing.is do
              routing.get do
                result = FindBankAccountRequest.new.call(
                  request_identity: @request_identity,
                  bank_account_id: id,
                  request_id: request_id
                )

                represent_response(result, BankAccountRequestRepresenter)
              rescue StandardError => error
                puts error.inspect
                puts error.backtrace
                http_response = HttpResponseRepresenter.new(
                  Result.new(:internal_error, 'Something went wrong.')
                )
                response.status = http_response.http_code
                http_response.to_json
              end
            end

            # /v1/bank_accounts/:id/requests/:request_id/status
            routing.on('status') do
              routing.is do
                routing.put do
                  body = JSON.parse(routing.body.read)
                  body['request_id'] = request_id

                  result = UpdateRequestStatus.new.call(
                    request_identity: @request_identity,
                    data: body
                  )

                  represent_response(result, BankAccountRequestRepresenter)
                rescue StandardError => error
                  puts error.inspect
                  puts error.backtrace
                  http_response = HttpResponseRepresenter.new(
                    Result.new(:internal_error, 'Something went wrong.')
                  )
                  response.status = http_response.http_code
                  http_response.to_json
                end
              end
            end
          end
        end

        # /v1/bank_accounts/:id/status
        routing.on('status') do
          routing.is do
            routing.put do
              body = JSON.parse(routing.body.read)
              body['bank_account_id'] = id

              result = UpdateBankAccountStatus.new.call(
                request_identity: @request_identity,
                data: body
              )

              represent_response(result, BankAccountRepresenter)
            rescue StandardError
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something went wrong.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end
        end

        # /v1/bank_accounts/:id/deposits
        routing.on('deposits') do
          routing.is do
            routing.get do
              parameters = routing.params
              parameters['type'] = 'deposit'
              parameters['bank_account_id'] = id

              results = FindBankAccountRequests.new.call(
                request_identity: @request_identity,
                params: parameters
              )

              represent_response(results, BankAccountRequestsRepresenter)
            rescue StandardError
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something went wrong.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end

            routing.post do
              body = JSON.parse(routing.body.read)
              body['bank_account_id'] = id
              body['type'] = 'deposit'

              result = CreateBankAccountRequest.new.call(
                request_identity: @request_identity,
                data: body
              )

              represent_response(result, BankAccountRequestRepresenter)
            rescue StandardError
              http_response = HttpResponseRepresenter.new(
                Result.new(:internal_error, 'Something went wrong.')
              )
              response.status = http_response.http_code
              http_response.to_json
            end
          end

          # /v1/bank_accounts/:id/deposits/:id
          routing.on String do |request_id|
            # /v1/bank_accounts/:id/deposits/:id/transaction
            routing.on('transaction') do
              routing.is do
                routing.get do
                  # TODO  
                end

                routing.put do
                  body = JSON.parse(routing.body.read)

                  result = UpdateRequestTransactionId.new.call(
                    request_identity: @request_identity,
                    req_id: request_id,
                    tx_id: body['transaction_id']
                  )

                  represent_response(result, BankAccountRequestRepresenter)
                rescue StandardError => error
                  http_response = HttpResponseRepresenter.new(
                    Result.new(:internal_error, 'Something went wrong.')
                  )
                  response.status = http_response.http_code
                  http_response.to_json
                end
              end
            end

            # /v1/bank_accounts/:id/deposits/:id/status
            routing.on('status') do
              routing.put do
                body = JSON.parse(routing.body.read)
                body['request_id'] = request_id

                result = UpdateRequestStatus.new.call(
                  request_identity: @request_identity,
                  data: body
                )

                represent_response(result, BankAccountRequestRepresenter)
              rescue StandardError
                http_response = HttpResponseRepresenter.new(
                  Result.new(:internal_error, 'Something went wrong.')
                )
                response.status = http_response.http_code
                http_response.to_json
              end
            end
          end
        end
      end
    end
    # rubocop:enable Metrics/BlockLength
  end
end
