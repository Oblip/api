# frozen_string_literal: true

module Oblip
  # API endpoints for envelope
  class Api < Roda
    plugin :all_verbs

    route('transactions') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params
          results = FindAccountTransactions.new.call(
            request_identity: @request_identity,
            params: parameters
          )

          represent_response(results, TransactionsRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        routing.post do
          body = JSON.parse(routing.body.read)
  
          result = PublishTransaction.new.call(
            request_identity: @request_identity,
            data: body
          )
  
          represent_response(result, nil)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
