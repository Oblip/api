# frozen_string_literal: true

module Oblip
  # API endpoints for envelope
  class Api < Roda
    plugin :all_verbs

    route('transaction_envelope') do |routing|
      routing.is do
        routing.post do
          body = JSON.parse(routing.body.read)

          result = CreateTransactionEnvelope.new.call(
            request_identity: @request_identity,
            data: body
          )

          represent_response(result, TransactionEnvelopeRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end
