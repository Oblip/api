# frozen_string_literal: true

module Oblip
  # API endpoints for businesses
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('businesses') do |routing|
      routing.is do
        routing.get do
          parameters = routing.params

          results = nil
          if parameters.size > 0
            results = FindAllBusinesses.new.call(
              request_identity: @request_identity,
              params: parameters
            )
          else
            results = FindAccountBusinesses.new.call(
              request_identity: @request_identity
            )
          end

          represent_response(results, BusinessesRepresenter)

        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      # /v1/businesses/:id
      routing.on String do |id|
        routing.is do
          routing.get do

            result = FindBusiness.new.call(
              request_identity: @request_identity,
              id: id
            )

            represent_response(result, BusinessRepresenter)
          end
        end
      end
    end
  end
end
