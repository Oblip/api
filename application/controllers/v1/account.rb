# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs
    
    # rubocop:disable Metrics/BlockLength
    route('account') do |routing|
      routing.is do
        routing.get do

          result = FindAccount.new.call(@request_identity)
          entity = result.value!.message

          case entity&.type
          when 'personal'
            represent_response(result, AccountRepresenter)
          when 'business'
            represent_response(result, BusinessRepresenter)
          else
            represent_response(result, AccountRepresenter)
          end
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
        routing.put do
          data = JSON.parse(routing.body.read)

          result = UpdateAccount.new.call(
            data: data,
            request_identity: @request_identity
          )

          represent_response(result, AccountRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      # rubocop:disable Metrics/BlockLength
      routing.on('checking_account') do
        routing.is do
          routing.get do
            result = FindAccountCheckingAccount.new.call(
              request_identity: @request_identity
            )

            represent_response(result, CheckingAccountRepresenter)
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end

          routing.post do
            result = CreateCheckingAccount.new.call(
              request_identity: @request_identity
            )

            entity = result.value_or(nil)
            
            case entity&.message&.account&.type
            when 'personal'
              represent_response(result, AccountSignerRepresenter)
            when 'business'
              represent_response(result, BusinessSignerRepresenter)
            else
              represent_response(result, AccountSignerRepresenter)
            end
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
      # rubocop:enable Metrics/BlockLength
    end
  end
end
