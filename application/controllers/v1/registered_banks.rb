# frozen_string_literal: true

module Oblip
  # API endpoints for registered banks
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('registered_banks') do |routing|
      routing.is do
        routing.get do
          result = FindRegisteredBanks.new.call(
            request_identity: @request_identity
          )

          represent_response(result, RegisteredBanksRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end

        routing.post do
          body = JSON.parse(routing.body.read)
          result = CreateRegisteredBank.new.call(
            request_identity: @request_identity,
            data: body
          )

          represent_response(result, RegisteredBankRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
    # rubocop:enable Metrics/BlockLength
  end
end
