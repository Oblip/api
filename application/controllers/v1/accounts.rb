# frozen_string_literal: true

module Oblip
  # API endpoints for accounts
  class Api < Roda
    plugin :all_verbs

    route('accounts') do |routing|
      routing.is do
        routing.post do
          body = JSON.parse(routing.body.read)

          result = CreateAccount.new.call(
            client_id: body['client_id'],
            data: body['data'],
            signature: body['signature']
          )

          represent_response(result, AuthAccountRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      # /v1/accounts/:id
      routing.on String do |id|
        routing.is do
          routing.get do

            result = FindSingleAccount.new.call(
              request_identity: @request_identity,
              id: id
            )

            represent_response(result, AccountRepresenter)
          rescue StandardError => error
            puts error.message
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
  end
end
