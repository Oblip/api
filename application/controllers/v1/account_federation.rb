# frozen_string_literal: true

module Oblip
  # API endpoints for businesses
  class Api < Roda
    plugin :all_verbs

     # /v1/account_federation
    route('account_federation') do |routing|
      routing.is do
        routing.post do
          body = JSON.parse(routing.body.read)

          results = FindFederatedAccounts.new.call(
            request_identity: @request_identity,
            ck_account_list: body['checking_accounts']
          )

          represent_response(results, FederatedAccountsRepresenter)
        rescue StandardError => error
          puts error.message
          puts error.inspect
          puts error.backtrace
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end
    end
  end
end

