# frozen_string_literal: true

module Oblip
  # API endpoints for authentication for admin
  class Api < Roda
    plugin :all_verbs
    plugin :multi_route

    route('admin') do |routing|
      routing.multi_route('admin')
    end
  end
end
