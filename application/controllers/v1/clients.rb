# frozen_string_literal: true

module Oblip
  # API endpoints for contacts
  class Api < Roda
    plugin :all_verbs

    # rubocop:disable Metrics/BlockLength
    route('clients') do |routing|
      routing.is do
        routing.get do
          # TODO: later
        end
        routing.post do
          data = JSON.parse(routing.body.read)

          result = CreateClient.call(
            verifier_key: data['verifier_key'],
            wrapper_key: data['wrapper_key'],
            fcm_token: data['fcm_token']
          )

          represent_response(result, ClientRepresenter)
        rescue StandardError
          http_response = HttpResponseRepresenter.new(
            Result.new(:internal_error, 'Something went wrong.')
          )
          response.status = http_response.http_code
          http_response.to_json
        end
      end

      # /v1/clients/:client_id
      routing.on String do |client_id|
        routing.is do
          routing.get do
            # TODO: later maybe
          end

          routing.put do
            data = JSON.parse(routing.body.read)
            data['client_id'] = client_id

            result = UpdateClientFcm.new.call(
              request_identity: @request_identity,
              data: data
            )

            represent_response(result, ClientRepresenter)
          rescue StandardError
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end

        routing.on('send_verification') do
          routing.post do
            body = JSON.parse(routing.body.read)

            result = SendPhoneVerification.new.call(
              client_id: client_id,
              signature: body['signature'],
              data: body['data']
            )

            represent_response(result, nil)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
        routing.on('verify') do
          routing.post do
            body = JSON.parse(routing.body.read)

            result = VerifyPhoneNumber.new.call(
              client_id: client_id,
              data: body['data'],
              signature: body['signature']
            )

            represent_response(result, ClientRepresenter)
          rescue StandardError => error
            puts error.inspect
            puts error.backtrace
            http_response = HttpResponseRepresenter.new(
              Result.new(:internal_error, 'Something went wrong.')
            )
            response.status = http_response.http_code
            http_response.to_json
          end
        end
      end
    end
    # rubocop:enable Metrics/BlockLength
  end
end
