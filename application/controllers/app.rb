# frozen_string_literal: true

require 'roda'
require_relative '../helpers/route_helpers'
require_relative '../helpers/exceptions'

module Oblip
  # Web API
  class Api < Roda 
    plugin :halt
    plugin :all_verbs
    plugin :multi_route
    plugin :request_headers

    require_relative 'v1/accounts.rb'
    require_relative 'v1/account.rb'
    require_relative 'v1/clients.rb'
    require_relative 'v1/token.rb'
    require_relative 'v1/businesses.rb'
    require_relative 'v1/admin.rb'
    require_relative 'v1/registered_banks.rb'
    require_relative 'v1/bank_accounts.rb'
    require_relative 'v1/transaction_envelope.rb'
    require_relative 'v1/transactions.rb'
    require_relative 'v1/available_assets.rb'
    require_relative 'v1/account_availability.rb'
    require_relative 'v1/account_federation.rb'
    require_relative 'v1/agent_commissions.rb'
    require_relative 'v1/payouts.rb'


    def secure_request?(routing)
      routing.scheme.casecmp(Api.config.SECURE_SCHEME).zero?
    end

    route do |routing|
      response['Content-Type'] = 'application/json'
      secure_request?(routing) ||
        routing.halt(403, { message: 'TLS/SSL Requested' }.to_json)

      @request_identity = verify_request_identity(routing)

      # GET / request
      routing.root do
        message = "Oblip API v1 up in #{Api.environment} mode"
        HttpResponseRepresenter.new(Result.new(:ok, message)).to_json
      end

      routing.on 'v1' do
        routing.multi_route
      end
    end
  end
end
