# frozen_string_literal: true

require 'token_phrase'

# Generates Secret Phrases
class SecretPhrases
  def self.generate
    phrases = []
    4.times { |n| phrases[n] = TokenPhrase.generate(',') }
    phrases.join(',').split(',')
  end
end
