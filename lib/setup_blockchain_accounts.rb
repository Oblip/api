# frozen_string_literal: false

require 'rest-client'
require 'stellar-base'

class SetupBlockchainAccounts
  module Networks
    TEST = "https://horizon-testnet.stellar.org"
    TEST_LOCAL = "http://localhost:8500"
    PUBLIC = "https://horizon.stellar.org"
  end

  module InitialBalances
    FUNDING_ACCOUNT_BZD = 10000
  end

  def initialize(network_url = Networks::TEST)
    @network_url = network_url
  end

  def create(anchor_seed = nil)
    anchor = initialize_anchor(anchor_seed)

    # create fund account
    fund_account = Stellar::KeyPair.random
    use_friendbot(fund_account.address)

    # configure accounts
    anchor_signer = configure_anchor(anchor)
    fund_signer = configure_fund_account(fund_account)

    # setup fund_account assets
    configure_fund_account_assets(anchor, fund_account)

    # send BZD to fund account
    send_bzd_to_fund(anchor, fund_account)

    # map keys
    accounts = {
      anchor: anchor,
      anchor_signer: anchor_signer,
      fund_account:  fund_account,
      fund_signer: fund_signer
    }

    print_key_info(accounts)
    puts "\n\nDONE!"

  rescue StandardError => e
    puts e.message
  end

  def initialize_anchor(anchor_seed)
    anchor = nil
    if(!anchor_seed && (@network_url == Networks::TEST || @network_url == Networks::TEST_LOCAL))
      anchor = Stellar::KeyPair.random
      use_friendbot(anchor.address)
    elsif !anchor_seed && @network_url == Networks::PUBLIC
      raise StandardError, 'ERROR: You tried to create an anchor account in the public network'
    else
      anchor = Stellar::KeyPair.from_seed(anchor_seed)
      use_friendbot(anchor)
    end
    anchor
  end

  def get_account_data (address)
    account_res= RestClient.get "#{@network_url}/accounts/#{address}"
    account_data = JSON.parse(account_res.body)
  end

  def submit_transaction(xdr_envelope)
    response = RestClient.post "#{@network_url}/transactions", { tx: xdr_envelope }
    JSON.parse(response.body)

  rescue RestClient::ExceptionWithResponse => e
    puts e.response
  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def use_friendbot(address)
    url = "https://friendbot.stellar.org?addr=#{address}"
    response = RestClient.get url
    (response.code == 200)
  end

  private

  def print_key_info(arr_accounts)
    puts "\n\n================================"
    puts "            Keys                "
    puts "================================\n\n"

    arr_accounts.each do |name, keypair|
      puts "Key: #{name}"
      puts "Address:  #{keypair.address}"
      puts "Secret: #{keypair.seed}"
      puts "\n"
    end

    puts "\n================================\n\n"
  end
  # rubocop:disable Metrics/ABCSize
  def configure_fund_account_assets (anchor, fund_account)
    fund_data = get_account_data(fund_account.address)
    anchor_data = get_account_data(anchor.address)

    # static: We will trust three currencies for now: BZD
    trust_bzd = Stellar::Transaction.change_trust(
      account: fund_account,
      sequence: fund_data['sequence'].to_i + 1,
      fee: 200,
      line: Stellar::Asset.alphanum4('BZD', anchor)
    )

    allow_bzd = Stellar::Transaction.allow_trust(
      trustor: fund_account,
      account: anchor,
      asset: [:alphanum4, 'BZD', anchor],
      authorize: true,
      fee: 200,
      sequence: anchor_data['sequence'].to_i + 1,
    )

    bundled_ops = trust_bzd.merge(allow_bzd)

    xdr_envelope = bundled_ops.to_envelope(fund_account, anchor).to_xdr(:base64)
    data = submit_transaction(xdr_envelope)

    puts 'Configure Fund Account Assets: DONE!'

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end
  # rubocop:enable Metrics/ABCSize

  # ONLY for Test & Staging
  def send_bzd_to_fund(anchor, fund_account)
    fund_data = get_account_data(fund_account.address)
    anchor_data = get_account_data(anchor.address)

    next_sequence = anchor_data['sequence'].to_i + 1

    tx = Stellar::Transaction.payment({
      account: anchor,
      destination: fund_account,
      fee: 100,
      sequence: next_sequence,
      amount: [:alphanum4, 'BZD', anchor, InitialBalances::FUNDING_ACCOUNT_BZD]
    })

    xdr_envelope = tx.to_envelope(anchor).to_xdr(:base64)
    submit_transaction(xdr_envelope)

    puts "Sent BZD to fund account: DONE!"

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def configure_fund_account(fund_account)
    fund_data = get_account_data(fund_account.address)
    signer = Stellar::KeyPair.random

    config_tx = Stellar::Transaction.set_options(
      account: fund_account,
      fee: 100,
      sequence: fund_data['sequence'].to_i + 1,
      master_weight: 100,
      high_threshold: 100,
      med_threshold: 50,
      low_threshold: 20,
      signer: Stellar::Signer.new(key: Stellar::SignerKey
        .new(:signer_key_type_ed25519, signer.raw_public_key),
         weight: 50)
    )

    xdr_envelope = config_tx.to_envelope(fund_account).to_xdr(:base64)
    submit_transaction(xdr_envelope)

    puts 'Configure Fund Account: DONE!'
    signer
  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  def configure_anchor(anchor)
    account_data = get_account_data(anchor.address)

    signer = Stellar::KeyPair.random

    config_tx = Stellar::Transaction.set_options(
      account: anchor,
      fee: 100,
      sequence: account_data['sequence'].to_i + 1,
      set: [Stellar::AccountFlags.auth_required_flag, Stellar::AccountFlags.auth_revocable_flag],
      master_weight: 100,
      high_threshold: 100,
      med_threshold: 50,
      low_threshold: 20,
      signer: Stellar::Signer.new(key: Stellar::SignerKey
        .new(:signer_key_type_ed25519, signer.raw_public_key),
         weight: 50)
    )

    xdr_envelope = config_tx.to_envelope(anchor).to_xdr(:base64)
    submit_transaction(xdr_envelope)

    puts 'Configure Anchor: DONE!'

    signer

  rescue StandardError => error
    puts error.message
    puts error.inspect
    puts error.backtrace
  end

  # rubocop:disable Metrics/MethodLength
  # def create_accounts(anchor, keys)
  #   account_data = get_account_data(anchor.address)

  #   zhenfu_tx = Stellar::Transaction.create_account(
  #     account: anchor,
  #     destination: keys[:zhenfu],
  #     fee: 300,
  #     sequence: account_data['sequence'].to_i + 1,
  #     starting_balance: InitialBalances::ZHENFU
  #   )

  #   fuel_station_tx = Stellar::Transaction.create_account(
  #     account: anchor,
  #     destination: keys[:fuel_station],
  #     fee: 300,
  #     sequence: account_data['sequence'].to_i + 1,
  #     starting_balance: InitialBalances::FUEL_STATION
  #   )

  #   private_vault_tx = Stellar::Transaction.create_account(
  #     account: anchor,
  #     destination: keys[:private_vault],
  #     fee: 300,
  #     sequence: account_data['sequence'].to_i + 1,
  #     starting_balance: InitialBalances::PRIVATE_VAULT
  #   )

  #   bundled_tx = zhenfu_tx.merge(fuel_station_tx).merge(private_vault_tx)
  #   xdr_envelope = bundled_tx.to_envelope(anchor).to_xdr(:base64)

  #   submit_transaction(xdr_envelope)
  #   puts "Create Accounts: DONE!\n"

  # rescue StandardError => error
  #   puts "Create Accounts: ERROR!\n"
  #   puts error.message
  #   puts error.inspect
  #   puts error.backtrace
  # end
  # rubocop:enable Metrics/MethodLength

  # def generate_keys
  #   {
  #     credit_issuer: Stellar::KeyPair.random, # signer
  #     baba: Stellar::KeyPair.random, # signer
  #     mama: Stellar::KeyPair.random, # signer
  #     zhenfu: Stellar::KeyPair.random, # acccount
  #     fuel_station: Stellar::KeyPair.random, # account
  #     private_vault: Stellar::KeyPair.random # account
  #   }
  # end
end
