# frozen_string_literal: true

require_relative 'access_token.rb'

# Generates a Token with a random code that expires
class ShortCodeToken
  ONE_MINUTE = 60
  TEN_MINUTES = 60 * 10
  TWENTY_MINUTES = 60 * 20
  ONE_HOUR = 60 * 60
  ONE_DAY = ONE_HOUR * 24

  class ExpiredTokenError < StandardError; end
  class InvalidTokenError < StandardError; end

  # returns a hash with the code and the token
  def self.create(expiration = TWENTY_MINUTES)
    code = rand(10_000...99_999).to_s
    { code: code, token_data: AccessToken.create({ code: code }, expiration) }
  end

  def self.payload(token)
    AccessToken.payload(token)

  rescue AccessToken::InvalidTokenError
    raise ShortCodeToken::InvalidTokenError
  rescue AccessToken::ExpiredTokenError
    raise ShortCodeToken::ExpiredTokenError
  end
end
