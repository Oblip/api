def generate_ref
  charset = Array('A'..'Z') + Array(0..9)
  initial = Array.new(2) { Array('A'..'Z').sample }.join
  ref = Array.new(10) { charset.sample }.join
  initial + '-' + ref
end
