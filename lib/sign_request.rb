# frozen_string_literal: true

require 'rbnacl'
require 'base64'
require_relative 'securable'

# Parses Json information as needed
class SignRequest
  extend Securable

  class SignatureVerificationFailed < StandardError; end

  def initialize(signing_key)
    @signing_key = signing_key
  end

  def signing_key
    Base64.strict_decode64(@signing_key)
  end

  def self.generate_keypair
    signing_key = RbNaCl::SigningKey.generate
    verify_key = signing_key.verify_key

    { signing_key: Base64.strict_encode64(signing_key),
      verify_key: Base64.strict_encode64(verify_key) }
  end

  def sign(object)
    message = object.to_json
    signer = RbNaCl::SigningKey.new(signing_key)
    signature_raw = signer.sign(message)

    Base64.strict_encode64(signature_raw)
  end  
end
