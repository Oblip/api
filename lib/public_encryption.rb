# frozen_string_literal: true

require 'rbnacl'
require 'base64'

class PublicEncryption

  def self.generate_keypair
    private_key = RbNaCl::PrivateKey.generate
    {
      public_key: Base64.strict_encode64(private_key.public_key),
      private_key: Base64.strict_encode64(private_key)
    }
  end

  # Encrypts messages such that it can be decrypted by
  # by the private key of the public_key (which is of another keypair)
  def self.simple_encrypt(message, public_key, private_key)
    private_key = Base64.strict_decode64(private_key)
    public_key = Base64.strict_decode64(public_key)
    box =  RbNaCl::SimpleBox.from_keypair(public_key, private_key)
    cipher_raw = box.encrypt(message)
    Base64.strict_encode64(cipher_raw)
  end

  def self.encrypt(message, someones_public_key, private_key)
    private_key = Base64.strict_decode64(private_key)
    public_key = Base64.strict_decode64(someones_public_key)

    # initialize the box
    box = RbNaCl::Box.new(public_key, private_key)
    nonce = RbNaCl::Random.random_bytes(box.nonce_bytes)
    ciphertext = box.encrypt(nonce, message)

    { encrypted_message: Base64.strict_encode64(ciphertext), nonce: Base64.strict_encode64(nonce) }
  end

  def self.decrypt(cipher, nonce, other_public_key, our_private_key)
    public_key = Base64.strict_decode64(other_public_key)
    private_key = Base64.strict_decode64(our_private_key)
    decoded_message = Base64.strict_decode64(cipher)
    decoded_nonce = Base64.strict_decode64(nonce)
    
    box = RbNaCl::Box.new(public_key, private_key)

    box.decrypt(decoded_nonce, decoded_message)
  end

  # Decrypts the message sent from the public key, using the
  # private key (master key)
  def self.simple_decrypt(cipher, public_key, private_key)
    private_key = Base64.strict_decode64(private_key)
    public_key = Base64.strict_decode64(public_key)
    box =  RbNaCl::SimpleBox.from_keypair(public_key, private_key)
    cipher_raw = Base64.strict_decode64(cipher)

    box.decrypt(cipher_raw)
  end
end
