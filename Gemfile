# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.5.3'

# Networking gems
gem 'http'
gem 'rest-client'
# gem 'rack-utf8_sanitizer'

# Asynchronicity gems
gem 'concurrent-ruby'

# Parallel worker
# gem 'aws-sdk-sqs', '~> 1'
gem 'faye', '~> 1.2.4'
gem 'shoryuken', '~> 5.0.2'

# Cloud Messaging
gem 'fcm'

# Web API
gem 'json'
gem 'puma'
gem 'roda'

# Image Resizing
gem "mini_magick"

# Configuration
gem 'econfig'
gem 'pry'
gem 'rake'

# Security
gem 'rbnacl'
gem 'token_phrase'
gem 'rotp'

# Data gems
gem 'dry-struct'
gem 'dry-types'
gem 'ruby-enum', '~> 0.7.2'

# Representers
gem 'multi_json'
gem 'roar'

# Services
gem 'dry-monads'
gem 'dry-transaction'
gem 'phonelib'
gem 'rqrcode'
gem 'bip_mnemonic'

# Third Party Services (API/SDK)
gem 'aws-sdk'
gem 'stellar-base', '~> 0.21.0'
gem 'sendgrid-ruby'
gem 'discordrb-webhooks'
gem 'twilio-ruby'

# Database
gem 'hirb'
gem 'sequel'

# Testing
group :test do
  gem 'minitest'
  gem 'minitest-rg'
  gem 'rack-test'
  gem 'simplecov'
  gem 'webmock'
end

# Development
group :development do
  gem 'flog'
  gem 'reek'
  gem 'rubocop'
end

group :development, :test do
  gem 'rerun'
end

# group :production, :staging do
#   gem 'pg'
# end

gem 'pg'