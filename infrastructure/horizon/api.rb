# frozen_string_literal: false

require 'rest-client'
require 'stellar-base'
require 'ostruct'
require 'base64'

module Oblip
  module Horizon
    # Horizon Api Wrapper
    class Api
      module Errors
        class Badequence < StandardError; end
        class UnderFunded < StandardError; end
        class InsufficientFee < StandardError; end
        class InvalidEnvelope < StandardError; end
        class TransactionError < StandardError; end
        class FetchValueError < StandardError; end
      end

      class <<self
        attr_reader :fund, :anchor
      end      

      # Setup's Horizon API with the configurations
      # input:
      #       network_url [string]: horizon server url
      #       funding_acct [hash]:
      #                               address [string]: stellar address
      #                               signer_seed [string]: stellar seed
      #        anchor_acct [hash]:
      #                               address [string]: stellar address
      #                               signer_seed [string]: stellar seed
      # rubocop:disable Metrics/MethodLength
      def self.setup(input)
        @network_url = input[:network_url]
        funding_acct = input[:funding_acct]
        anchor_acct = input[:anchor_acct]
        @fund = {
          account: Stellar::KeyPair.from_address(funding_acct[:address]),
          signer: Stellar::KeyPair.from_seed(funding_acct[:signer_seed]),
          username: 'oblipfund',
          name: 'Oblip Fund',
          type: 'fund'
        }

        @anchor = {
          account: Stellar::KeyPair.from_address(anchor_acct[:address]),
          signer: Stellar::KeyPair.from_seed(anchor_acct[:signer_seed]),
          username: 'oblipanchor',
          name: 'Oblip Anchor',
          type: 'anchor'
        }
      end
      # rubocop:enable Metrics/MethodLength

      def self.unwrap_xdr_envelope(envelope)
        Stellar::TransactionEnvelope.from_xdr(Base64.decode64(envelope))
      rescue StandardError
        raise StandardError
      end

      def self.load_account(id)
        url = "#{@network_url}/accounts/#{id}"
        response = RestClient.get url
        JSON.parse(response.body)
      end

      def self.load_single_transaction(id)
        begin 
          retries ||= 0
          puts "ID: #{id}; Retry: #{retries}"
          url = "#{@network_url}/transactions/#{id}"
          response = RestClient.get url
          JSON.parse(response.body)

        rescue RestClient::ExceptionWithResponse => e
          retry if (retries += 1) < 3
          puts "There is an error loading transaction"
          puts e.response.body
          raise Errors::FetchValueError
        end
      end

      def self.keypair_from_raw(seed64)
        Stellar::KeyPair.from_raw_seed(Base64.strict_decode64(seed64))
      end

      def self.funder_sign_envelope(envelope)
        unwrapped = unwrap_xdr_envelope(envelope)
        unwrapped.signatures = [] unless unwrapped.signatures

        unwrapped.signatures.push(unwrapped.tx.sign_decorated(@fund[:signer]))

        tx_envelope = Stellar::TransactionEnvelope.new({
          :signatures => unwrapped.signatures,
          :tx => unwrapped.tx
        })
        
        tx_envelope.to_xdr(:base64)
        # tx.to_envelope().to_xdr(:base64)
      end

      def self.sign_envelope(envelope, signer_seed)
        tx = unwrap_xdr_envelope(envelope)&.tx
        signer = Stellar::KeyPair.from_seed(signer_seed)
        tx.to_envelope(signer).to_xdr(:base64)
      end

      def self.create_account(new_address, starting_balance)
        new_account = Stellar::KeyPair.from_address(new_address)
        fund_data = load_account(@fund[:account].address)
        tx = Stellar::Transaction.create_account(
          account: @fund[:account],
          destination: new_account,
          fee: 300,
          sequence: fund_data['sequence'].to_i + 1,
          starting_balance: starting_balance
        )
        xdr_envelope = tx.to_envelope(@fund[:signer]).to_xdr(:base64)
        submit_operation(xdr_envelope)
      end

      def self.create_transaction(envelope)
        begin
          retries ||= 0
          result = submit_operation(envelope)

          result['hash']
          # load_single_transaction(result['hash'])
        rescue Errors::TransactionError => e
          retry if (retries += 1) < 3
          puts e.message
          # puts e.inspect
          # puts e.backtrace
          raise Errors::TransactionError, 'Failed to create transaction'
        rescue Errors::FetchValueError
          puts 'FetchValueError'
          raise Errors::FetchValueError
        rescue Errors::InvalidEnvelope
          puts 'Invalid Envelope error'
          raise Errors::InvalidEnvelope
        end
      end

      def self.create_payment_envelope(from:, to:,
        amount:, asset:)
        from_account = Stellar::KeyPair.from_address(from)
        to_account = Stellar::KeyPair.from_address(to)

        fund_data = load_account(@fund[:account].address)

        tx = Stellar::Transaction.payment(
          account: @fund[:account],
          destination: to_account,
          source_account: from_account,
          fee: 300,
          sequence: fund_data['sequence'].to_i + 1,
          amount: [:alphanum4, asset, @anchor[:account], amount]
        )

        tx.to_envelope.to_xdr(:base64)
      end

      # rubocop:disable Metrics/AbcSize
      def self.allow_trust(address, asset_code)
        account = Stellar::KeyPair.from_address(address)
        fund_data = load_account(@fund[:account].address)

        tx = Stellar::Transaction.allow_trust(
          trustor: account, account: @fund[:account],
          source_account: @anchor[:account],
          asset: [:alphanum4, asset_code, @anchor[:account]],
          authorize: true, fee: 200, sequence: fund_data['sequence'].to_i + 1
        )

        submit_operation(tx.to_envelope(@anchor[:signer], @fund[:signer])
                         .to_xdr(:base64))
      end
      # rubocop:enable Metrics/AbcSize

      def self.trust_issuer(account_seed, asset_code)
        account = Stellar::KeyPair.from_seed(account_seed)
        account_data = load_account(account.address)

        tx = Stellar::Transaction.change_trust(
          account: account,
          fee: 200,
          sequence: account_data['sequence'].to_i + 1,
          line: Stellar::Asset.alphanum4(asset_code, @anchor[:account])
        )

        xdr_envelope = tx.to_envelope(account).to_xdr(:base64)
        submit_operation(xdr_envelope)
      end

      # rubocop:disable Metrics/MethodLength
      def self.add_signer(account_seed, weight)
        account = Stellar::KeyPair.from_seed(account_seed)
        account_data = load_account(account.address)
        signer_account = Stellar::KeyPair.random
        tx = Stellar::Transaction.set_options(
          account: account, fee: 100,
          sequence: account_data['sequence'].to_i + 1,
          signer: Stellar::Signer.new(key: Stellar::SignerKey
            .new(:signer_key_type_ed25519, signer_account.raw_public_key),
                                      weight: weight)
        )
        xdr_envelope = tx.to_envelope(account).to_xdr(:base64)
        submit_operation(xdr_envelope)
        signer_account
      end
      # rubocop:enable Metrics/MethodLength

      # rubocop:disable Metrics/MethodLength
      def self.change_thresholds(account_seed, master:, high:, med:, low:)
        account = Stellar::KeyPair.from_seed(account_seed)
        account_data = load_account(account.address)

        tx = Stellar::Transaction.set_options(
          fee: 200, account: account,
          sequence: account_data['sequence'].to_i + 1,
          master_weight: master,
          high_threshold: high,
          med_threshold: med,
          low_threshold: low
        )

        xdr_envelope = tx.to_envelope(account).to_xdr(:base64)
        submit_operation(xdr_envelope)
      end
      # rubocop:enable Metrics/MethodLength

      def self.submit_operation(xdr_envelope)
        response = RestClient.post "#{@network_url}/transactions",
                                   tx: xdr_envelope
        JSON.parse(response)
      rescue RestClient::ExceptionWithResponse => e
        puts "There is an error"
        puts e.response.body
        raise Errors::TransactionError
        # TODO: handle this.
      end
    end
  end
end
