# frozen_string_literal: false

require 'aws-sdk'

module Oblip
  # AWS S3 Storage API helpers
  module CloudStorage
    def self.setup(config)
      @resource = Aws::S3::Resource.new(
        access_key_id: config['AWS_ACCESS_KEY_ID'],
        secret_access_key: config['AWS_SECRET_ACCESS_KEY'],
        region: config['AWS_REGION']
      )

      @buckets = {
        app: config['AWS_APP_BUCKET_NAME'],
        secure_documents: config['AWS_SECURE_DOCUMENTS_BUCKET_NAME']
      }
    end

    # Decode the base64 file to binary
    # Create a Tempfile and write the binary in it  then close it
    # Get the path of the temp file
    # Run the upload function
    # Unlink the tempfile to delete it from the server
    def self.upload(bucket:, name:, path:, content_type: 'image/jpeg')
      obj = @resource.bucket(@buckets[bucket]).object(name)
      obj.upload_file(path,
                      acl: 'public-read',
                      content_type: content_type)
      obj
    end

    def self.delete(bucket:, name:)
      obj = @resource.bucket(@buckets[bucket]).object(name)
      obj.delete
    end
  end
end
