# frozen_string_literal: false

require 'aws-sdk'

module Oblip
  module AWS
    module SNS
      TYPE_PROMOTIONAL = 'Promotional'.freeze
      TYPE_TRANSACTIONAL = 'Transactional'.freeze

      module Errors
        class InvalidMessageLength < StandardError; end
        class InvalidValue < StandardError; end
        class MissingParameter < StandardError; end
      end
      # Provides a set of API to handl sms messages
      class Api
        def initialize(config)
          @client = Aws::SNS::Client.new(
            region: config['AWS_REGION'],
            access_key_id: config['AWS_ACCESS_KEY_ID'],
            secret_access_key: config['AWS_SECRET_ACCESS_KEY']
          )

          @config = config
        end

        def send_sms(phone_number:, type:, message:)
          raise InvalidMessageLength unless message.size < 160

          @client.publish(
            phone_number: phone_number,
            message: message,
            message_attributes: message_attributes(type)
          )
        rescue Aws::SNS::Errors::InvalidValue => error
          puts "SMS error: #{error.message}"
          raise Errors::InvalidValue, error.message
        end

        private

        # rubocop:disable Metrics/MethodLength
        def message_attributes(type)
          {
            'AWS.SNS.SMS.SenderID' => {
              data_type: 'String',
              string_value: 'Oblip'
            },
            'AWS.SNS.SMS.SMSType' => {
              data_type: 'String',
              string_value: type
            },
            'AWS.SNS.SMS.MaxPrice' => {
              data_type: 'String',
              string_value: '0.50'
            }
          }
        end
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end
