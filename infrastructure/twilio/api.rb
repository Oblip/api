require 'twilio-ruby'

module Oblip
  module TwilioIntegration
    class Api
      def self.setup(config)
        @client = Twilio::REST::Client.new(
          config['TWILIO_ACCOUNT_SID'],
          config['TWILIO_AUTH_TOKEN']
        )

        @phone_number = config['TWILIO_PHONE_NUMBER']
      end

      def self.send_message(to:, message:)
        @client.messages.create(
          from: @phone_number,
          to: to,
          body: message
        )
      end
    end
  end 
end