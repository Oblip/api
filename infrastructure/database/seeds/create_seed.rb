# frozen_string_literal: true

require 'yaml'
require_relative '../../../init.rb'

APP = Oblip

# REGISTERED BANK ACCOUNT SEEDS
registered_bank_seeds = File.read(
  'infrastructure/database/seeds/registered_bank_seeds.yml'
)
REGISTERED_BANK_SEEDS = YAML.safe_load(registered_bank_seeds)
REGISTERED_BANK_SEEDS.freeze

# creates the registered bank account seeds
REGISTERED_BANK_SEEDS.each do |record|
  record = JsonRequestBody.symbolize(record)
  APP::Database::RegisteredBankOrm.create(record)
end

# ADMIN SEED
admin_credentials = {
  full_name: 'John Doe',
  email_address: 'john@oblip.com',
  password: 'test123',
  type: 'admin'
}

# creates an admin account
APP::Database::AdminAccountOrm.create(admin_credentials)

puts 'Running Seeds - Completed!'
