# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Registered Banks
    class RegisteredBankOrm < Sequel::Model(:registered_banks)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :name, :account_number,
                          :account_name, :balance, :country_id

      def account_number
        SecureDB.decrypt(account_number_secure)
      end

      def account_number=(text)
        self.account_number_secure = SecureDB.encrypt(text)
      end
    end
  end
end
