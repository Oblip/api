# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for usernames view
    class UsernameOrm < Sequel::Model(:usernames)

      def name
        value = self.name_altered.split('|-|')
        if value.size == 2
          first_name, last_name = value
          first_name = SecureDB.decrypt(first_name)
          last_name = SecureDB.decrypt(last_name)

          first_name + ' ' + last_name
        else
          self.name_altered
        end
      end
    end
  end
end
