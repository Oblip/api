# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Businesses
    class AdminAccountOrm < Sequel::Model(:admin_accounts)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :full_name, :email_address,
                          :password, :salt, :type

      def password=(new_password)
        self.salt = SecureDB.new_salt
        self.password_hash = SecureDB.hash_password(salt, new_password)
      end

      def password?(try_password)
        try_hashed = SecureDB.hash_password(salt, try_password)
        try_hashed == password_hash
      end
    end
  end
end
