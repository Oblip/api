# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Clients
    class CheckingAccountCredOrm < Sequel::Model(:checking_account_creds)
      plugin :timestamps, update_on_create: true

      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :checking_account_id, :secret, :wrapper_key

      def secret
        SecureDB.decrypt(secret_secure)
      end

      def secret=(text)
        self.secret_secure = SecureDB.encrypt(text)
      end

      def wrapper_key
        SecureDB.decrypt(wrapper_key_secure)
      end

      def wrapper_key=(text)
        self.wrapper_key_secure = SecureDB.encrypt(text)
      end
    end
  end
end
