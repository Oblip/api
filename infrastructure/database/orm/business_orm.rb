# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Businesses
    class BusinessOrm < Sequel::Model(:businesses)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :name, :legal_name, :street, :city,
                          :country_id, :lat, :lng,
                          :is_agent, :account_id, :username
    end
  end
end
