# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Agent Commissions
    class AgentCommissionOrm < Sequel::Model(:agent_commissions)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :transaction_id, :username, :amount, :asset,
                          :payout_id
    end
  end
end
