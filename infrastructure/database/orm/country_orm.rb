# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Document Types
    class CountryOrm < Sequel::Model(:countries)
    end
  end
end
