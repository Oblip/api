# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Payouts
    class PayoutOrm < Sequel::Model(:payouts)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :amount, :payout_ref, :username, :is_paid,
                          :paid_date
    end
  end
end
