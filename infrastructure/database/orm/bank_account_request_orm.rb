# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Bank Account Requests
    class BankAccountRequestOrm < Sequel::Model(:bank_account_requests)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :ref_no, :bank_account_id, :registered_bank_id,
                          :amount, :asset, :type, :status
    end
  end
end
