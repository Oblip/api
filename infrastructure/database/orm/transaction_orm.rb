# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Transactions
    class TransactionOrm < Sequel::Model(:transactions)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :id, :tx_hash, :from_internal, :to_internal,
                          :message, :paging_token, :network_fee, :amount,
                          :asset, :type
    end
  end
end
