# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Clients
    class ClientOrm < Sequel::Model(:clients)
      plugin :timestamps, update_on_create: true

      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :account_id, :secret_code, :expiry_date,
                          :verifier_key, :wrapper_key, :is_verified,
                          :fcm_token, :status, :verification_token

      def secret_code_valid?(code)
        SecureDB.hash_content(code) == secret_code_hash
      end

      def secret_code
        secret_code_hash
      end

      def secret_code=(code)
        hashed = SecureDB.hash_content(code)
        self.secret_code_hash = hashed
      end

      def verifier_key
        SecureDB.decrypt(verifier_key_secure)
      end

      def verifier_key=(text)
        self.verifier_key_secure = SecureDB.encrypt(text)
      end

      def wrapper_key
        SecureDB.decrypt(wrapper_key_secure)
      end

      def wrapper_key=(text)
        self.wrapper_key_secure = SecureDB.encrypt(text)
      end

      def fcm_token
        SecureDB.decrypt(fcm_token_secure)
      end

      def fcm_token=(token)
        self.fcm_token_secure = SecureDB.encrypt(token)
      end
    end
  end
end
