# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Registered Banks
    class BankAccountOrm < Sequel::Model(:bank_accounts)
      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :account_id, :registered_bank_id, :account_name,
                          :username, :account_number, :status,
                          :account_number_hash, :status_comment, :is_partner

      def account_number
        SecureDB.decrypt(account_number_secure)
      end

      def account_number=(text)
        self.account_number_secure = SecureDB.encrypt(text)
        self.account_number_hash = SecureDB.hash_content(text)
      end
    end
  end
end
