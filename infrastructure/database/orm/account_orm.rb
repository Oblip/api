# frozen_string_literal: true

module Oblip
  module Database
    # Object-Relational Mapper for Accounts
    class AccountOrm < Sequel::Model(:accounts)
      one_to_one    :country,
                    class: :'Oblip::Database::CountryOrm'

      one_to_many   :clients,
                    class: :'Oblip::Database::ClientOrm',
                    key: :account_id

      plugin :timestamps, update_on_create: true
      plugin :uuid, field: :id
      plugin :whitelist_security
      set_allowed_columns :first_name, :middle_name, :last_name, :secret_code,
                          :phone_number, :username, :country_id,
                          :email, :dob, :phone_url, :street_name,
                          :city, :zip_code, :state, :checking_account_id

      def photo_url=(text)
        self.photo_url_secure = SecureDB.encrypt(text)
      end

      def photo_url
        SecureDB.decrypt(photo_url_secure)
      end

      def secret_code=(code)
        self.secret_code_secure = SecureDB.encrypt(code)
      end

      def secret_code
        SecureDB.decrypt(secret_code_secure)
      end

      def street_name=(text)
        self.street_name_secure = SecureDB.encrypt(text)
      end

      def street_name
        SecureDB.decrypt(street_name_secure)
      end

      def first_name=(text)
        self.first_name_secure = SecureDB.encrypt(text)
      end

      def first_name
        SecureDB.decrypt(first_name_secure)
      end

      def middle_name=(text)
        self.middle_name_secure = SecureDB.encrypt(text)
      end

      def middle_name
        SecureDB.decrypt(middle_name_secure)
      end

      def last_name=(text)
        self.last_name_secure = SecureDB.encrypt(text)
      end

      def last_name
        SecureDB.decrypt(last_name_secure)
      end

      def phone_number
        SecureDB.decrypt(phone_number_secure)
      end

      def phone_number=(text)
        self.phone_number_secure = SecureDB.encrypt(text)
        self.phone_number_hash = SecureDB.hash_content(text)
      end
    end
  end
end
