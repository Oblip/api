# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:admin_accounts) do
      uuid :id, primary_key: true, unique: true
      String :full_name, null: false
      String :email_address, null: false
      String :password_hash, null: false
      String :salt
      String :type, null: false, default: 'admin'
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
