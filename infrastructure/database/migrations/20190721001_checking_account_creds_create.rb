# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:checking_account_creds) do
      uuid :id, primary_key: true, unique: true
      String :checking_account_id, unique: true
      String :secret_secure, null: false
      String :wrapper_key_secure, unique: true, null: false
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
