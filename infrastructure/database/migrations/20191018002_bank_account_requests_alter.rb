# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_account_requests) do
      add_foreign_key [:registered_bank_id], :registered_banks
      set_column_allow_null :registered_bank_id, true
    end
  end
end
