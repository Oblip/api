# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    query = 'SELECT id as reference_id, username, \'personal\'
     as type, checking_account_id, concat(first_name_secure, \'|-|\', last_name_secure) as name_altered, 
     id as master_account_id, false AS is_agent FROM accounts UNION ALL SELECT id as reference_id,
    username, \'business\' as type, checking_account_id, name AS name_altered, account_id as master_account_id, is_agent FROM businesses
    GROUP BY id'

    drop_view(:usernames)
    create_or_replace_view(:usernames, query)
  end
end
