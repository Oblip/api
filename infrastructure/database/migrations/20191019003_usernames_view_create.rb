# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    query = 'SELECT id as reference_id, username, \'account\'
     as type FROM accounts UNION ALL SELECT id as reference_id,
    username, \'business\' as type FROM businesses
    GROUP BY id'
    create_view(:usernames, query)
  end
end
