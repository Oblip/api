# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:accounts) do
      uuid :id, primary_key: true, unique: true

      String :first_name_secure
      String :middle_name_secure, null: true
      String :last_name_secure
      String :username, unique: true

      String :phone_number_secure, unique: true, null: true
      String :phone_number_hash, unique: true
      Integer :country_id, foreign_key: true, table: :countries
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
