# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_accounts) do
      add_column :account_name, 'varchar(100)'
      add_column :is_partner, 'boolean'
    end
  end
end
