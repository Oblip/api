# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_accounts) do
      add_column :account_number_hash, 'varchar(150)'
      add_column :salt, 'varchar(100)'
    end

    # update all existing records with new salt and hash
    dataset = from(:bank_accounts)
    dataset.each do |r|
      salt = SecureDB.new_salt
      decrypted_num = SecureDB.decrypt(r[:account_number_secure])
      hashed_num = SecureDB.hash_password(salt, decrypted_num)
      from(:bank_accounts).where(id: r[:id])
                          .update(salt: salt,
                                  account_number_hash: hashed_num)
    end
  end
end
