# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:accounts) do
      rename_column :secret_code, :secret_code_secure
    end
  end
end
