# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_accounts) do
      drop_constraint(:bank_accounts_username_key)
    end
  end
end
