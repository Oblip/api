# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_accounts) do
      set_column_default :is_partner, false
    end
  end
end
