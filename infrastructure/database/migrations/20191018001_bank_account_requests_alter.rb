# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_account_requests) do
      add_column :registered_bank_id, :uuid
    end
  end
end
