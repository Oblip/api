# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_accounts) do
      drop_column :salt
    end

    # update all existing records with new salt and hash
    dataset = from(:bank_accounts)
    dataset.each do |r|
      decrypted_num = SecureDB.decrypt(r[:account_number_secure])
      hashed_num = SecureDB.hash_content(decrypted_num)
      from(:bank_accounts).where(id: r[:id])
                          .update(account_number_hash: hashed_num)
    end
  end
end
