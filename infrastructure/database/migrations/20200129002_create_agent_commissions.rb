# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  up do
    create_table(:agent_commissions) do
      uuid :id, primary_key: true, unique: true
      uuid :transaction_id, foreign_key: true, table: :transactions
      String :username
      String :amount, null: false
      String :asset, null: false
      uuid :payout_id, foreign_key: true, table: :payouts
      DateTime :created_at
      DateTime :updated_at
    end
  end

  down do
    drop_table(:agent_commissions)
  end
end
