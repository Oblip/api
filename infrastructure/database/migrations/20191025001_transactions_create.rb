# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:transactions) do
      uuid :id, primary_key: true, unique: true
      String :tx_hash, unique: true
      String :from_internal
      String :to_internal
      String :message
      String :paging_token
      Integer :network_fee
      String :amount, null: false
      String :asset, null: false
      String :type, null: false
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
