# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_accounts) do
      drop_column :account_id
    end
  end
end
