# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:clients) do
      uuid :id, primary_key: true, unique: true
      uuid :account_id, foreign_key: true, table: :accounts
      String :secret_code_hash
      String :verifier_key_secure
      String :wrapper_key_secure
      String :fcm_token_secure
      String :verification_token
      Bool :is_verified, default: false
      Integer :status
      Time :created_at
      Time :updated_at
    end
  end
end
