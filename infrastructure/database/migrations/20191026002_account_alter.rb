# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    dataset = from(:accounts)
    # adding secret code to each existing account
    dataset.map do |r|
      id = r[:id]
      code = SecureDB.generate_key
      encrypted_code = SecureDB.encrypt(code)
      from(:accounts).where(id: id).update(secret_code: encrypted_code)
    end
  end
end
