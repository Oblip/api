# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:businesses) do
      add_column :username, String
      add_unique_constraint [:username]
    end
  end
end
