# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:businesses) do
      add_column :name_official, String
    end
  end
end
