# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:registered_banks) do
      uuid :id, primary_key: true, unique: true
      String :name, null: false
      String :account_number_secure, null: false
      String :account_name, null: false
      Float :balance, default: 0.0
      Integer :country_id, foreign_key: true, table: :countries
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
