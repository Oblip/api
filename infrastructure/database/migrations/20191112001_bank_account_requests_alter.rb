# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:bank_account_requests) do
      add_column :transaction_id, :uuid
      add_foreign_key [:transaction_id], :transactions
    end
  end
end
