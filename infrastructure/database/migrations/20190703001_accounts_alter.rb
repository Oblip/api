# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:accounts) do
      set_column_default :email, ''
      set_column_default :city, ''
      set_column_default :state, ''
      set_column_default :zip_code, ''
      set_column_allow_null :photo_url_secure, true
      set_column_allow_null :street_name_secure, true
    end
  end
end
