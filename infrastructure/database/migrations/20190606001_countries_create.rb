# frozen_string_literal: true

require 'sequel'
require 'yaml'

Sequel.migration do
  change do
    create_table(:countries) do
      primary_key :id

      String :name, unique: true, null: false
      String :code, unique: true, null: false
      String :phone_code, unique: true, null: false
      String :currency, unique: true
    end

    path = 'infrastructure/database/initializations/countries.yml'
    countries = File.read(path)
    data = YAML.safe_load(countries)

    data.each do |country|
      from(:countries).insert(country)
    end
  end
end
