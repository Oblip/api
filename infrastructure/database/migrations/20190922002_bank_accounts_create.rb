# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:bank_accounts) do
      uuid :id, primary_key: true, unique: true
      uuid :account_id, foreign_key: true, table: :accounts
      uuid :registered_bank_id, foreign_key: true, table: :registered_banks
      String :account_number_secure, null: false
      String :status, default: 'processing'
      String :status_comment, default: ''
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
