# frozen_string_literal: true

require 'sequel'
require 'yaml'

Sequel.migration do
  change do
    create_table(:businesses) do
      uuid :id, primary_key: true, unique: true
      String :name
      String :street
      String :city
      Integer :country_id, foreign_key: true, table: :countries
      Float :lat
      Float :lng
      Bool :is_agent
      uuid :account_id, foreign_key: true, table: :accounts
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
