# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:accounts) do
      add_column :email, String
      add_column :dob, Date
      add_column :photo_url_secure, String
      add_column :street_name_secure, String
      add_column :state, String
      add_column :city, String
      add_column :zip_code, String
    end
  end
end
