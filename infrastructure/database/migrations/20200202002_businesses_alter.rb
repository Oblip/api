# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:businesses) do
      drop_column :name_official
      add_column :legal_name, 'varchar(150)'
    end
  end
end
