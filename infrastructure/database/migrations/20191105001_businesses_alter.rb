# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:businesses) do
      add_column :checking_account_id, String
    end
  end
end
