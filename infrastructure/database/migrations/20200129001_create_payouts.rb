# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  up do
    create_table(:payouts) do
      uuid :id, primary_key: true, unique: true
      String :amount, default: '0'
      String :payout_ref, null: false
      String :username
      Bool :is_paid
      DateTime :paid_date
      DateTime :created_at
      DateTime :updated_at
    end
  end

  down do
    drop_table(:payouts)
  end
end
