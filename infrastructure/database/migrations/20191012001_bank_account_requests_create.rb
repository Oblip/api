# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    create_table(:bank_account_requests) do
      uuid :id, primary_key: true, unique: true
      uuid :bank_account_id, foreign_key: true, table: :bank_accounts
      String :ref_no, unique: true
      Float :amount, null: false
      String :asset, null: false
      String :type, null: false
      String :status, default: 'unconfirmed'
      String :status_comment
      DateTime :created_at
      DateTime :updated_at
    end
  end
end
