FROM ruby:2.5.3
RUN apt-get update -qy && apt-get install -y libsodium-dev postgresql postgresql-client libpq-dev
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN gem install bundler
RUN bundle install

ADD . /myapp
EXPOSE 9292