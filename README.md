## Introduction

Oblip is a payment service provider.

This is the private API for Oblip and the following are some of the specs and routes.

- The current version of this API is version 1.
- The path is: `/v1`.
- Staging App can be found at: https://oblip-api-staging.herokuapp.com/

## Development
Running the API in development should be easy.

### Requirements
1. You must have [docker](https://www.docker.com/) installed in your computer.
2. Create a secrets.yml file in the ``config/`` folder and fill in the information. Follow
   ``config/secrets.example.yml`` as an example. Ask Reggie for this file. 
3. The Database credentials are in the ``docker-compose.yml`` file. The host is ``db``.
4. If you're using Windows, you need to create a .env file in the root directory, add  ```COMPOSE_CONVERT_WINDOWS_PATHS=1``` and save it. 


### How to run
To run the server in the background:
```
$ docker-compose up -d
```

To run the server and create a new build:
```
$ docker-compose up --build
```

To stop the server:
```
$ docker-compose down
```

You can access the server at [localhost:9292](http://localhost:9292).

### Running the database seed

The database seed creates records in the database that we need re-create every now and again so that the api works
great!

***DO NOT RE-RUN THIS MULTIPLE TIMES!!***

The type of records the database seed generates are:
-   Registered Bank Accounts
-   Admin Account

#### How to use

If you're using docker compose use the following command:
```
$ docker-compose run core rake db:seed
```

If you're not using docker compose use:
```
$ rake db:seed
```

#### Admin Credentials

**email:** john@oblip.com
**password:** test123

### Database Resetting
Sometimes you want to delete the entire database because you want to start over. You can!!!

You can can delete everything from the database by running:
```
$ docker-compose run core rake db:drop
```

Then you can create the database again like this:
```
$ docker-compose run core rake db:migrate
```

## Setup Stellar Horizon locally (testnet)

We can't rely on running our tests and dev on Stellar's own testnet so we have to run our own local Horizon instance
locally. Great thing is that they have a docker container. So there is how to setup a horizon instance in your computer
using docker:

```
$ docker run --rm -it -p "8500:8000" -v "/Users/reggieescobar/docker-volumes/stellar:/opt/stellar" --name stellar stellar/quickstart --testnet
```

Make sure to replace `/Users/reggieescobar/docker-volumes/` with the path where you want to save the volume so it
persists in your computer.

The interactive shell will ask you for a postgres password. Enter a password (don't leave it blank). Confirm it and then
press enter. It will take a while (few seconds). You will see a `init-postgres`. Just wait it out until everything says
ok.

After it finishes verify that it's up by going to http://localhost:8500.

Then, come out of the shell (if you're on a mac press Control + C).

Run this so horizon runs in the background (make sure it matches the path to the volume you added on the last command):

```
docker run -d -p "8500:8000" -v "/Users/reggieescobar/docker-volumes/stellar:/opt/stellar" --name stellar stellar/quickstart --testnet
```

## Blockchain Resetting
Every couple of months, Stellar.org resets the TEST Network - deletes all the accounts and records. 
What that means is that we have to recreate our **anchor** and **fund account** and set it up.

To recreate and setup the blockchain accounts do the following:

**1. Loads all file and opens console**
```
$ docker-compose run core rake console
```
**2. Create a Setup Blockchain Accounts object**
```
$ creator = SetupBlockchainAccounts.new
```

**3. Create everything**
```
$ creator.create

```

Then, copy everything  that says Keys down and store the info in a text file somewhere in your computer
Note that ***Secret*** is another word for seed. 

Go to ``config/secrets.yml`` and update the variables in the development, test and staging environments:
```
ANCHOR_ACCOUNT_ADDRESS: <anchor_address>
ANCHOR_SIGNER_SEED: <anchor_signer_secret>
FUNDING_ACCOUNT_ADDRESS: <fund_account_address>
FUNDING_SIGNER_SEED: <fund_singer_secret>
```

